---
title: "IWC"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["i"]
language: "de"
keywords: ["IWC","International","Watch","Company","Schaffhausen","Schweiz","Swiss"]
categories: ["movements","movements_i"]
movementlistkey: "iwc"
description: "Uhrwerke der IWC, der International Watch Company aus Schaffhausen, Schweiz"
abstract: "Uhrwerke der IWC, der International Watch Company aus Schaffhausen, Schweiz"
---
(International Watch Company, Schaffhausen, Schweiz)
{{< movementlist "iwc" >}}

{{< movementgallery "iwc" >}}