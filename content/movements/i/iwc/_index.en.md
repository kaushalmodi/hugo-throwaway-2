---
title: "IWC"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["i"]
language: "en"
keywords: ["IWC","International Watch Company","Schaffhausen","Switzerland","Swiss"]
categories: ["movements","movements_i"]
movementlistkey: "iwc"
description: "Movements from the swiss manufacturer IWC of Schaffhausen"
abstract: "Movements from the swiss manufacturer IWC of Schaffhausen"
---
(International Watch Company, Schaffhausen, Switzerland)
{{< movementlist "iwc" >}}

{{< movementgallery "iwc" >}}