---
title: "Intex 1057 SCD"
date: 2015-01-01T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Intex 1057","Intex","Willi Friesinger","Handaufzug"]
description: "Intex 1057 - ein seltenes Handaufzugswerk aus Deutschland aus den frühen 60er Jahren"
abstract: "Ein seltenes deutsches Handaufzugswerk aus den frühen 60er Jahren."
preview_image: "intex_1057_scd-mini.jpg"
image: "Intex_1057_SCD.jpg"
movementlistkey: "intex"
caliberkey: "1057 SCD"
manufacturers: ["intex"]
manufacturers_weight: 1057
categories: ["movements","movements_i","movements_i_intex"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/anker/Anker_HAU_Intex_1057_SCD_Zifferblatt.jpg"
    description: "Anker Herrenuhr (nur Zifferblatt)"
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum