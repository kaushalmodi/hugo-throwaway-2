---
title: "Intex 1055 SCI"
date: 2009-12-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Intex 1055 SCI","Intex","1055 SCI","21 Jewels","Pforzheim","Willi Friesinger","21 Steine","Deutschland","Stiftanker","Blindsteine"]
description: "Intex 1055 SCI"
abstract: ""
preview_image: "intex_1055_sci-mini.jpg"
image: "Intex_1055_SCI.jpg"
movementlistkey: "intex"
caliberkey: "1055 SCI"
manufacturers: ["intex"]
manufacturers_weight: 1055
categories: ["movements","movements_i","movements_i_intex"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    Auf der Zeitwaage liefert das frisch revidierte Intex 1055 SCI das folgende Bild, das in den liegenden Lagen bis auf den großen Abfallfehler (sichtbar als recht breite "Eisenbahnschiene") sehr ordentlich und gleichmäßig ist. Die Ganggenauigkeit bei rund +10 Sekunden pro Tag - für ein so einfach konstruiertes Werk wäre das ein hervorragender Wert... Schlechter sieht es dagegen in den hängenden Lagen aus - hier ist die Abweichung wesentlich größer und erreicht in der Lage "12 oben" satte plus dreieinhalb Minuten. Schuld hieran dürfte vermutlich der große Höhenschlag der Unruh des vorliegenden Exemplars sein, der auf einen massiven Stoß hindeutet. Dennoch sind auch hier die Linien der Zeitwaage sehr gleichmäßig, was auf eine Hemmungspartie hinweist, die noch top in Schuß ist!
  images:
    ZO: Zeitwaage_Intex_1055_SCI_ZO.jpg
    ZU: Zeitwaage_Intex_1055_SCI_ZU.jpg
    3O: Zeitwaage_Intex_1055_SCI_3O.jpg
    6O: Zeitwaage_Intex_1055_SCI_6O.jpg
    9O: Zeitwaage_Intex_1055_SCI_9O.jpg
    12O: Zeitwaage_Intex_1055_SCI_12O.jpg
usagegallery: 
  - image: "a/anker/Anker_HAU_Intex_1055_SCI.jpg"
    description: "Anker Herrenuhr"
---
Lorem Ipsum