---
title: "Intex 1055 SCI"
date: 2009-12-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Intex 1055 SCI","Intex","1055 SCI","21 Jewels","Pforzheim","Willi Friesinger","germany","blind jewels","pin lever"]
description: "Intex 1055 SCI"
abstract: ""
preview_image: "intex_1055_sci-mini.jpg"
image: "Intex_1055_SCI.jpg"
movementlistkey: "intex"
caliberkey: "1055 SCI"
manufacturers: ["intex"]
manufacturers_weight: 1055
categories: ["movements","movements_i","movements_i_intex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_HAU_Intex_1055_SCI.jpg"
    description: "Anker gents watch"
timegrapher_old: 
  description: |
    On the timegrapher, the freshly serviced Intex 1055 SCI show the following graphs, which are very well on the horizontal positions, except the large beat error (seens as "railway track"). The accurancy is approx. +10 secs/day, which is very good for such a simple constructed movement. On the vertical positions however, the graphs are pretty poor, since the deviations goes up to more than three and a half minutes. This could be caused by the radial ran out balance of this specimen. Probably a severe hit was responsible for it. Nevertheless, the lines are still very steady, and this shows, that the escapement is still in perfect shape!
  images:
    ZO: Zeitwaage_Intex_1055_SCI_ZO.jpg
    ZU: Zeitwaage_Intex_1055_SCI_ZU.jpg
    3O: Zeitwaage_Intex_1055_SCI_3O.jpg
    6O: Zeitwaage_Intex_1055_SCI_6O.jpg
    9O: Zeitwaage_Intex_1055_SCI_9O.jpg
    12O: Zeitwaage_Intex_1055_SCI_12O.jpg
---
Lorem Ipsum