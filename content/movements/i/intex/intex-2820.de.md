---
title: "Intex 2820"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Intex","InTex","Intex 2820","Intex 282","2820","282","17","Steine","17 Steine","17 Rubis","Deutschland"]
description: "Intex 2820"
abstract: ""
preview_image: "intex_2820-mini.jpg"
image: "Intex_2820.jpg"
movementlistkey: "intex"
caliberkey: "2820"
manufacturers: ["intex"]
manufacturers_weight: 2820
categories: ["movements","movements_i","movements_i_intex"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum