---
title: "Intex 1057 AUT SCD CLD"
date: 2013-10-06T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Intex 1057","Intex","Willi Friesinger","Robot-Automatic","25 Jewels","Automatic","handwind","module","date"]
description: "Intex 1057 - a german handwoud movement with an additional \"Robot-Automatic\" selfwindig module"
abstract: "A german selfwinding movement, based on a handwound movement with an interesting date indication mechanism, and an additional \"Robot-Automatic\" selfwinding module on top."
preview_image: "intex_1057_aut_scd_cld-mini.jpg"
image: "Intex_1057_AUT_SCD_CLD.jpg"
movementlistkey: "intex"
caliberkey: "1057 AUT SCD CLD"
manufacturers: ["intex"]
manufacturers_weight: 1057
categories: ["movements","movements_i","movements_i_intex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  <p>The specimen shown here came in heavily used condition with a lose selfwinding module into the lab. There was much contamination because of abraised metal due to the lose module.</p><p>The movement was cleaned and olied, except the balance wheel, which was not touched to avoid magnetism of the hairspring.</p><p>Additionally, the second hand was missing and the axle was also missing at the top.</p>
usagegallery: 
  - image: "a/anker/Anker_HAU_Intex_1057.jpg"
    description: "Anker Automatic Herrenuhr"
timegrapher_old: 
  description: |
    Horizontally, the movement was perfect, but on the vertical rates, it performed pretty bad:
  images:
    ZO: Zeitwaage_Intex_1057_AUT_SCD_CLD_ZO.jpg
    ZU: Zeitwaage_Intex_1057_AUT_SCD_CLD_ZU.jpg
    3O: Zeitwaage_Intex_1057_AUT_SCD_CLD_3O.jpg
    6O: Zeitwaage_Intex_1057_AUT_SCD_CLD_6O.jpg
    9O: Zeitwaage_Intex_1057_AUT_SCD_CLD_9O.jpg
    12O: Zeitwaage_Intex_1057_AUT_SCD_CLD_12O.jpg
  values:
    ZO: "+-0"
    ZU: "+-0"
    3O: "-70"
    6O: "-170"
    9O: "-90"
    12O: "+15"
---
Lorem Ipsum