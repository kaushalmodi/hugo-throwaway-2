---
title: "Intex"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["i"]
language: "de"
keywords: ["FHF","Fontainemelon","Font","Schweiz"]
categories: ["movements","movements_i"]
movementlistkey: "intex"
description: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
abstract: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
---
(Intex-Uhrenfabriken GmbH Willi Friesinger, Pforzheim, Deutschland)
{{< movementlist "intex" >}}

{{< movementgallery "intex" >}}