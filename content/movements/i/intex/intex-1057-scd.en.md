---
title: "Intex 1057 SCD"
date: 2015-01-02T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Intex 1057","Intex","Willi Friesinger","manual wind","21 Jewels","21 Rubis"]
description: "Intex 1057 - a rare german manual wind movement from the early 1960ies"
abstract: "A rare german manual wind movement from the early 1960ies."
preview_image: "intex_1057_scd-mini.jpg"
image: "Intex_1057_SCD.jpg"
movementlistkey: "intex"
caliberkey: "1057 SCD"
manufacturers: ["intex"]
manufacturers_weight: 1057
categories: ["movements","movements_i","movements_i_intex_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Many thanks go to [Klaus Brunnemer]() for the donation of this movement!"
usagegallery: 
  - image: "a/anker/Anker_HAU_Intex_1057_SCD_Zifferblatt.jpg"
    description: "Anker mens' watch  (dial only)"
---
Lorem Ipsum