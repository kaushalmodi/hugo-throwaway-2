---
title: "Eppler 20"
date: 2013-11-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Eppler 20","Eppler","Steinanker","17 Jewels","17 Steine","RUFA"]
description: "Eppler 20 - Ein echtes Palettenankerwerk des Stiftankerspezialisten aus Schwenningen."
abstract: "Ein 17-steiniges, reinrassiges Palettenankerwerk der Firma Eppler, die eigentlich nur für ihre Stiftankerwerke bekannt ist."
preview_image: "eppler_20-mini.jpg"
image: "Eppler_20.jpg"
movementlistkey: "eppler"
caliberkey: "20"
manufacturers: ["eppler"]
manufacturers_weight: 20
categories: ["movements","movements_e","movements_e_eppler"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam in vergleichsweise ordentlichem Zustand ins Labor, bekam aber trotzdem einen Vollservice mit Reinigung und Ölung.
usagegallery: 
  - image: "e/eppler/Eppler_HAU_Eppler_20.jpg"
    description: "Eppler Herren-Taucheruhr"
  - image: "a/anker/Anker_HAU_Eppler_20.jpg"
    description: "Anker Herrenuhr"
timegrapher_old: 
  description: |
    Auf der Zeitwaage war das Eppler 20 recht heikel, da die Konstruktion anscheinend zu viele Nebengeräusche produziert, die ein sauberes Gangbild verhindern. Dennoch konnten in fast allen Lagen erfolgreiche Messungen vorgenommen werden, die für ein Ankerwerk in so einer einfachen Bauweise durchaus noch im Rahmen sind. Mit "richtigen" Ankerwerken in massiver Bauweise kann dieses Werk natürlich nicht konkurrieren.
  images:
    ZO: Zeitwaage_Eppler_20_ZO.jpg
    ZU: Zeitwaage_Eppler_20_ZU.jpg
    3O: Zeitwaage_Eppler_20_3O.jpg
    6O: Zeitwaage_Eppler_20_6O.jpg
    9O: Zeitwaage_Eppler_20_9O.jpg
    12O: Zeitwaage_Eppler_20_12O.jpg
  values:
    ZO: "-10"
    ZU: "+5"
    3O: "-20"
    6O: "+-0"
    9O: "-90"
    12O: "?"
---
Lorem Ipsum