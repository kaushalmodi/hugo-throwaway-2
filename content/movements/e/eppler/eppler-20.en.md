---
title: "Eppler 20"
date: 2013-11-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Eppler 20","Eppler","pallet lever","17 Jewels","17 Steine","RUFA"]
description: "Eppler 20 - A true pallet lever movement from the well known german pin lever manufacturer Eppler."
abstract: "A simple but true 17 jewel pallet lever movement of the manufacturer Eppler, which was well-known for their simple pin lever movements."
preview_image: "eppler_20-mini.jpg"
image: "Eppler_20.jpg"
movementlistkey: "eppler"
caliberkey: "20"
manufacturers: ["eppler"]
manufacturers_weight: 20
categories: ["movements","movements_e","movements_e_eppler_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/eppler/Eppler_HAU_Eppler_20.jpg"
    description: "Eppler mens' divers' watch"
  - image: "a/anker/Anker_HAU_Eppler_20.jpg"
    description: "Anker mens' watch"
timegrapher_old: 
  description: |
    On the timegrapher, the Eppler 20 was not easy to measure, since there was a lot of background noise, probably due to the pillar construction of the movement. Nevertheless, most measurings were successful, and for such a simple pallet lever construction with large axles, the rates were not too bad:
  images:
    ZO: Zeitwaage_Eppler_20_ZO.jpg
    ZU: Zeitwaage_Eppler_20_ZU.jpg
    3O: Zeitwaage_Eppler_20_3O.jpg
    6O: Zeitwaage_Eppler_20_6O.jpg
    9O: Zeitwaage_Eppler_20_9O.jpg
    12O: Zeitwaage_Eppler_20_12O.jpg
  values:
    ZO: "-10"
    ZU: "+5"
    3O: "-20"
    6O: "+-0"
    9O: "-90"
    12O: "?"
labor: |
  The specimen shown here was still working, but nevertheless got a full service with cleaning and oiling.
---
Lorem Ipsum