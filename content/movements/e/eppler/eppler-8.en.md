---
title: "Eppler 8"
date: 2013-12-01T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Eppler 8","Eppler","pin lever","7 Jewels","7 Rubis","center second"]
description: "Eppler 8 - a German made pin lever movement with 7 jewels and center second hand"
abstract: "A well made german 7 jewel pin lever movement with center second."
preview_image: "eppler_8-mini.jpg"
image: "Eppler_8.jpg"
movementlistkey: "eppler"
caliberkey: "8"
manufacturers: ["eppler"]
manufacturers_weight: 8
categories: ["movements","movements_e","movements_e_eppler_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The specimen shown here came dirty but alive in the lab and got a full service with cleaning and oiling.
usagegallery: 
  - image: "a/ancre_goupilles/Ancre_Goupilles_HAU_Eppler_8.jpg"
    description: "Ancre Goupilles mens' watch"
timegrapher_old: 
  description: |
    For a pin lever movement of more than 50 years old, the rates are, except the slip-up at "9 up", are absolutely ok, and during daily wear, the average deviation should be way less than one minute. Really not bad for a pin lever movement!
  images:
    ZO: Zeitwaage_Eppler_20_ZO.jpg
    ZU: Zeitwaage_Eppler_20_ZU.jpg
    3O: Zeitwaage_Eppler_20_3O.jpg
    6O: Zeitwaage_Eppler_20_6O.jpg
    9O: Zeitwaage_Eppler_20_9O.jpg
    12O: Zeitwaage_Eppler_20_12O.jpg
  values:
    ZO: "+20"
    ZU: "+25"
    3O: "-15"
    6O: "-40"
    9O: "-100"
    12O: "-50"
---
Lorem Ipsum