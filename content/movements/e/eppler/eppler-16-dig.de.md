---
title: "Eppler 16 dig"
date: 2013-12-23T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Eppler 16","Eppler","Stiftanker","Digital","1 Jewel"]
description: "Eppler 16 dig - die Digitalausführung des gängigen, einsteinigen Eppler-Stiftankerwerks"
abstract: "Das bekannte einsteinige Stiftankerwerk, hier in der Ausführung für digitale Zeitanzeige"
preview_image: "eppler_16_dig-mini.jpg"
image: "Eppler_16_dig.jpg"
movementlistkey: "eppler"
caliberkey: "16 dig"
manufacturers: ["eppler"]
manufacturers_weight: 16
categories: ["movements","movements_e","movements_e_eppler"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam verschmutzt aber gangbar in das Labor und bekam einen Vollservice mit Reinigung und Ölung. Die Uhr, die dieses Werk enthält, zeigte starke Gebrauchsspuren, man kann von jahrelangem Gebrauch ohne Schonung der Uhr ausgehen.
usagegallery: 
  - image: "a/anker/Anker_Digital_HAU_Eppler_16_dig.jpg"
    description: "Anker Digital Herrenuhr"
timegrapher_old: 
  description: |
    Die Amplitude ist nach der Reinigung hervorragend, die Gangwerte sind mit Ausnahme der Lage "12 oben" für ein Stiftankerwerk durchaus in Ordnung. Würde man mehr Zeit in die Regulierung investieren, vielleicht das Höhenspiel der Unruh und den Abfall noch exakter justieren, könnten wirklich gute Ergebnisse erzielt werden. 
  images:
    ZO: Zeitwaage_Eppler_16_dig_ZO.jpg
    ZU: Zeitwaage_Eppler_16_dig_ZU.jpg
    3O: Zeitwaage_Eppler_16_dig_3O.jpg
    6O: Zeitwaage_Eppler_16_dig_6O.jpg
    9O: Zeitwaage_Eppler_16_dig_9O.jpg
    12O: Zeitwaage_Eppler_16_dig_12O.jpg
  values:
    ZO: "+20"
    ZU: "-30"
    3O: "+10"
    6O: "+60"
    9O: "+30"
    12O: "+80"
---
Lorem Ipsum