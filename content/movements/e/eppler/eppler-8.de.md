---
title: "Eppler 8"
date: 2013-11-24T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Eppler 8","Eppler","Stiftanker","7 Jewels","7 Steine","Zentralsekunde"]
description: "Eppler 8 - ein 7-steiniges Stiftankerwerk mit Zentralsekunde von besserer Qualität aus Deutschland"
abstract: "Ein 7-steiniges Stiftankerwerk besserer Qualität mit Zentralsekunde."
preview_image: "eppler_8-mini.jpg"
image: "Eppler_8.jpg"
movementlistkey: "eppler"
caliberkey: "8"
manufacturers: ["eppler"]
manufacturers_weight: 8
categories: ["movements","movements_e","movements_e_eppler"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    Für ein über 50 Jahre altes Stiftankerwerk sind die Gangwerte mit Ausnahme des Ausrutschers bei "9 oben", der möglicherweise auf eine nicht optimal ausgewuchtete Unruh zurückzuführen ist, absolut im Rahmen, und im täglichen Tragen dürfte die Gangabweichung bei deutlich unter einer Minute pro Tag liegen, ein, für ein Stiftankerwerk sehr guter Wert.  
  images:
    ZO: Zeitwaage_Eppler_20_ZO.jpg
    ZU: Zeitwaage_Eppler_20_ZU.jpg
    3O: Zeitwaage_Eppler_20_3O.jpg
    6O: Zeitwaage_Eppler_20_6O.jpg
    9O: Zeitwaage_Eppler_20_9O.jpg
    12O: Zeitwaage_Eppler_20_12O.jpg
  values:
    ZO: "+20"
    ZU: "+25"
    3O: "-15"
    6O: "-40"
    9O: "-100"
    12O: "-50"
usagegallery: 
  - image: "a/ancre_goupilles/Ancre_Goupilles_HAU_Eppler_8.jpg"
    description: "Ancre Goupilles Herrenuhr"
labor: |
  Das vorliegende Werk kam verschmutzt aber gangbar in das Labor und bekam einen Vollservice mit Reinigung und Ölung.
---
Lorem Ipsum