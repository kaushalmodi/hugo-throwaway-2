---
title: "Eppler 16"
date: 2009-04-25T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Uhrenbastler","Eppler 16","Eppler","16","0 Jewels","pin lever","Germany"]
description: "Eppler 16"
abstract: ""
preview_image: "eppler_16-mini.jpg"
image: "Eppler_16.jpg"
movementlistkey: "eppler"
caliberkey: "16"
manufacturers: ["eppler"]
manufacturers_weight: 16
categories: ["movements","movements_e","movements_e_eppler_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_100_Lernuhr.jpg"
    description: "Anker 100 learning watch"
  - image: "a/ancre_goupilles/Ancre_Goupilles_DAU.jpg"
    description: "Ancre Goupilles ladies' watch"
---
Lorem Ipsum