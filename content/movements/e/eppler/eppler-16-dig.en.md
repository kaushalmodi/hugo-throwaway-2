---
title: "Eppler 16 dig"
date: 2013-12-26T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Eppler 16","Eppler","pin lever","digital","1 jewel"]
description: "Eppler 16 dig - the digital time indication version of the well known german pin lever movement Eppler 16"
abstract: "The popular one-jewel pin lever movement, here with a digital time indication"
preview_image: "eppler_16_dig-mini.jpg"
image: "Eppler_16_dig.jpg"
movementlistkey: "eppler"
caliberkey: "16 dig"
manufacturers: ["eppler"]
manufacturers_weight: 16
categories: ["movements","movements_e","movements_e_eppler_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    After cleaning, the amplitude of the balance wheel is excellent, and the timing rates are except on position "12 up" quite well for a pin lever movement. With more time invested on regulation, correcting the beat error and proper adjustment of the height of the balance, the timing rated could be well improved.
  images:
    ZO: Zeitwaage_Eppler_16_dig_ZO.jpg
    ZU: Zeitwaage_Eppler_16_dig_ZU.jpg
    3O: Zeitwaage_Eppler_16_dig_3O.jpg
    6O: Zeitwaage_Eppler_16_dig_6O.jpg
    9O: Zeitwaage_Eppler_16_dig_9O.jpg
    12O: Zeitwaage_Eppler_16_dig_12O.jpg
  values:
    ZO: "+20"
    ZU: "-30"
    3O: "+10"
    6O: "+60"
    9O: "+30"
    12O: "+80"
usagegallery: 
  - image: "a/anker/Anker_Digital_HAU_Eppler_16_dig.jpg"
    description: "Anker Digital mens' watch"
labor: |
  The specimen here came in working condition, but dirty into the lab and got a full service, including cleaning and oiling. The watch, which contained the movement shows lots of traces from long wearing. It seems like it was not treated with care at all.
---
Lorem Ipsum