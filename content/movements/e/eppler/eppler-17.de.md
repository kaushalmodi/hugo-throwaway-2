---
title: "Eppler 17"
date: 2009-04-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Christoph Lorenz","Uhrenbastler","Eppler 17","Eppler","17","0 Jewels","0 Steine","Handaufzug","Stiftanker","Deutschland"]
description: "Eppler 17"
abstract: ""
preview_image: "eppler_17-mini.jpg"
image: "Eppler_17.jpg"
movementlistkey: "eppler"
caliberkey: "17"
manufacturers: ["eppler"]
manufacturers_weight: 17
categories: ["movements","movements_e","movements_e_eppler"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_100_DAU.jpg"
    description: "Anker 100 Damenuhr"
  - image: "a/anker/Anker_100_DAU_2.jpg"
    description: "Anker 100 Damenuhr"
---
Lorem Ipsum