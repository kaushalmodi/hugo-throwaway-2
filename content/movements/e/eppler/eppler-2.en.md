---
title: "Eppler 2"
date: 2009-05-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Eppler 2","Eppler","2","4 Jewels","watch","watches","wristwatch","wristwatches","caliber","Germany","pin lever"]
description: "Eppler 2"
abstract: ""
preview_image: "eppler_2-mini.jpg"
image: "Eppler_2.jpg"
movementlistkey: "eppler"
caliberkey: "2"
manufacturers: ["eppler"]
manufacturers_weight: 2
categories: ["movements","movements_e","movements_e_eppler_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ancre_goupilles/Ancre_Goupilles_DAU_2.jpg"
    description: "Ancre Goupilles ladies' watch"
---
Lorem Ipsum