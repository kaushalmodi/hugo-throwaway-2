---
title: "Eppler 21"
date: 2009-04-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Eppler 21","Eppler","21","17 Jewels","caliber","Germany"]
description: "Eppler 21"
abstract: ""
preview_image: "eppler_21-mini.jpg"
image: "Eppler_21.jpg"
movementlistkey: "eppler"
caliberkey: "21"
manufacturers: ["eppler"]
manufacturers_weight: 21
categories: ["movements","movements_e","movements_e_eppler_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_DAU_3.jpg"
    description: "Anker ladies' watch"
---
Lorem Ipsum