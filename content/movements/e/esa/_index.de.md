---
title: "ESA"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "esa"
description: "Uhrwerke des schweizer Herstellers ESA aus Neuchatel"
abstract: "Uhrwerke des schweizer Herstellers ESA aus Neuchatel"
---
(Ebauches S.A., Neuchatel, Schweiz)
{{< movementlist "esa" >}}

{{< movementgallery "esa" >}}