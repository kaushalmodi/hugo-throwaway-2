---
title: "Elgin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: ["Elgin","Elgin National Watch Company","ENWC","USA","Illinois"]
categories: ["movements","movements_e"]
movementlistkey: "elgin"
description: "Movements from the Elgin National Watch Company ,USA"
abstract: "Movements from the Elgin National Watch Company ,USA"
---
(Elgin National Watch Company, Elgin, Illinois, USA)
{{< movementlist "elgin" >}}

{{< movementgallery "elgin" >}}