---
title: "Elgin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: ["Elgin","National Watch Company","Illinois","USA"]
categories: ["movements","movements_e"]
movementlistkey: "elgin"
abstract: "Movements of the Elgin National Watch Company in Illinois, USA"
description: "Movements of the Elgin National Watch Company in Illinois, USA"
---
(Elgin National Watch Company, Elgin, Illinois, USA)
{{< movementlist "elgin" >}}

{{< movementgallery "elgin" >}}