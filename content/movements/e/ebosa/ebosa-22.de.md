---
title: "Ebosa 22"
date: 2010-03-26T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Stiftankerwerk","Roskopf","Neusilber-Auflage","Streifenschliffen","Ebosa 22","Ebosa","22","15 Jewels","Grenchen","Schweiz","15 Steine","Handaufzug"]
description: "Ebosa 22 - Ein sehr gut verarbeites, 15-steiniges Stiftankerwerk. Detaillierte Beschreibung mit Bildern, Makro-Aufnahmen und Datenblatt."
abstract: ""
preview_image: "ebosa_22-mini.jpg"
image: "Ebosa_22.jpg"
movementlistkey: "ebosa"
caliberkey: "22"
manufacturers: ["ebosa"]
manufacturers_weight: 22
categories: ["movements","movements_e","movements_e_ebosa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Watch-Wiki: Ebosa S.A.](http://watch-wiki.org/index.php?title=Ebosa_S.A.)[](http://watch-wiki.org/index.php?title=Ebosa_S.A.)
usagegallery: 
  - image: "anonym/HAU_Ebosa_22.jpg"
    description: "anonyme Herrenuhr"
---
Lorem Ipsum