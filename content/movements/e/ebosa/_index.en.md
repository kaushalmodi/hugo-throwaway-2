---
title: "Ebosa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "ebosa"
abstract: "(Ebosa Trading AG, Grenchen, Switzerland)"
description: ""
---
(Ebosa Trading AG, Grenchen, Switzerland)
{{< movementlist "ebosa" >}}

{{< movementgallery "ebosa" >}}