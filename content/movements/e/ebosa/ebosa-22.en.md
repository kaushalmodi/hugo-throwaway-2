---
title: "Ebosa 22"
date: 2010-03-26T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["pin lever movement","Roskopf","stripes decoration","nickel silver","Ebosa 22","Ebosa","22","15 Jewels","Grenchen","swiss","switzerland","handwinding"]
description: "Ebosa 22 - A high quality pin lever movement with 15 jewels. Detailed description with photos, macro images and data sheet."
abstract: ""
preview_image: "ebosa_22-mini.jpg"
image: "Ebosa_22.jpg"
movementlistkey: "ebosa"
caliberkey: "22"
manufacturers: ["ebosa"]
manufacturers_weight: 22
categories: ["movements","movements_e","movements_e_ebosa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/HAU_Ebosa_22.jpg"
    description: "anonymous gents watch"
---
Lorem Ipsum