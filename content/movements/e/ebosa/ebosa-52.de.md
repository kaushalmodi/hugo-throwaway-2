---
title: "Ebosa 52"
date: 2009-04-10T13:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ebosa 52","Ebosa","52","17 Jewels","17 Steine","Stiftanker","Schweiz"]
description: "Ebosa 52"
abstract: ""
preview_image: "ebosa_52-mini.jpg"
image: "Ebosa_52.jpg"
movementlistkey: "ebosa"
caliberkey: "52"
manufacturers: ["ebosa"]
manufacturers_weight: 52
categories: ["movements","movements_e","movements_e_ebosa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Watch-Wiki: Ebosa S.A.](http://watch-wiki.org/index.php?title=Ebosa_S.A.)[](http://watch-wiki.org/index.php?title=Ebosa_S.A.)
usagegallery: 
  - image: "b/buler/Buler_DAU_2.jpg"
    description: "Buler Damenuhr"
---
Lorem Ipsum