---
title: "Ebosa 131"
date: 2009-04-10T13:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ebosa 131","Ebosa","131","1 Jewels","pin lever","swiss","switzerland"]
description: "Ebosa 131"
abstract: ""
preview_image: "ebosa_131-mini.jpg"
image: "Ebosa_131.jpg"
movementlistkey: "ebosa"
caliberkey: "131"
manufacturers: ["ebosa"]
manufacturers_weight: 131
categories: ["movements","movements_e","movements_e_ebosa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/burgana/Burgana_HAU.jpg"
    description: "Burgana gents watch"
---
Lorem Ipsum