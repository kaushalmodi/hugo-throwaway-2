---
title: "Ebosa 86"
date: 2009-04-10T13:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ebosa 86","Ebosa","86","0 Jewels","pin lever","swiss made","Switzerland","swiss"]
description: "Ebosa 86"
abstract: ""
preview_image: "ebosa_86-mini.jpg"
image: "Ebosa_86.jpg"
movementlistkey: "ebosa"
caliberkey: "86"
manufacturers: ["ebosa"]
manufacturers_weight: 86
categories: ["movements","movements_e","movements_e_ebosa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/davar/Davar_DAU.jpg"
    description: "Davar ladies' watch"
---
Lorem Ipsum