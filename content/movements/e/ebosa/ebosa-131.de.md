---
title: "Ebosa 131"
date: 2009-04-10T13:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ebosa 131","Ebosa","131","1 Jewels","1 Stein","Stiftanker","Schweiz"]
description: "Ebosa 131"
abstract: ""
preview_image: "ebosa_131-mini.jpg"
image: "Ebosa_131.jpg"
movementlistkey: "ebosa"
caliberkey: "131"
manufacturers: ["ebosa"]
manufacturers_weight: 131
categories: ["movements","movements_e","movements_e_ebosa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/burgana/Burgana_HAU.jpg"
    description: "Burgana Herrenuhr"
links: |
  * [Watch-Wiki: Ebosa S.A.](http://watch-wiki.org/index.php?title=Ebosa_S.A.)[](http://watch-wiki.org/index.php?title=Ebosa_S.A.)
---
Lorem Ipsum