---
title: "Eterna 1435 K"
date: 2009-04-11T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Eterna 1435 K","Eterna","1435 K","17 Jewels","17 Rubis","Swiss","Switzerland","ladies' watch"]
description: "Eterna 1435 K"
abstract: ""
preview_image: "eterna_1435_k-mini.jpg"
image: "Eterna_1435_K.jpg"
movementlistkey: "eterna"
caliberkey: "1435 K"
manufacturers: ["eterna"]
manufacturers_weight: 1435
categories: ["movements","movements_e","movements_e_eterna_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/eterna/Eterna_DAU.jpg"
    description: "Eterna ladies' watch"
links: |
  * [Ranfft Uhren: Eterna 1435K](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Eterna_1435K)
---
Lorem Ipsum