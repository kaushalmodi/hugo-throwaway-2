---
title: "Eterna 1408U"
date: 2015-12-23T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Eterna 1408U","17 Jewels","17 Rubis","Grenchen","manual wind","Glucydur"]
description: "Eterna 1408U"
abstract: "A 11 1/2 ligne manual wind movement from the early 1960ies with a very modern and pioneering construction."
preview_image: "eterna_1408u-mini.jpg"
image: "Eterna_1408U.jpg"
movementlistkey: "eterna"
caliberkey: "1408U"
manufacturers: ["eterna"]
manufacturers_weight: 1408
categories: ["movements","movements_e","movements_e_eterna_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "e/eterna/Eterna_HAU_Eterna_1408U.jpg"
    description: "Eterna gents watch (-parts)"
labor: |
  <p>The specimen shown here dates from 1962 and must have suffered from a terrible accident. The plates were bent(!), the screws partially blown away and the movement was heavily covered by rust. Because of this, no further service was made.</p><p>One detail is very interesting: The large Gluydur balance was not harmed at all and still beats strong like on the first day. Big compliments to the inhouse Eterna shock protections!</p>
---
Lorem Ipsum