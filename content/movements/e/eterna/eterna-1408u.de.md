---
title: "Eterna 1408U"
date: 2015-12-12T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Eterna 1408U","17 Jewels","17 Steine","Grenchen","Handaufzug","Glucycur"]
description: "Eterna 1408U"
abstract: "Ein 11 1/2 liniges Handaufzugswerk in damals sehr moderner und wegweisender Bauweise aus den frühen 1960er Jahren."
preview_image: "eterna_1408u-mini.jpg"
image: "Eterna_1408U.jpg"
movementlistkey: "eterna"
caliberkey: "1408U"
manufacturers: ["eterna"]
manufacturers_weight: 1408
categories: ["movements","movements_e","movements_e_eterna"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>Das hier gezeigte Exemplar aus dem Jahr 1962 muß einen verheerenden Unfall erlebt haben, die Platinen waren verbogen(!), die Schrauben teilweise abgeplatzt und zudem war das Werk stark von Flugrost befallen. Aus diesem Grund wurde auch kein weiterer Service durchgeführt.</p><p>Ein Detail ist aber sehr interessant: Die große Glucydur-Unruh wurde nicht beschädigt, und sie schwingt auch heute noch wie am ersten Tag. Großes Kompliment an die hauseigene Eterna-Stoßsicherung!</p>
usagegallery: 
  - image: "e/eterna/Eterna_HAU_Eterna_1408U.jpg"
    description: "Eterna Herrenuhr(-reste)"
---
Lorem Ipsum