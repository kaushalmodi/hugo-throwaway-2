---
title: "Eterna"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: ["Eterna","Grenchen","Schweiz"]
categories: ["movements","movements_e"]
movementlistkey: "eterna"
abstract: "Uhrwerke von Eterna aus Grenchen, Schweiz"
description: "Uhrwerke von Eterna aus Grenchen, Schweiz"
---
(Eterna S.A., Grenchen, Schweiz)
{{< movementlist "eterna" >}}

{{< movementgallery "eterna" >}}