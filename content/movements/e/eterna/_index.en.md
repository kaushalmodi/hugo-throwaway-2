---
title: "Eterna"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: ["Eterna","Switzerland","Grenchen","Swiss"]
categories: ["movements","movements_e"]
movementlistkey: "eterna"
abstract: "Movements of the swiss manufacturer Eterna, which is located in Grenchen, Switzerland"
description: "Movements of the swiss manufacturer Eterna, which is located in Grenchen, Switzerland"
---
(Eterna S.A., Grenchen, Switzerland)
{{< movementlist "eterna" >}}

{{< movementgallery "eterna" >}}