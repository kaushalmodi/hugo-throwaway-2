---
title: "EB"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "eb"
description: "Movements of the EB, the Ebauches Bettlach"
abstract: "Movements of the EB, the Ebauches Bettlach"
---
(Ebauches Bettlach, Bettlach, Switzerland)
{{< movementlist "eb" >}}

{{< movementgallery "eb" >}}