---
title: "EB 1612N / Kienzle 053/703"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["EB 1612N","Ebauches Bettlach","Bettlach","KIF","7 Jewels","Kienzle 053/703","Kienzle","053/073","Swiss","swiss","switzerland","ladies` watch","pin lever","form movement"]
description: "EB 1612N"
abstract: ""
preview_image: "eb_1612n-mini.jpg"
image: "EB_1612N.jpg"
movementlistkey: "eb"
caliberkey: "1612N"
manufacturers: ["eb"]
manufacturers_weight: 1612
categories: ["movements","movements_e","movements_e_eb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_DAU_7_Jewels.jpg"
    description: "Kienzle ladies' watch"
links: |
  * [Ranfft Uhren: EB 1612N](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&EB_1612N)
---
Lorem Ipsum