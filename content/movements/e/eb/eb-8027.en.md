---
title: "EB 8027"
date: 2011-02-06T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["pin lever movement","Ebauches Bettlach","pier construction","rocking bar winding system","yoke winding system","click mechanism","EB 8027","EB","Ebauches Bettlach","8027","23 Jewels","swiss","switzerland"]
description: "EB 8027 - A better pin lever movement with a whooping lot of 23 jewels. Detailed description with photos, video and data sheet."
abstract: ""
preview_image: "eb_8027-mini.jpg"
image: "EB_8027.jpg"
movementlistkey: "eb"
caliberkey: "8027"
manufacturers: ["eb"]
manufacturers_weight: 8027
categories: ["movements","movements_e","movements_e_eb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "h/helsa/Helsa_Precision_HAU_EB_8027.jpg"
    description: "Helsa Precision divers' watch"
---
Lorem Ipsum