---
title: "EB 1612N / Kienzle 053/703"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["EB 1612N","Ebauches Bettlach","Bettlach","KIF","7 Jewels","Kienzle 053/703","Kienzle","053/073","Swiss","Schweiz","7 Steine","Damenuhr","Stiftanker","Formwerk"]
description: "EB 1612N"
abstract: ""
preview_image: "eb_1612n-mini.jpg"
image: "EB_1612N.jpg"
movementlistkey: "eb"
caliberkey: "1612N"
manufacturers: ["eb"]
manufacturers_weight: 1612
categories: ["movements","movements_e","movements_e_eb"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: EB 1612N](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&EB_1612N)
usagegallery: 
  - image: "k/kienzle/Kienzle_DAU_7_Jewels.jpg"
    description: "Kienzle Damenuhr"
---
Lorem Ipsum