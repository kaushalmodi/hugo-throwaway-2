---
title: "EB"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "eb"
abstract: "(Ebauches Bettlach, Bettlach, Schweiz)"
description: ""
---
(Ebauches Bettlach, Bettlach, Schweiz)
{{< movementlist "eb" >}}

{{< movementgallery "eb" >}}