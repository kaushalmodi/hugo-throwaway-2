---
title: "EB 1225"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["EB 1225","EB","1225","17 Jewels","Ebauches Bettlach","pin lever","swiss","Switzerland","17 Rubis"]
description: "EB 1225"
abstract: ""
preview_image: "eb_1225-mini.jpg"
image: "EB_1225.jpg"
movementlistkey: "eb"
caliberkey: "1225"
manufacturers: ["eb"]
manufacturers_weight: 1225
categories: ["movements","movements_e","movements_e_eb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mustang/Mustang_DAU.jpg"
    description: "Mustang ladies' watch"
---
Lorem Ipsum