---
title: "EB 8027"
date: 2011-02-06T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Stiftankerwerk","Ebauches Bettlach","Pfeilerbauweise","Wippenaufzug","Kupplungsaufzug","Gesperr","EB 8027","EB","Ebauches Bettlach","8027","23 Jewels","Schweiz","23 Steine"]
description: "EB 8027 - Ein besseres Stiftankerwerk mit verschwenderischen 23 Steinen. Detaillierte Beschreibung mit Bildern, Video und Datenblatt."
abstract: ""
preview_image: "eb_8027-mini.jpg"
image: "EB_8027.jpg"
movementlistkey: "eb"
caliberkey: "8027"
manufacturers: ["eb"]
manufacturers_weight: 8027
categories: ["movements","movements_e","movements_e_eb"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "h/helsa/Helsa_Precision_HAU_EB_8027.jpg"
    description: "Helsa Precision Taucheruhr"
---
Lorem Ipsum