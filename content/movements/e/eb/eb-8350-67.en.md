---
title: "EB 8350-67"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["EB 8350-67","EB","8350-67","17 Jewels","Ebauches Bettlach","pin lever","swiss"]
description: "EB 8350-67"
abstract: ""
preview_image: "eb_8350-67-mini.jpg"
image: "EB_8350-67.jpg"
movementlistkey: "eb"
caliberkey: "8350-67"
manufacturers: ["eb"]
manufacturers_weight: 835067
categories: ["movements","movements_e","movements_e_eb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/rewatch/ReWatch_DAU.jpg"
    description: "Re-Watch ladies' watch"
---
Lorem Ipsum