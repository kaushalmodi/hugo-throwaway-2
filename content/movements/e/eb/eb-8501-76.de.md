---
title: "EB 8501-76"
date: 2017-02-05T01:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["EB 8501-76","EB","8501-76","Ebauches Bettlach","Swiss Made","schweizer Uhr","Schweiz","mechanisch","Handaufzug","Plastik"]
description: "EB 8501-76: Ein sehr modernes, skurriles und häßliches, aber dennoch innovatives Handaufzugswerk"
abstract: "Eine der häßlichsten, aber innovativsten Billig-Konstruktionen, die in den 70er-Jahren auf den Markt kam."
preview_image: "eb_8501-76-mini.jpg"
image: "EB_8501-76.jpg"
movementlistkey: "eb"
caliberkey: "8501-76"
manufacturers: ["eb"]
manufacturers_weight: 850176
categories: ["movements","movements_e","movements_e_eb"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "anonym/Minnie_Mouse_2.jpg"
    description: "&quot;Minnie Mouse&quot; Comic-Uhr"
  - image: "r/rewatch/RE-Watch_HAU_EB_8501-76.jpg"
    description: "RE-Watch Taucheruhr"
---
Lorem Ipsum