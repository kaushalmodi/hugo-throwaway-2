---
title: "EB 1553"
date: 2014-11-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["EB 1553","EB","Ebauches Bettlach","pin lever","17 Jewels","KIF","Fake screw balance","lookalike screw balance"]
description: "EB 1553 - a very well made swiss pin lever movement"
abstract: "A very well and costly made pin lever movement, which has even a lookalike screw balance."
preview_image: "eb_1553-mini.jpg"
image: "EB_1553.jpg"
movementlistkey: "eb"
caliberkey: "1553"
manufacturers: ["eb"]
manufacturers_weight: 1553
categories: ["movements","movements_e","movements_e_eb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tegrov/Tegrov_HAU_EB_1553.jpg"
    description: "Tegrov mens' watch"
  - image: "d/direct/Direct_HAU.jpg"
    description: "Direct mens' watch"
---
Lorem Ipsum