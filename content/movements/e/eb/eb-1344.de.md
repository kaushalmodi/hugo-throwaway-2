---
title: "EB 1344"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["EB 1344","EB","1344","Ebauches Bettlach","17 Jewels","17 Steine","Stiftanker","Schweiz"]
description: "EB 1344"
abstract: ""
preview_image: "eb_1344-mini.jpg"
image: "EB_1344.jpg"
movementlistkey: "eb"
caliberkey: "1344"
manufacturers: ["eb"]
manufacturers_weight: 1344
categories: ["movements","movements_e","movements_e_eb"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tior/Tior_HAU.jpg"
    description: "Tior Cocktail Herrenuhr"
---
Lorem Ipsum