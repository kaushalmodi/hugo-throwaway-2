---
title: "ETA 2412 / Dugena 1201"
date: 2009-09-30T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 2412","ETA","2412","17 Jewels","Grenchen","swiss","switzerland"]
description: "ETA 2412"
abstract: ""
preview_image: "eta_2412-mini.jpg"
image: "ETA_2412.jpg"
movementlistkey: "eta"
caliberkey: "2412"
manufacturers: ["eta"]
manufacturers_weight: 2412
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_DAU_Zifferblatt_2.jpg"
    description: "Dugena ladies' watch  (dial only)"
  - image: "r/roamer/Roamer_DAU_ETA_2412.jpg"
    description: "Roamer ladies' watch"
donation: "The movement in the Dugena version was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201), and the movement in the Roamer watch, together with the watch, was donated by [U.Ströbel-Dettmer](/supporters/u-stroebelttmer/). Thank you very much!"
links: |
  * [Ranfft Uhren: ETA 2410 / Dugena 1201](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Dugena_1201)
---
Lorem Ipsum