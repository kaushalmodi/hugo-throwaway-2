---
title: "ETA 2410"
date: 2009-09-30T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 2410","ETA","2410","17 Jewels","Grenchen","swiss","switzerland"]
description: "ETA 2410"
abstract: ""
preview_image: "eta_2410-mini.jpg"
image: "ETA_2410.jpg"
movementlistkey: "eta"
caliberkey: "2410"
manufacturers: ["eta"]
manufacturers_weight: 2410
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: ETA 2410](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&ETA_2410)
  * [Watch Wiki: ETA 2410](http://watch-wiki.de/index.php?title=ETA_2410)
usagegallery: 
  - image: "e/exquisit/Exquisit_DAU_2_Zifferblatt.jpg"
    description: "Exquisit ladies' watch  (dial only)"
  - image: "c/cito/Cito_DAU_Zifferblatt.jpg"
    description: "Cito ladies' watch  (dial only)"
---
Lorem Ipsum