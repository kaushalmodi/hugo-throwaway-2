---
title: "ETA 406"
date: 2009-04-10T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 406","ETA","406","10 Jewels","cylinder movement","swiss","switzerland"]
description: "ETA 406"
abstract: ""
preview_image: "eta_406-mini.jpg"
image: "ETA_406.jpg"
movementlistkey: "eta"
caliberkey: "406"
manufacturers: ["eta"]
manufacturers_weight: 406
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Unknown_ETA_Cylinder.jpg"
    description: "anonymous ladies' watch"
---
Lorem Ipsum