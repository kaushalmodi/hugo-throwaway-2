---
title: "ETA 2390"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA","ETA 2390","2390","17","Steine","17 Steine","17 Rubis","Schweiz","Schraubenunruh","Schraubenunruhe"]
description: "ETA 2390"
abstract: ""
preview_image: "eta_2390-mini.jpg"
image: "ETA_2390.jpg"
movementlistkey: "eta"
caliberkey: "2390"
manufacturers: ["eta"]
manufacturers_weight: 2390
categories: ["movements","movements_e","movements_e_eta"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/konnexa/Konnexa_42.jpg"
    description: "Konnexa 42 Herrenuhr"
---
Lorem Ipsum