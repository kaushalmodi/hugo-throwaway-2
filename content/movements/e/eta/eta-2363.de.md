---
title: "ETA 2363"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA","ETA 2363","2363","17","Steine","17 Steine","17 Rubis","Schweiz","Schraubenunruh","Schraubenunruhe"]
description: "ETA 2363"
abstract: ""
preview_image: "eta_2363-mini.jpg"
image: "ETA_2363.jpg"
movementlistkey: "eta"
caliberkey: "2363"
manufacturers: ["eta"]
manufacturers_weight: 2363
categories: ["movements","movements_e","movements_e_eta"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum