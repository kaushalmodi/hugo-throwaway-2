---
title: "ETA 2671"
date: 2009-04-10T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA","ETA 2671","2671","Damenuhr","Kaliber","Werk","21","Steine","Jewels","Automatic","Schweiz"]
description: "ETA 2671"
abstract: ""
preview_image: "eta_2671-mini.jpg"
image: "ETA_2671_21.jpg"
movementlistkey: "eta"
caliberkey: "2671"
manufacturers: ["eta"]
manufacturers_weight: 2671
categories: ["movements","movements_e","movements_e_eta"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/pallas/Pallas_Para_DAU_Automatic.jpg"
    description: "Pallas Para Automatic Damenuhr"
  - image: "t/tempic/Tempic_Automatic_DAU.jpg"
    description: "Tempic Automatic Damenuhr"
---
Lorem Ipsum