---
title: "ETA"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: []
categories: ["movements","movements_e"]
movementlistkey: "eta"
description: ""
abstract: "(ETA SA Manufacture Horlogère Suisse, Grenchen, Schweiz)"
---
(ETA SA Manufacture Horlogère Suisse, Grenchen, Schweiz)
{{< movementlist "eta" >}}

{{< movementgallery "eta" >}}