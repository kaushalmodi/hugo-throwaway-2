---
title: "ETA 1170"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA. ETA 1170","1170","Werk","21","Steine","Jewels","Handaufzug","Schweiz"]
description: "ETA 1170"
abstract: ""
preview_image: "eta_1170-mini.jpg"
image: "ETA_1170.jpg"
movementlistkey: "eta"
caliberkey: "1170"
manufacturers: ["eta"]
manufacturers_weight: 1170
categories: ["movements","movements_e","movements_e_eta"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum