---
title: "ETA 2650"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA","ETA 2650","2650","Felca","Junghans 603.00","caliber","17","jewels","Switzerland","automatic","selfwinding"]
description: "ETA 2650 / Junghans 603.00"
abstract: ""
preview_image: "eta_2650-mini.jpg"
image: "ETA_2650.jpg"
movementlistkey: "eta"
caliberkey: "2650"
manufacturers: ["eta"]
manufacturers_weight: 2650
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum