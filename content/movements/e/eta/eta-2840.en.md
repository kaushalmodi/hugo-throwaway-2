---
title: "ETA 2840"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA","ETA 2840","23","Jewels","23 Jewels","23 Rubis","Automatic","Switzerland","Swiss","selfwinding"]
description: "ETA 2840"
abstract: ""
preview_image: "eta_2840-mini.jpg"
image: "ETA_2840.jpg"
movementlistkey: "eta"
caliberkey: "2840"
manufacturers: ["eta"]
manufacturers_weight: 2840
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum