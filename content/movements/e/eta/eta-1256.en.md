---
title: "ETA 1256"
date: 2010-01-31T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["1950","Benrus","CF 1","spring less click reverser","double click wheel","ETA 1256","ETA","1256","17 Jewels","Grenchen","Automatic","ETARotor","swiss","switzerland","selfwinding"]
description: "ETA 1256 - The first selfwinding movement by ETA. Detailed description with photos, videos, data sheet and timegrapher protocol."
abstract: ""
preview_image: "eta_1256-mini.jpg"
image: "ETA_1256.jpg"
movementlistkey: "eta"
caliberkey: "1256"
manufacturers: ["eta"]
manufacturers_weight: 1256
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Alliance Horogere: Technical DataSheets ETA 1256](http://hiro.alliancehorlogere.com/en/Under_the_Loupe/Automatic_Winding_Mechanisms)
  * [US Patent #2456071 - Shock Absorbing Device For Pivots](http://books.google.com/patents?id=MixaAAAAEBAJ&zoom=4&pg=PA1#v=onepage&q=&f=false)  (Patent of the Benrus shock protection shown here)
  * [Ranfft Uhren: ETA 1256](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&ETA_1256) (The specimen shown there is the "standard" ETA 1256, not the Benrus type.)
timegrapher_old: 
  description: |
    The specimen shown here, was completely gummed and showed signs of water damage. It had to be cleaned and overhauled competely.  
  images:
    ZO: Zeitwaage_ETA_1256_ZO.jpg
    ZU: Zeitwaage_ETA_1256_ZU.jpg
    3O: Zeitwaage_ETA_1256_3O.jpg
    6O: Zeitwaage_ETA_1256_6O.jpg
    9O: Zeitwaage_ETA_1256_9O.jpg
    12O: Zeitwaage_ETA_1256_12O.jpg
---
Lorem Ipsum