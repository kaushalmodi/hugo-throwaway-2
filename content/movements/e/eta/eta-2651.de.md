---
title: "ETA 2651"
date: 2009-04-10T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA 2651","ETA","17 Jewels","Swiss","2651","Automatic","17 Steine","Automatik","Damenuhr","Schweiz"]
description: "ETA 2651"
abstract: ""
preview_image: "eta_2651-mini.jpg"
image: "ETA_2651.jpg"
movementlistkey: "eta"
caliberkey: "2651"
manufacturers: ["eta"]
manufacturers_weight: 2651
categories: ["movements","movements_e","movements_e_eta"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/destosos/Destos_Automatic_DAU.jpg"
    description: "Destos Automatic Damenuhr"
  - image: "a/avia/Avia_Matic_DAU.jpg"
    description: "Avia-matic Damenuhr"
---
Lorem Ipsum