---
title: "ETA 2512-1"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA 2512-1","ETA 2512","ETA","2512-1","17 Jewels","Grenchen","Switzerland","Swiss","17 Rubis"]
description: "ETA 2512-1"
abstract: ""
preview_image: "eta_2512-1-mini.jpg"
image: "ETA_2512-1.jpg"
movementlistkey: "eta"
caliberkey: "2512-1"
manufacturers: ["eta"]
manufacturers_weight: 25121
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/coronet/Coronet_06_DAU_Zifferblatt.jpg"
    description: "Coronet 06 ladies' watch  (dial only)"
links: |
  <p>* [Ranfft Uhren: ETA 2512](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&ETA_2512)
  * [Ranfft Uhren: ETA 2512-1](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&ETA_2512_1)
  * [Timedesign: ETA 2512](http://www.timedesign.de/uhrwerke/eta_2512.html)</p><p>This movement was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum