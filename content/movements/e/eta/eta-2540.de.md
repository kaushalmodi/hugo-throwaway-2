---
title: "ETA 2540"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA 2540","ETA","2540","17 Jewels","17 Steine","Schweiz"]
description: "ETA 2540"
abstract: ""
preview_image: "eta_2540-mini.jpg"
image: "ETA_2540.jpg"
movementlistkey: "eta"
caliberkey: "2540"
manufacturers: ["eta"]
manufacturers_weight: 2540
categories: ["movements","movements_e","movements_e_eta"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "n/nibo/Nibo_DAU.jpg"
    description: "Nibo Damenuhr"
---
Lorem Ipsum