---
title: "ETA 2452"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["ETA","ETA 2452","2452","25","jewels","25 jewels","25 Jewels","swiss","Switzerland","screw balance","automatic","selfwinding"]
description: "ETA 2452"
abstract: ""
preview_image: "eta_2452-mini.jpg"
image: "ETA_2452.jpg"
movementlistkey: "eta"
caliberkey: "2452"
manufacturers: ["eta"]
manufacturers_weight: 2452
categories: ["movements","movements_e","movements_e_eta_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum