---
title: "Emes"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: ["Emes","Müller-Schlenker","Schwenningen","Deutschland","Uhrwerk","Uhrwerke","Kaliber"]
categories: ["movements","movements_e"]
movementlistkey: "emes"
description: "Uhrwerke von Emes / Müller-Schlenker aus Schwenningen, Deutschland"
abstract: "Uhrwerke von Emes / Müller-Schlenker aus Schwenningen, Deutschland"
---
(Müller-Schlenker, Schwenningen, Deutschland)
{{< movementlist "emes" >}}

{{< movementgallery "emes" >}}