---
title: "Emes 7"
date: 2009-04-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Emes 7","EMES","Müller-Schlenker","7","7 Jewels","Damenuhr","Deutschland","7 Steine"]
description: "Emes 7"
abstract: ""
preview_image: "emes_7-mini.jpg"
image: "Emes_7.jpg"
movementlistkey: "emes"
caliberkey: "7"
manufacturers: ["emes"]
manufacturers_weight: 70
categories: ["movements","movements_e","movements_e_emes"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/emes/Emes_Monorex_DAU.jpg"
    description: "Emes Monorex Damenuhr"
---
Lorem Ipsum