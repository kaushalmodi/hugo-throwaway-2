---
title: "Emes 15"
date: 2009-04-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Emes 15","Emes","15","AEMES","Müller-Schlenker","4 Jewels","Germany","pin lever"]
description: "Emes 15"
abstract: ""
preview_image: "emes_15-mini.jpg"
image: "Emes_15.jpg"
movementlistkey: "emes"
caliberkey: "15"
manufacturers: ["emes"]
manufacturers_weight: 150
categories: ["movements","movements_e","movements_e_emes_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/emes/Emes_DAU_2.jpg"
    description: "Emes ladies' watch"
---
Lorem Ipsum