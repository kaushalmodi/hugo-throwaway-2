---
title: "Emes 5a"
date: 2010-09-19T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Emes 5a","EMES","Müller-Schlenker","5a","7 Jewels","Damenuhr","Deutschland","7 Steine"]
description: "Emes 5a"
abstract: ""
preview_image: "emes_5a-mini.jpg"
image: "Emes_5a.jpg"
movementlistkey: "emes"
caliberkey: "5a"
manufacturers: ["emes"]
manufacturers_weight: 51
categories: ["movements","movements_e","movements_e_emes"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/emes/Emes_DAU.jpg"
    description: "Emes Damenuhr"
  - image: "e/emes/Emes_DAU_Emes_5a.jpg"
    description: "Emes Damenuhr"
---
Lorem Ipsum