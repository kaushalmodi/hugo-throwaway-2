---
title: "Emes"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "en"
keywords: ["Emes","Müller-Schlenker","Schwenningen","Germany","movement","caliber"]
categories: ["movements","movements_e"]
movementlistkey: "emes"
abstract: "Movements from the german manufacturer Müller-Schlenker, which was located in Schwenningen"
description: "Movements from the german manufacturer Müller-Schlenker, which was located in Schwenningen"
---
(Müller-Schlenker, Schwenningen, Germany)
{{< movementlist "emes" >}}

{{< movementgallery "emes" >}}