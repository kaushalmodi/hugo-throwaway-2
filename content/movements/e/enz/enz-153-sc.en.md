---
title: "Enz 153 SC"
date: 2017-04-29T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Enz 153 SC","Franz Schnurr","Pforzheim","17 Jewels","17 Rubis","manual wind","center second","Roskopf"]
description: "Enz 153 SC - a very simple pallet lever movement with a Roskopf construction"
abstract: "A very simple pallet lever movement with a Roskopf-like construction."
preview_image: "enz_153_sc-mini.jpg"
image: "Enz_153_SC.jpg"
movementlistkey: "enz"
caliberkey: "153 SC"
manufacturers: ["enz"]
manufacturers_weight: 153
categories: ["movements","movements_e","movements_e_enz_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum