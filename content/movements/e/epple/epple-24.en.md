---
title: "Epple 24"
date: 2009-06-26T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Epple 24","Epple","24","4 Jewels","Julius Epple","Schwenningen","caliber","cylinder escapement","germany"]
description: "Epple 24"
abstract: ""
preview_image: "epple_24-mini.jpg"
image: "Epple_24.jpg"
movementlistkey: "epple"
caliberkey: "24"
manufacturers: ["epple"]
manufacturers_weight: 24
categories: ["movements","movements_e","movements_e_epple_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Unknown_Epple_24.jpg"
    description: "anonymous ladies' watch"
---
Lorem Ipsum