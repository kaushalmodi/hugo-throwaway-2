---
title: "Epple"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["e"]
language: "de"
keywords: ["Epple","Julius Epple K.G.","Pforzheim","Deutschland","Uhrwerk","Uhrwerke","Kaliber"]
categories: ["movements","movements_e"]
movementlistkey: "epple"
abstract: "Uhrwerke der Julius Epple K.G. aus Pforzheim"
description: "Uhrwerke der Julius Epple K.G. aus Pforzheim"
---
(Julius Epple K.G., Pforzheim, Deutschland)
{{< movementlist "epple" >}}

{{< movementgallery "epple" >}}