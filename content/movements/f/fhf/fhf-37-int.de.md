---
title: "FHF 37 (INT)"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["FHF 37 (INT)","FHF","37 (INT)","Fabrique d`Horlogerie de Fontainemelon","Fontainemelon","Incabloc","17 Jewels","INT","INT 37","FHF-INT 37","Schweiz","17 Steine","Damenuhr"]
description: "FHF 37 (INT)"
abstract: ""
preview_image: "fhf_37-mini.jpg"
image: "FHF_37.jpg"
movementlistkey: "fhf"
caliberkey: "37 (INT)"
manufacturers: ["fhf"]
manufacturers_weight: 3700
categories: ["movements","movements_f","movements_f_fhf"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: FHF 37](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&FHF_37)
usagegallery: 
  - image: "b/bergana/Bergana_DAU.jpg"
    description: "Bergana Damenuhr"
---
Lorem Ipsum