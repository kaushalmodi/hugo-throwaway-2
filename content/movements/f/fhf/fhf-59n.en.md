---
title: "FHF 59N"
date: 2009-10-16T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FHF 59N","FHF","59N","Fabrique d`Horlogerie de Fontainemelon","Fontainemelon","17 Jewels","swiss","switzerland","baguette movement","form movement"]
description: "FHF 59N"
abstract: ""
preview_image: "fhf_59n-mini.jpg"
image: "FHF_59N.jpg"
movementlistkey: "fhf"
caliberkey: "59N"
manufacturers: ["fhf"]
manufacturers_weight: 5901
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  images:
    ZO: Zeitwaage_FHF59N_ZO.jpg
    ZU: Zeitwaage_FHF59N_ZU.jpg
    3O: Zeitwaage_FHF59N_3O.jpg
    6O: Zeitwaage_FHF59N_6O.jpg
    9O: Zeitwaage_FHF59N_9O.jpg
    12O: Zeitwaage_FHF59N_12O.jpg
usagegallery: 
  - image: "f/fleuron/Fleuron_DAU_Zifferblatt.jpg"
    description: "Fleuron gents watch  (dial only)"
links: |
  <p>* [Ranfft Uhren: FHF 59](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&FHF_59&)
  * [Alliance Horlogere: FHF 59N](http://hiro.alliancehorlogere.com/en/Under_the_Loupe/FHF_59N)</p><p>This movement was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum