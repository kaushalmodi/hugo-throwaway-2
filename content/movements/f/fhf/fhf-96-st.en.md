---
title: "FHF 96 (Standard)"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FHF 96","FHF 96 (ST)","FHF","96 (ST)","Fontainemelon","Fabrique d`Horlogerie de Fontainemelon","17 Jewels","Swiss","Switzerland"]
description: "FHF 96 (Standard)"
abstract: ""
preview_image: "fhf_st_96-mini.jpg"
image: "FHF_ST_96.jpg"
movementlistkey: "fhf"
caliberkey: "96 (ST)"
manufacturers: ["fhf"]
manufacturers_weight: 9600
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mirexal/Mirexal_HAU.jpg"
    description: "Mirexal gents watch"
links: |
  * [Ranfft Uhren: FHF 96](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Gruen_542SS)
---
Lorem Ipsum