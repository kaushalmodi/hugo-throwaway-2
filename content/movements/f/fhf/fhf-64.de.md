---
title: "FHF 64"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["FHF 64","FHF","64","Fabrique d`Horlogerie de Fontainemelon","Fontainemelon","Incabloc","17 Jewels","Schweiz","17 Steine","Damenuhr"]
description: "FHF 64"
abstract: ""
preview_image: "fhf_64-mini.jpg"
image: "FHF_64.jpg"
movementlistkey: "fhf"
caliberkey: "64"
manufacturers: ["fhf"]
manufacturers_weight: 6400
categories: ["movements","movements_f","movements_f_fhf"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Das Werk in der anonymen Damenuhr ist eine Spende der [Familie Ristow](/supporters/ristow/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchives!"
usagegallery: 
  - image: "s/seco/Seco_DAU.jpg"
    description: "Seco (SE+CO) Damenuhr"
  - image: "anonym/DAU_FHF_64.jpg"
    description: "anonyme Damenuhr"
links: |
  * [Ranfft Uhren: FHF 64](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&FHF_64)
---
Lorem Ipsum