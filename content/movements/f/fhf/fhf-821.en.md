---
title: "FHF 821"
date: 2013-03-02T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FHF","Fontainemelon","Schwitzerland","Swiss","FHF 821","FHF 82","17 Jewels","flat","decentral second","subsecond","Darwil 7066"]
description: "FHF 821 - the large variant of the FHF 82. A flat manual wind movement with decentral second indication."
abstract: "A flat manual wound movement with subseconds. It bases on the FHF 82 with the only difference of the larger diameter of the main plate."
preview_image: "fhf_821-mini.jpg"
image: "FHF_821.jpg"
movementlistkey: "fhf"
caliberkey: "821"
manufacturers: ["fhf"]
manufacturers_weight: 82100
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/darwil/Darwil_HAU_FHF_82.jpg"
    description: "Darwil mens' watch, model 7012"
---
Lorem Ipsum