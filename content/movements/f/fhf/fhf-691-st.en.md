---
title: "FHF 691 (Standard)"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FHF 691 (Standard)","FHF","691 (Standard)","17 Jewels","ladies` watch","form movement","swiss","switzerland"]
description: "FHF 691"
abstract: " "
preview_image: "fhf_st_691-mini.jpg"
image: "FHF_ST_691.jpg"
movementlistkey: "fhf"
caliberkey: "691 (ST)"
manufacturers: ["fhf"]
manufacturers_weight: 69100
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mirexal/Mirexal_DAU_3.jpg"
    description: "Mirexal ladies' watch"
---
Lorem Ipsum