---
title: "FHF 70"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["FHF 70","FHF","Fontainemelon","15 Jewels","15 Steine","Schweiz"]
description: "FHF 70"
abstract: ""
preview_image: "fhf_70-mini.jpg"
image: "FHF_70.jpg"
movementlistkey: "fhf"
caliberkey: "70"
manufacturers: ["fhf"]
manufacturers_weight: 7000
categories: ["movements","movements_f","movements_f_fhf"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/aero/Aero.jpg"
    description: "Aero Herrenuhr"
---
Lorem Ipsum