---
title: "FHF 70"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FHF 70","FHF","Fontainemelon","15 Jewels","swiss"]
description: "FHF 70"
abstract: ""
preview_image: "fhf_70-mini.jpg"
image: "FHF_70.jpg"
movementlistkey: "fhf"
caliberkey: "70"
manufacturers: ["fhf"]
manufacturers_weight: 7000
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/aero/Aero.jpg"
    description: "Aero gents watch"
---
Lorem Ipsum