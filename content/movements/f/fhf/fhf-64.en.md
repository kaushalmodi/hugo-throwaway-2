---
title: "FHF 64"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FHF 64","FHF","64","Fabrique d`Horlogerie de Fontainemelon","Fontainemelon","Incabloc","17 Jewels","swiss","switzerland","ladies` watch"]
description: "FHF 64"
abstract: ""
preview_image: "fhf_64-mini.jpg"
image: "FHF_64.jpg"
movementlistkey: "fhf"
caliberkey: "64"
manufacturers: ["fhf"]
manufacturers_weight: 6400
categories: ["movements","movements_f","movements_f_fhf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "One of the movements, the one in the anonymous ladies' watch, is a kind donation of the [Ristow family](/supporters/ristow/). Thank you very much for supporting the movement archive!"
usagegallery: 
  - image: "s/seco/Seco_DAU.jpg"
    description: "Seco (SE+CO) ladies' watch"
  - image: "anonym/DAU_FHF_64.jpg"
    description: "anonymous ladies' watch"
links: |
  * [Ranfft Uhren: FHF 64](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&FHF_64)
---
Lorem Ipsum