---
title: "FHF 150"
date: 2009-11-14T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["FHF 150","FHF","150","15 Jewels","Fontainemelon","Schweiz","15 Steine"]
description: "FHF 150"
abstract: ""
preview_image: "fhf_150-mini.jpg"
image: "FHF_150.jpg"
movementlistkey: "fhf"
caliberkey: "150"
manufacturers: ["fhf"]
manufacturers_weight: 15000
categories: ["movements","movements_f","movements_f_fhf"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: FHF 150](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&FHF_150_0)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
usagegallery: 
  - image: "anonym/DAU_FHF_150.jpg"
    description: "anonyme Damen- oder Herrenuhr"
---
Lorem Ipsum