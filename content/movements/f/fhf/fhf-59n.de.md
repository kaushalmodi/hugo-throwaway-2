---
title: "FHF 59N"
date: 2009-10-16T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Uhrenbastler","FHF 59N","FHF","59N","Fabrique d`Horlogerie de Fontainemelon","Fontainemelon","17 Jewels","Schweiz","17 Steine","Baguettewerk","Formwerk"]
description: "FHF 59N"
abstract: ""
preview_image: "fhf_59n-mini.jpg"
image: "FHF_59N.jpg"
movementlistkey: "fhf"
caliberkey: "59N"
manufacturers: ["fhf"]
manufacturers_weight: 5901
categories: ["movements","movements_f","movements_f_fhf"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: FHF 59](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&FHF_59&)
  * [Alliance Horlogere: FHF 59N](http://hiro.alliancehorlogere.com/en/Under_the_Loupe/FHF_59N)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende dieses Werks!</p>
timegrapher_old: 
  images:
    ZO: Zeitwaage_FHF59N_ZO.jpg
    ZU: Zeitwaage_FHF59N_ZU.jpg
    3O: Zeitwaage_FHF59N_3O.jpg
    6O: Zeitwaage_FHF59N_6O.jpg
    9O: Zeitwaage_FHF59N_9O.jpg
    12O: Zeitwaage_FHF59N_12O.jpg
usagegallery: 
  - image: "f/fleuron/Fleuron_DAU_Zifferblatt.jpg"
    description: "Fleuron Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum