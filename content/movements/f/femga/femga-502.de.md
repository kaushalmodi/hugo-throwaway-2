---
title: "Femga 502"
date: 2014-02-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Femga 502","Femga","502","Frankreich","France","Annemasse","Zentralsekunde","17 Rubis","17 Steine","17 Jewels"]
description: "Femga 502 - ein französisches Herrenuhrenwerk mit indirekter Zentralsekunde aus den frühen 50er Jahren"
abstract: "Ein französisches Herrenuhrenwerk mit indirekter Zentralsekunde aus den frühen 50er Jahren"
preview_image: "femga_502-mini.jpg"
image: "Femga_502.jpg"
movementlistkey: "femga"
caliberkey: "502"
manufacturers: ["femga"]
manufacturers_weight: 502
categories: ["movements","movements_f","movements_f_femga"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    In den liegenden Positionen läuft dieses Werk sensationell gut, die hängenden Lagen zeigen sowohl eine absolut gesehen recht große Abweichung, als auch größere Lageunterschiede.
  images:
    ZO: Zeitwaage_Femga_502_ZO.jpg
    ZU: Zeitwaage_Femga_502_ZU.jpg
    3O: Zeitwaage_Femga_502_3O.jpg
    6O: Zeitwaage_Femga_502_6O.jpg
    9O: Zeitwaage_Femga_502_9O.jpg
    12O: Zeitwaage_Femga_502_12O.jpg
  values:
    ZO: "-1"
    ZU: "+-0"
    3O: "-30"
    6O: "-35"
    9O: "-60"
    12O: "-42"
usagegallery: 
  - image: "a/anker/Anker_HAU_Femga_502.jpg"
    description: "Anker Herrenuhr"
labor: |
  Das vorliegende Werk befand sich in gutem Zustand, bekam trotzdem aber einen Vollservice mit Reinigung und Ölung.
---
Lorem Ipsum