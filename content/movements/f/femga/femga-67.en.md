---
title: "Femga 67"
date: 2009-12-22T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Femga 67","Femga","67","17 Jewels","Annemasse","form movement","france"]
description: "Femga 67"
abstract: ""
preview_image: "femga_67-mini.jpg"
image: "Femga_67.jpg"
movementlistkey: "femga"
caliberkey: "67"
manufacturers: ["femga"]
manufacturers_weight: 67
categories: ["movements","movements_f","movements_f_femga_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    This specimen was cleaned and olied.
  images:
    ZO: Zeitwaage_Femga_67_ZO.jpg
    ZU: Zeitwaage_Femga_67_ZU.jpg
    3O: Zeitwaage_Femga_67_3O.jpg
    6O: Zeitwaage_Femga_67_6O.jpg
    9O: Zeitwaage_Femga_67_9O.jpg
    12O: Zeitwaage_Femga_67_12O.jpg
usagegallery: 
  - image: "o/oridam/Oridam_DAU_Zifferblatt_Femga_67.jpg"
    description: "Oridam ladies' watch"
links: |
  <p>* [Ranfft Uhren: Femga 67](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Femga_67)</p><p>This movement was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum