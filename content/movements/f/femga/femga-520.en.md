---
title: "Femga 520"
date: 2014-01-26T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FEMGA","Femga","FEMGA 520","Annemasse","France","pallet lever","lever","center second"]
description: "FEMGA 520 - A french pallet lever movement with directly driven center second hand"
abstract: "A very typical 1950ies pallet lever movement with directly driven center second, this time made in France"
preview_image: "femga_520-mini.jpg"
image: "Femga_520.jpg"
movementlistkey: "femga"
caliberkey: "520"
manufacturers: ["femga"]
manufacturers_weight: 520
categories: ["movements","movements_f","movements_f_femga_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/engo/Engo_HAU_Femga_520.jpg"
    description: "Engo mens' watch"
---
Lorem Ipsum