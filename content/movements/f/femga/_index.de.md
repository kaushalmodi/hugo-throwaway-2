---
title: "Femga"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "de"
keywords: ["FHF","Fontainemelon","Font","Schweiz"]
categories: ["movements","movements_f"]
movementlistkey: "femga"
description: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
abstract: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
---
(Fabrique d'ebauche et de montres genevois d'Annemasse, Annemasse, Frankreich)
{{< movementlist "femga" >}}

{{< movementgallery "femga" >}}