---
title: "Felsa 415"
date: 2009-06-06T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa 415","Felsa","415","Incabloc","17 Jewels","Automatic","Bidynator","swiss","switzerland","selfwinding"]
description: "Felsa 415"
abstract: ""
preview_image: "felsa_415-mini.jpg"
image: "Felsa_415.jpg"
movementlistkey: "felsa"
caliberkey: "415"
manufacturers: ["felsa"]
manufacturers_weight: 4150
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/ogival/Ogival_Automatic_HAU.jpg"
    description: "Ogival Automatic gents watch"
links: |
  * [Ranfft Uhren: Felsa 415 Bidynator](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Felsa_410)
---
Lorem Ipsum