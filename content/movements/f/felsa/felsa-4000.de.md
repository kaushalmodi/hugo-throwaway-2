---
title: "Felsa 4000"
date: 2010-01-01T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["KIF-Duofix","Automatic-Modul","Wippenwechsler","Schraubenunruh","Schwanenhalsfeinregulierung","Girocap","doppeltes Zahnrad","Felsa 4000","Felsa","4000","35 Jewels","Lengau","Grenchen","Automatic","Schweiz","35 Steine","Automatik"]
description: "Felsa 4000 - Ein fantastisch verarbeitetes Automaticwerk mit 35 Steinen."
abstract: ""
preview_image: "felsa_4000-mini.jpg"
image: "Felsa_4000.jpg"
movementlistkey: "felsa"
caliberkey: "4000"
manufacturers: ["felsa"]
manufacturers_weight: 40000
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Felsa 4000](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Felsa_4000)
timegrapher_old: 
  description: |
    Das hier gezeigte Werk, das aus dem Fundus einer Uhrmacherwerkstatt kam und nicht gereinigt wurde, zeigt auf der Zeitwaage passable Werte, die allerdings weit davon entfernt sind, was man von einem derart aufwändig ausgestatteten Werk erwarten würde. Sehr auffällig sind die Wellenmuster in den hängenden Lagen, die auf ein leicht unrundes Ankerrad bzw. einen fehlerhaften Eingriff des Ankers in das Rad schliessen lassen.
  images:
    ZO: Zeitwaage_Felsa_4000_ZO.jpg
    ZU: Zeitwaage_Felsa_4000_ZU.jpg
    3O: Zeitwaage_Felsa_4000_3O.jpg
    6O: Zeitwaage_Felsa_4000_6O.jpg
    9O: Zeitwaage_Felsa_4000_9O.jpg
    12O: Zeitwaage_Felsa_4000_12O.jpg
---
Lorem Ipsum