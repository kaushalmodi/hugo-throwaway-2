---
title: "Felsa 465N"
date: 2017-10-01T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa 465N","Felsa","465N","17 Jewels","Swiss","Switzerland","17 Rubis"]
description: "Felsa 465N - an often used, well made swiss manual wind movement"
abstract: "A popular, well made swiss manual wind movement from the 1950ies "
preview_image: "felsa_465n-mini.jpg"
image: "Felsa_465N.jpg"
movementlistkey: "felsa"
caliberkey: "465N"
manufacturers: ["felsa"]
manufacturers_weight: 4655
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "h/hoffmann/Hoffmann_HAU.jpg"
    description: "Hoffmann gents watch"
  - image: "d/delbana/Delbana_HU_Felsa_465N_Zifferblatt.jpg"
    description: "Delbana gents' watch  (missing case)"
---
Lorem Ipsum