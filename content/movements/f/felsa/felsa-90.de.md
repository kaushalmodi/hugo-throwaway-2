---
title: "Felsa 90"
date: 2009-11-28T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 90","Formwerk","Felsa","Handaufzug","Zylinderhemmung","Cylinder","10 Steine","10 Jewels"]
description: "Felsa 90"
abstract: ""
preview_image: "felsa_90-mini.jpg"
image: "Felsa_90.jpg"
movementlistkey: "felsa"
caliberkey: "90"
manufacturers: ["felsa"]
manufacturers_weight: 900
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Servicing a Felsa 90](http://inforeloj.forums.ylos.com/viewtopic.php?p=4499&sid=29278d6c7de8def2af8455d52d6129a5) (Spanischer, reich bebilderter Artikel über Inspektionsarbeiten an einem Felsa 90)
usagegallery: 
  - image: "e/ecly/Ecly.jpg"
    description: "Ecly Damenuhr"
  - image: "o/ormo/Ormo_DAU.jpg"
    description: "Ormo Damenuhr"
  - image: "k/kasta/Kasta_DAU_Zifferblatt.jpg"
    description: "Kasta Damenuhr  (nur Zifferblatt)"
  - image: "g/gam/Gam_DAU_Felsa_90.jpg"
    description: "Gam Damenuhr"
---
Lorem Ipsum