---
title: "Felsa 22"
date: 2009-10-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 22","Felsa","22","17 Jewels","Schweiz","17 Steine"]
description: "Felsa 22"
abstract: ""
preview_image: "felsa_22-mini.jpg"
image: "Felsa_22.jpg"
movementlistkey: "felsa"
caliberkey: "22"
manufacturers: ["felsa"]
manufacturers_weight: 220
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/onif/Onif_DAU_Zifferblatt.jpg"
    description: "Onif Damenuhr  (nur Zifferblatt)"
links: |
  <p>* [Ranfft Uhren: Felsa 22](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&2&2uswk&Felsa_22)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende des Werks in der moderneren Ausführung mit Stoßsicherung!</p>
---
Lorem Ipsum