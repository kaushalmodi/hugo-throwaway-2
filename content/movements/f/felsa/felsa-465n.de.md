---
title: "Felsa 465N"
date: 2017-10-01T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 465N","Felsa","465N","Incabloc","17 Jewels","Schweiz","17 Steine"]
description: "Felsa 465N - Ein recht gängiges, gut gebautes schweizer Handaufzugswerk"
abstract: "Ein nicht selten verbautes, recht gut verarbeitetes schweizer Handaufzugswerk aus den späten 1950er Jahren."
preview_image: "felsa_465n-mini.jpg"
image: "Felsa_465N.jpg"
movementlistkey: "felsa"
caliberkey: "465N"
manufacturers: ["felsa"]
manufacturers_weight: 4655
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Vielen Dank an [Rolf-Jürgen](/supporters/rolf-juergen/) für das Delbana-Spenderwerk, dessen Zugfeder erfolgreich in das Werk der Hoffmann-Uhr verpflanzt werden konnte, so daß diese jetzt als funktionsfähiges Exemplar im Archiv vorhanden ist!"
usagegallery: 
  - image: "h/hoffmann/Hoffmann_HAU.jpg"
    description: "Hoffmann Herrenuhr"
  - image: "d/delbana/Delbana_HU_Felsa_465N_Zifferblatt.jpg"
    description: "Delbana Herrenuhr  (ohne Gehäuse)"
---
Lorem Ipsum