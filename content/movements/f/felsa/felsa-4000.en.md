---
title: "Felsa 4000"
date: 2010-01-01T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["KIF-Duofix","selfwinding module","rocking bar changer","screw balance","swan neck regulator","Girocap","Felsa 4000","Felsa","4000","35 Jewels","Lengau","Grenchen","Automatic","swiss","switzerland","selfwinding"]
description: "Felsa 4000 - A high quality selfwinding movement with 35 jewels."
abstract: ""
preview_image: "felsa_4000-mini.jpg"
image: "Felsa_4000.jpg"
movementlistkey: "felsa"
caliberkey: "4000"
manufacturers: ["felsa"]
manufacturers_weight: 40000
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    The specimen shown here, came from a watchmakers' pool and was not cleaned. The results on the timegrapher were not bad, but far away from what is expected from such an elaborate movements. On the hanging position, you see a noticeable wave pattern, which indicates, that the escapement wheel is not perfectly round or has got an errorenous depthing.
  images:
    ZO: Zeitwaage_Felsa_4000_ZO.jpg
    ZU: Zeitwaage_Felsa_4000_ZU.jpg
    3O: Zeitwaage_Felsa_4000_3O.jpg
    6O: Zeitwaage_Felsa_4000_6O.jpg
    9O: Zeitwaage_Felsa_4000_9O.jpg
    12O: Zeitwaage_Felsa_4000_12O.jpg
links: |
  * [Ranfft Uhren: Felsa 4000](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Felsa_4000)
---
Lorem Ipsum