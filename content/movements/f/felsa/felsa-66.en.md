---
title: "Felsa 66"
date: 2013-12-22T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa","Felsa 66","form movement","bimetal balance","6 Jewels","6 Rubis","15 Jewels","15 Rubis","Arcadia","Boston Watch Company"]
description: "Felsa 66 - an old ellipse shaped ladies' watch movement, in different versions."
abstract: "An old, ellipse shaped ladies' watch movement, which is shown here in the original 15j version and in the 6j US export version."
preview_image: "felsa_66-mini.jpg"
image: "Felsa_66.jpg"
movementlistkey: "felsa"
caliberkey: "66"
manufacturers: ["felsa"]
manufacturers_weight: 660
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/arcadia/Arcadia_DAU_Felsa_66.jpg"
    description: "Arcadia ladies' watch"
  - image: "p/para/Para_DAU_Felsa_66.jpg"
    description: "Para ladies' watch (dial only)"
timegrapher_old: 
  description: |
    The export movement showed poor performance on all positions. Probably the balance wheel is a little bit bent and the hairspring is also out of position, which you can see on the large beat error on all positions.
  images:
    ZO: Zeitwaage_Felsa_66_ZO.jpg
    ZU: Zeitwaage_Felsa_66_ZU.jpg
    3O: Zeitwaage_Felsa_66_3O.jpg
    6O: Zeitwaage_Felsa_66_6O.jpg
    9O: Zeitwaage_Felsa_66_9O.jpg
    12O: Zeitwaage_Felsa_66_12O.jpg
  values:
    ZO: "+150"
    ZU: "+30"
    3O: "-180"
    6O: "-270"
    9O: "-560"
    12O: "+270"
labor: |
  One specimen (the brass colored original) came with a broken balance wheel in the lab and therefore could not get back to life, the other one, the export version, was gummed and dirty and got a full treatment with cleaning and oiling.
---
Lorem Ipsum