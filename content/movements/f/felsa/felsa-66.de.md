---
title: "Felsa 66"
date: 2013-12-22T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa","Felsa 66","Formwerk","Bimetall-Unruh","6 Jewels","6 Steine","15 Jewels","15 Steine","Exportversion"]
description: "Felsa 66 - ein altes, ellipsenförmiges Damenuhrenwerk, hier in der Original- und in der USA-Exportversion zu sehen"
abstract: "Ein altes, ellipsenförmiges Damenuhrenwerk, das in einer 6-steinigen Exportversion und in der original 15-steinigen Version vorliegt."
preview_image: "felsa_66-mini.jpg"
image: "Felsa_66.jpg"
movementlistkey: "felsa"
caliberkey: "66"
manufacturers: ["felsa"]
manufacturers_weight: 660
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Eines der Werke (das messingfarbige Original) kam mit gebrochener Unruh ins Labor und konnte daher nicht zum Leben erweckt werden, das andere Werk, das Exportwerk, kam verschmutzt und verharzt ins Labor und bekam einen Komplettservice.
usagegallery: 
  - image: "a/arcadia/Arcadia_DAU_Felsa_66.jpg"
    description: "Arcadia Damenuhr"
  - image: "p/para/Para_DAU_Felsa_66.jpg"
    description: "Para Damenuhr (nur Zifferblatt)"
timegrapher_old: 
  description: |
    In allen Lagen zeigte das Exportwerk sehr bescheidene Ergebnisse, vermutlich ist die Unruhwelle beschädigt (hier genügt schon ein kleinestes Verbiegen um derartig schlechte Laufwerte hervorzurufen) und zudem die Unruhspirale nicht mehr optimal ausgerichtet, erkennbar am großen Abfallfehler, der sich durch alle Lagen durchzieht.
  images:
    ZO: Zeitwaage_Felsa_66_ZO.jpg
    ZU: Zeitwaage_Felsa_66_ZU.jpg
    3O: Zeitwaage_Felsa_66_3O.jpg
    6O: Zeitwaage_Felsa_66_6O.jpg
    9O: Zeitwaage_Felsa_66_9O.jpg
    12O: Zeitwaage_Felsa_66_12O.jpg
  values:
    ZO: "+150"
    ZU: "+30"
    3O: "-180"
    6O: "-270"
    9O: "-560"
    12O: "+270"
---
Lorem Ipsum