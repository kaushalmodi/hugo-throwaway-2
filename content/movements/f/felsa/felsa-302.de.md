---
title: "Felsa 302"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 302","Felsa","302","15 Jewels","15 Steine","Schweiz","Formwerk"]
description: "Felsa 302"
abstract: ""
preview_image: "felsa_302-mini.jpg"
image: "Felsa_302.jpg"
movementlistkey: "felsa"
caliberkey: "302"
manufacturers: ["felsa"]
manufacturers_weight: 3020
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dicker/Dicker_DAU.jpg"
    description: "Dicker Damenuhr"
---
Lorem Ipsum