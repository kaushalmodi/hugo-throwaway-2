---
title: "Felsa 22"
date: 2009-10-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa 22","Felsa","22","17 Jewels","swiss","switzerland"]
description: "Felsa 22"
abstract: ""
preview_image: "felsa_22-mini.jpg"
image: "Felsa_22.jpg"
movementlistkey: "felsa"
caliberkey: "22"
manufacturers: ["felsa"]
manufacturers_weight: 220
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/onif/Onif_DAU_Zifferblatt.jpg"
    description: "Onif ladies' watch  (dial only)"
links: |
  <p>* [Ranfft Uhren: Felsa 22](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&2&2uswk&Felsa_22)</p><p>The movement in the modern version with shock protection was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum