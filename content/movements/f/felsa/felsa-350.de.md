---
title: "Felsa 350"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Felsa 350","Felsa","350","Lengnau","Swiss","15 Jewels","Schweiz","15 Steine"]
description: "Felsa 350"
abstract: ""
preview_image: "felsa_350-mini.jpg"
image: "Felsa_350.jpg"
movementlistkey: "felsa"
caliberkey: "350"
manufacturers: ["felsa"]
manufacturers_weight: 3500
categories: ["movements","movements_f","movements_f_felsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/terval/Terval_DAU.jpg"
    description: "Terval Damenuhr"
---
Lorem Ipsum