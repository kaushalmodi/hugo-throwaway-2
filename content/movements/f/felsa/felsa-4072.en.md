---
title: "Felsa 4072"
date: 2009-10-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Felsa 4072","Felsa","4072","17 Jewels","swiss","switzerland"]
description: "Felsa 4072"
abstract: ""
preview_image: "felsa_4072-mini.jpg"
image: "Felsa_4072.jpg"
movementlistkey: "felsa"
caliberkey: "4072"
manufacturers: ["felsa"]
manufacturers_weight: 40720
categories: ["movements","movements_f","movements_f_felsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/chopard/Chopard_DAU_2_Zifferblatt.jpg"
    description: "Chopard ladies' watch  (dial only)"
links: |
  <p>* [Ranfft Uhren: Felsa 4072](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Felsa_4072)</p><p>The movement was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum