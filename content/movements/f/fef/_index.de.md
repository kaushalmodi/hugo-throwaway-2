---
title: "FEF"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "de"
keywords: ["FEF","Fleurier","Schweiz","Swiss","Fabrique d'Ebauches de Fleurier"]
categories: ["movements","movements_f"]
movementlistkey: "fef"
description: "Uhrwerke des schweizer Herstellers FEF aus Fleurier"
abstract: "Uhrwerke des schweizer Herstellers FEF aus Fleurier"
---
(Fabrique d'Ebauches de Fleurier, Fleurier, Schweiz)
{{< movementlist "fef" >}}

{{< movementgallery "fef" >}}