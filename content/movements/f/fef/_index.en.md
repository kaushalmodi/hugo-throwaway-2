---
title: "FEF"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "en"
keywords: ["FEF","Fleurier","Swiss","Switzerland","Fabrique d'Ebauches de Fleurier"]
categories: ["movements","movements_f"]
movementlistkey: "fef"
abstract: "Movements from the swiss manufacturer FEF from Fleurier"
description: "Movements from the swiss manufacturer FEF from Fleurier"
---
(Fabrique d'Ebauches de Fleurier, Fleurier, Switzerland)
{{< movementlist "fef" >}}

{{< movementgallery "fef" >}}