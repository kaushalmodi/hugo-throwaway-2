---
title: "FE"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "en"
keywords: ["FE","France Ebauches","Cupillard","Femga","Jeambrun","Girardet","Technic Ebauches","Harrenin","Lorsa"]
categories: ["movements","movements_f"]
movementlistkey: "fe"
description: "Movements of the France Ebauches S.A. (FE)"
abstract: "Movements of the France Ebauches S.A. (FE)"
---
(France Ebauches S.A. (FE), Valdahon Doubs, France)
{{< movementlist "fe" >}}

{{< movementgallery "fe" >}}