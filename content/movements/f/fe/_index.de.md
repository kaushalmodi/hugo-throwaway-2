---
title: "FE"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "de"
keywords: ["FE","France Ebauches","Cupillard","Femga","Jeambrun","Girardet","Technic Ebauches","Harrenin","Lorsa"]
categories: ["movements","movements_f"]
movementlistkey: "fe"
abstract: "Uhrwerke der France Ebauches S.A. (FE)"
description: "Uhrwerke der France Ebauches S.A. (FE)"
---
(France Ebauches S.A. (FE), Valdahon Doubs, Frankreich)
{{< movementlist "fe" >}}

{{< movementgallery "fe" >}}