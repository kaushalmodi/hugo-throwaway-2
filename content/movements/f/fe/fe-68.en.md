---
title: "FE 68"
date: 2014-12-25T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["FE 68","FE","68","France Ebauches","17 Jewels","17 Rubis","France","Dugena 4750"]
description: "FE 68 / Dugena 4750"
abstract: ""
preview_image: "fe_68-mini.jpg"
image: "FE_68.jpg"
movementlistkey: "fe"
caliberkey: "68"
manufacturers: ["fe"]
manufacturers_weight: 68000
categories: ["movements","movements_f","movements_f_fe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_DAU_3.jpg"
    description: "Dugena ladies' watch"
  - image: "s/silvana/Silvana_DAU_FE_68.jpg"
    description: "Silvana ladies' watch"
---
Lorem Ipsum