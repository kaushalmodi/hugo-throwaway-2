---
title: "Förster 80"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Förster 80","Förster","Foerster 80","Foerster","Foresta","BF","Bernhard Förster","25 Jewels","Deutschland","Schwarzwald"]
description: "Förster 80"
abstract: " "
preview_image: "foerster_80-mini.jpg"
image: "Foerster_80.jpg"
movementlistkey: "foerster"
caliberkey: "80"
manufacturers: ["foerster"]
manufacturers_weight: 80
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "f/foresta/Foresta_Automatic.jpg"
    description: "Foresta Automatic Herrenuhr"
---
Lorem Ipsum