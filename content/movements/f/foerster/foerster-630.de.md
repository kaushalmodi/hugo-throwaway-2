---
title: "Förster 630"
date: 2017-01-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Förster 630","Förster","630","Formwerk","Wippenaufzug","17 Jewels","Schraubenunruh"]
description: "Förster 630 - ein altes Damenuhren-Formwerk mit Schraubenunruh"
abstract: " Ein altes Damenuhren-Formwerk mit Schraubenunruh."
preview_image: "foerster_630-mini.jpg"
image: "Foerster_630.jpg"
movementlistkey: "foerster"
caliberkey: "630"
manufacturers: ["foerster"]
manufacturers_weight: 630
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk samt Uhr (und Armband) ist eine Spende von [G.Keilhammer](/supporters/g-keilhammer/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
usagegallery: 
  - image: "u/uva/UVA_DU_Foerster_630.jpg"
    description: "UVA Damenuhr"
---
Lorem Ipsum