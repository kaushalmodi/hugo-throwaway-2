---
title: "Förster"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "en"
keywords: ["Förster","BF","Pforzheim","Germany","movements"]
categories: ["movements","movements_f"]
movementlistkey: "foerster"
description: "Movements of the german manufacturer Förster (BF) of Pforzheim"
abstract: "Movements of the german manufacturer Förster (BF) of Pforzheim"
---
(Bernhard Förster (BF), Pforzheim, Deutschland)
{{< movementlist "foerster" >}}

{{< movementgallery "foerster" >}}