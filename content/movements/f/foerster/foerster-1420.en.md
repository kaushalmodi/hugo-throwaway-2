---
title: "Förster 1420"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Förster","Förster 1420","1420","420","Foerster","Forster","FB","BF","ladies` watch","movement","caliber","21","jewels","21 jewels","21 Jewels","automatic","selfwinding","germany","Germany"]
description: "Förster 1420"
abstract: " "
preview_image: "foerster_1420-mini.jpg"
image: "Foerster_1420.jpg"
movementlistkey: "foerster"
caliberkey: "1420"
manufacturers: ["foerster"]
manufacturers_weight: 1420
categories: ["movements","movements_f","movements_f_foerster_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/meister-anker/Meister_Anker_DAU_Automatic_3.jpg"
    description: "Meister Anker Automatic ladies' watch"
---
Lorem Ipsum