---
title: "Förster 2080"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "de"
keywords: []
description: ""
abstract: ""
preview_image: "foerster_2080-mini.jpg"
image: "Foerster_2080.jpg"
movementlistkey: "foerster"
caliberkey: "2080"
manufacturers: ["foerster"]
manufacturers_weight: 2080
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam in stark verbastelten Zustand, unvollständig und mit per Filzstift "gebläuten" Schrauben als Spende in meine Werkstatt. Obwohl es noch tickte, bekam es die "Standardbehandlung" in Form einer Komplettreinigung und Ölung verpaßt.
links: |
  * [Ranfft Uhren: Foerster 2080](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Foerster_2080)
donation: "Vielen Dank an [Rolf-Jürgen](/supporters/rolf-juergen/) für die Spende dieses Werks!"
usagegallery: 
  - image: "a/axa/AXA_HAU_Foerster_2080.jpg"
    description: "AXA Herrenuhr"
timegrapher_old: 
  description: |
    Auf der, auf doppelte Genauigkeit eingestellten Zeitwaage bot das vorliegende Exemplar leider einen traurigen Anblick, die gemessenen Werte waren sowohl extrem lageabhängig, als auch absolut gesehen jenseits von Gut und Böse. Auch wenn die früheren Bastelversuche das Werk noch am Leben (Ticken) ließen, so haben sie doch leider starke Schäden hinterlassen. Dieses Werk ist ein schönes Beispiel dafür, daß Ticken alleine noch nichts darüber aussagt, wie gut oder schlecht der Zustand eines Werks ist.
  images:
    ZO: Zeitwaage_Foerster_2080_ZO.jpg
    ZU: Zeitwaage_Foerster_2080_ZU.jpg
    3O: Zeitwaage_Foerster_2080_3O.jpg
    6O: Zeitwaage_Foerster_2080_6O.jpg
    9O: Zeitwaage_Foerster_2080_9O.jpg
    12O: Zeitwaage_Foerster_2080_12O.jpg
---
Lorem Ipsum