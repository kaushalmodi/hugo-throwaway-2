---
title: "Förster"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["f"]
language: "de"
keywords: ["Förster","BF","Pforzheim","Deutschland","Uhrwerk"]
categories: ["movements","movements_f"]
movementlistkey: "foerster"
description: "Uhrwerke des deutschen Herstellers Förster (BF) aus Pforzheim"
abstract: "Uhrwerke des deutschen Herstellers Förster (BF) aus Pforzheim"
---
(Bernhard Förster GmbH (BF), Pforzheim, Deutschland)
{{< movementlist "foerster" >}}

{{< movementgallery "foerster" >}}