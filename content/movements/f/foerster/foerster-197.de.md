---
title: "Förster 197"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Foerster 197","Foerster","197","Förster","BF","Foresta","Bernhard Föster","Pforzheim","25 Jewels","Automatic","Kaliber","Deutschland","25 Steine"]
description: "Förster 197"
abstract: " "
preview_image: "foerster_197-mini.jpg"
image: "Foerster_197.jpg"
movementlistkey: "foerster"
caliberkey: "197"
manufacturers: ["foerster"]
manufacturers_weight: 197
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/countess/Countess_automatic_HAU.jpg"
    description: "Countess automatic Herrenuhr"
links: |
  * [Ranfft Uhren: Förster 197](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Foerster_197)
  * [Watch-Wiki: Förster 197](http://watch-wiki.de/index.php?title=Förster_197)
---
Lorem Ipsum