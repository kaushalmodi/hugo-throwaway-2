---
title: "Förster 222"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Foerster 222","Foerster","222","Förster","BF","Foresta","Bernhard Förster","Pforzheim","17 Jewels","Schweiz","Damenuhr","17 Steine"]
description: "Förster 222"
abstract: " "
preview_image: "foerster_222-mini.jpg"
image: "Foerster_222.jpg"
movementlistkey: "foerster"
caliberkey: "222"
manufacturers: ["foerster"]
manufacturers_weight: 222
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Förster 222](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Foerster_222)
usagegallery: 
  - image: "a/anker/Anker_HAU_Automatic.jpg"
    description: "Anker Automatic Herrenuhr"
---
Lorem Ipsum