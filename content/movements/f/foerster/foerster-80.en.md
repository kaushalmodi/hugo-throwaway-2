---
title: "Förster 80"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Förster 80","Förster","Foerster 80","Foerster","Foresta","BF","Bernhard Förster","25 Jewels","Germany","Blackwood Forest"]
description: "Förster 80"
abstract: " "
preview_image: "foerster_80-mini.jpg"
image: "Foerster_80.jpg"
movementlistkey: "foerster"
caliberkey: "80"
manufacturers: ["foerster"]
manufacturers_weight: 80
categories: ["movements","movements_f","movements_f_foerster_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "f/foresta/Foresta_Automatic.jpg"
    description: "Foresta Automatic gents watch"
---
Lorem Ipsum