---
title: "Förster 226"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Förster","Förster 226","Foerster 226","BF 226","226","Forster","FB","BF","Automatic","25 Jewels","25 Rubis","Germany","black wood forest","selfwinding"]
description: "Förster 226"
abstract: " "
preview_image: "foerster_226-mini.jpg"
image: "Foerster_226.jpg"
movementlistkey: "foerster"
caliberkey: "226"
manufacturers: ["foerster"]
manufacturers_weight: 226
categories: ["movements","movements_f","movements_f_foerster_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/meister-anker/Meister_Anker_Automatic_2.jpg"
    description: "Meister-Anker Automatic gents watch"
---
Lorem Ipsum