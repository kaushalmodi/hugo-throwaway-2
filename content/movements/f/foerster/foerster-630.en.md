---
title: "Förster 630"
date: 2017-01-21T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Förster 630","Förster","630","form movement","rocking bar winding system","17 Jewels","screw balance"]
description: "Förster 630 - an old ladies' watch form movement with screw balance"
abstract: " An old form movement with screw balance for ladies' watches."
preview_image: "foerster_630-mini.jpg"
image: "Foerster_630.jpg"
movementlistkey: "foerster"
caliberkey: "630"
manufacturers: ["foerster"]
manufacturers_weight: 630
categories: ["movements","movements_f","movements_f_foerster_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "u/uva/UVA_DU_Foerster_630.jpg"
    description: "UVA ladies' watch"
donation: "This movement (and bracelet) is a donation of [G.Keilhammer](/supporters/g-keilhammer/). Thank you very much for supporting the movement archive!"
---
Lorem Ipsum