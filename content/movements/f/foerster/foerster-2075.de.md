---
title: "Förster 2075"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Förster 2075","Foerster 2075","BF","Förster","15","Steine","Jewels","Wippenaufzug"]
description: "Förster 2075"
abstract: " "
preview_image: "foerster_2075-mini.jpg"
image: "Foerster_2075.jpg"
movementlistkey: "foerster"
caliberkey: "2075"
manufacturers: ["foerster"]
manufacturers_weight: 2075
categories: ["movements","movements_f","movements_f_foerster"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Unmarked_Foerster_2075.jpg"
    description: "anonyme Herrenuhr"
---
Lorem Ipsum