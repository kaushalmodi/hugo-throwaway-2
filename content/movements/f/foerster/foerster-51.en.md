---
title: "Förster 51"
date: 2017-11-30T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Förster 51","Förster","BF","Elastor","21 Jewels","21 Rubis","center second"]
description: "Förster 51 - A manual wind movement with inhouse shock protection"
abstract: "A rather unspectacular manual wind movement, which is equipped with numerous cap jewels and an inhouse Elastor-Förster shock protection."
preview_image: "foerster_51-mini.jpg"
image: "Foerster_51.jpg"
movementlistkey: "foerster"
caliberkey: "51"
manufacturers: ["foerster"]
manufacturers_weight: 51
categories: ["movements","movements_f","movements_f_foerster_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum