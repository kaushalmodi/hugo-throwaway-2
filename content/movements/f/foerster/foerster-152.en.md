---
title: "Förster 152"
date: 2009-03-31T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Foerster 152","Foerster","152","21 Jewels","BF","Germany","21 Rubis","Förster"]
description: "Förster 152"
abstract: " "
preview_image: "foerster_152-mini.jpg"
image: "Foerster_152.jpg"
movementlistkey: "foerster"
caliberkey: "152"
manufacturers: ["foerster"]
manufacturers_weight: 152
categories: ["movements","movements_f","movements_f_foerster_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum