---
title: "Rego 129"
date: 2014-12-06T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Rego","Lapanouse","Rego 129","Formanker","Stiftanker","Palierfix","1 Jewel"]
description: "Rego (Lapanouse) 129 - eine Stiftankerwrk mit Stoßsicherung und Datumsschnellschaltung"
abstract: "Ein ein-steiniges schweizer Stiftankerwerk mit Formanker und einem ungewöhnlich einfachen, aber effizienten Datumskorrektur-Mechanismus."
preview_image: "rego_129-mini.jpg"
image: "Rego_129.jpg"
movementlistkey: "rego"
caliberkey: "129"
manufacturers: ["rego"]
manufacturers_weight: 129
categories: ["movements","movements_r","movements_r_rego"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/rouan/Rouan_de_Luxe_HAU_Rego_129.jpg"
    description: "Rouan de Luxe Herrenuhr"
---
Lorem Ipsum