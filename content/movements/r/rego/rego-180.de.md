---
title: "Rego 180"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Rego 180","Rego","180","Lapanouse","15 Jewels","Schweiz","Stiftanker"]
description: "Rego 180"
abstract: ""
preview_image: "rego_180-mini.jpg"
image: "Rego_180.jpg"
movementlistkey: "rego"
caliberkey: "180"
manufacturers: ["rego"]
manufacturers_weight: 180
categories: ["movements","movements_r","movements_r_rego"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/cimier/Cimier_DAU_2.jpg"
    description: "Cimier Damenuhr"
---
Lorem Ipsum