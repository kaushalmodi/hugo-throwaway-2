---
title: "Rego 129"
date: 2014-12-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Rego","Lapanouse","Rego 129","form lever","pin lever","Palierfix","1 Jewel"]
description: "Rego (Lapanouse) 129 - a pin lever movement with shock protection and a special date correcting mechanism"
abstract: "A one-jewel swiss pin lever movement with a shaped lever and an odd, simple, but efficient date correcting mechanism."
preview_image: "rego_129-mini.jpg"
image: "Rego_129.jpg"
movementlistkey: "rego"
caliberkey: "129"
manufacturers: ["rego"]
manufacturers_weight: 129
categories: ["movements","movements_r","movements_r_rego_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/rouan/Rouan_de_Luxe_HAU_Rego_129.jpg"
    description: "Rouan de Luxe mens' watch"
---
Lorem Ipsum