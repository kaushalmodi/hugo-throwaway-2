---
title: "Rego 311"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Rego 311","Lapanouse 311","Rego","Lapanouse","311","pin lever","switzerland","swiss","ladies` watch"]
description: "Rego 311"
abstract: ""
preview_image: "rego_311-mini.jpg"
image: "Rego_311.jpg"
movementlistkey: "rego"
caliberkey: "311"
manufacturers: ["rego"]
manufacturers_weight: 311
categories: ["movements","movements_r","movements_r_rego_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/cimier/Cimier_DAU.jpg"
    description: "Cimier ladies' watch"
---
Lorem Ipsum