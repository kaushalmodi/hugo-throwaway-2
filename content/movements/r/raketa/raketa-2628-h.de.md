---
title: "Raketa 2628.H"
date: 2011-01-22T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["vierschenklige Ringunruh","Raketa 2628.H","Raketa","2628.H","17 Jewels","Petrodworez","Rußland","Sankt Petersburg","17 Steine"]
description: "Raketa 2628.H"
abstract: ""
preview_image: "raketa_2628_h-mini.jpg"
image: "Raketa_2628_H.jpg"
movementlistkey: "raketa"
caliberkey: "2628.H"
manufacturers: ["raketa"]
manufacturers_weight: 2628
categories: ["movements","movements_r","movements_r_raketa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Raketa 2628.H](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Raketa_2628_H)
  * [Watch Wiki: Raketa 2628.H](http://watch-wiki.seener.de/index.php?title=Raketa_2628.H)
usagegallery: 
  - image: "r/raketa/Raketa_HAU_Zifferblatt_Raketa_2628_H.jpg"
    description: "Raketa Herrenuhr  (nur Zifferblatt)"
---
Lorem Ipsum