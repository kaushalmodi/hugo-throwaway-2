---
title: "Raketa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "de"
keywords: []
categories: ["movements","movements_r"]
movementlistkey: "raketa"
abstract: "Uhrwerke des russischen Herstellers Raketa aus Petrodvoretz"
description: "Uhrwerke des russischen Herstellers Raketa aus Petrodvoretz"
---
(Uhrenfabrik Petrodvoretz, Petrodvoretz, Russland)
{{< movementlist "raketa" >}}

{{< movementgallery "raketa" >}}