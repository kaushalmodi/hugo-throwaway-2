---
title: "Raketa 2609"
date: 2009-04-26T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Raketa 2609","Raketa","2609","16 Jewels","watch","watches","wristwatch","wristwatches","caliber","russia","USSR"]
description: "Raketa 2609"
abstract: ""
preview_image: "raketa_2609-mini.jpg"
image: "Raketa_2609.jpg"
movementlistkey: "raketa"
caliberkey: "2609"
manufacturers: ["raketa"]
manufacturers_weight: 2609
categories: ["movements","movements_r","movements_r_raketa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Raketa 2609](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Raketa_2609)
usagegallery: 
  - image: "r/raketa/Raketa_HAU.jpg"
    description: "Raketa gents watch"
---
Lorem Ipsum