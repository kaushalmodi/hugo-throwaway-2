---
title: "Raketa 2623.H"
date: 2011-01-21T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Raketa","Raketa 2623","Raketa 2623.H","2623","2623.H","24","watch","watches","wristwatch","wristwatches","movement","caliber","17","jewels","Russia","russia"]
description: "Raketa 2623.H"
abstract: ""
preview_image: "raketa_2623_h-mini.jpg"
image: "Raketa_2623_H.jpg"
movementlistkey: "raketa"
caliberkey: "2623.H"
manufacturers: ["raketa"]
manufacturers_weight: 2623
categories: ["movements","movements_r","movements_r_raketa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/raketa/Raketa_KGB_HAU_Raketa_2623_H.jpg"
    description: "Raketa &quot;KGB&quot; gents watch"
---
Lorem Ipsum