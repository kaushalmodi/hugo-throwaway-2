---
title: "Record 50"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Record 50","Record","50","17 Jewels","Swiss","Chatons","Uhrwerk","17 Steine","Handaufzug","Schweiz","Damenuhr"]
description: "Record 50"
abstract: ""
preview_image: "record_50-mini.jpg"
image: "Record_50.jpg"
movementlistkey: "record"
caliberkey: "50"
manufacturers: ["record"]
manufacturers_weight: 50
categories: ["movements","movements_r","movements_r_record"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/record/Record_DAU.jpg"
    description: "Record Damenuhr"
---
Lorem Ipsum