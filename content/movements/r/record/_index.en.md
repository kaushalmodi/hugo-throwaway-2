---
title: "Record"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "en"
keywords: ["Record","Tramelan","Switzerland","Swiss"]
categories: ["movements","movements_r"]
movementlistkey: "record"
description: "Movements of the swiss manufacturer Record from Tramelan"
abstract: "Movements of the swiss manufacturer Record from Tramelan"
---
(Record Watch Co SA, Tramelan, Switzerland)
{{< movementlist "record" >}}

{{< movementgallery "record" >}}