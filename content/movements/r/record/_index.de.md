---
title: "Record"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "de"
keywords: ["Record","Tramelan","Schweiz","Swiss"]
categories: ["movements","movements_r"]
movementlistkey: "record"
description: "Uhrwerke des schweizer Herstellers Record aus Tramelan"
abstract: "Uhrwerke des schweizer Herstellers Record aus Tramelan"
---
(Record Watch Co SA, Tramelan, Schweiz)
{{< movementlist "record" >}}

{{< movementgallery "record" >}}