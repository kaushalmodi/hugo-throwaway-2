---
title: "Ricoh R20?"
date: 2018-02-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ricoh","Automatic","Japan"]
description: "Ricoh, eventuell R20 - Ein japanisches Automaticwerk"
abstract: "Ein leider nicht markiertes, eher seltenes Ricoh-Automaticwerk ohne Korrekturdrücker"
preview_image: "ricoh_unknown-mini.jpg"
image: "Ricoh_unknown.jpg"
movementlistkey: "ricoh"
caliberkey: "unknown"
manufacturers: ["ricoh"]
manufacturers_weight: 0
categories: ["movements","movements_r","movements_r_ricoh"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "r/rontic/Rontic_Aquanaut_L_HAU_Ricoh_unknown.jpg"
    description: "Rontic Aquanaut L Herrenuhr"
---
Lorem Ipsum