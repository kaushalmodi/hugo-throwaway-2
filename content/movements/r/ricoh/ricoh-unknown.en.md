---
title: "Ricoh R20?"
date: 2018-02-15T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ricoh","Automatic","Japan"]
description: "Ricoh, probably R20 - A japanese selfwinding movement"
abstract: "A pity, that this rare Ricoh selfwinding movement without date correction pusher is not marked, so that its caliber number is unknown."
preview_image: "ricoh_unknown-mini.jpg"
image: "Ricoh_unknown.jpg"
movementlistkey: "ricoh"
caliberkey: "unknown"
manufacturers: ["ricoh"]
manufacturers_weight: 0
categories: ["movements","movements_r","movements_r_ricoh_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "r/rontic/Rontic_Aquanaut_L_HAU_Ricoh_unknown.jpg"
    description: "Rontic Aquanaut L gents watch"
---
Lorem Ipsum