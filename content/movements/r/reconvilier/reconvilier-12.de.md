---
title: "Reconvilier 12"
date: 2017-09-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Reconvilier 12","Reconvilier","2310","Schweiz","Swiss","Handaufzug"]
description: "Reconvilier 12 - Ein recht selten zu findendes schweizer Handaufzugswerk"
abstract: "Ein recht seltenes klassisches Ankerwerk aus der Schweiz"
preview_image: "reconvilier_12-mini.jpg"
image: "Reconvilier_12.jpg"
movementlistkey: "reconvilier"
caliberkey: "12"
manufacturers: ["reconvilier"]
manufacturers_weight: 12
categories: ["movements","movements_r","movements_r_reconvilier"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum