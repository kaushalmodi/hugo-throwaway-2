---
title: "Reconvilier 12"
date: 2017-09-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Reconvilier 12","Reconvilier","2310","Switzerland","Swiss","manual wind"]
description: "Reconvilier 12 - A pretty rare swiss manual wind movement"
abstract: "A pretty rare, classical swiss lever movement"
preview_image: "reconvilier_12-mini.jpg"
image: "Reconvilier_12.jpg"
movementlistkey: "reconvilier"
caliberkey: "12"
manufacturers: ["reconvilier"]
manufacturers_weight: 12
categories: ["movements","movements_r","movements_r_reconvilier_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum