---
title: "Ronda 1017"
date: 2017-12-16T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 1017","Ronda","1017","Lausen","day-date","center second","quickset","21 Jewels","21 Rubis","pin lever","blind jewels"]
description: "Ronda 1017 - A pin lever movement with 21 jewels"
abstract: "A better pin lever movement with lots of jewels, among them some are for decoration only."
preview_image: "ronda_1017-mini.jpg"
image: "Ronda_1017.jpg"
movementlistkey: "ronda"
caliberkey: "1017"
manufacturers: ["ronda"]
manufacturers_weight: 1017
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum