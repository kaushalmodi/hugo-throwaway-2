---
title: "Ronda 9018"
date: 2015-01-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 9018","Ronda","Lausen","pin lever","17 Jewels","17 Rubis","Automatic","selfwinding","date","quickset","center second"]
description: "Ronda 9018 - one of the smallest and best swiss pin lever movements"
abstract: "One of the smallest and best swiss pin lever selfwinding movements of astonishing high quality"
preview_image: "ronda_9018-mini.jpg"
image: "Ronda_9018.jpg"
movementlistkey: "ronda"
caliberkey: "9018"
manufacturers: ["ronda"]
manufacturers_weight: 9018
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>On the specimen shown here, the oscillating weight was dissolved into two parts, since the outer weight is just plugged into the rotor plate. According to a crack in the crystal, a hard hit must have been responsible.</p><p>In the workshop, the oscillating weight was fixed and a full service was made. The rates are now very good, except in one position (see later):</p><p>\{zeitwaage\}ZO=+46|0.1|287,ZU=+11|0.1|237,12O=+9|0.5|257,3O=+11|0.1|237,6O=+17|0.2|254,9O=-1|0.3|256\{/zeitwaage\}</p><p>For such a small pin lever movement, the rates are excellent! Only the rate "dial up" is a bit bad, but it's due to the hard shock described above, which also affected the balance, which now has got a noteable runout!</p>
usagegallery: 
  - image: "s/sindaco/Sindaco_DAU_Ronda_9018.jpg"
    description: "Sindaco ladies' watch"
---
Lorem Ipsum