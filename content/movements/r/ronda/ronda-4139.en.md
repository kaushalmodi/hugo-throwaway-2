---
title: "Ronda 4139"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda","RONDA-MATIC","ladies` watch","21","jewels","Switzerland"]
description: "Ronda 4139"
abstract: ""
preview_image: "ronda_4139-mini.jpg"
image: "Ronda_4139.jpg"
movementlistkey: "ronda"
caliberkey: "4139"
manufacturers: ["ronda"]
manufacturers_weight: 4139
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/pomar/Pomar_DAU_Automatic.jpg"
    description: "Pomar Automatic ladies' watch"
---
Lorem Ipsum