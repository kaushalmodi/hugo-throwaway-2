---
title: "Ronda 2538 Harley"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 2538 Harley","Harley 2538 Ronda","Ronda Harley","Ronda 2538","Microrotor","Swiss","22 Jewels","swiss"]
description: "Ronda 2538 Harley"
abstract: ""
preview_image: "ronda_2538-mini.jpg"
image: "Ronda_2538_Harley.jpg"
movementlistkey: "ronda"
caliberkey: "2538"
manufacturers: ["ronda"]
manufacturers_weight: 2538
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Kaliber AR2538 Harley (German: Historie um das Kaliber AR2538Harley mit automatischem Aufzug)](http://www.aristo-uhren.de/news/ar2538harley.htm)
usagegallery: 
  - image: "m/meister-anker/Meister_Anker_Automatic_3.jpg"
    description: "Meister-Anker Automatic Herrenuhr"
---
Lorem Ipsum