---
title: "Ronda 5111"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 5111","Ronda","5111","1 Jewel","Werk","Schweiz","Stiftanker","Damenuhr","1 Stein","Formwerk"]
description: "Ronda 5111"
abstract: ""
preview_image: "ronda_5111-mini.jpg"
image: "Ronda_5111.jpg"
movementlistkey: "ronda"
caliberkey: "5111"
manufacturers: ["ronda"]
manufacturers_weight: 5111
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "y/yves_renaud/Yves_Renaud_DAU.jpg"
    description: "Yves Renaud Damenuhr"
---
Lorem Ipsum