---
title: "Ronda 1377"
date: 2017-01-02T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 1377","Ronda","1377","Lausen","Quarz","Quartz","Ronda-Quartz","Datum","Zentralsekunde","Wochentag"]
description: "Ronda 1377 - Das erste Ronda-Quarzwerk, hier in seiner vierten und letzten Version"
abstract: "Das erste Quarzwerk von Ronda, hier in seiner dritten und letzten Überarbeitung."
preview_image: "ronda_1377-mini.jpg"
image: "Ronda_1377.jpg"
movementlistkey: "ronda"
caliberkey: "1377"
manufacturers: ["ronda"]
manufacturers_weight: 1377
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "m/meister-anker/Meister_Anker_HU_Ronda_1377.jpg"
    description: "Meister Anker Herrenuhr"
links: |
  * [CrazyWatches: Ronda 1377](http://www.crazywatches.pl/ronda-32768hz-1377-quartz-1973)
---
Lorem Ipsum