---
title: "Ronda 1223"
date: 2013-12-29T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 1223","Ronda","1223","1 Jewels","Lausen","Alarm","swiss","Switzerland"]
description: "Ronda 1223"
abstract: ""
preview_image: "ronda_1223-mini.jpg"
image: "Ronda_1223.jpg"
movementlistkey: "ronda"
caliberkey: "1223"
manufacturers: ["ronda"]
manufacturers_weight: 1223
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Watches: Ronda 1223](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Ronda_1223)
---
Lorem Ipsum