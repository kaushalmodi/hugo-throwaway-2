---
title: "Ronda"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["r"]
language: "de"
keywords: []
categories: ["movements","movements_r"]
movementlistkey: "ronda"
description: ""
abstract: "(Ronda AG, Lausen, Schweiz)"
---
(Ronda AG, Lausen, Schweiz)
{{< movementlist "ronda" >}}

{{< movementgallery "ronda" >}}