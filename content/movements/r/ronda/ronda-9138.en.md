---
title: "Ronda 9138"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 9138","Ronda","9138","Lausen","Harley","Swiss","25 Jewels","Automatic","switzerland","selfwinding"]
description: "Ronda 9138"
abstract: ""
preview_image: "ronda_9138-mini.jpg"
image: "Ronda_9138.jpg"
movementlistkey: "ronda"
caliberkey: "9138"
manufacturers: ["ronda"]
manufacturers_weight: 9138
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "g/grovana/Grovana_Automatic_DAU.jpg"
    description: "Grovana Automatic ladies' watch"
---
Lorem Ipsum