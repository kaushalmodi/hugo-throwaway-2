---
title: "Ronda 1215"
date: 2017-09-20T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 1215","Ronda","1215","Lausen","Swiss","date","center second","quickset","21 Jewels","21 Rubis","pin lever","blind jewels","cap jewels"]
description: "Ronda 1215 - A well made pin lever movement, here with 21 jewels"
abstract: "A better pin lever movement with lots of jewels, not all of them useful."
preview_image: "ronda_1215-mini.jpg"
image: "Ronda_1215.jpg"
movementlistkey: "ronda"
caliberkey: "1215"
manufacturers: ["ronda"]
manufacturers_weight: 1215
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum