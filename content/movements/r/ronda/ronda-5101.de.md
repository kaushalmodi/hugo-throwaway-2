---
title: "Ronda 5101"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 5101","Ronda","5101","17 Jewels","Schweiz","Formwerk","Stiftanker"]
description: "Ronda 5101"
abstract: ""
preview_image: "ronda_5101-mini.jpg"
image: "Ronda_5101.jpg"
movementlistkey: "ronda"
caliberkey: "5101"
manufacturers: ["ronda"]
manufacturers_weight: 5101
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Jeansuhr.jpg"
    description: "Unsignierte Jeans-Damenuhr"
---
Lorem Ipsum