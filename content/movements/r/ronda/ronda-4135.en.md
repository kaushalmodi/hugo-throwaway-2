---
title: "Ronda 4135"
date: 2013-02-27T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 4135","Ronda","4135","Lausen","RAX","17 Jewels","17 Rubin","date","quickset","handwinding","manual winding"]
description: "One of the last mechanical Ronda movement, with a diameter of 8 3/4 lignes and a date indication with an interesting quickset mechanism.  See this movement with video, photos, macro photos and a timegrapher output."
abstract: "One of the last mid-size manual wind Ronda movements. A true pallet lever movement with 17 jewels and a date indication with an interesting quickset mechanism."
preview_image: "ronda_4135-mini.jpg"
image: "Ronda_4135.jpg"
movementlistkey: "ronda"
caliberkey: "4135"
manufacturers: ["ronda"]
manufacturers_weight: 4135
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    On the timegrapher, this unadjusted movement performed well, especially, when you take into account, that it was stored for many years before.
  images:
    ZO: Zeitwaage_Ronda_4135_ZO.jpg
    ZU: Zeitwaage_Ronda_4135_ZU.jpg
    3O: Zeitwaage_Ronda_4135_3O.jpg
    6O: Zeitwaage_Ronda_4135_6O.jpg
    9O: Zeitwaage_Ronda_4135_9O.jpg
    12O: Zeitwaage_Ronda_4135_12O.jpg
  values:
    ZO: "+24"
    ZU: "+10"
    3O: "-15"
    6O: "-30"
    9O: "+-0"
    12O: "-15"
labor: |
  The specimen shown here is a NOS movement, which was probably stored for twenty years, before it got into the lab. It was not serviced at all.
links: |
  <p>* [Ermano Uhrwerke GmbH](http://www.ermano.de):  Official German Ronda Distributor
  * [Watch-Wiki: Ronda 4135](http://watch-wiki.org/index.php?title=Ronda_4135)</p><p>Thank you very much to the Ermano Uhrwerke Gmbh for the donation of this movement and further technical papers.</p>
---
Lorem Ipsum