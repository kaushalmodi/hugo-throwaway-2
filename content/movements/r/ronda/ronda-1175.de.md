---
title: "Ronda 1175"
date: 2016-11-18T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 1177","Ronda","1177","Lausen","Quarz","Quartz","Ronda-Quartz","Datum","Zentralsekunde"]
description: "Ronda 1177 - Ein sehr ausgereiftes Quarzwerk aus der zweiten Generation von Ronda-Quarzwerken."
abstract: "Ein sehr ausgereiftes Quarzwerk der zweiten Generation von Quarzwerken der Firma Ronda. Hergestellt ca. 1975."
preview_image: "ronda_1175-mini.jpg"
image: "Ronda_1175.jpg"
movementlistkey: "ronda"
caliberkey: "1175"
manufacturers: ["ronda"]
manufacturers_weight: 1175
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "r/ronda/Ronda_Quartz_HAU_Ronda_1177.jpg"
    description: "Ronda-Quartz Herrenuhr"
---
Lorem Ipsum