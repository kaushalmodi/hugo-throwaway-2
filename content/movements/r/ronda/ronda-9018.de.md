---
title: "Ronda 9018"
date: 2015-01-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 9018","Ronda","Lausen","Stiftanker","17 Jewels","17 Steine","Automatic","Automatik","Datum","Zentralsekunde"]
description: "Ronda 9018 - eines der kleinsten und besten Stiftanker-Automaticwerke aus der Schweiz"
abstract: "Eines der kleinsten und besten Stiftanker-Automaticwerke aus der Schweiz"
preview_image: "ronda_9018-mini.jpg"
image: "Ronda_9018.jpg"
movementlistkey: "ronda"
caliberkey: "9018"
manufacturers: ["ronda"]
manufacturers_weight: 9018
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  <p>Das vorliegende Exemplar hatte einen gelösten Rotor, bei dem sich der äußere, nur gesteckte Ring von der Rotorplatte gelöst hatte. Dem Sprung im Glas nach zu urteilen, müßte dies durch einen heftigen Schlag passiert sein.</p><p>In der Werkstatt wurde nicht nur der Rotor wieder zusammengesteckt, sondern auch ein Komplettservice durchgeführt, incl Regulierung auf der Zeitwaage. Die gemessenen Werte können sich hierbei mit einer Ausnahme (mehr dazu später) absolut sehen lassen:</p><p>\{zeitwaage\}ZO=+46|0.1|287,ZU=+11|0.1|237,12O=+9|0.5|257,3O=+11|0.1|237,6O=+17|0.2|254,9O=-1|0.3|256\{/zeitwaage\}</p><p>Für ein Stiftankerwerk sind dieses Werte exzellent, mehr geht wirklich fast nicht. Einzig der Außreißer liegend bei "Zifferblatt oben" trübt ein wenig das Bild, er ist die Folge des heftigen Schlags, der auch der Unruh einen merklichen und sichtbaren Höhenschlag brachte.</p>
usagegallery: 
  - image: "s/sindaco/Sindaco_DAU_Ronda_9018.jpg"
    description: "Sindaco Damenuhr"
---
Lorem Ipsum