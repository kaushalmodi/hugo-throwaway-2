---
title: "Ronda 5131"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ronda 5131","Ronda","5131","RL","17 Jewels","Antichoc","mechanisch","Handaufzug","Damenuhr","Steine"]
description: "Ronda 5131"
abstract: ""
preview_image: "ronda_5131-mini.jpg"
image: "Ronda_5131.jpg"
movementlistkey: "ronda"
caliberkey: "5131"
manufacturers: ["ronda"]
manufacturers_weight: 5131
categories: ["movements","movements_r","movements_r_ronda"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/jasmin/Jasmin.jpg"
    description: "Jasmin Damenuhr"
---
Lorem Ipsum