---
title: "Ronda 5131"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 5131","Ronda","5131","RL","17 Jewels","Antichoc","mechanical","balance","ladies watch"]
description: "Ronda 5131"
abstract: ""
preview_image: "ronda_5131-mini.jpg"
image: "Ronda_5131.jpg"
movementlistkey: "ronda"
caliberkey: "5131"
manufacturers: ["ronda"]
manufacturers_weight: 5131
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/jasmin/Jasmin.jpg"
    description: "Jasmin ladies' watch"
---
Lorem Ipsum