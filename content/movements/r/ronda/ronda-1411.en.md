---
title: "Ronda 1411"
date: 2013-12-29T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Ronda 1411","Ronda","1411","1413","Swiss","1 Jewel","manual wind","plastic"]
description: "Ronda 1411 - a simple, late pin lever movement with only one jewel and plastic bearings for the balance"
abstract: "A simple, late pin lever movement with only one jewel and plastic bearings for the balance staff"
preview_image: "ronda_1411-mini.jpg"
image: "Ronda_1411.jpg"
movementlistkey: "ronda"
caliberkey: "1411"
manufacturers: ["ronda"]
manufacturers_weight: 1411
categories: ["movements","movements_r","movements_r_ronda_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/parker/Parker_DAU_Ronda_1411.jpg"
    description: "Parker 2000 ladies' watch"
---
Lorem Ipsum