---
title: "Geneva Sport 41"
date: 2010-05-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["pallet lever movement","screw balance","Incabloc","Geneva Sport 40","Geneva Sport 41","Geneva Sport","41","GSW","17 Jewels","swiss","switzerland"]
description: "Geneva Sport 41 - A simple swiss pallet lever movement with 17 jewels. Detailed description with photos, video, data sheet and timegrapher protocol."
abstract: " "
preview_image: "geneva_sport_41-mini.jpg"
image: "Geneva_Sport_41.jpg"
movementlistkey: "geneva-sport"
caliberkey: "41"
manufacturers: ["geneva-sport"]
manufacturers_weight: 41
categories: ["movements","movements_g","movements_g_geneva_sport_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Geneva Sport 41](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Geneva_Sport_41)
labor: |
  The movement came in running condition into the lab, but due to the bad condition of the watch around, it got a full service.
usagegallery: 
  - image: "g/geneva_sport/Geneva_Sport_HAU_Geneva_Sport_41.jpg"
    description: "Geneva Sport gents watch"
timegrapher_old: 
  description: |
    On the timegrapher, the Geneva Sport 41 provided execllent results on the horizontal position; on the vertical positions, it was less perfect, especially on "9 up". The reason is probably, that the balance wheel has got a quite large horizontal slackness in its bearings, so it starts wobbeling, which slows it down, of course. Nevertheless, the results are quite OK for a rather simple movement, which is about 55 years old!
  images:
    ZO: Zeitwaage_Geneva_Sport_41_ZO.jpg
    ZU: Zeitwaage_Geneva_Sport_41_ZU.jpg
    3O: Zeitwaage_Geneva_Sport_41_3O.jpg
    6O: Zeitwaage_Geneva_Sport_41_6O.jpg
    9O: Zeitwaage_Geneva_Sport_41_9O.jpg
    12O: Zeitwaage_Geneva_Sport_41_12O.jpg
---
Lorem Ipsum