---
title: "Geneva Sport 41"
date: 2010-05-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ankerwerk","Schraubenunruh","Incabloc","Geneva Sport 40","Geneva Sport 41","Geneva Sport","41","GSW","17 Jewels","Schweiz","17 Steine"]
description: "Geneva Sport 41 - Ein einfaches schweizer Ankerwerk mit 17 Steinen. Detaillierte Beschreibung mit Bildern, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: " "
preview_image: "geneva_sport_41-mini.jpg"
image: "Geneva_Sport_41.jpg"
movementlistkey: "geneva-sport"
caliberkey: "41"
manufacturers: ["geneva-sport"]
manufacturers_weight: 41
categories: ["movements","movements_g","movements_g_geneva_sport"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "g/geneva_sport/Geneva_Sport_HAU_Geneva_Sport_41.jpg"
    description: "Geneva Sport Herrenuhr"
timegrapher_old: 
  description: |
    Auf der Zeitwaage machte das Geneva Sport 41 in den horizontalen Lagen eine exzellente Figur; in den hängenden Lagen konnte es nicht mehr vollständig überzeugen, da vor allem die Position "9 oben" stark aus der Rolle fiel. Möglicherweise liegt das an dem relativ großen vertikalen Spiel, das die Unruh in den Lagern hat, wodurch sie in den senkrechten Lagen ein wenig ins Taumeln gerät, was natürlich ein Abbremsen zur Folge hat. Dennoch sind die Werte für ein rund 55 Jahre altes Werk, das vergleichweise einfach konstruiert ist, ziemlich gut!
  images:
    ZO: Zeitwaage_Geneva_Sport_41_ZO.jpg
    ZU: Zeitwaage_Geneva_Sport_41_ZU.jpg
    3O: Zeitwaage_Geneva_Sport_41_3O.jpg
    6O: Zeitwaage_Geneva_Sport_41_6O.jpg
    9O: Zeitwaage_Geneva_Sport_41_9O.jpg
    12O: Zeitwaage_Geneva_Sport_41_12O.jpg
links: |
  * [Ranfft Uhren: Geneva Sport 41](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?02&ranfft&0&2uswk&Geneva_Sport_41)
labor: |
  Das vorliegende Werk kam in gutem lauffähigen Zustand ins Labor, wurde aber aufgrund des schlechten äußeren Zustandes der Uhr, in der es tickte, revisioniert.
---
Lorem Ipsum