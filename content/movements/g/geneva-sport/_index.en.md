---
title: "Geneva Sport"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["g"]
language: "en"
keywords: ["Geneva Sport","SYW","Switzerland","Swiss"]
categories: ["movements","movements_g"]
movementlistkey: "geneva-sport"
abstract: "Movements of the swiss manufacturer Geneva Sport (SYW)"
description: "Movements of the swiss manufacturer Geneva Sport (SYW)"
---
(Geneva Sport (SYW), Switzerland)
{{< movementlist "geneva-sport" >}}

{{< movementgallery "geneva-sport" >}}