---
title: "GUB 09-20"
date: 2017-11-15T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["GUB","Glashuette","Glashutte","Glashütte","Glashütte","Uhrenbetrieb","09-20","DDR","GDR","17","jewels","manual winding"]
description: "GUB 09-20 - An often used ladies' watch form movement from the former GDR"
abstract: " One of the most often produced movement from the VEB Glashütter Uhrenbetriebe. It's not very well known, that it was available in four different versions."
preview_image: "gub_09-20-mini.jpg"
image: "GUB_09-20.jpg"
movementlistkey: "gub"
caliberkey: "09-20"
manufacturers: ["gub"]
manufacturers_weight: 920
categories: ["movements","movements_g","movements_g_gub_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Some of the watches / movements were donated by [Günter G.](/supporters/guenter-g/) and [G.Keilhammer](/supporters/g-keilhammer/). Thank you very much for the support of the movement archive!"
usagegallery: 
  - image: "c/clipper/Clipper_DAU.jpg"
    description: "Clipper ladies' watch"
  - image: "a/am/AM_DAU.jpg"
    description: "AM ladies' watch"
  - image: "e/egosta/Egosta_DU_GUB_09-20.jpg"
    description: "Egosta ladies' watch"
  - image: "m/meister-anker/Meister-Anker_DU_2_GUB_09-20.jpg"
    description: "Meister-Anker ladies' watch"
  - image: "k/karex/Karex_DU_GUB_09-20.jpg"
    description: "Karex ladies' watch"
  - image: "m/meister-anker/Meister-Anker_DU_GUB_09-20.jpg"
    description: "Meister-Anker ladies' watch"
  - image: "a/anker/Anker_DU_GUB_09-20.jpg"
    description: "Anker ladies' watch"
  - image: "g/glashuette/Glashuette_DU_2_GUB_09-20.jpg"
    description: "Glashütte ladies' watch"
  - image: "k/karex/Karex_DU_2_GUB_09-20.jpg"
    description: "Karex ladies' watch"
---
Lorem Ipsum