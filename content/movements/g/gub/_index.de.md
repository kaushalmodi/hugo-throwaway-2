---
title: "GUB"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["g"]
language: "de"
keywords: ["GUB","DDR","Glashütte","Glashütter Uhrenbetriebe","VEB"]
categories: ["movements","movements_g"]
movementlistkey: "gub"
description: "Uhrwerke der ehemaligen Glashütter Uhrenbetriebe aus der DDR"
abstract: "Uhrwerke der ehemaligen Glashütter Uhrenbetriebe aus der DDR"
---
(VEB Glashütter Uhrenbetriebe GUB, Glashütte, Deutschland)
{{< movementlist "gub" >}}

{{< movementgallery "gub" >}}