---
title: "GUB"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["g"]
language: "en"
keywords: ["GUB","Glashütte","GDR"]
categories: ["movements","movements_g"]
movementlistkey: "gub"
abstract: "Movements of the Glashütter Uhrenbetriebe (GUB) of the former German Democratic Republic (GDR)"
description: "Movements of the Glashütter Uhrenbetriebe (GUB) of the former German Democratic Republic (GDR)"
---
(VEB Glashütter Uhrenbetriebe GUB, Glashütte, former GDR)
{{< movementlist "gub" >}}

{{< movementgallery "gub" >}}