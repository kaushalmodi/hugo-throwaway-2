---
title: "Guba B1050"
date: 2009-10-30T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Guba B1050","Guba","B1050","20 Jewels","Guba 1050","Gustav Bauer","Ellmendingen","Pforzheim","Deutschland","20 Steine"]
description: "Guba B1050"
abstract: " "
preview_image: "guba_b1050-mini.jpg"
image: "Guba_B1050.jpg"
movementlistkey: "guba"
caliberkey: "B1050"
manufacturers: ["guba"]
manufacturers_weight: 10501
categories: ["movements","movements_g","movements_g_guba"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Guba 1050 SC](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Guba_1050SC)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum