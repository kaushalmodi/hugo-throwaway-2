---
title: "Guba B1050"
date: 2009-10-30T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Guba B1050","Guba","B1050","20 Jewels","Guba 1050","Gustav Bauer","Ellmendingen","Pforzheim"]
description: "Guba B1050"
abstract: " "
preview_image: "guba_b1050-mini.jpg"
image: "Guba_B1050.jpg"
movementlistkey: "guba"
caliberkey: "B1050"
manufacturers: ["guba"]
manufacturers_weight: 10501
categories: ["movements","movements_g","movements_g_guba_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Guba 1050 SC](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Guba_1050SC)</p><p>This movement was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum