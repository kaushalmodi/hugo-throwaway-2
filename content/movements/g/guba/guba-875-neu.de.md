---
title: "Guba 875 neu"
date: 2018-01-25T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Guba 875 neu","Guba 875","Palettenanker","17 Jewels","17 Rubis","17 Steine","kleine Sekunde","dezentrale Sekunde"]
description: "Guba 875 neu - Ein relativ kleines und seltenes Handaufzugswerk"
abstract: "Das rotgoldene Damenuhrenwerk ist nicht oft zu finden"
preview_image: "guba_875_neu-mini.jpg"
image: "Guba_875_neu.jpg"
movementlistkey: "guba"
caliberkey: "875 neu"
manufacturers: ["guba"]
manufacturers_weight: 8751
categories: ["movements","movements_g","movements_g_guba"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/accurata/Accurata_DU_Guba_875_neu.jpg"
    description: "Accurata Damenuhr"
donation: "Dieses Werk wurde von [Günter G.](/supporters/guenter-g/) gespendet. Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
---
Lorem Ipsum