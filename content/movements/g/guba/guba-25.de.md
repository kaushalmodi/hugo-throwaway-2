---
title: "Guba 25"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Guba 25","Guba","25","17 Jewels","17 Steine","Handaufzug","Deutschland","Formwerk"]
description: "Guba 25"
abstract: " "
preview_image: "guba_25-mini.jpg"
image: "Guba_25.jpg"
movementlistkey: "guba"
caliberkey: "25"
manufacturers: ["guba"]
manufacturers_weight: 250
categories: ["movements","movements_g","movements_g_guba"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/civis/Civis_DAU.jpg"
    description: "Civis Damenuhr"
  - image: "s/schunk/Schunk_DAU.jpg"
    description: "Schunk Damenuhr"
  - image: "t/timestar/Timestar_DAU.jpg"
    description: "Timestar Damenuhr"
---
Lorem Ipsum