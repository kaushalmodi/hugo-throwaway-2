---
title: "Guba"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["g"]
language: "en"
keywords: ["Guba","Ellmendingen","Gustav Bauer","Bauer","Germany"]
categories: ["movements","movements_g"]
movementlistkey: "guba"
description: "Movements of the german manufacturer Guba (Gustav Bauer)   from Ellmendingen, Germany"
abstract: "Movements of the german manufacturer Guba (Gustav Bauer)   from Ellmendingen, Germany"
---
(Guba Uhrenrohwerke GmbH (Gustav Bauer), Ellmendingen, Germany)
{{< movementlist "guba" >}}

{{< movementgallery "guba" >}}