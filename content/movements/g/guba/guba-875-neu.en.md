---
title: "Guba 875 new"
date: 2018-01-26T11:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Guba 875 new","Guba 875 neu","Guba 875","pallet lever","17 Jewels","17 Rubis","decentral second","screw balance"]
description: "Guba 875 new - a pretty rare mid-size manual wind movement from germany"
abstract: "This red-gold toned movement is not easy to find"
preview_image: "guba_875_neu-mini.jpg"
image: "Guba_875_neu.jpg"
movementlistkey: "guba"
caliberkey: "875 neu"
manufacturers: ["guba"]
manufacturers_weight: 8751
categories: ["movements","movements_g","movements_g_guba_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/accurata/Accurata_DU_Guba_875_neu.jpg"
    description: "Accurata ladies' watch"
donation: "This movement was kindly donated by [Günter G.](/supporters/guenter-g/) Thank you very much for your support of the movement archive!"
---
Lorem Ipsum