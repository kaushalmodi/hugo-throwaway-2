---
title: "Poljot 2612.1"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Poljot","Poljot 2612.1","2612.1","18 Jewels","18 Rubis","Alarm","watch","wrist alarm","Russia","USSR","Sowjet Union"]
description: "Poljot 2612.1"
abstract: ""
preview_image: "poljot_2612_1-mini.jpg"
image: "Poljot_2612_1.jpg"
movementlistkey: "poljot"
caliberkey: "2612.1"
manufacturers: ["poljot"]
manufacturers_weight: 261210
categories: ["movements","movements_p","movements_p_poljot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/poljot/Poljot_Alarm.jpg"
    description: "Poljot Alarm gents watch"
---
Lorem Ipsum