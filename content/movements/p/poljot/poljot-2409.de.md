---
title: "Poljot 2409"
date: 2013-11-02T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Poljot 2409","Poljot","2409","Kirowskie","Breguetspirale","Schraubenunruh","17 Jewels","17 Steine"]
description: "Poljot 2409 - Das 17-steinige Handaufzugswerk aus der Kirowskie-Linie, ausgestattet mit einer Schraubenunruh, Breguetspirale und stoßgesichertem Ankerrad"
abstract: "Das 17-steinige Handaufzugswerk aus der Kirowskie-Familie, das mit einer tollen Ausstattung wie Breguet-Spirale, Schraubenunruh und stoßgesichertem Ankerrad beeindruckt."
preview_image: "poljot_2409-mini.jpg"
image: "Poljot_2409.jpg"
movementlistkey: "poljot"
caliberkey: "2409"
manufacturers: ["poljot"]
manufacturers_weight: 240900
categories: ["movements","movements_p","movements_p_poljot"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam mit leichtem Wasserschaden und schwacher Amplitude ins Labor und bekam einen Komplettservice mit Reinigen und Ölen.
links: |
  * [Watch-Wiki: Poljot 2409](http://watch-wiki.org/index.php?title=Poljot_2409)
  * [Soviet Movements (Part 1) (englisch)](http://www.horology.ru/en/articles/TZ.htm)
  * [Ranfft Uhren: Poljot 2409](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&ae&2uswk&Poljot_2409)
usagegallery: 
  - image: "p/poljot/Poljot_Kirowski_HAU_Poljot_2409.jpg"
    description: "Poljot Kirowski Herrenuhr (Exportmodell)"
timegrapher_old: 
  description: |
    Wie gut dieses Werk auch nach jahrelanger Benutzung ist, kann man auf der Zeitwaage sehen. Ohne, daß es nachreguliert werden mußte, lief es sekundengenau in den liegenden Positionen. In den drei der vier hängenden Lagen waren die Abweichungen zwar durchaus meßbar, aber beim täglichen Tragen dürften sie sich gut neutralisieren. Würde man die Unruh hier noch ein wenig auswuchten, könnten man glatt in den Chronometerbereich vordringen. 
  images:
    ZO: Zeitwaage_Poljot_2409_ZO.jpg
    ZU: Zeitwaage_Poljot_2409_ZU.jpg
    3O: Zeitwaage_Poljot_2409_3O.jpg
    6O: Zeitwaage_Poljot_2409_6O.jpg
    9O: Zeitwaage_Poljot_2409_9O.jpg
    12O: Zeitwaage_Poljot_2409_12O.jpg
  values:
    ZO: "-+0"
    ZU: "+-0"
    3O: "+23"
    6O: "+3"
    9O: "-20"
    12O: "-20"
---
Lorem Ipsum