---
title: "Poljot 2612.1"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Poljot","Poljot 2612.1","2612.1","18 Jewels","18 Rubis","Alarm","18 Steine","18","Steine","Wecker","Armbandwecker","Rußland","Russland","UdSSR","Sowjetunion"]
description: "Poljot 2612.1"
abstract: ""
preview_image: "poljot_2612_1-mini.jpg"
image: "Poljot_2612_1.jpg"
movementlistkey: "poljot"
caliberkey: "2612.1"
manufacturers: ["poljot"]
manufacturers_weight: 261210
categories: ["movements","movements_p","movements_p_poljot"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/poljot/Poljot_Alarm.jpg"
    description: "Poljot Alarm Herrenuhr"
---
Lorem Ipsum