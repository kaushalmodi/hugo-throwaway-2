---
title: "Poljot 2609H"
date: 2017-04-30T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Poljot 2609H","Poljot","NOVET","2609H","17 Jewels","17 Rubis","Handaufzug","Stoßsicherung"]
description: "Poljot 2609H - Ein gerne verbautes russisches Handaufzugswerk mit 17 Steinen und Zentralsekunde"
abstract: "Ein gerne verbautes, russisches, 17-steiniges Handaufzugswerk."
preview_image: "poljot_2609H-mini.jpg"
image: "Poljot_2609H.jpg"
movementlistkey: "poljot"
caliberkey: "2609H"
manufacturers: ["poljot"]
manufacturers_weight: 260901
categories: ["movements","movements_p","movements_p_poljot"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das vorliegende Werk kam als Lagerware ins Labor und direkt auf die Zeitwaage. Es wurde keine Revision durchgeführt, und auch keine Justierung des Gangs.
donation: "Dieses Werk wurde von [Hans](/supporters/hans/) gespendet. Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
timegrapher_old: 
  rates:
    ZO: "+20"
    ZU: "+15"
    3O: "+17"
    6O: "+20"
    9O: "+11"
    12O: "+24"
  amplitudes:
    ZO: "255"
    ZU: "240"
    3O: "203"
    6O: "232"
    9O: "224"
    12O: "223"
  beaterrors:
    ZO: "0.3"
    ZU: "0.2"
    3O: "0.2"
    6O: "0.3"
    9O: "0.3"
    12O: "0.2"
---
Lorem Ipsum