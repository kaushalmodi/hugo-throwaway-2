---
title: "Poljot 3133"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Poljot 3133","Poljot","3133","Valjoux 7734","Valjoux","7734","23 Jewels","Chronograph","mechanical","stop watch","Russia","USSR","Sowjet union"]
description: "Poljot 3133"
abstract: ""
preview_image: "poljot_3133-mini.jpg"
image: "Poljot_3133.jpg"
movementlistkey: "poljot"
caliberkey: "3133"
manufacturers: ["poljot"]
manufacturers_weight: 313300
categories: ["movements","movements_p","movements_p_poljot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/poljot/Poljot_Chronograph.jpg"
    description: "Poljot Chronograph"
---
Lorem Ipsum