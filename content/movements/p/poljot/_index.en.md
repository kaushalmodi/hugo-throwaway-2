---
title: "Poljot"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "en"
keywords: []
categories: ["movements","movements_p"]
movementlistkey: "poljot"
description: ""
abstract: "(1. Moscow Watch Factory, Moscow, USSR)"
---
(1. Moscow Watch Factory, Moscow, USSR)
{{< movementlist "poljot" >}}

{{< movementgallery "poljot" >}}