---
title: "Poljot 2616.1H"
date: 2018-03-19T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Poljot","Poljot 2616.1H","2616.1","30 Jewels","30 Rubis","Automatic","Russia","USSR","Sowjet Union"]
description: "Poljot 2616.1H: A russian selfwinding movement with 30 jewels and some very noteable details"
abstract: "This russian selfwinding movement has got only 30 jewels, but also surprises with a number of unusual details."
preview_image: "poljot_2616_1h-mini.jpg"
image: "Poljot_2616_1H.jpg"
movementlistkey: "poljot"
caliberkey: "2616.1H"
manufacturers: ["poljot"]
manufacturers_weight: 261611
categories: ["movements","movements_p","movements_p_poljot_en"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  rates:
    ZO: "+1"
    ZU: "+6"
    3O: "-15"
    6O: "+20"
    9O: "-20"
    12O: "-11"
  amplitudes:
    ZO: "311"
    ZU: "292"
    3O: "196"
    6O: "183"
    9O: "215"
    12O: "226"
  beaterrors:
    ZO: "0.1"
    ZU: "0.0"
    3O: "0.3"
    6O: "0.0"
    9O: "0.2"
    12O: "0.2"
usagegallery: 
  - image: "m/meister-anker/Meister-Anker_HU_Poljot_2616_H1.jpg"
    description: "Meister-Anker Automatic gents watch"
labor: |
  The specimen shown here was gummed and dirty and was lacking the fixation plate of the oscillating weight. The hour wheel was also missing some teeth. It was olied and cleaned, but due to the lack of spare parts, it could not be reparied, but at least it was possible to put it onto the timegrapher.
---
Lorem Ipsum