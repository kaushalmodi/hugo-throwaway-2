---
title: "Poljot 2409"
date: 2013-11-03T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Poljot 2409","Poljot","2409","Kirowskie","Breguet hairspring","screw balance","17 Jewels","shock protection"]
description: "Poljot 2409 - The handwound movement from the \"Kirowskie\" line, equipped with a screw balance, a breguet hairspring and a shock protected escapement wheel"
abstract: "The 17 jewels handwound movement of the \"Kirowski\" family, which impresses with its Glucycur screw balance, Breguet hairspring and shock protected escapement wheel."
preview_image: "poljot_2409-mini.jpg"
image: "Poljot_2409.jpg"
movementlistkey: "poljot"
caliberkey: "2409"
manufacturers: ["poljot"]
manufacturers_weight: 240900
categories: ["movements","movements_p","movements_p_poljot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The specimen shown here got into the lab with a minor water damage and weak amplitude of the balance and got a full service with cleaning and oiling.
links: |
  * [Watch-Wiki: Poljot 2409](http://watch-wiki.org/index.php?title=Poljot_2409) (german)
  * [Soviet Movements (Part 1)](http://www.horology.ru/en/articles/TZ.htm)
  * [Ranfft Uhren: Poljot 2409](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&ae&2uswk&Poljot_2409)
timegrapher_old: 
  description: |
    How well this movement performs, even after year long use, can be seen on the timegrapher. Even without any further adjustments, it ran perfect on the horizontal positions and also compensated its errors in the vertical positions. With a little better poising of the balance, it would be capable to run within chronometer specs.
  images:
    ZO: Zeitwaage_Poljot_2409_ZO.jpg
    ZU: Zeitwaage_Poljot_2409_ZU.jpg
    3O: Zeitwaage_Poljot_2409_3O.jpg
    6O: Zeitwaage_Poljot_2409_6O.jpg
    9O: Zeitwaage_Poljot_2409_9O.jpg
    12O: Zeitwaage_Poljot_2409_12O.jpg
  values:
    ZO: "-+0"
    ZU: "+-0"
    3O: "+23"
    6O: "+3"
    9O: "-20"
    12O: "-20"
usagegallery: 
  - image: "p/poljot/Poljot_Kirowski_HAU_Poljot_2409.jpg"
    description: "Poljot Kirowski mens' watch (export model)"
---
Lorem Ipsum