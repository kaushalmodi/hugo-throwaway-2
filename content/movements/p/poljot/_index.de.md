---
title: "Poljot"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "de"
keywords: []
categories: ["movements","movements_p"]
movementlistkey: "poljot"
description: ""
abstract: "(1. Moskauer Uhrenfabrik, Moskau, UdSSR)"
---
(1. Moskauer Uhrenfabrik, Moskau, UdSSR)
{{< movementlist "poljot" >}}

{{< movementgallery "poljot" >}}