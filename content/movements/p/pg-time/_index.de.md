---
title: "PG Time"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "de"
keywords: []
categories: ["movements","movements_p"]
movementlistkey: "pg-time"
description: "Uhrwerke des unbekannten Herstellers \"PG Time\", möglicherweise verwandt mit Sonceboz / Semag"
abstract: "Uhrwerke des unbekannten Herstellers \"PG Time\", möglicherweise verwandt mit Sonceboz / Semag"
---
(PG-Time, möglicherweise verwandt mit Sonceboz / Semag?)
{{< movementlist "pg-time" >}}

{{< movementgallery "pg-time" >}}