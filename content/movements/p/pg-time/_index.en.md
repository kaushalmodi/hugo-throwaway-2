---
title: "PG Time"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "en"
keywords: ["PG Time"]
categories: ["movements","movements_p"]
movementlistkey: "pg-time"
abstract: "Movement from the unknown manufacturer PG Time, probably related to Sonceboz / Semag"
description: "Movement from the unknown manufacturer PG Time, probably related to Sonceboz / Semag"
---
(PG Time, probably related to Sonceboz / Semag)
{{< movementlist "pg-time" >}}

{{< movementgallery "pg-time" >}}