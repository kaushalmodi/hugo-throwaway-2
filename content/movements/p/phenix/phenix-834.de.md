---
title: "Phenix 834"
date: 2018-01-15T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Phenix 834","Phenix","Formwerk","Handaufzug","Schraubenunruh"]
description: "Phenix 834 - Ein altes Formwerk, vermutlich aus den 1940er Jahren"
abstract: "Ein großes Formwerk, vermutlich aus den 1940er Jahren."
preview_image: "phenix_834-mini.jpg"
image: "Phenix_834.jpg"
movementlistkey: "phenix"
caliberkey: "834"
manufacturers: ["phenix"]
manufacturers_weight: 834
categories: ["movements","movements_p","movements_p_phenix"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum