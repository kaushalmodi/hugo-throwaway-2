---
title: "Phenix"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "de"
keywords: ["Peseux","Switzerland"]
categories: ["movements","movements_p"]
movementlistkey: "phenix"
description: "Uhrwerke des schweizer Herstellers Phenix aus Pruntut (Porrentruy) im Jura."
abstract: "Uhrwerke des schweizer Herstellers Phenix aus Pruntut (Porrentruy) im Jura."
---
(Phenix Watch Co., Pruntrut (Porrentruy), Schweiz)
{{< movementlist "phenix" >}}

{{< movementgallery "phenix" >}}