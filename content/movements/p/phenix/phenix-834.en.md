---
title: "Phenix 834"
date: 2018-01-17T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Phenix 834","Phenix","form movement","manual wind","screw balance"]
description: "Phenix 834 - An old and large swiss form movement, probably from the 1940ies"
abstract: "A large form movement from the 1940ies with unique shaped bridges"
preview_image: "phenix_834-mini.jpg"
image: "Phenix_834.jpg"
movementlistkey: "phenix"
caliberkey: "834"
manufacturers: ["phenix"]
manufacturers_weight: 834
categories: ["movements","movements_p","movements_p_phenix_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum