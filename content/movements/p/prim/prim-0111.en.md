---
title: "Prim 0111"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Prim 0111","Prim 52","Prim","0111","52","mechanical","manual wind","czech","breguet hairspring"]
description: "Prim 0111"
abstract: ""
preview_image: "prim_0111-mini.jpg"
image: "Prim_0111.jpg"
movementlistkey: "prim"
caliberkey: "0111"
manufacturers: ["prim"]
manufacturers_weight: 111
categories: ["movements","movements_p","movements_p_prim_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/prim/Prim_HAU.jpg"
    description: "Prim gents watch"
---
Lorem Ipsum