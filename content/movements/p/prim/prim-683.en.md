---
title: "Prim 683"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Prim 683","Prim","17","jewels","manual winding","Czech","Czechoslovakia"]
description: "Prim 683"
abstract: ""
preview_image: "prim_683-mini.jpg"
image: "Prim_683.jpg"
movementlistkey: "prim"
caliberkey: "683"
manufacturers: ["prim"]
manufacturers_weight: 683
categories: ["movements","movements_p","movements_p_prim_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum