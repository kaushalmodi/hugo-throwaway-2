---
title: "Prim"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "en"
keywords: []
categories: ["movements","movements_p"]
movementlistkey: "prim"
description: "Movements of the Czech manufacturer Prim from Nové Město nad Metuj"
abstract: "Movements of the Czech manufacturer Prim from Nové Město nad Metuj"
---
(Prim, Nové Město nad Metuj, Czech Republic)
{{< movementlist "prim" >}}

{{< movementgallery "prim" >}}