---
title: "PUW 660"
date: 2015-11-28T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["PUW 660","Pforzheimer Uhrenrohwerke","17 Jewels","17 Rubis","Germany","manual wind"]
description: "PUW 660 - The last large manual wind movement of the german Pforzheimer Uhren-Rohwerke"
abstract: " The last large manual wind movement from the german Pforzheimer Uhren-Rohwerke, here the base caliber."
preview_image: "puw_660-mini.jpg"
image: "PUW_660.jpg"
movementlistkey: "puw"
caliberkey: "660"
manufacturers: ["puw"]
manufacturers_weight: 6600
categories: ["movements","movements_p","movements_p_puw_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>The specimen shown here came without case and in hardly used condition into the lab and hence did not get a service.</p><p>On the timegrapher, it ran fast between +11 and +32 seconds per day with an amplitude between 250 and 290 degrees.</p>
usagegallery: 
  - image: "a/anker/Anker_85_TU_PUW_660.jpg"
    description: "Anker 85 tails' watch  (without case)"
---
Lorem Ipsum