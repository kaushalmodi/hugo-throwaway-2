---
title: "PUW 1261"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PUW 1261","PUW","1261","Pforzheimer Uhrenwerke","Pforzheimer Uhrenrohwerke","Pforzheim","Automatic","Automatik","Deutschland"]
description: "PUW 1261"
abstract: ""
preview_image: "puw_1261-mini.jpg"
image: "PUW_1261.jpg"
movementlistkey: "puw"
caliberkey: "1261"
manufacturers: ["puw"]
manufacturers_weight: 12610
categories: ["movements","movements_p","movements_p_puw"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Watches: PUW 1261](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&PUW_1261)
usagegallery: 
  - image: "f/finorva/Finorva_Automatic_HAU.jpg"
    description: "Finorva Automatic Herrenuhr"
---
Lorem Ipsum