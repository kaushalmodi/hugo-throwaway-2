---
title: "PUW 461"
date: 2011-03-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PUW 461","PUW","461","17 Jewels","Pforzheimer Uhrenrohwerke","Pforzheim","Wehner","Deutschland","17 Steine","Handaufzug"]
description: "PUW 461 - Ein hochwertiges Massen-Werk aus Pforzheim mit 17 Steinen. Detaillierte Beschreibung mit Bildern, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "puw_461-mini.jpg"
image: "PUW_461.jpg"
movementlistkey: "puw"
caliberkey: "461"
manufacturers: ["puw"]
manufacturers_weight: 4610
categories: ["movements","movements_p","movements_p_puw"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    Leider zeigen die Gangwerte extrem große Abweichungen, die entweder auf ein stark revisionsbedürftiges Werk hindeuten, oder aber es liegt eine Beschädigung des Gangreglers vor. Jedenfalls sind derartig große Abweichungen sehr untypisch für diese Werksserie.
  images:
    ZO: Zeitwaage_PUW_461_ZO.jpg
    ZU: Zeitwaage_PUW_461_ZU.jpg
    3O: Zeitwaage_PUW_461_3O.jpg
    6O: Zeitwaage_PUW_461_6O.jpg
    9O: Zeitwaage_PUW_461_9O.jpg
    12O: Zeitwaage_PUW_461_12O.jpg
usagegallery: 
  - image: "p/porta/Porta_HAU_PUW_461.jpg"
    description: "Porta Herrenuhr"
labor: |
  Das Werk kam in einer äußerlich unversehrten Uhr in gutem Zustand ins Labor, daher wurde auf eine Reinigung verzichtet.
links: |
  <p>* [Ranfft Uhren: PUW 461](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&PUW_461)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende dieses Werks samt Uhr!</p>
---
Lorem Ipsum