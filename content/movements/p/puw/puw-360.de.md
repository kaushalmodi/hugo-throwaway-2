---
title: "PUW 360"
date: 2009-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PUW 360","PUW","360","Pforzheimer Uhrenwerke","Pforzheimer Uhrenrohwerke","Pforzheim","Deutschland"]
description: "PUW 360"
abstract: ""
preview_image: "puw_360-mini.jpg"
image: "PUW_360.jpg"
movementlistkey: "puw"
caliberkey: "360"
manufacturers: ["puw"]
manufacturers_weight: 3600
categories: ["movements","movements_p","movements_p_puw"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: PUW 360](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&PUW_360)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks in der Ausführung mit der Incabloc-Stoßsicherung!"
usagegallery: 
  - image: "z/zentra/Zentra_HAU.jpg"
    description: "ZentRa Herrenuhr"
  - image: "z/zentra/Zentra_HAU_2.jpg"
    description: "ZentRa Herrenuhr"
  - image: "a/ankra/Ankra_71_HAU.jpg"
    description: "Ankra 71 Herrenuhr"
---
Lorem Ipsum