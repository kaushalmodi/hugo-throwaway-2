---
title: "PUW 73"
date: 2009-04-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["PUW 73","Dugena 975","Pforzheimer Uhrenrohwerke","17 Jewels","watch","watches","wristwatch","wristwatches","form movement","caliber","germany"]
description: "PUW 73 / Dugena 975"
abstract: ""
preview_image: "puw_73-mini.jpg"
image: "PUW_73.jpg"
movementlistkey: "puw"
caliberkey: "73"
manufacturers: ["puw"]
manufacturers_weight: 730
categories: ["movements","movements_p","movements_p_puw_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ankra/Ankra_DAU_2.jpg"
    description: "Ankra ladies' watch"
  - image: "d/dugena/Dugena_DAU_4.jpg"
    description: "Dugena ladies' watch"
---
Lorem Ipsum