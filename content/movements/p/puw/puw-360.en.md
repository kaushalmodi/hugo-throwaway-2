---
title: "PUW 360"
date: 2009-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["PUW 360","PUW","360","Pforzheimer Uhrenwerke","Pforzheimer Uhrenrohwerke","Pforzheim","Germany"]
description: "PUW 360"
abstract: ""
preview_image: "puw_360-mini.jpg"
image: "PUW_360.jpg"
movementlistkey: "puw"
caliberkey: "360"
manufacturers: ["puw"]
manufacturers_weight: 3600
categories: ["movements","movements_p","movements_p_puw_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "z/zentra/Zentra_HAU.jpg"
    description: "ZentRa gents watch"
  - image: "z/zentra/Zentra_HAU_2.jpg"
    description: "ZentRa gents watch"
  - image: "a/ankra/Ankra_71_HAU.jpg"
    description: "Ankra 71 gents watch"
links: |
  <p>* [Ranfft Uhren: PUW 360](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&PUW_360)</p><p>This movement in the version with the Incabloc shock protection was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum