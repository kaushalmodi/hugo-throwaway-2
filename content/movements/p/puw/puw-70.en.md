---
title: "PUW 70 / Dugena 702"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["PUW 70","PUW","70","Dugena 702","17 Jewels","Germany","form movement"]
description: "PUW 70 / Dugena 702"
abstract: ""
preview_image: "puw_70-mini.jpg"
image: "PUW_70.jpg"
movementlistkey: "puw"
caliberkey: "70"
manufacturers: ["puw"]
manufacturers_weight: 700
categories: ["movements","movements_p","movements_p_puw_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum