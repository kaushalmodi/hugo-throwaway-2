---
title: "PUW 461"
date: 2011-03-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["PUW 461","PUW","461","17 Jewels","Pforzheimer Uhrenrohwerke","Pforzheim","Wehner","germany","manual wind"]
description: "PUW 461 - A high quality mass movement from germany with 17 jewels. Detailed description with photos, video, data sheet and timegrapher protocol."
abstract: ""
preview_image: "puw_461-mini.jpg"
image: "PUW_461.jpg"
movementlistkey: "puw"
caliberkey: "461"
manufacturers: ["puw"]
manufacturers_weight: 4610
categories: ["movements","movements_p","movements_p_puw_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/porta/Porta_HAU_PUW_461.jpg"
    description: "Porta gents watch"
timegrapher_old: 
  description: |
    Unfortunately, the timing results are very poor. The reason is unknown, but either the balance wheel was damanged (bent pivots) or the whole movement really needs revision. In either case, those large deviations are completely atypical for that movement series.
  images:
    ZO: Zeitwaage_PUW_461_ZO.jpg
    ZU: Zeitwaage_PUW_461_ZU.jpg
    3O: Zeitwaage_PUW_461_3O.jpg
    6O: Zeitwaage_PUW_461_6O.jpg
    9O: Zeitwaage_PUW_461_9O.jpg
    12O: Zeitwaage_PUW_461_12O.jpg
labor: |
  The movement arrived in the lab in a watch, which appeared to be in good condition. Because of that, the movement was not serviced and cleaned.
links: |
  <p>* [Ranfft Uhren: PUW 461](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&PUW_461)</p><p>This movement and watch was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum