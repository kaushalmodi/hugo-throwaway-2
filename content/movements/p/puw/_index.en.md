---
title: "PUW"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "en"
keywords: []
categories: ["movements","movements_p"]
movementlistkey: "puw"
abstract: "(Pforzheimer Uhren-Rohwerke GmbH, Pforzheim, Germany)"
description: ""
---
(Pforzheimer Uhren-Rohwerke GmbH, Pforzheim, Germany)
{{< movementlist "puw" >}}

{{< movementgallery "puw" >}}