---
title: "PUW 60"
date: 2009-12-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PUW 60","PUW","60","Pforzheimer Uhrenwerke","Pforzheimer Uhrenrohwerke","Pforzheim","Deutschland"]
description: "PUW 60"
abstract: ""
preview_image: "puw_60-mini.jpg"
image: "PUW_60.jpg"
movementlistkey: "puw"
caliberkey: "60"
manufacturers: ["puw"]
manufacturers_weight: 600
categories: ["movements","movements_p","movements_p_puw"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/konnexa/Konnexa_9_HAU.jpg"
    description: "Konnexa 9 Herrenuhr"
  - image: "p/porta/Porta_Amphibia_HAU.jpg"
    description: "Porta Amphibia Herrenuhr"
  - image: "a/anker/Anker_HAU_PUW_60.jpg"
    description: "Anker Herrenuhr"
links: |
  * [Ranfft Uhren: PUW 60](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&PUW_60)
---
Lorem Ipsum