---
title: "PUW 74"
date: 2014-10-15T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["PUW 74","Pforzheimer Uhrenrohwerke","17 Jewels","form movement","wristwatch","handwound"]
description: "PUW 74"
abstract: "A common ladies' watch form movement of the 1960ies"
preview_image: "puw_74-mini.jpg"
image: "PUW_74.jpg"
movementlistkey: "puw"
caliberkey: "74"
manufacturers: ["puw"]
manufacturers_weight: 740
categories: ["movements","movements_p","movements_p_puw_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_DAU_PUW_74.jpg"
    description: "Dugena ladies' watch"
donation: "Many thanks go to [G.Müller](/supporters/g-mueller/) for the donation of that movement and watch!"
---
Lorem Ipsum