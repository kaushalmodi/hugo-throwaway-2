---
title: "PUW 700"
date: 2011-04-30T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["PUW 700","PUW","700","4 Jewels","Pforzheim","Deutschland","4 Steine","Zylinderwerk"]
description: "PUW 700 - Eines der ältesten PUW-Werke, ein Zylinderwerk mit 4 Steinen. Detaillierte Beschreibung mit Bildern und Nahaufnahmen."
abstract: " "
preview_image: "puw_700-mini.jpg"
image: "PUW_700.jpg"
movementlistkey: "puw"
caliberkey: "700"
manufacturers: ["puw"]
manufacturers_weight: 7000
categories: ["movements","movements_p","movements_p_puw"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_ZIfferblatt_PUW_700.jpg"
    description: "anonyme Damenuhr  (ohne Gehäuse)"
---
Lorem Ipsum