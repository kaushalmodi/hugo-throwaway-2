---
title: "PUW 700"
date: 2011-04-30T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["PUW 700","PUW","700","4 Jewels","Pforzheim","germany","cylinder movement"]
description: "PUW 700 - One of the oldest PUW movements, a cylinder movement with 4 jewels. Detailed description with photos and closeups."
abstract: " "
preview_image: "puw_700-mini.jpg"
image: "PUW_700.jpg"
movementlistkey: "puw"
caliberkey: "700"
manufacturers: ["puw"]
manufacturers_weight: 7000
categories: ["movements","movements_p","movements_p_puw_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_ZIfferblatt_PUW_700.jpg"
    description: "anonymous ladies' watch  (case missing)"
---
Lorem Ipsum