---
title: "Parrenin 1641D"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Parrenin 1641D","Parrenin","1641D","17 Jewels","Villers-le-lac","France","pin lever"]
description: "Parrenin 1641D"
abstract: ""
preview_image: "parrenin_1641d-mini.jpg"
image: "Parrenin_1641D.jpg"
movementlistkey: "parrenin"
caliberkey: "1641D"
manufacturers: ["parrenin"]
manufacturers_weight: 16414
categories: ["movements","movements_p","movements_p_parrenin_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/oxford/Oxford_HAU.jpg"
    description: "Oxford gents watch"
links: |
  <p>* [Ranfft Uhren: Parennin 1641D](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Parrenin_1641)</p><p>Big thanks to dutchwatch1, who identified this movement.</p>
---
Lorem Ipsum