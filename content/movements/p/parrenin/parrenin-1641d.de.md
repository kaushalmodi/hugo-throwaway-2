---
title: "Parrenin 1641D"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Parrenin 1641D","Parrenin","1641D","17 Jewels","Villers-le-lac","Frankreich","Stiftanker","Roskopf","17 Steine"]
description: "Parrenin 1641D"
abstract: ""
preview_image: "parrenin_1641d-mini.jpg"
image: "Parrenin_1641D.jpg"
movementlistkey: "parrenin"
caliberkey: "1641D"
manufacturers: ["parrenin"]
manufacturers_weight: 16414
categories: ["movements","movements_p","movements_p_parrenin"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/oxford/Oxford_HAU.jpg"
    description: "Oxford Herrenuhr"
links: |
  <p>* [Ranfft Uhren: Parennin 1641D](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Parrenin_1641)</p><p>Herzlichen Dank an dutchwatch1, der dieses Werk identifizeren konnte.</p>
---
Lorem Ipsum