---
title: "Parrenin 25"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Parrenin 25","17 Jewels","17","Steine","Jewels","Handaufzug","Frankreich"]
description: "Parrenin 25"
abstract: ""
preview_image: "parrenin_25-mini.jpg"
image: "Parrenin_25.jpg"
movementlistkey: "parrenin"
caliberkey: "25"
manufacturers: ["parrenin"]
manufacturers_weight: 250
categories: ["movements","movements_p","movements_p_parrenin"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ancora/Ancora_Anker_DAU_Werk.jpg"
    description: "Ancora Anker Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum