---
title: "Parrenin 25"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Parrenin 25","17 Jewels","17","jewels","manual winding","France","french"]
description: "Parrenin 25"
abstract: ""
preview_image: "parrenin_25-mini.jpg"
image: "Parrenin_25.jpg"
movementlistkey: "parrenin"
caliberkey: "25"
manufacturers: ["parrenin"]
manufacturers_weight: 250
categories: ["movements","movements_p","movements_p_parrenin_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ancora/Ancora_Anker_DAU_Werk.jpg"
    description: "Ancora Anker ladies' watch  (dial only)"
---
Lorem Ipsum