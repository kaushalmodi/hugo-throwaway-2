---
title: "Parrenin 66"
date: 2017-05-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Parrenin 66","Emes 18a","pin lever","decentral second","5 Jewels","5 Rubis","RUFA Anti-Shock"]
description: "Parrenin 66 / Emes 18a: A french pin lever movement"
abstract: "A french pin lever movement with balance bridge and top-down pin lever."
preview_image: "parrenin_66-mini.jpg"
image: "Parrenin_66.jpg"
movementlistkey: "parrenin"
caliberkey: "66"
manufacturers: ["parrenin"]
manufacturers_weight: 660
categories: ["movements","movements_p","movements_p_parrenin_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement in the version Emes 18a is a donation from [Christoph Reiner](/supporters/emesuhren-2/). Thank you very much for the kind support of the movement archive!"
usagegallery: 
  - image: "e/emes/Emes_DAU_Emes_18a.jpg"
    description: "Emes ladies' watch"
---
Lorem Ipsum