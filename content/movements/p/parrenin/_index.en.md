---
title: "Parrenin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "en"
keywords: ["Parrenin","France","VIllers-le-Lac"]
categories: ["movements","movements_p"]
movementlistkey: "parrenin"
abstract: "Movements from the french manufacturer Parrenin of Villers-le-Lac"
description: "Movements from the french manufacturer Parrenin of Villers-le-Lac"
---
(Parrenin, Villers-le-Lac, France)
{{< movementlist "parrenin" >}}

{{< movementgallery "parrenin" >}}