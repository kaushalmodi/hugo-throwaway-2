---
title: "Parrenin Z170"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Parrenin Z170","Parrenin 170","Parrenin","Z170","170","17","jewels","manual winding","France","french"]
description: "Parrenin Z170"
abstract: ""
preview_image: "parrenin_z170-mini.jpg"
image: "Parrenin_Z170.jpg"
movementlistkey: "parrenin"
caliberkey: "Z170"
manufacturers: ["parrenin"]
manufacturers_weight: 1700
categories: ["movements","movements_p","movements_p_parrenin_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "v/vehns/Vehns_HAU.jpg"
    description: "Vehns gents watch"
---
Lorem Ipsum