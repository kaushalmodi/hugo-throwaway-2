---
title: "Peseux 80"
date: 2011-10-06T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Peseux 80","Peseux","80","15 Jewels","Grenchen","Automatic","swiss","switzerland"]
description: "Peseux 80"
abstract: ""
preview_image: "peseux_80-mini.jpg"
image: "Peseux_80.jpg"
movementlistkey: "peseux"
caliberkey: "80"
manufacturers: ["peseux"]
manufacturers_weight: 80
categories: ["movements","movements_p","movements_p_peseux_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum