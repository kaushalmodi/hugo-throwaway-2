---
title: "Peseux"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "de"
keywords: ["Peseux","Switzerland"]
categories: ["movements","movements_p"]
movementlistkey: "peseux"
description: "Uhrwerke des schweizer Herstellers Peseux"
abstract: "Uhrwerke des schweizer Herstellers Peseux"
---
(Fabrique d'Ebauches de Peseux SA, Peseux, Schweiz)
{{< movementlist "peseux" >}}

{{< movementgallery "peseux" >}}