---
title: "Peseux 7000"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Peseux 7000","Peseux","7000","17 Jewels","flach","Schweiz","17 Steine"]
description: "Peseux 7000"
abstract: ""
preview_image: "peseux_7000-mini.jpg"
image: "Peseux_7000.jpg"
movementlistkey: "peseux"
caliberkey: "7000"
manufacturers: ["peseux"]
manufacturers_weight: 7000
categories: ["movements","movements_p","movements_p_peseux"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Peseux 7000](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Peseux_7000)
usagegallery: 
  - image: "p/precimax/Precimax_HAU.jpg"
    description: "Precimax Ultra-Flat Herrenuhr"
---
Lorem Ipsum