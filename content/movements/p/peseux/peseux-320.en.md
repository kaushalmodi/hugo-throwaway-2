---
title: "Peseux 320"
date: 2009-10-23T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Peseux 320","Peseux","320","17 Jewels","Switzerland","17 Rubis"]
description: "Peseux 320"
abstract: ""
preview_image: "peseux_320-mini.jpg"
image: "Peseux_320.jpg"
movementlistkey: "peseux"
caliberkey: "320"
manufacturers: ["peseux"]
manufacturers_weight: 320
categories: ["movements","movements_p","movements_p_peseux_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/eldor/Eldor_HAU.jpg"
    description: "Eldor gents watch"
  - image: "p/precimax/Precimax_HAU_3_Zifferblatt.jpg"
    description: "Precimax gents watch  (dial only)"
links: |
  <p>* [Ranfft Uhren: Peseux 320](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Peseux_320)</p><p>The movement in the modern version in copper color with shock protection was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum