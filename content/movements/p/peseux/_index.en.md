---
title: "Peseux"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["p"]
language: "en"
keywords: ["Peseux","Swiss"]
categories: ["movements","movements_p"]
movementlistkey: "peseux"
abstract: "Movement from the swiss manufacturer Peseux"
description: "Movement from the swiss manufacturer Peseux"
---
(Fabrique d'Ebauches de Peseux SA, Peseux, Switzerland)
{{< movementlist "peseux" >}}

{{< movementgallery "peseux" >}}