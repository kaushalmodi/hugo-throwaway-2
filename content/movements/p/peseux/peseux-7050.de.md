---
title: "Peseux 7050"
date: 2015-10-18T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Peseux","Neuenburg","Schweiz","Handaufzug","flach","Ankerwerk","Anker","17 Jewels","17 Rubnis"]
description: "Peseus 7050 - ein sehr flaches klassisches Handaufzugswerk aus der letzten Werksserie von Peseux"
abstract: "Ein sehr flaches Handaufzugswerk aus der letzten Werksfamilie von Peseux"
preview_image: "peseux_7050-mini.jpg"
image: "Peseux_7050.jpg"
movementlistkey: "peseux"
caliberkey: "7050"
manufacturers: ["peseux"]
manufacturers_weight: 7050
categories: ["movements","movements_p","movements_p_peseux"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  rates:
    ZO: "-7"
    ZU: "0"
    3O: "-26"
    6O: "-38"
    9O: "-22"
    12O: "-19"
  amplitudes:
    ZO: "249"
    ZU: "211"
    3O: "216"
    6O: "217"
    9O: "224"
    12O: "220"
  beaterrors:
    ZO: "0.4"
    ZU: "0.3"
    3O: "0.1"
    6O: "0.5"
    9O: "0.5"
    12O: "0.2"
usagegallery: 
  - image: "a/aero_watch/Aero_Watch_Peseux_7050.jpg"
    description: "Aero Watch Taschenuhr  (Fragment)"
labor: |
  Das vorliegende Exemplar, das in einer Taschenuhr aus den vermutlich späten 70er / frühen 80er Jahren verbaut war, kam in gutem Zustand in die Werkstatt, ein Service war nicht notwendig.
---
Lorem Ipsum