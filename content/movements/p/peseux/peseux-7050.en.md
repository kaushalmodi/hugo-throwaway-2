---
title: "Peseux 7050"
date: 2015-10-20T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Peseux","Neuenburg","Switzerland","manual wind","flat","lever movement","lever","17 Jewels","17 Rubis"]
description: "Peseus 7050 - a very flat and classicaly constructed manual wind movement from the last caliber family of Peseux"
abstract: "A very flat manual wind movement from the last movement family of Peseux"
preview_image: "peseux_7050-mini.jpg"
image: "Peseux_7050.jpg"
movementlistkey: "peseux"
caliberkey: "7050"
manufacturers: ["peseux"]
manufacturers_weight: 7050
categories: ["movements","movements_p","movements_p_peseux_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/aero_watch/Aero_Watch_Peseux_7050.jpg"
    description: "Aero Watch pocket watch  (fragment)"
timegrapher_old: 
  rates:
    ZO: "-7"
    ZU: "0"
    3O: "-26"
    6O: "-38"
    9O: "-22"
    12O: "-19"
  amplitudes:
    ZO: "249"
    ZU: "211"
    3O: "216"
    6O: "217"
    9O: "224"
    12O: "220"
  beaterrors:
    ZO: "0.4"
    ZU: "0.3"
    3O: "0.1"
    6O: "0.5"
    9O: "0.5"
    12O: "0.2"
labor: |
  The specimen shown here was in good condition and probably comes from a pocket watch of the late 1970ies or early 1980ies. No service was required.
---
Lorem Ipsum