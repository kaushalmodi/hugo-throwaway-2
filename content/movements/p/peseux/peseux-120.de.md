---
title: "Peseux 120"
date: 2017-01-01T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Peseux 120","Formwerk","Schraubenunruh","doppeltes Kronrad"]
description: "Peseux 120"
abstract: "Ein recht seltenes Damenuhren-Formwerk mit doppeltem Kronrad"
preview_image: "peseux_120-mini.jpg"
image: "Peseux_120.jpg"
movementlistkey: "peseux"
caliberkey: "120"
manufacturers: ["peseux"]
manufacturers_weight: 120
categories: ["movements","movements_p","movements_p_peseux"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk samt Uhr sind eine Spende von [Erwin](/supporters/erwin/). Ganz herzlichen Dank dafür!"
timegrapher_old: 
  description: |
    Das vorliegende Werk kam in stark gebrauchtem Zustand ins Archiv, bot aber noch akzeptable Gangwerte, so daß auf eine Revision verzichtet wurde:
  rates:
    ZO: "-45"
    ZU: "+65"
    3O: "-73"
    6O: "-88"
    9O: "-109"
    12O: "-136"
  amplitudes:
    ZO: ""
    ZU: "282"
    3O: ""
    6O: ""
    9O: ""
    12O: ""
  beaterrors:
    ZO: ""
    ZU: "1.6"
    3O: ""
    6O: ""
    9O: ""
    12O: ""
usagegallery: 
  - image: "b/blumus/Blumus_DAU_Peseux_120.jpg"
    description: "Blumus Damenuhr"
---
Lorem Ipsum