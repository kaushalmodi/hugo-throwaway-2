---
title: "Silvana"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: []
categories: ["movements","movements_s"]
movementlistkey: "silvana"
abstract: "(Fabrique d'Horlogerie Silvana SA, Tramelan, Switzerland)"
description: ""
---
(Fabrique d'Horlogerie Silvana SA, Tramelan, Switzerland)
{{< movementlist "silvana" >}}

{{< movementgallery "silvana" >}}