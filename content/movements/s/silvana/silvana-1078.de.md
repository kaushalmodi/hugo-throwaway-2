---
title: "Silvana 1078"
date: 2013-10-13T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Silvana 1078","Venus 75","15 Jewels","15 Steine","Anker","Silvana"]
description: "Silvana 1078 - Ein typisches frühes Handaufzugswerk mit Ankerhemmung, möglicherweise aus den 30er Jahren."
abstract: "Ein typisches frühes Handaufzugswerk für Armbanduhren mit Ankerhemmung, das vermutlich aus den 1930er Jahren stammt."
preview_image: "silvana_1078-mini.jpg"
image: "Silvana_1078.jpg"
movementlistkey: "silvana"
caliberkey: "1078"
manufacturers: ["silvana"]
manufacturers_weight: 1078
categories: ["movements","movements_s","movements_s_silvana"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    In der Position "Zifferblatt unten" lief das Werk mit tadellosen Gangwerten, abgesehen von einem größeren Abfallfehler, der mangels beweglichem Spiralklötzchenträger auch nicht mit vertretbarem Aufwand zu beseitigen gewesen wäre. Schon in der Position "Zifferblatt oben" sieht man einen starken Nachgang. Entweder ist die Unruhwelle auf der Werksseite beschädigt, oder aber es fehlt hier an einem Tröpfchen Öl.  Die hängenden Lagen zeigen starke Fehlgänge auf, entweder ist die Unruh nicht (mehr?) richtig ausgewuchtet und/oder wir haben es hier tatsächlich mit einer beschädigten Unruhwelle zu tun. Letzteres erscheint mir recht wahrscheinlich, da der äußere Zustand des Gehäuses des Testobjekts sehr bescheiden war und u.a. auch einen Glasbruch vorzuweisen hatte.
  images:
    ZO: Zeitwaage_Silvana_1078_ZO.jpg
    ZU: Zeitwaage_Silvana_1078_ZU.jpg
    3O: Zeitwaage_Silvana_1078_3O.jpg
    6O: Zeitwaage_Silvana_1078_6O.jpg
    9O: Zeitwaage_Silvana_1078_9O.jpg
    12O: Zeitwaage_Silvana_1078_12O.jpg
  values:
    ZO: "-120"
    ZU: "+-0"
    3O: "-350"
    6O: "-200"
    9O: "+270"
    12O: "-400"
usagegallery: 
  - image: "p/pharos/Pharos_HAU_Silvana_1078.jpg"
    description: "Pharos Herrenuhr"
labor: |
  Das vorliegende Werk kam in einer Uhr mit starken Gebrauchsspuren (und gekürzter Sekundenwelle) ins Labor und wurde, mit Ausnahme der Unruh, gereinigt und neu geölt. Bei so alten Werken, deren Unruhspiralen oft noch nicht aus amagnetischem Material bestehen, verzichte ich in der Regel auf eine Reinigung der Unruh, da diese erfahrungsgemäß durch die hierbei verwendeten Geräte leicht magnetisiert wird.
---
Lorem Ipsum