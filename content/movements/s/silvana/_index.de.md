---
title: "Silvana"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: []
categories: ["movements","movements_s"]
movementlistkey: "silvana"
description: ""
abstract: "(Fabrique d'Horlogerie Silvana SA, Tramelan, Schweiz)"
---
(Fabrique d'Horlogerie Silvana SA, Tramelan, Schweiz)
{{< movementlist "silvana" >}}

{{< movementgallery "silvana" >}}