---
title: "Smiths"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: ["Smiths","Anglo-Celtic Watch Company","Ystradgynlais","Wales","Großbritannien","GB"]
categories: ["movements","movements_s"]
movementlistkey: "smiths"
abstract: "Uhrwerke von Smiths, der Anglo-Celtic watch company aus Ystradgynlais in Wales, Großbritannien"
description: "Uhrwerke von Smiths, der Anglo-Celtic watch company aus Ystradgynlais in Wales, Großbritannien"
---
(Anglo-Celtic watch company, Ystradgynlais, Wales, Grossbritannien)
{{< movementlist "smiths" >}}

{{< movementgallery "smiths" >}}