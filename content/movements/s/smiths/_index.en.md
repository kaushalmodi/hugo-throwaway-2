---
title: "Smiths"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: ["Smiths","Anglo-Celtic Watch Company","Ystradgynlais","Wales","Great Britain","GB"]
categories: ["movements","movements_s"]
movementlistkey: "smiths"
abstract: "Movements by Smiths, the Anglo-Celtic watch company from Ystradgynlais in Wales, Great Britain"
description: "Movements by Smiths, the Anglo-Celtic watch company from Ystradgynlais in Wales, Great Britain"
---
(Anglo-Celtic watch company, Ystradgynlais, Wales, Great Britain)
{{< movementlist "smiths" >}}

{{< movementgallery "smiths" >}}