---
title: "Smiths RY Empire"
date: 2018-01-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Smiths","Wales","England","5 Jewels","5 Rubis","Stiftanker"]
description: "Smiths RY Empire - Ein sehr seltenes englisches Stiftankerwerk in Roskopf-Bauweise"
abstract: "Optisch könnte dieses Stiftankerwerk auch der Schweiz zugeordnet werden, aber es kommt tatsächlich von der britischen Insel."
preview_image: "smiths_ry_emipre-mini.jpg"
image: "Smiths_RY_Emipre.jpg"
movementlistkey: "smiths"
caliberkey: "RY Empire"
manufacturers: ["smiths"]
manufacturers_weight: 0
categories: ["movements","movements_s","movements_s_smiths"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das hier gezeigte Exemplar kam in vergleichsweise gutem Zustand ins Labor, deswegen konnte auch auf einen Service verzichtet werden.
timegrapher_old: 
  description: |
    Auf der Zeitwaage lieferte das Smiths RY Empire eine zu erwartende Vorstellung:<br/> Die Werte schwanken relativ stark, würden sich aber beim Tragen größtenteils gut ausgleichen. Mit größerer Amplitude (die nur durch einen Service und idealerweise durch Verbauen einer neuen Zugfeder zu erreichen wäre) und einem niedrigeren Abfallfehler könnte das Werk noch eine ganze Ecke stabiler laufen, nichts destrotrotz sind die Werte für ein Werk dieser Bauart und des doch beträchtlichen Alters nicht schlecht.  
  rates:
    ZO: "+15"
    ZU: "-18"
    3O: "+60"
    6O: "+45"
    9O: "-97"
    12O: "-150"
  amplitudes:
    ZO: "176"
    ZU: "216"
    3O: "213"
    6O: "212"
    9O: "249"
    12O: "208"
  beaterrors:
    ZO: "2.1"
    ZU: "2.6"
    3O: "2.1"
    6O: "2.3"
    9O: "2.2"
    12O: "2.9"
usagegallery: 
  - image: "s/smiths/Smiths_Empire_HU_Smiths_RY_Empire.jpg"
    description: "Smiths Empire Herrenuhr"
---
Lorem Ipsum