---
title: "Slava 2427"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Slava","Slava 2427","2427","27","jewels","27 jewels","27 Jewels","two mainsprings","double mainspring barrel","russia"]
description: "Slava 2427"
abstract: ""
preview_image: "slava_2427-mini.jpg"
image: "Slava_2427.jpg"
movementlistkey: "slava"
caliberkey: "2427"
manufacturers: ["slava"]
manufacturers_weight: 2427
categories: ["movements","movements_s","movements_s_slava_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum