---
title: "Slava 1601"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Slava 1601","Slava","1601","17 Jewels","Russland","UdSSR","Sowjetunion","Formwerk"]
description: "Slava 1601"
abstract: ""
preview_image: "slava_1601-mini.jpg"
image: "Slava_1601_Slava.jpg"
movementlistkey: "slava"
caliberkey: "1601"
manufacturers: ["slava"]
manufacturers_weight: 1601
categories: ["movements","movements_s","movements_s_slava"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/slava/Slava_DAU_1601.jpg"
    description: "Slava Damenuhr"
  - image: "o/osco/Osco_DAU_blaues_Armband.jpg"
    description: "Osco Damenuhr"
  - image: "o/osco/Osco_DAU_blaues_Zifferblatt.jpg"
    description: "Osco Damenuhr"
---
Lorem Ipsum