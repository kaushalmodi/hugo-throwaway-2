---
title: "Slava 1809"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Slava 1809","Slava","1809","16 Jewels","Russia","USSR","Sowjet Union","ladies` watch","form movement"]
description: "Slava 1809"
abstract: ""
preview_image: "slava_1809-mini.jpg"
image: "Slava_1809.jpg"
movementlistkey: "slava"
caliberkey: "1809"
manufacturers: ["slava"]
manufacturers_weight: 1809
categories: ["movements","movements_s","movements_s_slava_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/slava/Slava_DAU_1809.jpg"
    description: "Slava ladies' watch"
---
Lorem Ipsum