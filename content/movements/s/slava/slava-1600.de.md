---
title: "Slava 1600"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Slava 1600","Slava","1600","17 Jewels","Russland","UdSSR","Sowjetunion","Formwerk"]
description: "Slava 1600"
abstract: ""
preview_image: "slava_1600-mini.jpg"
image: "Slava_1600.jpg"
movementlistkey: "slava"
caliberkey: "1600"
manufacturers: ["slava"]
manufacturers_weight: 1600
categories: ["movements","movements_s","movements_s_slava"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/slava/Slava_DAU_1600.jpg"
    description: "Slava Damenuhr"
  - image: "r/ruhla/Ruhla_DAU_Slava.jpg"
    description: "Ruhla Damenuhr"
---
Lorem Ipsum