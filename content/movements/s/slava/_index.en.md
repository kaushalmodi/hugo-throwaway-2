---
title: "Slava"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: ["Slava","Moscow","Russia","Second Moscow Watch Factory"]
categories: ["movements","movements_s"]
movementlistkey: "slava"
description: "Movements from the Second Moscow Watch Factory in Russia"
abstract: "Movements from the Second Moscow Watch Factory in Russia"
---
(Second Moscow Watch Factory, Moscow, USSR)
{{< movementlist "slava" >}}

{{< movementgallery "slava" >}}