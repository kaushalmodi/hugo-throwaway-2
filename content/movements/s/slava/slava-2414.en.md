---
title: "Slava 2414"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Slava 2414","Slava","2414","17 Jewels","19 Jewels","watch","watches","double barrel","wristwatch","wristwatches","russia"]
description: "Slava 2414"
abstract: ""
preview_image: "slava_2414-mini.jpg"
image: "Slava_2414.jpg"
movementlistkey: "slava"
caliberkey: "2414"
manufacturers: ["slava"]
manufacturers_weight: 2414
categories: ["movements","movements_s","movements_s_slava_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "i/inturex/Inturex_HAU.jpg"
    description: "Inturex gents' watch"
  - image: "s/slava/Slava_HAU.jpg"
    description: "Slava gents' watch"
---
Lorem Ipsum