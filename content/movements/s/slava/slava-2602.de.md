---
title: "Slava 2602"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Slava 2602","Slava","2602","Pobeda","Pobjeda","17 Jewels","Russland","17 Steine"]
description: "Slava 2602"
abstract: ""
preview_image: "slava_2602-mini.jpg"
image: "Slava_2602.jpg"
movementlistkey: "slava"
caliberkey: "2602"
manufacturers: ["slava"]
manufacturers_weight: 2602
categories: ["movements","movements_s","movements_s_slava"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/start/Start_HAU.jpg"
    description: "CTAPT (Start) Herrenuhr"
---
Lorem Ipsum