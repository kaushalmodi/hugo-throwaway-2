---
title: "Sefea 50"
date: 2014-10-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Sefea 50","Judex 50","21 Jewels","manual wind","handwound","Incabloc"]
description: "Sefea 50 - a french manual wind movement, which was formerly available as Judex 50"
abstract: "A french manual wind movement with center second, which was formerly available as Judex 50."
preview_image: "sefea_50-mini.jpg"
image: "Sefea_50.jpg"
movementlistkey: "sefea"
caliberkey: "50"
manufacturers: ["sefea"]
manufacturers_weight: 50
categories: ["movements","movements_s","movements_s_sefea_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/countess/Countess_HAU_Zifferblatt_Sefea_50.jpg"
    description: "Countess mens' watch  (only dial survived)"
---
Lorem Ipsum