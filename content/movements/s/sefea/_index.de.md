---
title: "Sefea"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: []
categories: ["movements","movements_s"]
movementlistkey: "sefea"
description: ""
abstract: "(Sefea, Société européenne de fabrication d'ébauches d'Annemasse, Annemasse, Frankreich)"
---
(Sefea, Société européenne de fabrication d'ébauches d'Annemasse, Annemasse, Frankreich)
{{< movementlist "sefea" >}}

{{< movementgallery "sefea" >}}