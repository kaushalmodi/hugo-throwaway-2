---
title: "Sefea 50"
date: 2014-10-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Sefea 50","Judex 50","21 Jewels","Handaufzug","Incabloc"]
description: "Sefea 50 - ein französisches Handaufzugswerk, welches vor der Umfirmierung als Judex 50 erhältlich war"
abstract: "Ein französisches Handaufzugswerk mit Zentralsekunde, das vor der Umfirmierung als Judex 50 erhältlich war."
preview_image: "sefea_50-mini.jpg"
image: "Sefea_50.jpg"
movementlistkey: "sefea"
caliberkey: "50"
manufacturers: ["sefea"]
manufacturers_weight: 50
categories: ["movements","movements_s","movements_s_sefea"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/countess/Countess_HAU_Zifferblatt_Sefea_50.jpg"
    description: "Countess Herrenuhr (Goldgehäuse, nur noch Zifferblatt erhalten)"
labor: |
  Das vorliegende Exemplar stammt von einem "Schlachter", also einem Altgoldverwerter, der es mit brachialer Gewalt aus dem Gehäuse entfernt hat. Dabei wurde nicht nur die Kronenwelle gebrochen, sondern auch die Grundplatine so stark verbogen, daß kein vernünftiger Gang mehr möglich ist.
---
Lorem Ipsum