---
title: "Shanghai"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: ["Shanghai","China","Shanghai Watch Factory","watches","movement","movements"]
categories: ["movements","movements_s"]
movementlistkey: "shanghai"
abstract: "Movements of the chinese manufacturer Shanghai."
description: "Movements of the chinese manufacturer Shanghai."
---
(Shanghai Watch Factory, Shanghai, China)
{{< movementlist "shanghai" >}}

{{< movementgallery "shanghai" >}}