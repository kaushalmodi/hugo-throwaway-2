---
title: "Shanghai ZSE"
date: 2009-11-13T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Shanghai ZSE","Shanghai","ZSE","17 Jewels","17 Zuan","China","17 Steine","Handaufzug"]
description: "Shanghai ZSE"
abstract: ""
preview_image: "shanghai_zse-mini.jpg"
image: "Shanghai_ZSE.jpg"
movementlistkey: "shanghai"
caliberkey: "ZSE"
manufacturers: ["shanghai"]
manufacturers_weight: 0
categories: ["movements","movements_s","movements_s_shanghai"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Micmicmor Vintage Watch: Shanghai 7 Series](http://micmicmor.blogspot.com/2008/01/shanghia-7120-7series.html)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
---
Lorem Ipsum