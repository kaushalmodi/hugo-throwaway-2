---
title: "Shanghai ZSE Z-A"
date: 2009-11-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Shanghai ZSE Z-A","Shanghai","ZSE Z-A","17 Jewels","17 Zuan","China","17 Rubis","manual wind"]
description: "Shanghai ZSE Z-A"
abstract: ""
preview_image: "shanghai_zse_z-a-mini.jpg"
image: "Shanghai_ZSE_Z-A.jpg"
movementlistkey: "shanghai"
caliberkey: "ZSE Z-A"
manufacturers: ["shanghai"]
manufacturers_weight: 0
categories: ["movements","movements_s","movements_s_shanghai_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Micmicmor Vintage Watch: Shanghai 7 Series](http://micmicmor.blogspot.com/2008/01/shanghia-7120-7series.html)</p><p>This movement was kindly donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum