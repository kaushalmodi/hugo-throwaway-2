---
title: "Shanghai"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: ["Shanghai","China","Uhrwerk","Uhrwerke","Shanghai Watch Factory"]
categories: ["movements","movements_s"]
movementlistkey: "shanghai"
description: "Uhrwerke des chinesischen Herstellers Shanghai aus der gleichnamigen Stadt."
abstract: "Uhrwerke des chinesischen Herstellers Shanghai aus der gleichnamigen Stadt."
---
(Shanghai Watch Factory, Shanghai, China)
{{< movementlist "shanghai" >}}

{{< movementgallery "shanghai" >}}