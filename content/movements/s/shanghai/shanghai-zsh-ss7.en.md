---
title: "Shanghai ZSH SS7"
date: 2009-11-07T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Shanghai ZSH SS7","Shanghai","ZSH SS7","17 Jewels","17 Zuan","China","17 Rubis","manual wind"]
description: "Shanghai ZSH SS7"
abstract: ""
preview_image: "shanghai_zsh_ss7-mini.jpg"
image: "Shanghai_ZSH_SS7.jpg"
movementlistkey: "shanghai"
caliberkey: "ZSH SS7"
manufacturers: ["shanghai"]
manufacturers_weight: 7
categories: ["movements","movements_s","movements_s_shanghai_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Micmicmor Vintage Watch: Shanghai 7 Series](http://micmicmor.blogspot.com/2008/01/shanghia-7120-7series.html)</p><p>This movement was kindly donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum