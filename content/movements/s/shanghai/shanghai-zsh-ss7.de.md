---
title: "Shanghai ZSH SS7"
date: 2009-11-07T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Shanghai ZSH SS7","Shanghai","ZSH SS7","17 Jewels","17 Zuan","China","17 Steine","Handaufzug"]
description: "Shanghai ZSH SS7"
abstract: ""
preview_image: "shanghai_zsh_ss7-mini.jpg"
image: "Shanghai_ZSH_SS7.jpg"
movementlistkey: "shanghai"
caliberkey: "ZSH SS7"
manufacturers: ["shanghai"]
manufacturers_weight: 7
categories: ["movements","movements_s","movements_s_shanghai"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
links: |
  * [Micmicmor Vintage Watch: Shanghai 7 Series](http://micmicmor.blogspot.com/2008/01/shanghia-7120-7series.html)
---
Lorem Ipsum