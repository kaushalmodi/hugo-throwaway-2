---
title: "Sea-Gull ST2503"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Sea-Gull","ST2503","ST","China","2503","selfwinding","full calendar","Automatic","Seagull"]
description: "A chinese selfwinding movement with manual full calendar, open heart and tourbillon lookalike"
abstract: "A chinese selfwinding movement with manual full calendar, open heart and tourbillon lookalike"
preview_image: "sea-gull_st2503-mini.jpg"
image: "Sea-Gull_ST2503.jpg"
movementlistkey: "sea-gull"
caliberkey: "ST2503"
manufacturers: ["sea-gull"]
manufacturers_weight: 2503
categories: ["movements","movements_s","movements_s_sea_gull_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/Buchner_Bovalier_HAU_Sea-Gull_ST_2503.jpg"
    description: "Buchner Bovalier Advocat"
donation: "Many thanks to [Karl Bindlehner](/supporters/of-karl-bindlehner/) for donating this movement!"
links: |
  * [1] [Watch-Wiki: Sea-Gull ST25](http://watch-wiki.org/index.php?title=Sea-Gull_ST25)
  * [2] <http://www.tjseagull.com/ArticleShow.asp?ArticleID=126>
  * [3] <http://jearle.free.fr/pugwash/watches/drawings/TY-2503.jpg>
  * [[4]]() <http://forums.watchuseek.com/f72/sea-gull-st25-movement-durability-acuracy-compared-eta-g-gerlach-watch-646859.html#post4719018>[](http://forums.watchuseek.com/f72/sea-gull-st25-movement-durability-acuracy-compared-eta-g-gerlach-watch-646859.html#post4719018)
---
Lorem Ipsum