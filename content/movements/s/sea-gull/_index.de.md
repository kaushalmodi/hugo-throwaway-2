---
title: "Sea-Gull"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: []
categories: ["movements","movements_s"]
movementlistkey: "sea-gull"
description: ""
abstract: "(Tianjin Seagull, Tianjin, China)"
---
(Tianjin Seagull, Tianjin, China)
{{< movementlist "sea-gull" >}}

{{< movementgallery "sea-gull" >}}