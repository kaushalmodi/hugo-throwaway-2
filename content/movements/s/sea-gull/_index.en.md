---
title: "Sea-Gull"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: []
categories: ["movements","movements_s"]
movementlistkey: "sea-gull"
abstract: "(Tianjin Seagull, Tianjin, China)"
description: ""
---
(Tianjin Seagull, Tianjin, China)
{{< movementlist "sea-gull" >}}

{{< movementgallery "sea-gull" >}}