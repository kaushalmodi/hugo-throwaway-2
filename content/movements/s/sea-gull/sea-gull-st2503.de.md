---
title: "Sea-Gull ST2503"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Sea-Gull","ST2503","ST","China","2503","Vollkalender","full calendar","Automatic","Seagull"]
description: "Ein chinesisches Automaticwerk mit manuellem Vollkalender, offener Unruh und Pseudo-Tourbillon"
abstract: "Ein chinesisches Automaticwerk mit manuellem Vollkalender, offener Unruh und Pseudo-Tourbillon"
preview_image: "sea-gull_st2503-mini.jpg"
image: "Sea-Gull_ST2503.jpg"
movementlistkey: "sea-gull"
caliberkey: "ST2503"
manufacturers: ["sea-gull"]
manufacturers_weight: 2503
categories: ["movements","movements_s","movements_s_sea_gull"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [1] [Watch-Wiki: Sea-Gull ST25](http://watch-wiki.org/index.php?title=Sea-Gull_ST25)
  * [2] <http://www.tjseagull.com/ArticleShow.asp?ArticleID=126>
  * [3] <http://jearle.free.fr/pugwash/watches/drawings/TY-2503.jpg>
  * [4] <http://forums.watchuseek.com/f72/sea-gull-st25-movement-durability-acuracy-compared-eta-g-gerlach-watch-646859.html#post4719018>
usagegallery: 
  - image: "b/Buchner_Bovalier_HAU_Sea-Gull_ST_2503.jpg"
    description: "Buchner Bovalier Advocat"
donation: "Vielen Dank an [Karl Bindlehner](/supporters/karl-bindlehner/) für die Spende dieses Werks!"
---
Lorem Ipsum