---
title: "Sonceboz ES 55"
date: 2011-01-22T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Sonceboz ES 55","Sonceboz","ES 55","Sonceboz SA","Semag AV","Swiss","17 Jewels","Switzerland"]
description: "Sonceboz ES 55"
abstract: ""
preview_image: "sonceboz_es_55-mini.jpg"
image: "Sonceboz_ES_55.jpg"
movementlistkey: "sonceboz"
caliberkey: "ES 55"
manufacturers: ["sonceboz"]
manufacturers_weight: 55
categories: ["movements","movements_s","movements_s_sonceboz_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_3.jpg"
    description: "Kienzle gents watch"
---
Lorem Ipsum