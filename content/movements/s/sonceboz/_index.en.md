---
title: "Sonceboz"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: ["Sonceboz","Semag","Ebauches Sonceboz","ES","Switzerland","Swiss"]
categories: ["movements","movements_s"]
movementlistkey: "sonceboz"
abstract: "Movements from the swiss manufacturer Sonceboz / Semag / Ebauches Sonceboz (ES)"
description: "Movements from the swiss manufacturer Sonceboz / Semag / Ebauches Sonceboz (ES)"
---
(Sonceboz, Semag, Ebauches Sonceboz (ES), Sonceboz, Switzerland)
{{< movementlist "sonceboz" >}}

{{< movementgallery "sonceboz" >}}