---
title: "Seiko"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "en"
keywords: ["Seiko","Japan","Tokio","Tokyo"]
categories: ["movements","movements_s"]
movementlistkey: "seiko"
description: "Movements from the japanese manufacturer Seiko, located in Tokyo."
abstract: "Movements from the japanese manufacturer Seiko, located in Tokyo."
---
(Seiko Watch Corporation, Tokyo, Japan)
{{< movementlist "seiko" >}}

{{< movementgallery "seiko" >}}