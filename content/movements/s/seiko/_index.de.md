---
title: "Seiko"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["s"]
language: "de"
keywords: ["Seiko","Japan","Tokio"]
categories: ["movements","movements_s"]
movementlistkey: "seiko"
abstract: "Uhrwerke des japanischen Herstellers Seiko mit Sitz in Tokio."
description: "Uhrwerke des japanischen Herstellers Seiko mit Sitz in Tokio."
---
(Seiko Watch Corporation, Tokio, Japan)
{{< movementlist "seiko" >}}

{{< movementgallery "seiko" >}}