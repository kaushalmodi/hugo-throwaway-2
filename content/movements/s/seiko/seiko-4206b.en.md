---
title: "Seiko 4206B"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko 4206B","Seiko","4206B","17 Jewels","automatic","Japan","selfwinding"]
description: "Seiko 4206B"
abstract: ""
preview_image: "seiko_4206b-mini.jpg"
image: "Seiko_4206B.jpg"
movementlistkey: "seiko"
caliberkey: "4206B"
manufacturers: ["seiko"]
manufacturers_weight: 4206
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_DAU_Automatic.jpg"
    description: "Seiko Automatic ladies' watch model 0420"
---
Lorem Ipsum