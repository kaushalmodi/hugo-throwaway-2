---
title: "Seiko 7009A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko","Seikosha","Seiko 7009A","7009A","7009","Japan","Automatic","movement","caliber","17","jewels"]
description: "Seiko 7009A"
abstract: ""
preview_image: "seiko_7009a-mini.jpg"
image: "Seiko_7009A.jpg"
movementlistkey: "seiko"
caliberkey: "7009A"
manufacturers: ["seiko"]
manufacturers_weight: 7009
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum