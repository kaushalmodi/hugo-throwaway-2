---
title: "Seiko 6119A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko 6119A","Seiko 6119","6119A","Seiko","6119","6119A","Automatic","Diashock","Seiko 5","Japan","21 Jewels","21 jewels","selfwinding"]
description: "Seiko 6119A"
abstract: ""
preview_image: "seiko_6119a-mini.jpg"
image: "Seiko_6119A.jpg"
movementlistkey: "seiko"
caliberkey: "6119A"
manufacturers: ["seiko"]
manufacturers_weight: 6119
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum