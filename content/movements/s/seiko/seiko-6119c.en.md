---
title: "Seiko 6119C"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko 6119C","Seiko 6119","6119C","Seiko","Automatic","21 Jewels","mechanical","balance","selfwinding","japan","japanese"]
description: "Seiko 6119C"
abstract: ""
preview_image: "seiko_6119c-mini.jpg"
image: "Seiko_6119C.jpg"
movementlistkey: "seiko"
caliberkey: "6119C"
manufacturers: ["seiko"]
manufacturers_weight: 6119
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_HAU_Automatic_4.jpg"
    description: "Seiko 5 gents watch"
---
Lorem Ipsum