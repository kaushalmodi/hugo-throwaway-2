---
title: "Seiko 1104A"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Seiko 1104A","Seiko","1104A","Japan","17 Jewels","ladies' watch","Japan","center second","form movement"]
description: "Seiko 1104A"
abstract: ""
preview_image: "seiko_1104a-mini.jpg"
image: "Seiko_1104A.jpg"
movementlistkey: "seiko"
caliberkey: "1104A"
manufacturers: ["seiko"]
manufacturers_weight: 1104
categories: ["movements","movements_s","movements_s_seiko_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_DAU.jpg"
    description: "Seiko ladies' watch"
---
Lorem Ipsum