---
title: "Seiko 7S26A"
date: 2017-12-31T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Seiko 7S26A","Seiko 7S26","7S26 Automatic","Magic Lever","Wochentag","Datum","Zentralsekunde","Quickset","21 Jewels","21 Rubis","21 Steine","Stiftanker"]
description: "Seiko 7S26A - Ein sehr modern und rationell konstruiertes Automatikwerk aus Japan"
abstract: "Ein Werk, das fast jeder schonmal gesehen hat - sehr zweckmäßig konstruiert, auf Kosten des Erscheinungsbildes."
preview_image: "seiko_7s26a-mini.jpg"
image: "Seiko_7S26A.jpg"
movementlistkey: "seiko"
caliberkey: "7S26A"
manufacturers: ["seiko"]
manufacturers_weight: 726
categories: ["movements","movements_s","movements_s_seiko"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "s/seiko/Seiko_HAU_7S26-0364_Seiko_7S26_Zifferblatt.jpg"
    description: "Seiko Herrenuhr Modell 0364  (ohne Gehäuse)"
timegrapher_old: 
  description: |
    Bevor das Werk auf die Zeitwaage kam, wurde es noch ein wenig justiert, da es recht stark im Minus lief. Die anschließenden Ergebnisse können sich wirklich sehen lassen, vor allem vor dem Hintergrund, daß das Werk in keinem Top-Zustand mehr war:
  rates:
    ZO: "+33"
    ZU: "+12"
    3O: "0"
    6O: ""
    9O: "+8"
    12O: "+3"
  amplitudes:
    ZO: "184"
    ZU: "209"
    3O: "168.6O=-17"
    6O: ""
    9O: "169"
    12O: "175"
  beaterrors:
    ZO: "0.3"
    ZU: "0.0"
    3O: "0.1"
    6O: ""
    9O: "0.5"
    12O: "0.0"
labor: |
  Das hier gezeigte Exemplar kam als loses Werk mit leichtem Wasserschaden und defektem Magic Lever ins Labor, beim weiteren Zerlegen zeigte sich, daß vor allem die zahlreichen Plastikteile recht empfindlich sind, vor allem die der Datumsschaltung. Da die ganze Konstruktion aber kompromißlos auf Langlebigkeit ausgelegt ist (sofern keine manuellen Eingriffe erfolgen, wie sie hier der Fall waren), kann man davon ausgehen, daß dieses Werk ohne Probleme zehn oder gar zwanzig Jahre durchhält. Eine Revision lohnt sich danach meist nicht, da der Restwert nach dieser Zeit in keinem Verhältnis zum Revisionsaufwand steht.
---
Lorem Ipsum