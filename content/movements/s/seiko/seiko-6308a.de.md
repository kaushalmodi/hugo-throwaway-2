---
title: "Seiko 6308A"
date: 2009-04-18T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Seiko 6308A","Seiko","6308A","17 Jewels","Japan","Automatic","17 Steine"]
description: "Seiko 6308A"
abstract: ""
preview_image: "seiko_6308a-mini.jpg"
image: "Seiko_6308A.jpg"
movementlistkey: "seiko"
caliberkey: "6308A"
manufacturers: ["seiko"]
manufacturers_weight: 6308
categories: ["movements","movements_s","movements_s_seiko"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_6308A_8030.jpg"
    description: "Seiko Automatic Herrenuhr Modell 8030"
links: |
  * [Posting 6308-8030](http://www.network54.com/Forum/78440/message/1215819920/In+Search+Of+The+Blues+\85+Found!)
  * [Ranfft Uhren: Seiko 6308A](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Seiko_6308A)
---
Lorem Ipsum