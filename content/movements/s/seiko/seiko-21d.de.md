---
title: "Seiko 21D"
date: 2009-04-13T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Seiko 21D","Seiko 21","Seiko","21D","17 Jewels","Japan","Damenuhr","17 Steine","Formwerk"]
description: "Seiko 21D"
abstract: ""
preview_image: "seiko_21d-mini.jpg"
image: "Seiko_21D.jpg"
movementlistkey: "seiko"
caliberkey: "21D"
manufacturers: ["seiko"]
manufacturers_weight: 21
categories: ["movements","movements_s","movements_s_seiko"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/seiko/Seiko_DAU_2.jpg"
    description: "Seiko Damenuhr"
---
Lorem Ipsum