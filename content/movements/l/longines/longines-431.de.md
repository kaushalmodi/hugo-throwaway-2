---
title: "Longines 431"
date: 2015-03-13T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Longines","Saint-Imier","Swiss","Uhr","Uhren","Armbanduhr","Armbanduhren","Uhrwerk","Uhrwerke","Kaliber","Steine","17","Automatic","Handaufzug","Schweiz"]
description: "Longines 431"
abstract: ""
preview_image: "longines_431-mini.jpg"
image: "Longines_431.jpg"
movementlistkey: "longines"
caliberkey: "431"
manufacturers: ["longines"]
manufacturers_weight: 431
categories: ["movements","movements_l","movements_l_longines"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum