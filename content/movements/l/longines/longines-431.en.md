---
title: "Longines 431"
date: 2015-03-13T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Longines","Saint-Imier","Swiss","Longines 431","movement","fast beat","date","17 Jewels"]
description: "Longines 431"
abstract: ""
preview_image: "longines_431-mini.jpg"
image: "Longines_431.jpg"
movementlistkey: "longines"
caliberkey: "431"
manufacturers: ["longines"]
manufacturers_weight: 431
categories: ["movements","movements_l","movements_l_longines_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum