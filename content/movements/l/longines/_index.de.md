---
title: "Longines"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "de"
keywords: ["Longines","Saint-Imier","Schweiz","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_l"]
movementlistkey: "longines"
description: "Uhrwerke des schweizer Herstellers Longines aus Saint-Imier"
abstract: "Uhrwerke des schweizer Herstellers Longines aus Saint-Imier"
---
(Longines, Saint-Imier, Schweiz)
{{< movementlist "longines" >}}

{{< movementgallery "longines" >}}