---
title: "Lorsa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "en"
keywords: ["Lorsa","Annemasse","Haute-Savoie","France"]
categories: ["movements","movements_l"]
movementlistkey: "lorsa"
abstract: "Movements from the french manufacturer Lorsa from Annemasse in Haute-Savioe"
description: "Movements from the french manufacturer Lorsa from Annemasse in Haute-Savioe"
---
(Lorsa, Annemasse, Haute-Savoie, France)
{{< movementlist "lorsa" >}}

{{< movementgallery "lorsa" >}}