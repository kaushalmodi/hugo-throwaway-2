---
title: "Lorsa 8F"
date: 2009-11-13T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa","Lorsa 8F","8F","Frankreich","17 Steine","17","Steine"]
description: "Lorsa 8F"
abstract: ""
preview_image: "lorsa_8f-mini.jpg"
image: "Lorsa_8F.jpg"
movementlistkey: "lorsa"
caliberkey: "8F"
manufacturers: ["lorsa"]
manufacturers_weight: 890
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/esco/Esco_DAU.jpg"
    description: "Esco Damenuhr"
  - image: "k/kiple/Kiple_DAU_Zifferblatt.jpg"
    description: "Kiple Damenuhr  (nur Zifferblatt)"
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks in der Ausführung mit der Novodiac-Stoßsicherung!"
links: |
  * [Ranfft Uhren: Lorsa 8F](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Lorsa_8F)
---
Lorem Ipsum