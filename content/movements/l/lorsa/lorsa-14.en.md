---
title: "Lorsa 14"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Lorsa 14","Lorsa","17 Jewels","form movement","france"]
description: "Lorsa 14"
abstract: ""
preview_image: "lorsa_14-mini.jpg"
image: "Lorsa_14.jpg"
movementlistkey: "lorsa"
caliberkey: "14"
manufacturers: ["lorsa"]
manufacturers_weight: 140
categories: ["movements","movements_l","movements_l_lorsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bisor/Bisor_310.jpg"
    description: "Bisor 310 ladies' watch"
  - image: "r/rio/Rio_DAU.jpg"
    description: "Rio ladies' watch"
---
Lorem Ipsum