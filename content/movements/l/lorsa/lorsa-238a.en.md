---
title: "Lorsa 238A"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Lorsa 238A","Lorsa","238A","17 Jewels","France"]
description: "Lorsa 238A"
abstract: ""
preview_image: "lorsa_238a-mini.jpg"
image: "Lorsa_238A.jpg"
movementlistkey: "lorsa"
caliberkey: "238A"
manufacturers: ["lorsa"]
manufacturers_weight: 2381
categories: ["movements","movements_l","movements_l_lorsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/corocraft/Corocraft.jpg"
    description: "Corocraft gents watch"
---
Lorem Ipsum