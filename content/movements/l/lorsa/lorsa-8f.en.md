---
title: "Lorsa 8F"
date: 2009-11-13T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Lorsa","Lorsa 8F","8F","France","17 jewels","17","jewels"]
description: "Lorsa 8F"
abstract: ""
preview_image: "lorsa_8f-mini.jpg"
image: "Lorsa_8F.jpg"
movementlistkey: "lorsa"
caliberkey: "8F"
manufacturers: ["lorsa"]
manufacturers_weight: 890
categories: ["movements","movements_l","movements_l_lorsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Lorsa 8F](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Lorsa_8F)</p><p>This movement in the version with Novodiac shock protection was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum