---
title: "Lorsa 514A"
date: 2009-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Lorsa 514A","Lorsa","514A","17 Jewels","17 Rubis","France"]
description: "Lorsa 514A"
abstract: ""
preview_image: "lorsa_514a-mini.jpg"
image: "Lorsa_514A.jpg"
movementlistkey: "lorsa"
caliberkey: "514A"
manufacturers: ["lorsa"]
manufacturers_weight: 5141
categories: ["movements","movements_l","movements_l_lorsa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Lorsa 514A](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Lorsa_514A)</p><p>This movement with the RUFA-Antishock protection was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
usagegallery: 
  - image: "i/isoma/Isoma_DAU.jpg"
    description: "Isoma ladies' watch"
  - image: "j/juta/Juta_DAU_Zifferblatt.jpg"
    description: "Juta ladies' watch  (dial only)"
---
Lorem Ipsum