---
title: "Lorsa 514A"
date: 2009-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa 514A","Lorsa","514A","17 Jewels","17 Steine","Frankreich"]
description: "Lorsa 514A"
abstract: ""
preview_image: "lorsa_514a-mini.jpg"
image: "Lorsa_514A.jpg"
movementlistkey: "lorsa"
caliberkey: "514A"
manufacturers: ["lorsa"]
manufacturers_weight: 5141
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks in der Ausführung mit der RUFA-Antishock-Stoßsicherung!"
usagegallery: 
  - image: "i/isoma/Isoma_DAU.jpg"
    description: "Isoma Damenuhr"
  - image: "j/juta/Juta_DAU_Zifferblatt.jpg"
    description: "Juta Damenuhr  (nur Zifferblatt)"
links: |
  * [Ranfft Uhren: Lorsa 514A](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Lorsa_514A)
---
Lorem Ipsum