---
title: "Lorsa P62"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa P62","Lorsa","P62","Lorsa","Annemasse","Haute-Savoie","21 Jewels","Frankreich","21 Steine"]
description: "Lorsa P62"
abstract: ""
preview_image: "lorsa_p62-mini.jpg"
image: "Lorsa_P62.jpg"
movementlistkey: "lorsa"
caliberkey: "P62"
manufacturers: ["lorsa"]
manufacturers_weight: 620
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/evero/Evero_HAU.jpg"
    description: "Evero Herrenuhr"
links: |
  * [Ranfft Uhren: Lorsa P62](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Lorsa_P62)
---
Lorem Ipsum