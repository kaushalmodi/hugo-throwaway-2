---
title: "Lorsa 8FB"
date: 2017-03-18T01:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lorsa 8FB","Lorsa","Frankreich","Formwerk","17 Jewels"]
description: "Lorsa 8FB: Ein kleines Formwerk für Damenuhren"
abstract: "Ein Damenuhren-Formwerk mit Ankerhemmung "
preview_image: "lorsa_8fb-mini.jpg"
image: "Lorsa_8FB.jpg"
movementlistkey: "lorsa"
caliberkey: "8FB"
manufacturers: ["lorsa"]
manufacturers_weight: 892
categories: ["movements","movements_l","movements_l_lorsa"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  description: |
    Das vorliegende NOS-Werk ist unreguliert, dürfte aber 30-40 Jahre in der Schublade gelegen haben. Dennoch sind die Gangwerte auf der Zeitwaage größtenteils sehr gut, mit einer nicht wirklich zu erklärenden Ausnahme bei 9O:
  rates:
    ZO: "+10"
    ZU: "+4"
    3O: "+20"
    6O: "-18"
    9O: "-85"
    12O: "+1"
  amplitudes:
    ZO: "285"
    ZU: "274"
    3O: "230"
    6O: "231"
    9O: "239"
    12O: "240"
  beaterrors:
    ZO: "1.9"
    ZU: "1.9"
    3O: "2.3"
    6O: "2.6"
    9O: "2.0"
    12O: "1.6"
usagegallery: 
  - image: "p/pallas/Pallas_Ormo_DU_Lorsa_8FB.jpg"
    description: "Pallas Ormo Damenuhr"
---
Lorem Ipsum