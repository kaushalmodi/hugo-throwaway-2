---
title: "Langendorf 824"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Lanco 824","Lanco","Langendorf 824","Langendorf","824","17 Jewels","Damenuhr","Schweiz"]
description: "Langendorf 824"
abstract: ""
preview_image: "langendorf_824-mini.jpg"
image: "Langendorf_824.jpg"
movementlistkey: "langendorf"
caliberkey: "824"
manufacturers: ["langendorf"]
manufacturers_weight: 824
categories: ["movements","movements_l","movements_l_langendorf"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/lanco/Lanco_DAU_2.jpg"
    description: "Lanco Damenuhr"
---
Lorem Ipsum