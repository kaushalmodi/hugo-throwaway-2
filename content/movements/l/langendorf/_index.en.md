---
title: "Langendorf (Lanco)"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "en"
keywords: ["Langendorf","Lanco","Solothurn","Switzerland","swiss"]
categories: ["movements","movements_l"]
movementlistkey: "langendorf"
abstract: "Movements from the swiss manufacturer Langendorf from Solothurn, also known as Lanco"
description: "Movements from the swiss manufacturer Langendorf from Solothurn, also known as Lanco"
---
(Langendorf SA, Solothurn, Switzerland)
{{< movementlist "langendorf" >}}

{{< movementgallery "langendorf" >}}