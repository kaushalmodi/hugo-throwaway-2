---
title: "Langendorf 5106"
date: 2009-11-14T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Langendorf 5106","Langendorf","5106","4 Jewels","Solothurn","Lanco","LWC","swiss","switzerland","cylinder","cylinder escapement","cylinder movement"]
description: "Langendorf 5106"
abstract: ""
preview_image: "langendorf_5106-mini.jpg"
image: "Langendorf_5106.jpg"
movementlistkey: "langendorf"
caliberkey: "5106"
manufacturers: ["langendorf"]
manufacturers_weight: 5106
categories: ["movements","movements_l","movements_l_langendorf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum