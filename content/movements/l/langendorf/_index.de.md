---
title: "Langendorf (Lanco)"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "de"
keywords: ["Langendorf","Lanco","Solothurn","Schweiz"]
categories: ["movements","movements_l"]
movementlistkey: "langendorf"
abstract: "Uhrwerke der Langendorf S.A. aus Solothurn in der Schweiz, auch bekannt unter der Abkürzung Lanco"
description: "Uhrwerke der Langendorf S.A. aus Solothurn in der Schweiz, auch bekannt unter der Abkürzung Lanco"
---
(Langendorf SA, Solothurn, Schweiz)
{{< movementlist "langendorf" >}}

{{< movementgallery "langendorf" >}}