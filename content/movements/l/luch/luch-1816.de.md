---
title: "Luch 1816"
date: 2017-10-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Luch 1816","Lutsch 1816","Minsk","Automatic","21 Jewels","21 Rubis","Datum"]
description: "Luch 1816 - Das bekannteste russische Damenuhren-Automaticwerk"
abstract: "Das bekannteste Automaticwerk in Damenuhrengröße aus der ehemaligen UdSSR."
preview_image: "luch_1816-mini.jpg"
image: "Luch_1816.jpg"
movementlistkey: "luch"
caliberkey: "1816"
manufacturers: ["luch"]
manufacturers_weight: 1816
categories: ["movements","movements_l","movements_l_luch"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "l/luch/Luch_DAU_Luch_1816.jpg"
    description: "Luch Automatic Damenuhr"
---
Lorem Ipsum