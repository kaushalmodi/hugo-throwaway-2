---
title: "Luch"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["l"]
language: "en"
keywords: ["Luch","Belarus","Minsk"]
categories: ["movements","movements_l"]
movementlistkey: "luch"
description: "Movements from belarus manufacturer Luch of Minsk"
abstract: "Movements from belarus manufacturer Luch of Minsk"
---
(Minsk Watch Factory, Minsk, Belarus)
{{< movementlist "luch" >}}

{{< movementgallery "luch" >}}