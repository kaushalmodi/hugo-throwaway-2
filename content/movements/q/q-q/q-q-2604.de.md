---
title: "Q&Q 2604"
date: 2015-09-08T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Q&Q 2604","2604","Plastik","0 Jewels","Anker"]
description: "Q&Q 2604 - Ein fernöstliches Plastikanker-Automaticwerk mit Datum und Wochentagsanzeige"
abstract: "Ein sehr rationell gefertiges fernöstliches Automaticwerk mit Vollausstattung und jeder Menge Plastikteile"
preview_image: "q_q_2604-mini.jpg"
image: "Q_Q_2604.jpg"
movementlistkey: "q-q"
caliberkey: "2604"
manufacturers: ["q-q"]
manufacturers_weight: 2604
categories: ["movements","movements_q","movements_q_q_q"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "q/q_q/Q_Q_Automatic_Q_Q_2604.jpg"
    description: "Q&amp;Q Automatic Herrenuhr"
  - image: "q/q_q/Q_Q_HAU_Automatic.jpg"
    description: "Q&amp;Q Automatic Herrenuhr"
labor: |
  <p>Das hier vorgestellte Werk kam in einem stark verkratzten und beriebenen Gehäuse in die Werkstatt. Schätzungsweise über 10 Jahre tägliches Tragen ohne Rücksicht auf Verluste hinterließen Spuren.</p><p>Anscheinend wurde dann das Lebensende dieses Werks erreicht (sichtbar an ausgelaufenen Plastiklagern der Unruhwelle und des Automatikaufzugs und zu weichen Plastik-Achsen des Ankerrads) und man versuchte recht hemdsärmlig durch Fluten des Werks mit Öl, es wieder zum Laufen zu bringen. Die Folge waren eine ölverklebte Unruhspirale und eine Hemmungspartie, deren Teile alle durch die Adhäsionskraft zusammenklebten. "Viel hilft viel" ist eine Binsenweisheit, die in der Uhrmacherei nicht funktioniert!</p><p>Das Werk wurde komplett demontiert und gereinigt, aber gegen die weichgewordenen Plastikachsen des Hemmungsrads und die ausgelaufenen Plastiklager kann leider nichts mehr ausgerichtet werden, immerhin tickt das Werk jetzt wieder einige Sekunden, ist also kein Totalschaden im Sinne des Archivs.</p>
---
Lorem Ipsum