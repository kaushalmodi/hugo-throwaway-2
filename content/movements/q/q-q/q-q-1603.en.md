---
title: "Q&Q 1603"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Q&Q 1603","Q&Q","1603","Korea","Japan","Taiwan","0 Jewels","plastic lever","plastic","far east"]
description: "Q&Q 1603"
abstract: ""
preview_image: "q_q_1603-mini.jpg"
image: "Q_Q_1603.jpg"
movementlistkey: "q-q"
caliberkey: "1603"
manufacturers: ["q-q"]
manufacturers_weight: 1603
categories: ["movements","movements_q","movements_q_q_q_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "q/q_q/Q_Q_DAU.jpg"
    description: "Q&amp;Q &quot;Mec&quot; ladies' watch"
  - image: "i/intertronic/Intertronic_HAU.jpg"
    description: "Intertronic &quot;digi-ana&quot; gents watch"
---
Lorem Ipsum