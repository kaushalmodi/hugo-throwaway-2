---
title: "AHS 53"
date: 2017-09-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AHS 53","AHS","53","pin lever","Finger","1980"]
description: "AHS 53 - A simple final generation pin-lever movement from Germany"
abstract: "One of the last pin lever movements from Germany."
preview_image: "ahs_53-mini.jpg"
image: "AHS_53.jpg"
movementlistkey: "ahs"
caliberkey: "53"
manufacturers: ["ahs"]
manufacturers_weight: 53
categories: ["movements","movements_a","movements_a_ahs_en"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  rates:
    ZO: "+110"
    ZU: "+30"
    3O: "+30"
    6O: "+200"
    9O: "+120"
    12O: "-61"
  amplitudes:
    ZO: "328"
    ZU: "245"
    3O: "238"
    6O: "213"
    9O: "213"
    12O: "210"
  beaterrors:
    ZO: "1.5"
    ZU: "0.4"
    3O: "0.5"
    6O: "0.2"
    9O: "0.4"
    12O: "1.4"
usagegallery: 
  - image: "a/ahs/AHS_HU_AHS_53.jpg"
    description: "AHS gents watch (11/1980)"
  - image: "d/domina/Domina_DU_AHS_53.jpg"
    description: "Domina ladies' watch"
donation: "The \"Domina\" watch with movement in working condition is a kind donation of [Günter G](/supporters/guenter-g/). Thank you very much!"
labor: |
  The specimen shown here came in used but working condition into the lab and thus was only slightly oiled and adjusted.
---
Lorem Ipsum