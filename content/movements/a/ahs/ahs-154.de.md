---
title: "AHS 154"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Palettenankerwerk","Robot-Automatic","Exzenterwechsler","Rubinrollen","Schraubenunruh","Rufarex","Pfeileraufbau","AHS 154","AHS","154","August Hirsch","Schwenningen","21 Jewels","Automatic","Deutschland","21 Steine","Automatik"]
description: "AHS 154 - Ein einfaches Automaticwerk mit 21 Steinen und Exzenterwechsler. Detaillierte Beschreibung mit Bildern, Makro-Aufnahmen, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: "Ein einfaches Automaticwerk mit 21 Steinen und Exzenterwechsler."
preview_image: "ahs_154-mini.jpg"
image: "AHS_154.jpg"
movementlistkey: "ahs"
caliberkey: "154"
manufacturers: ["ahs"]
manufacturers_weight: 154
categories: ["movements","movements_a","movements_a_ahs"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dilau/Dilau_HAU_AHS_154.jpg"
    description: "Dilau Automatic Herrenuhr"
timegrapher_old: 
  description: |
    An den Zeitwaagenergebnissen erkennt man, in welch schlechtem technischen Zustand sich das Werk befindet. Der Verschleiß ist hier schon sehr weit fortgeschritten, so daß eine auch nur halbwegs vernünftigte Regulierung leider aussichtslos ist.
  images:
    ZO: Zeitwaage_AHS_154_ZO.jpg
    ZU: Zeitwaage_AHS_154_ZU.jpg
    3O: Zeitwaage_AHS_154_3O.jpg
    6O: Zeitwaage_AHS_154_6O.jpg
    9O: Zeitwaage_AHS_154_9O.jpg
    12O: Zeitwaage_AHS_154_12O.jpg
  values:
    ZO: "+30"
    ZU: "-75"
    3O: "..."
    6O: "..."
    9O: "-210"
    12O: "31"
labor: |
  Das Werk kam mit starken Abnutzungserscheinungen, vor allem am Automaticmechanismus, ins Labor. Es wurde komplett zerlegt, gereinigt und neu geölt.
---
Lorem Ipsum