---
title: "AHS 40"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "en"
keywords: []
description: ""
abstract: ""
preview_image: "ahs_40-mini.jpg"
image: "AHS_40.jpg"
movementlistkey: "ahs"
caliberkey: "40"
manufacturers: ["ahs"]
manufacturers_weight: 40
categories: ["movements","movements_a","movements_a_ahs_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/Selecta_de_Luxe_DAU.jpg"
    description: "Selecta de Luxe ladies' watch"
---
Lorem Ipsum