---
title: "AHS 43"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "de"
keywords: []
description: "Ein Stiftankerwerk mit indirekter Zentralsekunde und steingelagerter Unruh"
abstract: "Ein Stiftankerwerk mit indirekter Zentralsekunde und steingelagerter Unruh"
preview_image: "AHS_43.jpg"
image: "AHS_43.jpg"
movementlistkey: "ahs"
caliberkey: "43"
manufacturers: ["ahs"]
manufacturers_weight: 43
categories: ["movements","movements_a","movements_a_ahs"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: AHS 43](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&AHS_43)
  * [Watch WIKI: AHS 43](http://www.watch-wiki.de/index.php?title=AHS_43)
usagegallery: 
  - image: "s/Selecta_de_Luxe_HAU_2.jpg"
    description: "Selecta de Luxe Herrenuhr"
---
Lorem Ipsum