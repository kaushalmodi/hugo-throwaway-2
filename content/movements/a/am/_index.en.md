---
title: "AM"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "en"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "am"
description: ""
abstract: "(Adolph Michel SA, Grenchen, Switzerland)"
---
(Adolph Michel SA, Grenchen, Switzerland)
{{< movementlist "am" >}}

{{< movementgallery "am" >}}