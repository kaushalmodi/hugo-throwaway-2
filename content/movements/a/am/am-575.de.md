---
title: "AM 575"
date: 2012-12-28T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AM 575","AM","Ebauches S.A.","Trust","1 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Damenuhr","Kaliber","Werk","1 Stein","Zylinderhemmung"]
description: "AM 575 - Ein Zylinderwerk für Damenuhren"
abstract: "Ein Werk mit Zylinderhemmung für Damenuhren"
preview_image: "am_575-mini.jpg"
image: "AM_575.jpg"
movementlistkey: "am"
caliberkey: "575"
manufacturers: ["am"]
manufacturers_weight: 575
categories: ["movements","movements_a","movements_a_am"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_Anonym_AM_575.jpg"
    description: "Anonyme Damenuhr"
---
Lorem Ipsum