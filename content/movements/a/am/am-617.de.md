---
title: "AM 617"
date: 2012-12-30T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Zylinderwerk","AM 17","AM","617","10 Jewels","Adolph Michel","Ebauches Trust","Grenchen","Schweiz","10 Steine"]
description: "AM 617 - Ein besseres Zylinderwerk mit 10 Steinen. Detaillierte Beschreibung mit Bildern, Video und Datenblatt."
abstract: "Ein besseres und modern konstruiertes Zylinderwerk mit 10 Steinen"
preview_image: "am_617-mini.jpg"
image: "AM_612_617.jpg"
movementlistkey: "am"
caliberkey: "617"
manufacturers: ["am"]
manufacturers_weight: 617
categories: ["movements","movements_a","movements_a_am"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Harald Hoeber](/supporters/de/) für die Spende dieses Werks und an _Badener_ aus dem [UhrForum](http://uhrforum.de) für die Identifizierung!"
usagegallery: 
  - image: "z/zentra/Zentra_DAU_AM_612_617_Zifferblatt.jpg"
    description: "ZentRa Damenuhr"
---
Lorem Ipsum