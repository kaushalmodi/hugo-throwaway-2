---
title: "AM 575"
date: 2012-09-30T14:18:18+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AM 575","AM","Ebauches S.A.","Trust","1 Jewels","watch","watches","wristwatch","wristwatches","ladies` watch","movement","caliber","cylinder escapement"]
description: "AM 575 - A cylinder movement for ladies' watches"
abstract: "A cylinder movement for ladies' watches"
preview_image: "am_575-mini.jpg"
image: "AM_575.jpg"
movementlistkey: "am"
caliberkey: "575"
manufacturers: ["am"]
manufacturers_weight: 575
categories: ["movements","movements_a","movements_a_am_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_Anonym_AM_575.jpg"
    description: "Anonyme Damenuhr"
---
Lorem Ipsum