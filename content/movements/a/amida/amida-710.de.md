---
title: "Amida 710"
date: 2012-12-31T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Roskopf-Werk","Pfeilerwerk","einstöckig","Amida 710","Amida","710","17 Jewels","Grenchen","Schweiz","17 Steine","Handaufzug"]
description: "Amida 710 - Ein recht modernes und seltenes Handaufzugswerk. Detaillierte Beschreibung mit Bildern, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: "Ein recht modernes und seltenes schweizer Handaufzugswerk"
preview_image: "amida_710-mini.jpg"
image: "Amida_710.jpg"
movementlistkey: "amida"
caliberkey: "710"
manufacturers: ["amida"]
manufacturers_weight: 710
categories: ["movements","movements_a","movements_a_amida"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/amida/Amida_HAU_Amida_710.jpg"
    description: "Amida Herrenuhr"
timegrapher_old: 
  images:
    ZO: Zeitwaage_Amida_710_ZO.jpg
    ZU: Zeitwaage_Amida_710_ZU.jpg
    3O: Zeitwaage_Amida_710_3O.jpg
    6O: Zeitwaage_Amida_710_6O.jpg
    9O: Zeitwaage_Amida_710_9O.jpg
    12O: Zeitwaage_Amida_710_12O.jpg
  values:
    ZO: "-30"
    ZU: "-10"
    3O: "-38"
    6O: "+6"
    9O: "+4"
    12O: "-15"
labor: |
  Das Werk kam in gutem Zustand ins Labor, wurde aber dennoch komplett gereinigt und geölt.
---
Lorem Ipsum