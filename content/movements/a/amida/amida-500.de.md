---
title: "Amida 500"
date: 2013-03-17T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Amida 500","Amida S.A.","Stiftanker","Schraubenunruh","15 Jewels","15 Steine","Pseudo-Schraubenunruh","Zierschliffe","Genfer Streifen"]
description: "Amida 500: Ein sehr schön dekoriertes Stiftankerwerk mit 15 Steinen, einer Pseudo-Schraubenunruh und einem der schönsten Ankerräder, das je in einem Stiftankerwerk verbaut wurde"
abstract: "Ein optisch äußers ansprechendes Stiftankerwerk, das mit 15 Steinen und einer Pseudo-Schraubenunruh ausgestattet ist und eines der schönsten, je in einem Stiftankerwerk verbauten Ankerräder besitzt"
preview_image: "amida_500-mini.jpg"
image: "Amida_500.jpg"
movementlistkey: "amida"
caliberkey: "500"
manufacturers: ["amida"]
manufacturers_weight: 500
categories: ["movements","movements_a","movements_a_amida"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/amida/Amida_HAU_Amida_500.jpg"
    description: "Amida Herrenuhr"
---
Lorem Ipsum