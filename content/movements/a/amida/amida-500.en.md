---
title: "Amida 500"
date: 2013-03-28T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Amida 500","Amida S.A.","pin lever","screw balance","15 Jewels","15 Rubis","lookalike screw balance","decoration","geneva stripes"]
description: "Amida 500: A nicely decorated pin lever movement with 15 jewels, a lookalike screw balance and one of the most beautiful lever wheels, which were ever used in a pin lever movement."
abstract: "A very nicely decorated pin lever movement with a lookalike screw balance, 15 jewels and one of the most beautiful escapement wheels, which were ever used in a pin lever movement."
preview_image: "amida_500-mini.jpg"
image: "Amida_500.jpg"
movementlistkey: "amida"
caliberkey: "500"
manufacturers: ["amida"]
manufacturers_weight: 500
categories: ["movements","movements_a","movements_a_amida_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/amida/Amida_HAU_Amida_500.jpg"
    description: "Amida mens' watch"
---
Lorem Ipsum