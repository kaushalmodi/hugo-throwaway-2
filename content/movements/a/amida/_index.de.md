---
title: "Amida"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "de"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "amida"
abstract: "(Amida SA, Grenchen, Schweiz)"
description: ""
---
(Amida SA, Grenchen, Schweiz)
{{< movementlist "amida" >}}

{{< movementgallery "amida" >}}