---
title: "Amida"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "en"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "amida"
abstract: "(Amida SA, Grenchen, Switzerland)"
description: ""
---
(Amida SA, Grenchen, Switzerland)
{{< movementlist "amida" >}}

{{< movementgallery "amida" >}}