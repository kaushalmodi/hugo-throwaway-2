---
title: "Arogno 151"
date: 2017-01-13T20:17:17+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Arogno 151","Arogno","Handaufzug","dezentrale Sekunde","kleine Sekunde","Palettenanker"]
description: "Arogno 151 - ein Handaufzugswerk aus den späten 1940er Jahren"
abstract: "Arogno 151 - ein gut verabeitetes Handaufzugswerk aus den späten 1940er Jahren."
preview_image: "arogno_151-mini.jpg"
image: "Arogno_151.jpg"
movementlistkey: "arogno"
caliberkey: "151"
manufacturers: ["arogno"]
manufacturers_weight: 151
categories: ["movements","movements_a","movements_a_arogno"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das hier vorgestellte Exemplar kam in leicht verharzten Zustand ins Labor und bekam eine einfache Reinigung und einen einfachen Service.
usagegallery: 
  - image: "t/tramont/Tramont_HAU_Arogno_151.jpg"
    description: "Tramont Herrenuhr"
timegrapher_old: 
  rates:
    ZO: "+22"
    ZU: "+23"
    3O: "+15"
    6O: "+115"
    9O: "-138"
    12O: "-115"
  amplitudes:
    ZO: "203"
    ZU: "213"
    3O: "170"
    6O: "174"
    9O: "179"
    12O: "174"
  beaterrors:
    ZO: "6.4"
    ZU: "5.9"
    3O: "7.8"
    6O: "7.2"
    9O: "6.8"
    12O: "7.7"
---
Lorem Ipsum