---
title: "Arogno 2"
date: 2013-01-01T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Arogno 2","Arogno","2","10 Jewels","Arogno","watch","watches","wristwatch","wristwatches","caliber","swiss","switzerland","cylinder","cylinder escapement"]
description: "Arogno 2 - A cylinder movement with 10 jewels and yoke winding system"
abstract: "A 10 jewel cylinder movement with yoke winding system"
preview_image: "arogno_2-mini.jpg"
image: "Arogno_2.jpg"
movementlistkey: "arogno"
caliberkey: "2"
manufacturers: ["arogno"]
manufacturers_weight: 2
categories: ["movements","movements_a","movements_a_arogno_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum