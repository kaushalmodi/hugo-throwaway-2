---
title: "Arogno"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["a"]
language: "de"
keywords: []
categories: ["movements","movements_a"]
movementlistkey: "arogno"
description: ""
abstract: "(Fabriques d'Ébauches Réunies d'Arogno SA, Arogno, Schweiz)"
---
(Fabriques d'Ébauches Réunies d'Arogno SA, Arogno, Schweiz)
{{< movementlist "arogno" >}}

{{< movementgallery "arogno" >}}