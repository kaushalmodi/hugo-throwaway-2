---
title: "AS 1199"
date: 2017-10-31T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 1199","EB 1199","Stiftanker","15 Jewels","15 Rubis","Zentralsekunde","Schweiz"]
description: "AS 1199 (EB 1199) - Ein sehr selten anzutreffendes, hochwertiges Stiftankerwerk"
abstract: "Ein hochwertiges Stiftankerwerk, daß es eigentlich gar nicht gab!"
preview_image: "as_1199-mini.jpg"
image: "AS_1199.jpg"
movementlistkey: "as"
caliberkey: "1199"
manufacturers: ["as"]
manufacturers_weight: 1199
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "g/goldanker/Goldanker_DU_AS_1199.jpg"
    description: "Goldanker Damenuhr"
timegrapher_old: 
  rates:
    ZO: "+12"
    ZU: "+14"
    3O: "-142"
    6O: "-141"
    9O: "-77"
    12O: "-73"
  amplitudes:
    ZO: "231"
    ZU: "260"
    3O: "166"
    6O: "158"
    9O: "173"
    12O: "168"
  beaterrors:
    ZO: "0.9"
    ZU: "0.4"
    3O: "0.3"
    6O: "1.3"
    9O: "1.1"
    12O: "0.3"
labor: |
  Das vorliegende Exemplar dürfte aufgrund seines extrem guten und sauberen Zustands ein NOS-Exemplar sein. Auf den eigentlich fälligen Service samt Ölwechsel wurde verzichtet.
---
Lorem Ipsum