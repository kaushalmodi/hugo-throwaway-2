---
title: "AS 1220"
date: 2014-02-16T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 1220","AS","Adolph Schild SA","17 Jewels","17 Steine","Handaufzug","Zentralsekunde"]
description: "AS 1220 - Ein 10 1/2 liniges Werk mit einer sehr ungewönlichen Konstruktion für die Minutenanzeige"
abstract: "Ein schweizer Herrenuhrenwerk mit sehr charakteristischer Brückenform und ungewöhnlicher Konstruktion der zentralen Minutenanzeige"
preview_image: "as_1220-mini.jpg"
image: "AS_1220.jpg"
movementlistkey: "as"
caliberkey: "1220"
manufacturers: ["as"]
manufacturers_weight: 1220
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk befand sich in sehr gutem Zustand, so daß auf eine intensive Reinigung verzichtet wurde, der Service bestand hier lediglich aus einer einfachen Reinigung, sowie dem Ölen der Räder.
timegrapher_old: 
  description: |
    Für ein nicht intensiv gereinigtes Werk sind die Gangwerke in Ordnung. Da es mit sehr starker Amplitude schwingt (läßt sich leider nur visuell erkennen), könnte man durch eine Auswuchtung der Unruh und Korrektur des Abfallfehlers sicher noch einiges herausholen.
  images:
    ZO: Zeitwaage_AS_1220_ZO.jpg
    ZU: Zeitwaage_AS_1220_ZU.jpg
    3O: Zeitwaage_AS_1220_3O.jpg
    6O: Zeitwaage_AS_1220_6O.jpg
    9O: Zeitwaage_AS_1220_9O.jpg
    12O: Zeitwaage_AS_1220_12O.jpg
  values:
    ZO: "+10"
    ZU: "+7"
    3O: "-13"
    6O: "+3"
    9O: "-25"
    12O: "-50"
usagegallery: 
  - image: "f/felsus/Felsus_HAU_AS_1220.jpg"
    description: "Felsus Herrenuhr (mit inkorrektem Zifferblatt)"
---
Lorem Ipsum