---
title: "AS 1580"
date: 2010-10-24T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Maximatic","click changer","breguet coupling","AS 1580","AS","1580","17 Jewels","Schild","Adolph Schild","Grenchen","Automatic","swiss","switzerland","selfwinding"]
description: "AS 1580 - A very interesting selfwinding movement with 17 jewels. Detailed description with photos, video, macro images, data sheet and timegrapher protocol."
abstract: "The so-called \"Maximatic\" caliber from the year 1958."
preview_image: "as_1580-mini.jpg"
image: "AS_1580.jpg"
movementlistkey: "as"
caliberkey: "1580"
manufacturers: ["as"]
manufacturers_weight: 1580
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    The results on the timegrapher are pretty poor, especially on such a fine movement (selfwindig and with Glucydur screw balance) from such a fine manufactory. It's likely, that the water damage above, and maybe also some shocks in the past and probably long revision periods harmed the movement and are responsible for the results. These are strongly depending on the position, but they are very constant!.
  images:
    ZO: Zeitwaage_AS_1580_ZO.jpg
    ZU: Zeitwaage_AS_1580_ZU.jpg
    3O: Zeitwaage_AS_1580_3O.jpg
    6O: Zeitwaage_AS_1580_6O.jpg
    9O: Zeitwaage_AS_1580_9O.jpg
    12O: Zeitwaage_AS_1580_12O.jpg
  values:
    ZO: "+25"
    ZU: "+20"
    3O: "-20"
    6O: "+10"
    9O: "+60"
    12O: "-5"
usagegallery: 
  - image: "r/recta/HAU_Recta_AS_1580.jpg"
    description: "mens' watch, rotor signed with &quot;Recta&quot;"
links: |
  * [Ranfft Uhren: AS 1580](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&9&2uswk&AS_1580&)
labor: |
  <p>The movement came loose, only with a dial, into the lab. It was probably taken of a gold watch (whose case was melted, what a pity!). To ensure, that there's no dirt in the movement, it was completely disassembled, cleaned and oiled.</p><p>During the disassembly, a large water damage became visible. It could be removed more or less, but there's sure some invisible damage on the movement.</p>
---
Lorem Ipsum