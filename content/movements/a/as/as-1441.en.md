---
title: "AS 1441"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1441","AS","Adolph Schild","Schild","1441","17 Jewels","screw balance"]
description: "AS 1441"
abstract: ""
preview_image: "as_1441-mini.jpg"
image: "AS_1441.jpg"
movementlistkey: "as"
caliberkey: "1441"
manufacturers: ["as"]
manufacturers_weight: 1441
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum