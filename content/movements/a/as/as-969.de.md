---
title: "AS 969"
date: 2013-01-30T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Deckstein","Glucydur","Schraubenunruh","Rückerzeiger","Kupplungsaufzug","AS 969","AS","969","17 Jewels","Adolph Schild","Schild","Schweiz","17 Steine","Handaufzug"]
description: "AS 969 - Eines der kleinesten runden AS-Handaufzugswerke. Detaillierte Beschreibung mit Bildern, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: "Eines der kleinsten Rundwerke von AS"
preview_image: "as_969-mini.jpg"
image: "AS_969.jpg"
movementlistkey: "as"
caliberkey: "969"
manufacturers: ["as"]
manufacturers_weight: 969
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Harald Hoeber](/supporters/de/) für die Spende dieses Werks!"
timegrapher_old: 
  images:
    ZO: Zeitwaage_AS_969_ZO.jpg
    ZU: Zeitwaage_AS_969_ZU.jpg
    3O: Zeitwaage_AS_969_3O.jpg
    6O: Zeitwaage_AS_969_6O.jpg
    9O: Zeitwaage_AS_969_9O.jpg
    12O: Zeitwaage_AS_969_12O.jpg
  values:
    ZO: "-10"
    ZU: "+20"
    3O: "-90"
    6O: "-60"
    9O: "-90"
    12O: "-90"
usagegallery: 
  - image: "d/dugena/Dugena_Festa_DAU_Zifferblatt_AS_969.jpg"
    description: "Dugena Festa Damenuhr"
labor: |
  Das vorliegende Exemplar kam verharzt in die Werkstatt, wurde komplett zerlegt, gereinigt und geölt. Dennoch gab es ein Problem: Das Ankerrad-Deckplättchen drückt so sehr auf die Achse, daß das Werk zum Stillstand kommt. Möglicherweise handelt es sich hierbei nicht mehr um das Original-Decksteinplättchen?
---
Lorem Ipsum