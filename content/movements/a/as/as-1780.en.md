---
title: "AS 1780"
date: 2009-04-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1780","AS","1780","Adolph Schild","Grenchen","17 Jewels","Swiss","watch","watches","caliber","Switzerland"]
description: "AS 1780"
abstract: ""
preview_image: "as_1780-mini.jpg"
image: "AS_1780.jpg"
movementlistkey: "as"
caliberkey: "1780"
manufacturers: ["as"]
manufacturers_weight: 1780
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bwc/BWC_DAU_2.jpg"
    description: "BWC ladies' watch"
---
Lorem Ipsum