---
title: "AS 1701"
date: 2009-04-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS","AS 1700/01","1700","1701","Schild","watch","watches","wristwatch","wristwatches","movement","caliber","25","jewels"]
description: "AS 1701"
abstract: ""
preview_image: "as_1701-mini.jpg"
image: "AS_1701.jpg"
movementlistkey: "as"
caliberkey: "1701"
manufacturers: ["as"]
manufacturers_weight: 1701
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum