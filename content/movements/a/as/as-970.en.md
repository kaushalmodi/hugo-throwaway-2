---
title: "AS 970"
date: 2013-01-31T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 970","AS","970","Adolph Schild","17 Jewels","watch","watches","wristwatch","wristwatches","caliber","swiss","switzerland"]
description: "AS 970 - A popular ladies' watch movement"
abstract: "A popular and often used ladies' watch movement from the mid last century."
preview_image: "as_970-mini.jpg"
image: "AS_970.jpg"
movementlistkey: "as"
caliberkey: "970"
manufacturers: ["as"]
manufacturers_weight: 970
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/beleco/Beleco_Sport.jpg"
    description: "Beleco Sport ladies' watch"
  - image: "r/royal/Royal_DAU.jpg"
    description: "Royal ladies' watch"
  - image: "a/avia/Avia_DAU_2.jpg"
    description: "Avia ladies' watch"
---
Lorem Ipsum