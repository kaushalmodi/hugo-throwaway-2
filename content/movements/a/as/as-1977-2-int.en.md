---
title: "AS 1977-2 INT"
date: 2009-10-17T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1977-2 INT","AS 1977-2","AS 1977","INT","1977","AS","Adolph Schild","17 jewels","watch","watches","wristwatch","wristwatches","movement","caliber","17","jewels","manual winding","ladies' watch"]
description: "AS 1977-2 INT"
abstract: ""
preview_image: "as_1977_2_int-mini.jpg"
image: "AS_1977-2_INT.jpg"
movementlistkey: "as"
caliberkey: "1977-2 INT"
manufacturers: ["as"]
manufacturers_weight: 1977
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum