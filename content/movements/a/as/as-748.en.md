---
title: "AS 748"
date: 2013-01-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 748","AS","Adolph Schild","Ebauches Trust","Ebauches","Trust","15 jewels","digitalwatch","watches","wristwatch","wristwatches","movement","caliber","15","jewels","manual winding","ladies` watch","direct read"]
description: "AS 748 - A very long-shaped direct read caliber, still using the Ebauches Trust label"
abstract: "A very long shaped direct read caliber, released under the Ebauches Trust label"
preview_image: "as_748-mini.jpg"
image: "AS_748.jpg"
movementlistkey: "as"
caliberkey: "748"
manufacturers: ["as"]
manufacturers_weight: 748
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum