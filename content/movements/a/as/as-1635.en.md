---
title: "AS 1635"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1635","AS","Adolph Schild","Schild","1635","21 Jewels","Automatic","watch","watches","wristwatch","wristwatches","ladies","movement","caliber","21","jewels","screw balance"]
description: "AS 1635"
abstract: ""
preview_image: "as_1635-mini.jpg"
image: "AS_1635.jpg"
movementlistkey: "as"
caliberkey: "1635"
manufacturers: ["as"]
manufacturers_weight: 1635
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum