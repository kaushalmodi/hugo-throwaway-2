---
title: "AS 5156"
date: 2009-06-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Christoph Lorenz","AS 5156","5156","AS","Adolph Schild","Swiss","Automatic","17 Jewels","28800","mechanisch","Automatic","Schweiz","Steine"]
description: "AS 5156"
abstract: ""
preview_image: "as_5156-mini.jpg"
image: "AS_5156.jpg"
movementlistkey: "as"
caliberkey: "5156"
manufacturers: ["as"]
manufacturers_weight: 5156
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/meister-anker/Meister_Anker_DAU_Automatic_1.jpg"
    description: "Meister Anker Automatic Damenuhr"
---
Lorem Ipsum