---
title: "AS 1780"
date: 2009-04-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 1780","AS","1780","Adolph Schild","Grenchen","17 Jewels","Swiss","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","17 Steine","Schweiz"]
description: "AS 1780"
abstract: ""
preview_image: "as_1780-mini.jpg"
image: "AS_1780.jpg"
movementlistkey: "as"
caliberkey: "1780"
manufacturers: ["as"]
manufacturers_weight: 1780
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bwc/BWC_DAU_2.jpg"
    description: "BWC Damenuhr"
---
Lorem Ipsum