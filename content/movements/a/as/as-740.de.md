---
title: "AS 740"
date: 2016-01-06T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 740","Adolph Schild","Grenchen","Zylinderwerk","Zylinderhemmung","6 Jewels","6 Rubis","6 Steine","dezentrale Sekunde","EB"]
description: "AS 740 - Ein altes Zylinderwerk mit interessanter Punzierung"
abstract: "Das AS 740 ist ein recht altes Zylinderwerk, dessen hier vorgestelltes Exemplar zwei interessante Punzen vorzuweisen hat."
preview_image: "as_740-mini.jpg"
image: "AS_740.jpg"
movementlistkey: "as"
caliberkey: "740"
manufacturers: ["as"]
manufacturers_weight: 740
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk, das von Badener aus dem [UhrForum](http://uhr-forum.de/) identifiziert wurde und die dazugehörige Uhr sind eine Spende von [Klaus Brunnemer](/supporters/klaus-brunnemer/). Ganz herzlichen Dank dafür!"
usagegallery: 
  - image: "anonym/HAU_AS_740.jpg"
    description: "anonyme Herrenuhr"
labor: |
  Das vorliegende Werk kam verharzt ins Labor und wurde einem Service unterzogen. Da es sich um ein Zylinderwerk handelt, kann es mit der Zeitwaage nicht gemessen werden.
---
Lorem Ipsum