---
title: "AS 1001"
date: 2013-02-18T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1001","AS","1001","Adolph Schild","7 Jewels","watch","watches","wristwatch","wristwatches","caliber","export","USA","swiss","switzerland","7WM"]
description: "AS 1001 - here for export"
abstract: "Ein handwound movement, here for export with only 7 jewels"
preview_image: "as_1001-mini.jpg"
image: "AS_1001.jpg"
movementlistkey: "as"
caliberkey: "1001"
manufacturers: ["as"]
manufacturers_weight: 1001
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "w/westfield/Westfield_DAU.jpg"
    description: "Westfield ladies' watch"
---
Lorem Ipsum