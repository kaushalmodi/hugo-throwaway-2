---
title: "AS 1180"
date: 0209-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["AS 1180","AS","1180","Adolph Schild","17 Jewels","watch","watches","wristwatch","wristwatches","caliber","swiss","switzerland","screw balance"]
description: "AS 1180"
abstract: ""
preview_image: "as_1180-mini.jpg"
image: "AS_1180.jpg"
movementlistkey: "as"
caliberkey: "1180"
manufacturers: ["as"]
manufacturers_weight: 1180
categories: ["movements","movements_a","movements_a_as_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/monreve/MonReve_DAU.jpg"
    description: "MonReve ladies' watch"
---
Lorem Ipsum