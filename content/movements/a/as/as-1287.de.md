---
title: "AS 1287"
date: 2011-03-27T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["AS 1287","AS","Adolph Schild SA","Grenchen","17 Jewels","17 Steine","Handaufzug","Zentralsekunde","Schraubenunruh","Unishock"]
description: "AS 1287 - Ein hochwertiges Handaufzugswerk mit 17 Steinen. Detaillierte Beschreibung mit Bildern, Video und Datenblatt."
abstract: ""
preview_image: "as_1287-mini.jpg"
image: "AS_1287.jpg"
movementlistkey: "as"
caliberkey: "1287"
manufacturers: ["as"]
manufacturers_weight: 1287
categories: ["movements","movements_a","movements_a_as"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: AS 1287](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&AS_1287)
usagegallery: 
  - image: "i/invicta/Invicta_HAU_AS_1287.jpg"
    description: "Invicta Herrenuhr"
---
Lorem Ipsum