---
title: "Mido 00916P"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Mido 00916P","Mido","00916","17 Jewels","Automatic","caliber","selfwinding","swiss"]
description: "Mido 00916P"
abstract: ""
preview_image: "mido_00916p-mini.jpg"
image: "Mido_00916P.jpg"
movementlistkey: "mido"
caliberkey: "00916P"
manufacturers: ["mido"]
manufacturers_weight: 9161
categories: ["movements","movements_m","movements_m_mido_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mido/Mido_Multifort_Powerwind_2.jpg"
    description: "Mido Multifort Powerwind gents watch"
---
Lorem Ipsum