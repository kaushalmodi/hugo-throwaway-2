---
title: "Mido 917PC"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Mido 917PC","Mido 917","Mido","AS","Powerwind","17 Jewels","Incastar","swiss","Switzerland"]
description: "Mido 917PC"
abstract: ""
preview_image: "mido_917pc-mini.jpg"
image: "Mido_917PC.jpg"
movementlistkey: "mido"
caliberkey: "917PC"
manufacturers: ["mido"]
manufacturers_weight: 9172
categories: ["movements","movements_m","movements_m_mido_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mido/Mido_Multifort_Powerwind.jpg"
    description: "Mido Multifort Superautomatic Powerwind gents watch"
links: |
  * [Ranfft Uhrwerkearchiv: Mido 917PC](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&uswk&Mido_917PC) (Information about the movement and details about the Incastar system)
---
Lorem Ipsum