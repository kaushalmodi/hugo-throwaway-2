---
title: "Mido"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["m"]
language: "de"
keywords: ["Mido","Le Locle","Schweiz","Schaeren"]
categories: ["movements","movements_m"]
movementlistkey: "mido"
description: "Uhrwerke des schweizer Herstellers Mido aus Le Locle"
abstract: "Uhrwerke des schweizer Herstellers Mido aus Le Locle"
---
(MIDO G. Schaeren & Co., Le Locle, Schweiz)
{{< movementlist "mido" >}}

{{< movementgallery "mido" >}}