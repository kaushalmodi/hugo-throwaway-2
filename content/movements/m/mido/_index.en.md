---
title: "Mido"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["m"]
language: "en"
keywords: ["Mido","Le Locle","Swiss","Switzerland","Schaeren"]
categories: ["movements","movements_m"]
movementlistkey: "mido"
abstract: "Movements from the swiss manufacturer Mido of Le Locle"
description: "Movements from the swiss manufacturer Mido of Le Locle"
---
(MIDO G. Schaeren & Co., Le Locle, Switzerland)
{{< movementlist "mido" >}}

{{< movementgallery "mido" >}}