---
title: "Molnija 3602"
date: 2016-02-07T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Molnija 3602","Molnija","Molnia","3602","pocket watch","18 Jewels","18 Rubis","decentral second","small second"]
description: "Molnija 3602 - The most popular russian pocket watch movement, which was produced for almost 50 years."
abstract: "The most popular russian pocket watch movement, here in its final version."
preview_image: "molnija_3602-mini.jpg"
image: "Molnija_3602.jpg"
movementlistkey: "molnija"
caliberkey: "3602"
manufacturers: ["molnija"]
manufacturers_weight: 3602
categories: ["movements","movements_m","movements_m_molnija_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "c/corsar/Corsar_TU_Molnija_3602.jpg"
    description: "Corsar pocket watch"
donation: "This movement and the pocket watch is a kind donation of [Erwin](/supporters/erwin/)! Thank you very much!"
labor: |
  A large movement, a heavy screw balance but no shock protection - the specimen shown here had a broken balance wheel, which is not a surprise. Because of that, no service was made and of course there was also no test on the timegrapher.
---
Lorem Ipsum