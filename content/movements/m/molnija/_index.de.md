---
title: "Molnija"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["m"]
language: "de"
keywords: ["Molnija","Tscheljabinsk","Russland","UdSSR"]
categories: ["movements","movements_m"]
movementlistkey: "molnija"
description: "Uhrwerke des ehemaligen Herstellers Molnija aus Tscheljabinsk, Russland"
abstract: "Uhrwerke des ehemaligen Herstellers Molnija aus Tscheljabinsk, Russland"
---
(Molnija, Tscheljabinsk, Russland)
{{< movementlist "molnija" >}}

{{< movementgallery "molnija" >}}