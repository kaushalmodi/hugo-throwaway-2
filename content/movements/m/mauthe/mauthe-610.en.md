---
title: "Mauthe 610"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Mauthe","Mauthe 610","610","19 Jewels","19 Rubis","black wood forest","Germany","germany"]
description: "Mauthe 610"
abstract: ""
preview_image: "mauthe_610-mini.jpg"
image: "Mauthe_610.jpg"
movementlistkey: "mauthe"
caliberkey: "610"
manufacturers: ["mauthe"]
manufacturers_weight: 610
categories: ["movements","movements_m","movements_m_mauthe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum