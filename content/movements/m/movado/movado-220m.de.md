---
title: "Movado 220M"
date: 2017-09-27T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Movado 220M","Movado","220M","Pendelautomatic","Hammerautomatic","Pendelschwungmasse","Bumper","15 Jewels","Zentralsekunde"]
description: "Movado 220M - das früheste Selbstaufzugswerk von Movado, ausgestattet mit einer Pendelschwungmasse."
abstract: "Eines der frühesten serienmäßigen Automaticwerke aus der Schweiz. Es besitzt keinen Rotor, sondern einen Pendelaufzug und ist hervorragend verarbeitet."
preview_image: "movado_220m-mini.jpg"
image: "Movado_220M.jpg"
movementlistkey: "movado"
caliberkey: "220M"
manufacturers: ["movado"]
manufacturers_weight: 220
categories: ["movements","movements_m","movements_m_movado"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Das hier vorgestellt Werk kam vermutlich ausgeschalt aus einer Golduhr in gutem Zustand ins Labor, daher war keine Revision notwendig. Nichtsdestotrotz ist es sehr bedauerlich, daß dieses Werk nicht wertgeschätzt wurde und für ein paar wenige Euro seines Gehäuses beraubt wurde - nach fast 80 Jahren!
timegrapher_old: 
  rates:
    ZO: "-1"
    ZU: "+9"
    3O: "+11"
    6O: "+37"
    9O: "-1"
    12O: "-9"
  amplitudes:
    ZO: "284"
    ZU: "272"
    3O: "233"
    6O: "222"
    9O: "230"
    12O: "225"
  beaterrors:
    ZO: "0.2"
    ZU: "0.5"
    3O: "0.6"
    6O: "0.1"
    9O: "0.4"
    12O: "0.9"
usagegallery: 
  - image: "m/movado/Movado_Automatic_HAU_Movado_220M.jpg"
    description: "Movado Automatic Herrenuhr  (ohne Gehäuse und Zeiger)"
---
Lorem Ipsum