---
title: "Movado"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["m"]
language: "en"
keywords: ["Movado","Switzerland","Swiss","La Chaux-de-Fonds"]
categories: ["movements","movements_m"]
movementlistkey: "movado"
description: "Movements of the swiss manufacturer Movado from the village of La Chaux-de-Fonds"
abstract: "Movements of the swiss manufacturer Movado from the village of La Chaux-de-Fonds"
---
(Movado, La Chaux-de-Fonds, Switzerland)
{{< movementlist "movado" >}}

{{< movementgallery "movado" >}}