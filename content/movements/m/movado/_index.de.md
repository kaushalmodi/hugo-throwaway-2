---
title: "Movado"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["m"]
language: "de"
keywords: ["Movado","Schweiz","La Chaux-de-Fonds"]
categories: ["movements","movements_m"]
movementlistkey: "movado"
description: "Uhrwerke des schweizer Herstellers Movado aus La Chaux-de-Fonds"
abstract: "Uhrwerke des schweizer Herstellers Movado aus La Chaux-de-Fonds"
---
(Movado, La Chaux-de-Fonds, Schweiz)
{{< movementlist "movado" >}}

{{< movementgallery "movado" >}}