---
title: "Movado 220M"
date: 2017-09-29T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Movado 220M","Movado","220M","selfwindig","automatic","bumper","15 Jewels","center second","screw balance"]
description: "Movado 220M - the earliest selfwinding movement from Movado. It uses a bumper winding mechanism"
abstract: "One of the earliest swiss selfwinding movements. It does not have an oscillating weight, but uses a bumper mechanism and is very well made."
preview_image: "movado_220m-mini.jpg"
image: "Movado_220M.jpg"
movementlistkey: "movado"
caliberkey: "220M"
manufacturers: ["movado"]
manufacturers_weight: 220
categories: ["movements","movements_m","movements_m_movado"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "m/movado/Movado_Automatic_HAU_Movado_220M.jpg"
    description: "Movado Automatic gents watch  (w/o case and golden hands)"
timegrapher_old: 
  rates:
    ZO: "-1"
    ZU: "+9"
    3O: "+11"
    6O: "+37"
    9O: "-1"
    12O: "-9"
  amplitudes:
    ZO: "284"
    ZU: "272"
    3O: "233"
    6O: "222"
    9O: "230"
    12O: "225"
  beaterrors:
    ZO: "0.2"
    ZU: "0.5"
    3O: "0.6"
    6O: "0.1"
    9O: "0.4"
    12O: "0.9"
labor: |
  The specimen shown here was probably taken off a gold watch and was in good condition, so no revision was made. Nevertheless it is very sad, that this fantastic movement was not appreciated properly and robbed its case - after almost 80 years!
---
Lorem Ipsum