---
title: "MST 374"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["MST 374","MST","374","Medana","15 Jewels","Meyer & Stüdeli","swiss","Switzerland","pin lever"]
description: "MST 374 "
abstract: ""
preview_image: "mst_374-mini.jpg"
image: "MST_374.jpg"
movementlistkey: "mst"
caliberkey: "374"
manufacturers: ["mst"]
manufacturers_weight: 374
categories: ["movements","movements_m","movements_m_mst_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/medana/Medana_HAU.jpg"
    description: "Medana gents watch"
---
Lorem Ipsum