---
title: "MST 963"
date: 2011-01-15T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Roamer","pillar construction","MST 963","MST","963","Solothurn","Swiss","Lever","17 Jewels","swiss","switzerland","selfwinding"]
description: "MST 963 - A rather simple pallet lever movement with 17 jewels. Detailed description with photos, video, data sheet and timegrapher protocol."
abstract: ""
preview_image: "mst_963-mini.jpg"
image: "MST_963.jpg"
movementlistkey: "mst"
caliberkey: "963"
manufacturers: ["mst"]
manufacturers_weight: 963
categories: ["movements","movements_m","movements_m_mst_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/medana/Medana_Lever_HAU_MST_963.jpg"
    description: "Medana Lever gents watch"
timegrapher_old: 
  description: |
    The specimen shown here came in much used and dirty condition into the lab and was dissected completely and cleaned afterwards. The movement got the same treatment: It was disassabled, cleaned and oiled. According to the condition of the dial, at least once water got into the watch, but did not really harm the movement.     Although it was a bit difficult to adjust, on the timegrapher, the MST 963 specimen showed very good results: A maximum overall deviation of 25 seconds is very good for a movement, which as used (and abused) for several decades!
  images:
    ZO: Zeitwaage_MST_963_ZO.jpg
    ZU: Zeitwaage_MST_963_ZU.jpg
    3O: Zeitwaage_MST_963_3O.jpg
    6O: Zeitwaage_MST_963_6O.jpg
    9O: Zeitwaage_MST_963_9O.jpg
    12O: Zeitwaage_MST_963_12O.jpg
links: |
  * [Ranfft Uhren: MST 963](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&MST_963)
---
Lorem Ipsum