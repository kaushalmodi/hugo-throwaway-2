---
title: "Wostok"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["w"]
language: "de"
keywords: ["Wostok","Chistopol","Russland","UdSSR"]
categories: ["movements","movements_w"]
movementlistkey: "wostok"
abstract: "Uhrwerke des russischen Herstellers Wostok aus Chistopol."
description: "Uhrwerke des russischen Herstellers Wostok aus Chistopol."
---
(Wostok, Chistopol, Russland)
{{< movementlist "wostok" >}}

{{< movementgallery "wostok" >}}