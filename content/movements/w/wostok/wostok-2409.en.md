---
title: "Wostok 2409"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Wostok 2409","Wostok","2409","17 Jewels","mechanical","Russia","USSR","Sowjet union"]
description: "Wostok 2409"
abstract: ""
preview_image: "wostok_2409-mini.jpg"
image: "Wostok_2409.jpg"
movementlistkey: "wostok"
caliberkey: "2409"
manufacturers: ["wostok"]
manufacturers_weight: 240900
categories: ["movements","movements_w","movements_w_wostok_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "w/wostok/Wostok_Taucheruhr_HA.jpg"
    description: "Wostok divers' watch"
---
Lorem Ipsum