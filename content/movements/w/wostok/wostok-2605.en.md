---
title: "Wostok 2605"
date: 2018-02-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Wostok 2605","Wostok","2605","17 Jewels","CCCP","USSR","17 Rubis","UdSSR","Russia","screw balance","date"]
description: "Wostok 2605 - An aesthetically pleasant manual wind movement from Russia."
abstract: "An aesthetically very pleasant russian movement with screw balance and stripes decoration"
preview_image: "wostok_2605-mini.jpg"
image: "Wostok_2605.jpg"
movementlistkey: "wostok"
caliberkey: "2605"
manufacturers: ["wostok"]
manufacturers_weight: 260500
categories: ["movements","movements_w","movements_w_wostok_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "w/wostok/Wostok_HU_Wostok_2605.jpg"
    description: "Wostok gents watch"
timegrapher_old: 
  rates:
    ZO: "+94"
    ZU: "+98"
    3O: "-50"
    6O: "-20"
    9O: "-114"
    12O: "-7"
  amplitudes:
    ZO: "323"
    ZU: "298"
    3O: "209"
    6O: "220"
    9O: "206"
    12O: "216"
  beaterrors:
    ZO: "1.5"
    ZU: "1.4"
    3O: "2.3"
    6O: "2.4"
    9O: "2.3"
    12O: "1.7"
labor: |
  The tested specimen showed signs of wear and water damage on the dial. The movement looked pretty much OK, but nevertheless, it was cleaned and oiled.
---
Lorem Ipsum