---
title: "Wostok 2416"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Wostok","Wostok 2416","2416","30","jewels","30 jewels","30 Jewels","automatic","selfwinding","russia"]
description: "Wostok 2416"
abstract: ""
preview_image: "wostok_2416-mini.jpg"
image: "Wostok_2416.jpg"
movementlistkey: "wostok"
caliberkey: "2416"
manufacturers: ["wostok"]
manufacturers_weight: 241600
categories: ["movements","movements_w","movements_w_wostok_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum