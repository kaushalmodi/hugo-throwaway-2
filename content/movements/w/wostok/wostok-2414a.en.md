---
title: "Wostok 2414A"
date: 2017-01-12T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Wostok 2414A","Wostok","2414A","17 Jewels","CCCP","USSR","17 Steine","UdSSR","Russland"]
description: "Wostok 2414A - A very common russian manual wind movement"
abstract: "One of the most common any widley used russian manual wind movement, here in its first revision."
preview_image: "wostok_2414a-mini.jpg"
image: "Wostok_2414A.jpg"
movementlistkey: "wostok"
caliberkey: "2414A"
manufacturers: ["wostok"]
manufacturers_weight: 241401
categories: ["movements","movements_w","movements_w_wostok_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The tested specimen was well working, when it came into the lab and hence was not serviced, but only adjusted a bit.
donation: "This movement and the three shown watches are a kind donation of the [Mr. and Mrs. Ditte](/supporters/ditte/). Thank you very much for the support of the movement archive!"
timegrapher_old: 
  description: |
    The rates are pretty well, nevertheless on a serviced or brand new specimen, they will be much better:
  rates:
    ZO: "+7"
    ZU: "-1"
    3O: "+25"
    6O: "+10"
    9O: "-90"
    12O: "-70"
  amplitudes:
    ZO: "222"
    ZU: "205"
    3O: "192"
    6O: "188"
    9O: "171"
    12O: "172"
  beaterrors:
    ZO: "0.0"
    ZU: "0.0"
    3O: "0.1"
    6O: "0.1"
    9O: "0.1"
    12O: "0.1"
usagegallery: 
  - image: "w/wostok/Wostok_Kommandirski_HAU_Wostok_2414A.jpg"
    description: "Wostok Kommandirski gents watch"
  - image: "w/wostok/Wostok_Kommandirski_HAU_Wostok_2414A_2.jpg"
    description: "Wostok Kommandirski gents watch"
  - image: "w/wostok/Wostok_Kommandirski_HAU_Wostok_2414A_Russia.jpg"
    description: "Wostok Kommandirski gents watch with original bracelet"
---
Lorem Ipsum