---
title: "Wostok"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["w"]
language: "en"
keywords: ["Wostok","Chistopol","Russia","USSR"]
categories: ["movements","movements_w"]
movementlistkey: "wostok"
abstract: "Movements of the russian manufacturer Wostok of Chistopol"
description: "Movements of the russian manufacturer Wostok of Chistopol"
---
(Wostok, Chistopol, Russia)
{{< movementlist "wostok" >}}

{{< movementgallery "wostok" >}}