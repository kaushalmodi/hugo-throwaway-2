---
title: "Würthner"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["w"]
language: "de"
keywords: ["Würthner","Deisslingen","Deutschland","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_w"]
movementlistkey: "wuerthner"
abstract: "Uhrwerke des deutschen Herstellers Würthner (Georg Würthner GmbH) aus Deisslingen"
description: "Uhrwerke des deutschen Herstellers Würthner (Georg Würthner GmbH) aus Deisslingen"
---
(Georg Würthner GmbH, Deisslingen, Deutschland)
{{< movementlist "wuerthner" >}}

{{< movementgallery "wuerthner" >}}