---
title: "Certina"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "de"
keywords: []
categories: ["movements","movements_c"]
movementlistkey: "certina"
description: ""
abstract: "(Kurth Frères, Grenchen, Schweiz)"
---
(Kurth Frères, Grenchen, Schweiz)
{{< movementlist "certina" >}}

{{< movementgallery "certina" >}}