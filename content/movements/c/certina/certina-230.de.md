---
title: "Certina 230"
date: 2010-05-16T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Grana 230","K.F.230","Schraubenunruh","Incabloc","Perlierung","Kupplungsaufzug","Certina 230","Certina","230","Grana 230","K.F. 230","Kurth Freres","17 Jewels","Grenchen","Schweiz","17 Steine"]
description: "Certina 230 - Ein sehr gutes 5 1/4'''- Formwerk mit 17 Steinen. Detaillierte Beschreibung mit Bildern, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "certina_230-mini.jpg"
image: "Certina_230.jpg"
movementlistkey: "certina"
caliberkey: "230"
manufacturers: ["certina"]
manufacturers_weight: 230
categories: ["movements","movements_c","movements_c_certina"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/certina/Certina_DAU_Certina_230.jpg"
    description: "Certina Damenuhr"
timegrapher_old: 
  description: |
    Auf der Zeitwaage, deren Gangstreifen in doppelter Genauigkeit gedruckt wurden, macht dieses Werk vor allem hinsichtlich seiner geringen Größe und seines Alters von geschätzten 60 Jahren, einen guten Eindruck. Besonders auffällig ist der extrem niedrige Gangfehler, erkennbar daran, daß die Punktelinien sehr schmal sind. Hier hatten seinerzeit die Regulateure bei Certina ganze Arbeit geleistet! Die Abweichungen halten sich sehr im Rahmen, die beiden Außreißer bei "3 oben" und "9 oben" dürften sich beim Tragen in der Praxis mehr oder weniger ausgeleichen. Für ein Werk dieser Größe sind die Gangabweichungen jedenfalls sehr gut und beweisen die hohe Verarbeitungsqualität des Certina 230.
  images:
    ZO: Zeitwaage_Certina_230_ZO.jpg
    ZU: Zeitwaage_Certina_230_ZU.jpg
    3O: Zeitwaage_Certina_230_3O.jpg
    6O: Zeitwaage_Certina_230_6O.jpg
    9O: Zeitwaage_Certina_230_9O.jpg
    12O: Zeitwaage_Certina_230_12O.jpg
  values:
    ZO: "-8"
    ZU: "+13"
    3O: "-20"
    6O: "+1"
    9O: "+30"
    12O: "+5"
labor: |
  Dieses Werk kam in relativ gutem Zustand ins Labor, da jedoch die Uhr außenrum in schlechtem Zustand war (fehlendes Glas, angelaufenes Zifferblatt), wurde vorsorglich eine Komplettreinigung samt Ölung vorgenommen.
---
Lorem Ipsum