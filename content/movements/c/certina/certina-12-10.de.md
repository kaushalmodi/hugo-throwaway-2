---
title: "Certina 12-10"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Certina 12-10","Certina","12-10","Kurth Freres","Kurth","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Uhrwerk","17 Steine","Damenuhr","Handaufzug"]
description: "Certina 12-10"
abstract: ""
preview_image: "certina_12-10-mini.jpg"
image: "Certina_12-10.jpg"
movementlistkey: "certina"
caliberkey: "12-10"
manufacturers: ["certina"]
manufacturers_weight: 1210
categories: ["movements","movements_c","movements_c_certina"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/certina/Certina_DAU.jpg"
    description: "Certina Damenuhr"
---
Lorem Ipsum