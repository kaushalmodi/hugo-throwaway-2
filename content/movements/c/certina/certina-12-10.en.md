---
title: "Certina 12-10"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Certina 12-10","Certina","12-10","Kurth Freres","Kurth","17 Jewels","watch","watches","wristwatch","wristwatches","movement","selfwinding","caliber","Swiss","ladies` watch","handwinding"]
description: "Certina 12-10"
abstract: ""
preview_image: "certina_12-10-mini.jpg"
image: "Certina_12-10.jpg"
movementlistkey: "certina"
caliberkey: "12-10"
manufacturers: ["certina"]
manufacturers_weight: 1210
categories: ["movements","movements_c","movements_c_certina_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/certina/Certina_DAU.jpg"
    description: "Certina ladies' watch"
---
Lorem Ipsum