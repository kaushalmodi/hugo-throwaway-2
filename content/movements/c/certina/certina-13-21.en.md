---
title: "Certina 13-21"
date: 2013-12-27T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Certina 13-21","Certina","Grana","Kurth Freres","handwind","manual wind","17 Jewels","excenter","fine adjustment"]
description: "Certina 13-21 - a really tiny high quality movement with excenter fine adjustment"
abstract: "A very small, high quality ladies' watch movement with excenter fine adjustment"
preview_image: "certina_13-21-mini.jpg"
image: "Certina_13-21.jpg"
movementlistkey: "certina"
caliberkey: "13-21"
manufacturers: ["certina"]
manufacturers_weight: 1321
categories: ["movements","movements_c","movements_c_certina_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The specimen shown here was in very good condition, so that servicing it was not neccessiary. The timegrapher rates show the original condition of that movement, which was taken out of a watch, and sent via mail, housed only in a newspaper sheet. The detail photos were taken from a second specimen, which was defective.
timegrapher_old: 
  description: |
    For a used and not serviced movement, especially so tiny, the rates are very good. It's clear, why Certina was once famous for their precision and reliability. In some positions, there's a rather large beat error, but that doesn't affect the precision very much. Only on "crown up", it looks, like the balance is not perfectly poised, maybe a tiny dust piece of the uncleaned movement was responsible for that.
  images:
    ZO: Zeitwaage_Certina_13-21_ZO.jpg
    ZU: Zeitwaage_Certina_13-21_ZU.jpg
    3O: Zeitwaage_Certina_13-21_3O.jpg
    6O: Zeitwaage_Certina_13-21_6O.jpg
    9O: Zeitwaage_Certina_13-21_9O.jpg
    12O: Zeitwaage_Certina_13-21_12O.jpg
  values:
    ZO: "+15"
    ZU: "+-0"
    3O: "-25"
    6O: "+-0"
    9O: "+10"
    12O: "+7"
usagegallery: 
  - image: "c/certina/Certina_DAU_0806_433_Certina_13-21.jpg"
    description: "Certina Damenuhr Modell 0806 433"
  - image: "c/certina/Certina_DAU_Zifferblatt_Certina_13-21.jpg"
    description: "Certina Damenuhr (nur Zifferblatt)"
---
Lorem Ipsum