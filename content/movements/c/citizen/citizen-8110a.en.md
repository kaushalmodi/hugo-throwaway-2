---
title: "Citizen 8110A"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 8110","Citizen","Citizen 8110A","8110","8110A","23","jewels","chrono","chronograph"]
description: "Citizen 8110A"
abstract: ""
preview_image: "citizen_8110a-mini.jpg"
image: "Citizen_8110A.jpg"
movementlistkey: "citizen"
caliberkey: "8110A"
manufacturers: ["citizen"]
manufacturers_weight: 8110
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_Chronograph_Automatic.jpg"
    description: "Citizen Chronograph Automatic"
---
Lorem Ipsum