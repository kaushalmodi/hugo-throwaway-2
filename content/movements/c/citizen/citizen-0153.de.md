---
title: "Citizen 0153"
date: 2009-04-10T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 0153","Citizen","Miyota","0153","17 jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Werk","17","Steine","Jewels","Handaufzug","Damenuhr"]
description: "Citizen 0153"
abstract: ""
preview_image: "citizen_0153-mini.jpg"
image: "Citizen_0153.jpg"
movementlistkey: "citizen"
caliberkey: "0153"
manufacturers: ["citizen"]
manufacturers_weight: 153
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_DAU_HA_3.jpg"
    description: "Citizen Damenuhr"
---
Lorem Ipsum