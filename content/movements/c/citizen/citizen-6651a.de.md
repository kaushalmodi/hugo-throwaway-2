---
title: "Citizen 6651A"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 6651A","Citizen","6651A","6T51","Miyota 6T51","21 Jewels","Automatic","Kaliber","21 Steine","Japan","Damenuhr","Damenautomatic","Automatik"]
description: "Citizen 6651A"
abstract: ""
preview_image: "citizen_6651a-mini.jpg"
image: "Citizen_6651A.jpg"
movementlistkey: "citizen"
caliberkey: "6651A"
manufacturers: ["citizen"]
manufacturers_weight: 6651
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_DAU_Automatic_3.jpg"
    description: "Citizen Automatic Damenuhr"
---
Lorem Ipsum