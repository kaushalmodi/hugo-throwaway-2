---
title: "Citizen 6651A"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 6651A","Citizen","6651A","6T51","Miyota 6T51","21 Jewels","Japan","ladies` automatic","selfwinding"]
description: "Citizen 6651A"
abstract: ""
preview_image: "citizen_6651a-mini.jpg"
image: "Citizen_6651A.jpg"
movementlistkey: "citizen"
caliberkey: "6651A"
manufacturers: ["citizen"]
manufacturers_weight: 6651
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_DAU_Automatic_3.jpg"
    description: "Citizen ladies' automatic"
---
Lorem Ipsum