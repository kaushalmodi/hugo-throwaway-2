---
title: "Citizen 8203A"
date: 2014-10-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 8203A","Miyota 8205","Citizen","Miyota","Automatic","selfwinding","21 Jewels","day","date"]
description: "Citizen 8203A - the anchestor of the Miyota 8205, which is one of the most commonly used fully equipped japanese selfwinding movements."
abstract: "The anchestor of the Miyota 8205, which is one of the most commonly used fully equipped selfwindig movements from japan today"
preview_image: "citizen_8203a-mini.jpg"
image: "Citizen_8203A.jpg"
movementlistkey: "citizen"
caliberkey: "8203A"
manufacturers: ["citizen"]
manufacturers_weight: 8203
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  <p>On the specimen shown here, the friction of the cannon pinion was too low and had to be corrected. There was also a full service made, but unfortunately, the gunked mainspring still sticks together, so that the rates are terrible and later re-servicing is required.</p><p>Because of that, there are no timegrapher measurings.</p>
usagegallery: 
  - image: "c/citizen/Citizen_HAU_Citizen_8203A.jpg"
    description: "Citizen mens' watch Modell GN-4W-S"
---
Lorem Ipsum