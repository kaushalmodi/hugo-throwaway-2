---
title: "Citizen 2340A"
date: 2009-04-10T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 2340A","Citizen 2340","Citizen","2340","2340A","watch","watches","ladies` watch","wristwatch","wristwatches","movement","caliber","17","jewels","28800","manual wind"]
description: "Citizen 2340A"
abstract: ""
preview_image: "citizen_2340a-mini.jpg"
image: "Citizen_2340A.jpg"
movementlistkey: "citizen"
caliberkey: "2340A"
manufacturers: ["citizen"]
manufacturers_weight: 2340
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_DAU_HA_1.jpg"
    description: "Citizen ladies' watch"
  - image: "c/citizen/Citizen_DAU_HA_2.jpg"
    description: "Citizen ladies' watch"
---
Lorem Ipsum