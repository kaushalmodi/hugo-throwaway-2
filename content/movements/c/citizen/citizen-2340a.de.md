---
title: "Citizen 2340A"
date: 2009-04-10T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Citizen 2340A","Citizen 2340","Citizen","2340","2340A","Uhr","Uhren","Armbanduhr","Damenuhr","Armbanduhren","Kaliber","Werk","17","Steine","Jewels","Handaufzug"]
description: "Citizen 2340A"
abstract: ""
preview_image: "citizen_2340a-mini.jpg"
image: "Citizen_2340A.jpg"
movementlistkey: "citizen"
caliberkey: "2340A"
manufacturers: ["citizen"]
manufacturers_weight: 2340
categories: ["movements","movements_c","movements_c_citizen"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_DAU_HA_1.jpg"
    description: "Citizen Damenuhr"
  - image: "c/citizen/Citizen_DAU_HA_2.jpg"
    description: "Citizen Damenuhr"
---
Lorem Ipsum