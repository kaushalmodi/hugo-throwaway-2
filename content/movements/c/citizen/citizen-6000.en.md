---
title: "Citizen 6000"
date: 2009-04-09T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Citizen 6000","Citizen","6000","21 Jewels","Japan","Automatic","selfwinding"]
description: "Citizen 6000"
abstract: ""
preview_image: "citizen_6000-mini.jpg"
image: "Citizen_6000.jpg"
movementlistkey: "citizen"
caliberkey: "6000"
manufacturers: ["citizen"]
manufacturers_weight: 6000
categories: ["movements","movements_c","movements_c_citizen_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "c/citizen/Citizen_HAU_Automatic.jpg"
    description: "Citizen gents watch"
---
Lorem Ipsum