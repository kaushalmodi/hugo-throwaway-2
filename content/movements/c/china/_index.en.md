---
title: "China"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: ["China"]
categories: ["movements","movements_c"]
movementlistkey: "china"
abstract: "Chinese movements, whose manufacturer could not be determined"
description: "Chinese movements, whose manufacturer could not be determined"
---
(chinese movements of unidentified manufacturers)
{{< movementlist "china" >}}

{{< movementgallery "china" >}}