---
title: "China"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "de"
keywords: ["China"]
categories: ["movements","movements_c"]
movementlistkey: "china"
abstract: "Chinesische Uhrwerke, deren  Hersteller nicht ermittelt werden konnte"
description: "Chinesische Uhrwerke, deren  Hersteller nicht ermittelt werden konnte"
---
(Werke unbekannter Herkunft aus China)
{{< movementlist "china" >}}

{{< movementgallery "china" >}}