---
title: "China R16-1"
date: 2016-12-27T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["China R16-1","Automatic","selfwinding","Gyrotron","big date","decentral second","decentral minute","day/night disc","24h indicator"]
description: "China R16-1 - A selfwinding movement with big date indication and several useless additional indicators. "
abstract: "A chinese selfwinding movement with a large date indication and many usless additional indicators on subdials"
preview_image: "china_r16-1-mini.jpg"
image: "China_R16-1.jpg"
movementlistkey: "china"
caliberkey: "R16-1"
manufacturers: ["china"]
manufacturers_weight: 161
categories: ["movements","movements_c","movements_c_china_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>The specimen shown here was worn for a certain time (can be seen on the fully tarnished crown), but had to be cleaned due to bad rates and contamimation with metal scrapings. Nevertheless, the rates stay poor and the date indication mechanism rarely works.</p><p>After a light shock, the movement completely lost its time-consuming regulation. Because of the fragile regulation mechanism, another new regulation was not made.</p>
donation: "This movement and watch was kindly donated by [Günter G](/supporters/guenter-g/). Thank you very much for supporting the movement archive!"
usagegallery: 
  - image: "anonym/Fake_HAU_China_R16-1.jpg"
    description: "Fake watch from China"
---
Lorem Ipsum