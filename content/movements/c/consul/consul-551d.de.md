---
title: "Consul 551D"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Consul","551","551D","Consul 551","Consul 551D","15 Jewels","15 Steine"]
description: "Consul 551D"
abstract: ""
preview_image: "consul_551d-mini.jpg"
image: "Consul_551D.jpg"
movementlistkey: "consul"
caliberkey: "551D"
manufacturers: ["consul"]
manufacturers_weight: 551
categories: ["movements","movements_c","movements_c_consul"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum