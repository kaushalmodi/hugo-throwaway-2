---
title: "CRC"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: ["CRC","Cupillard","Vuez","Rieme","Morteau","France"]
categories: ["movements","movements_c"]
movementlistkey: "crc"
description: "Movements from CRC, Cupillard-Vuez et Rieme, Morteau, France"
abstract: "Movements from CRC, Cupillard-Vuez et Rieme, Morteau, France"
---
(CRC, Cupillard-Vuez et Rieme, Morteau, France)
{{< movementlist "crc" >}}

{{< movementgallery "crc" >}}