---
title: "CRC"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "de"
keywords: ["CRC","Cupillard","Vuez","Rieme","Morteau","Frankreich"]
categories: ["movements","movements_c"]
movementlistkey: "crc"
abstract: "Uhrwerke von CRC, einer Firma von Cupillard-Vuez et Rieme aus Morteau, Frankreich."
description: "Uhrwerke von CRC, einer Firma von Cupillard-Vuez et Rieme aus Morteau, Frankreich."
---
(CRC, Cupillard-Vuez et Rieme, Morteau, Frankreich)
{{< movementlist "crc" >}}

{{< movementgallery "crc" >}}