---
title: "Cortébert"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "de"
keywords: []
categories: ["movements","movements_c"]
movementlistkey: "cortebert"
description: ""
abstract: "(Cortébert Watch & Co., Cortébert, Schweiz)"
---
(Cortébert Watch & Co., Cortébert, Schweiz)
{{< movementlist "cortebert" >}}

{{< movementgallery "cortebert" >}}