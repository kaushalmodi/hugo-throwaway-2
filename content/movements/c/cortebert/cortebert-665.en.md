---
title: "Cortébert 665"
date: 2015-10-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Cortebert 665","Cortebert","17 Jewels","17 Rubis","pallet lever","Chatons","decentral second"]
description: "Cortebert 665 - a high quality swiss pallet lever movement with 17 jewels, part of them beared in chatons"
abstract: "A very nice appealing swiss movement with 17 jewels, some of them even beared in chatons."
preview_image: "cortebert_665-mini.jpg"
image: "Cortebert_665.jpg"
movementlistkey: "cortebert"
caliberkey: "665"
manufacturers: ["cortebert"]
manufacturers_weight: 665
categories: ["movements","movements_c","movements_c_cortebert_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "c/cortebert/Cortebert_HAU_Cortebert_665.jpg"
    description: "Cortebert mens' watch (U.S. export version)"
labor: |
  <p>The specimen shown here came in a fragment of a case with missing crystal and non fitting back. It was glued(!) into the case together with the dial. The glue couldn't fully be removed, there were even parts on the balance cock, which stayed and hence, the movement cannot be regulated any more. Additionally, the hairspring is a bit damaged with glue.</p><p>The movement got a full service since it was gummed up. It now works at least, but with +190 seconds per day in position "dial down". But in consideration of the massive glue damage, this is surprisingly good and shows, but well the movement and its bearings are made.</p>
---
Lorem Ipsum