---
title: "Cortébert"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: []
categories: ["movements","movements_c"]
movementlistkey: "cortebert"
abstract: "Movements of the swiss manufacturer Cortébert"
description: "Movements of the swiss manufacturer Cortébert"
---
(Cortébert Watch & Co., Cortébert, Switzerland)
{{< movementlist "cortebert" >}}

{{< movementgallery "cortebert" >}}