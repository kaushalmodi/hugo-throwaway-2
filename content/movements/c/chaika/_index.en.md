---
title: "Chaika"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: ["Chaika","Uglitch","USSR","Russia","Sowiet Union","Sowjet Union"]
categories: ["movements","movements_c"]
movementlistkey: "chaika"
description: "Movements from the russian manufacturer Chaika"
abstract: "Movements from the russian manufacturer Chaika"
---
(Chaika, Uglitch, USSR)
{{< movementlist "chaika" >}}

{{< movementgallery "chaika" >}}