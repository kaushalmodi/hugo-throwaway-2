---
title: "Chaika"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "de"
keywords: ["Chaika","Uglitch","Rußland","Sowietunion","Sowjetunion","UdSSR"]
categories: ["movements","movements_c"]
movementlistkey: "chaika"
description: "Uhrwerke des russischen Herstellers Chaika aus Uglitch"
abstract: "Uhrwerke des russischen Herstellers Chaika aus Uglitch"
---
(Chaika, Uglitch, Rußland)
{{< movementlist "chaika" >}}

{{< movementgallery "chaika" >}}