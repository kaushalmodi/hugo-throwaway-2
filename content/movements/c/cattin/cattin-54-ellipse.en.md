---
title: "Cattin 54 (Ellipse)"
date: 2017-01-04T15:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Cattin 54","Cattin","Morteau","France","pin lever","french","center second"]
description: "Cattin 54 (Ellipse) - A very rare french pin lever movement"
abstract: ""
preview_image: "cattin_54-mini.jpg"
image: "Cattin_54.jpg"
movementlistkey: "cattin"
caliberkey: "54 (Ellipse)"
manufacturers: ["cattin"]
manufacturers_weight: 540
categories: ["movements","movements_c","movements_c_cattin_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "m/mortima/Mortima_HU_Cattin_54.jpg"
    description: "Mortima gents watch"
---
Lorem Ipsum