---
title: "Cattin 66 (CLD)"
date: 2009-04-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Uhrenbastler","Cattin 66 (CLD)","Cattin","66 (CLD)","17 Jewels","21 Jewels","watch","watches","wristwatch","wristwatches","caliber","pin lever"]
description: "Cattin 66 (CLD)"
abstract: ""
preview_image: "cattin_66_cld-mini.jpg"
image: "Cattin_66_cld.jpg"
movementlistkey: "cattin"
caliberkey: "66 (CLD)"
manufacturers: ["cattin"]
manufacturers_weight: 661
categories: ["movements","movements_c","movements_c_cattin_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/korwina/Korwina_Mayerling.jpg"
    description: "Korwina Mayerling mens' watch"
---
Lorem Ipsum