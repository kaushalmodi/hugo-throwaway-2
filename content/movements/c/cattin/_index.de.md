---
title: "Cattin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "de"
keywords: ["Cattin","Les Breuleux","Schweiz"]
categories: ["movements","movements_c"]
movementlistkey: "cattin"
abstract: "Uhrwerke des schweizer Herstellers Cattin aus Les Beuleux"
description: "Uhrwerke des schweizer Herstellers Cattin aus Les Beuleux"
---
(Cattin & Cie. SA, Les Breuleux, Schweiz)
{{< movementlist "cattin" >}}

{{< movementgallery "cattin" >}}