---
title: "Cattin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["c"]
language: "en"
keywords: ["Cattin","Les Breuleux","Swiss","Switzerland"]
categories: ["movements","movements_c"]
movementlistkey: "cattin"
abstract: "Movements of the swiss manufacturer Cattin from Les Breuleux."
description: "Movements of the swiss manufacturer Cattin from Les Breuleux."
---
(Cattin & Cie. SA, Les Breuleux, Switzerland)
{{< movementlist "cattin" >}}

{{< movementgallery "cattin" >}}