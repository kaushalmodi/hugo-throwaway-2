---
title: "Cattin 66"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Cattin 66","Cattin","66","17 Jewels","21 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Stiftanker","17 Steine","21 Steine","15 Steine"]
description: "Cattin 66"
abstract: ""
preview_image: "cattin_66-mini.jpg"
image: "Cattin_66.jpg"
movementlistkey: "cattin"
caliberkey: "66"
manufacturers: ["cattin"]
manufacturers_weight: 660
categories: ["movements","movements_c","movements_c_cattin"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "h/h_w/H_und_W_HAU.jpg"
    description: "H &amp; W Herrenuhr"
---
Lorem Ipsum