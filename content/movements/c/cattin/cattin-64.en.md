---
title: "Cattin 64"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Cattin 64","Cattin","64","17 Jewels","watch","watches","wristwatch","wristwatches","caliber","pin lever","form movement"]
description: "Cattin 64"
abstract: ""
preview_image: "cattin_64-mini.jpg"
image: "Cattin_64.jpg"
movementlistkey: "cattin"
caliberkey: "64"
manufacturers: ["cattin"]
manufacturers_weight: 640
categories: ["movements","movements_c","movements_c_cattin_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/lady/Lady_DeLuxe.jpg"
    description: "Lady De Luxe ladies' watch"
---
Lorem Ipsum