---
title: "Judex 120"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Judex 120","Judex","120","15 Jewels","15 Steine","Frankreich"]
description: "Judex 120"
abstract: ""
preview_image: "judex_120-mini.jpg"
image: "Judex_120.jpg"
movementlistkey: "judex"
caliberkey: "120"
manufacturers: ["judex"]
manufacturers_weight: 120
categories: ["movements","movements_j","movements_j_judex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/judex/Judex.jpg"
    description: "Judex Herrenuhr"
---
Lorem Ipsum