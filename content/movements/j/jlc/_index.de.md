---
title: "JLC"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "de"
keywords: ["JLC","Jaeger-LeCoultre","Jaeger","LeCoultre","Le Sentier","Schweiz","Swiss"]
categories: ["movements","movements_j"]
movementlistkey: "jlc"
description: "Uhrwerke des schweizer Herstellers Jaeger-LeCoultre (JLC) aus Le Sentier."
abstract: "Uhrwerke des schweizer Herstellers Jaeger-LeCoultre (JLC) aus Le Sentier."
---
(Jaeger-LeCoultre, Le Sentier, Schweiz)
{{< movementlist "jlc" >}}

{{< movementgallery "jlc" >}}