---
title: "Junghans J93/1 (693.83)"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans","J93/1","693.83","Schramberg","17 Steine","Trilastic","Trishock","Germany"]
description: "Junghans J93/1"
abstract: ""
preview_image: "junghans_j_93_1-mini.jpg"
image: "Junghans_J_93_1.jpg"
movementlistkey: "junghans"
caliberkey: "J93/1"
manufacturers: ["junghans"]
manufacturers_weight: 69383
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Junghans 693.83 (J93/1)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Junghans_693.83)
usagegallery: 
  - image: "j/junghans/Junghans_HAU.jpg"
    description: "Junghans gents watch"
---
Lorem Ipsum