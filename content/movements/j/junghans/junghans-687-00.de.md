---
title: "Junghans 687.00 (J87/10)"
date: 2014-09-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Junghans","Junghans 687","J687","687.00","J87/10","17 Jewels","17 Rubis","17 Steine","Kupplungsaufzug","Handaufzug","Palettenanker","Zentralsekunde"]
description: "Junghans 687 - ein großes Herrenuhrenwerk mit Handaufzug aus den 60er Jahren, das mit Ankerhemmung, 17 Steinen und Kupplungsaufzug bestmöglich ausgestattet war."
abstract: "Ein großes (11 1/2 liniges) Herrenuhrenwerk mit Handaufzug aus den 60er Jahren, das mit Ankerhemmung, 17 Steinen und Kupplungsaufzug bestmöglich ausgestattet war."
preview_image: "junghans_687-mini.jpg"
image: "Junghans_687.jpg"
movementlistkey: "junghans"
caliberkey: "687.00"
manufacturers: ["junghans"]
manufacturers_weight: 68700
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam ungereinigt ins Labor und wurde lediglich minimal justiert, um einen leichten Nachgang zu kompensieren.
donation: "Dieses Werk ist in, in Form von Uhr (1) eine Spende von [Frau Müller](/supporters/g-mueller/) und in Form von Uhr (2) eine [anonyme Spende](/supporters/anonym/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
timegrapher_old: 
  images:
    ZO: Zeitwaage_Junghans_687_ZO.jpg
    ZU: Zeitwaage_Junghans_687_ZU.jpg
    3O: Zeitwaage_Junghans_687_3O.jpg
    6O: Zeitwaage_Junghans_687_6O.jpg
    9O: Zeitwaage_Junghans_687_9O.jpg
    12O: Zeitwaage_Junghans_687_12O.jpg
  values:
    ZO: "+10"
    ZU: "-8"
    3O: "+-0"
    6O: "-30"
    9O: "-10"
    12O: "+20"
usagegallery: 
  - image: "j/junghans/Junghans_HAU_Junghans_687.jpg"
    description: "Junghans Herrenuhr (1)"
  - image: "j/junghans/Junghans_HAU_2_Junghans_687.jpg"
    description: "Junghans Herrenuhr (2)"
---
Lorem Ipsum