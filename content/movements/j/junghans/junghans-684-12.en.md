---
title: "Junghans 684.12"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans 684.12","Junghans","684.12","Junghans Meister","17 Jewels","Chronometer","swan neck","precision","Meister","germany","black forest","Schramberg"]
description: "Junghans 684.12"
abstract: ""
preview_image: "junghans_684_12-mini.jpg"
image: "Junghans_684_12.jpg"
movementlistkey: "junghans"
caliberkey: "684.12"
manufacturers: ["junghans"]
manufacturers_weight: 68412
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Junghans 684.12](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Junghans_684_12)
usagegallery: 
  - image: "j/junghans/Junghans_Meister.jpg"
    description: "Junghans Meister gents watch"
---
Lorem Ipsum