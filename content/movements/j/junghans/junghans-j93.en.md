---
title: "Junghans J93 (693.81)"
date: 2010-05-23T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["decentral second","two-leg ring balance","Junghans 693.81","rocking bar winding mechanismn","click spring","Junghans J93","Junghans","J93","15 Jewels","Schramberg","693.81","Germany","Black forest"]
description: "Junghans J93 - A good mens' handwinding movement with 15 jewels. Detailed description with photos, macro image, data sheet and timegrapher protocol."
abstract: ""
preview_image: "junghans_j_93-mini.jpg"
image: "Junghans_J_93.jpg"
movementlistkey: "junghans"
caliberkey: "J93"
manufacturers: ["junghans"]
manufacturers_weight: 69381
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Junghans 693.81 (J93)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Junghans_693_81)</p><p>This movement was donated by [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
timegrapher_old: 
  description: |
    The movement shown here smelled very strong like a ship diesel engine. Because of that it was completely cleaned and oiled, but the smell stayed. The technical condition is quite OK, especially, when you take the age and long useage of the watch into account.     On the timegrapher (here in double precision), the fair condition could not be verified, unfortunately; the J 93 showed big variations in all positions, ranging from -2 1/2 minutes per day to +40 seconds per day.
  images:
    ZO: Zeitwaage_Junghans_J_93_ZO.jpg
    ZU: Zeitwaage_Junghans_J_93_ZU.jpg
    3O: Zeitwaage_Junghans_J_93_3O.jpg
    6O: Zeitwaage_Junghans_J_93_6O.jpg
    9O: Zeitwaage_Junghans_J_93_9O.jpg
    12O: Zeitwaage_Junghans_J_93_12O.jpg
usagegallery: 
  - image: "z/zeda/Zeda_HAU_Junghans_J_93.jpg"
    description: "Zeda gents watch"
---
Lorem Ipsum