---
title: "Junghans"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "de"
keywords: ["Junghans","Schramberg","Deutschland"]
categories: ["movements","movements_j"]
movementlistkey: "junghans"
abstract: "Uhrwerke des deutschen Herstellers Junghans aus Schramberg."
description: "Uhrwerke des deutschen Herstellers Junghans aus Schramberg."
---
(Junghans Uhren GmbH, Schramberg, Deutschland)
{{< movementlist "junghans" >}}

{{< movementgallery "junghans" >}}