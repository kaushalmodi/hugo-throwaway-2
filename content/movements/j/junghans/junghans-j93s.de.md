---
title: "Junghans J93S (693.10)"
date: 2018-01-27T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Junghans","J93S","693.10","Schramberg","15 Jewels","15 Steine","Palettenanker"]
description: "Junghans J93S - Ein gängiges Handaufzugswerk mit dezentraler Sekunde und Palettenanker"
abstract: "Eine der vielen Versionen des Junghans J93, hier mit dezentraler Sekunde"
preview_image: "junghans_j93s-mini.jpg"
image: "Junghans_J93S.jpg"
movementlistkey: "junghans"
caliberkey: "J93S"
manufacturers: ["junghans"]
manufacturers_weight: 69310
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "j/junghans/Junghans_HU_Junghans_J93S.jpg"
    description: "Junghans Herrenuhr"
---
Lorem Ipsum