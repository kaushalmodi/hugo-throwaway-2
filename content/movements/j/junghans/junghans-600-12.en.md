---
title: "Junghans 600.12"
date: 2011-01-21T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans 600.12","Junghans","600.12","600.12","ato-chron","electronic","17","jewels","17 jewels","17 Jewels","black forest","germany"]
description: "Junghans 600.12"
abstract: ""
preview_image: "junghans_600_12-mini.jpg"
image: "Junghans_600_12.jpg"
movementlistkey: "junghans"
caliberkey: "600.12"
manufacturers: ["junghans"]
manufacturers_weight: 60012
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Junghans electronic Ato-Chron   (Very informative page about the movement)](http://www.hwynen.de/jgh-600.html)
usagegallery: 
  - image: "j/junghans/Junghans_DatoChron_HAU_Junghans_600_12.jpg"
    description: "Junghans DatoChron gents watch"
---
Lorem Ipsum