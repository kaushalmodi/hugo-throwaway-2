---
title: "Junghans 670/5"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Junghans","Junghans 670/5","17","Steine","Jewels","DeutschlandSchwarzwald","Schramberg"]
description: "Junghans 670/5"
abstract: ""
preview_image: "junghans_670_5-mini.jpg"
image: "Junghans_670_5.jpg"
movementlistkey: "junghans"
caliberkey: "670/5"
manufacturers: ["junghans"]
manufacturers_weight: 67005
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum