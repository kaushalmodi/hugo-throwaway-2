---
title: "Junghans J93S10 (693.02)"
date: 2010-10-09T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Minutenradkloben","direkt angetriebener Zentralsekunde","Rückerzeiger","Junghans J93S10","Junghans","J93S10","693.02","17 Jewels","Schramberg","Schwarzwald","Deutschland","17 Steine","Handaufzug"]
description: "Junghans J93S10 - Ein verbreitetes Ankerwerk mit 17 Steinen und Datumsanzeige. Detaillierte Beschreibung mit Bildern und Datenblatt."
abstract: ""
preview_image: "junghans_j_93s10-mini.jpg"
image: "Junghans_J_93S10.jpg"
movementlistkey: "junghans"
caliberkey: "J93S10"
manufacturers: ["junghans"]
manufacturers_weight: 69302
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Junghans 693.02 (J93S10)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Junghans_693_02)
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
usagegallery: 
  - image: "j/junghans/Junghans_Trilastic_HAU_Junghans_J93_S10.jpg"
    description: "Junghans Trilastic Herrenuhr  (nur Zifferblatt)"
---
Lorem Ipsum