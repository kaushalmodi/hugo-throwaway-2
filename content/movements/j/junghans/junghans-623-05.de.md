---
title: "Junghans 623.05"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Junghans","623.05","623","15 Jewels","15 Steine","Deutschland"]
description: "Junghans 623.05"
abstract: ""
preview_image: "junghans_623_05-mini.jpg"
image: "Junghans_623_05.jpg"
movementlistkey: "junghans"
caliberkey: "623.05"
manufacturers: ["junghans"]
manufacturers_weight: 62305
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/diehl/Diehl_compact.jpg"
    description: "Diehl Compact"
---
Lorem Ipsum