---
title: "Junghans J99/1 (699.71)"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["J99/1","Junghans","J99","699.71","15 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Werk","Damenuhr","17 Steine","Formwerk"]
description: "Junghans J99/1"
abstract: ""
preview_image: "junghans_j_99_1-mini.jpg"
image: "Junghans_J_99_1.jpg"
movementlistkey: "junghans"
caliberkey: "J99/1"
manufacturers: ["junghans"]
manufacturers_weight: 69971
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/junghans/Junghans_DAU_3.jpg"
    description: "Junghans Damenuhr"
donation: "Dieses Werk samt Uhr wurde von [Sigrid Schmiedel-Köster](/supporters/junghans/) gespendet. Ganz herzlichen Dank dafür!"
---
Lorem Ipsum