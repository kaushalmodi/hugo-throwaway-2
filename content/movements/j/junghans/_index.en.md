---
title: "Junghans"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "en"
keywords: ["Junghans","Schramberg","Germany","german"]
categories: ["movements","movements_j"]
movementlistkey: "junghans"
description: "Movements of the german manufacturer Junghans from Schramberg."
abstract: "Movements of the german manufacturer Junghans from Schramberg."
---
(Junghans Uhren GmbH, Schramberg, Germany)
{{< movementlist "junghans" >}}

{{< movementgallery "junghans" >}}