---
title: "Junghans J73"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Junghans J73","Junghans","J73","673","17 Jewels","5 Jewels","5 Steine","5 Rubis","17 Steine","Formwerk"]
description: "Junghans J73"
abstract: ""
preview_image: "junghans_j_73-mini.jpg"
image: "Junghans_J_73.jpg"
movementlistkey: "junghans"
caliberkey: "J73"
manufacturers: ["junghans"]
manufacturers_weight: 7300
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/junghans/Junghans_DAU.jpg"
    description: "Junghans Damenuhr"
  - image: "j/junghans/Junghans_DAU_2.jpg"
    description: "Junghans Damenuhr"
---
Lorem Ipsum