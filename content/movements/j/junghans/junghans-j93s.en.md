---
title: "Junghans J93S (693.10)"
date: 2018-02-02T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Junghans","J93S","693.10","Schramberg","15 Jewels","15 Rubis","pallet lever","decentral second"]
description: "Junghans J93S - A common manual wind movement which was often used in Junghans watches in the 1950ies"
abstract: "One of the many versions of the Junghans J93, here with decentral seconds indication."
preview_image: "junghans_j93s-mini.jpg"
image: "Junghans_J93S.jpg"
movementlistkey: "junghans"
caliberkey: "J93S"
manufacturers: ["junghans"]
manufacturers_weight: 69310
categories: ["movements","movements_j","movements_j_junghans_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "j/junghans/Junghans_HU_Junghans_J93S.jpg"
    description: "Junghans gents watch"
---
Lorem Ipsum