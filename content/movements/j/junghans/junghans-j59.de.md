---
title: "Junghans J59"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Junghans J 59","Junghans","J 59","15 Jewels","Schramberg","15 Steine","Deutschland","Schwarzwald"]
description: "Junghans J 59"
abstract: ""
preview_image: "junghans_j_59-mini.jpg"
image: "Junghans_J_59.jpg"
movementlistkey: "junghans"
caliberkey: "J59"
manufacturers: ["junghans"]
manufacturers_weight: 5900
categories: ["movements","movements_j","movements_j_junghans"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/junghans/Junghans_Anonym_Silber.jpg"
    description: "Junghans Silberuhr"
---
Lorem Ipsum