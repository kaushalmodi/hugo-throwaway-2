---
title: "Jeambrun"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "de"
keywords: ["Jeambrun","Fabrique d'ébauches Jeambrun","Frankreich","Maîche","Uhrwerk","Kaliber"]
categories: ["movements","movements_j"]
movementlistkey: "jeambrun"
description: "Uhrwerke des franzözischen Herstellers Jeambrun (Fabrique d'ébauches Jeambrun) aus Maîche, Frankreich"
abstract: "Uhrwerke des franzözischen Herstellers Jeambrun (Fabrique d'ébauches Jeambrun) aus Maîche, Frankreich"
---
(Fabrique d'ébauches Jeambrun, Maîche, Frankreich)
{{< movementlist "jeambrun" >}}

{{< movementgallery "jeambrun" >}}