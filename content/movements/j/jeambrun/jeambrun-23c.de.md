---
title: "Jeambrun 23C"
date: 2015-02-28T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Jeambrun 23C","Jeambrun","Zylinder","Cylinder","5 Jewels","5 Rubis","5 Steine"]
description: "Jeambrun 23C - eines der letzten französischen Zylinderwerke"
abstract: " Ein spätes und von der Ausführung her schon recht modernes Zylinderwerk aus Frankreich"
preview_image: "jeambrun_23c-mini.jpg"
image: "Jeambrun_23C.jpg"
movementlistkey: "jeambrun"
caliberkey: "23C"
manufacturers: ["jeambrun"]
manufacturers_weight: 23
categories: ["movements","movements_j","movements_j_jeambrun"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "anonym/HAU_Jeambrun_23C.jpg"
    description: "anonyme Herrenuhr um 1940"
---
Lorem Ipsum