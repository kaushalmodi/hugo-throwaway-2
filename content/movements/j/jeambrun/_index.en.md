---
title: "Jeambrun"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["j"]
language: "en"
keywords: ["Jeambrun","Fabrique d'ébauches Jeambrun","Maîche","France","movement","caliber"]
categories: ["movements","movements_j"]
movementlistkey: "jeambrun"
abstract: "Movements of the french manufacturer Jeambrun (Fabrique d'ébauches Jeambrun) in Maîche. "
description: "Movements of the french manufacturer Jeambrun (Fabrique d'ébauches Jeambrun) in Maîche. "
---
(Fabrique d'ébauches Jeambrun, Maîche, France)
{{< movementlist "jeambrun" >}}

{{< movementgallery "jeambrun" >}}