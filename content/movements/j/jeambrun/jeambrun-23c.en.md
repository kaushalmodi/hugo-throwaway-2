---
title: "Jeambrun 23C"
date: 2015-03-01T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Jeambrun 23C","Jeambrun","Cylinder","5 Jewels","5 Rubis"]
description: "Jeambrun 23C - one of the last french cylinder movements"
abstract: "A late but yet pretty modern appealing french cylinder movement  "
preview_image: "jeambrun_23c-mini.jpg"
image: "Jeambrun_23C.jpg"
movementlistkey: "jeambrun"
caliberkey: "23C"
manufacturers: ["jeambrun"]
manufacturers_weight: 23
categories: ["movements","movements_j","movements_j_jeambrun_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "anonym/HAU_Jeambrun_23C.jpg"
    description: "anonymous mens' watch around 1940"
---
Lorem Ipsum