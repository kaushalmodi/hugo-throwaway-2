---
title: "Brac 502"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Brac 502","Brac","502","Swiss","1 Jewel","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","Stiftanker"]
description: "Brac 502"
abstract: ""
preview_image: "brac_502-mini.jpg"
image: "Brac_502.jpg"
movementlistkey: "brac"
caliberkey: "502"
manufacturers: ["brac"]
manufacturers_weight: 502
categories: ["movements","movements_b","movements_b_brac"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/orion/Orion_HAU.jpg"
    description: "Orion Herrenuhr"
---
Lorem Ipsum