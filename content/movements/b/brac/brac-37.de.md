---
title: "Brac 37"
date: 2009-05-08T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Christoph Lorenz","Uhrenbastler","Brac 37","Brac","37","Swiss","0 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","Stiftanker"]
description: "Brac 37"
abstract: ""
preview_image: "brac_37-mini.jpg"
image: "Brac_37.jpg"
movementlistkey: "brac"
caliberkey: "37"
manufacturers: ["brac"]
manufacturers_weight: 37
categories: ["movements","movements_b","movements_b_brac"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/rewatch/ReWatch.jpg"
    description: "ReWatch Herrenuhr"
  - image: "m/mentor/Mentor_Special_HAU.jpg"
    description: "Mentor Special Herrenuhr"
---
Lorem Ipsum