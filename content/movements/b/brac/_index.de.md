---
title: "Brac"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "de"
keywords: ["Brac","Breitenbach","Solothurn"]
categories: ["movements","movements_b"]
movementlistkey: "brac"
abstract: "Uhrwerke des schweizerischen Herstellers Brac aus Breitenbach (Solothurn)"
description: "Uhrwerke des schweizerischen Herstellers Brac aus Breitenbach (Solothurn)"
---
(Brac, Breitenbach (Solothurn), Schweiz)
{{< movementlist "brac" >}}

{{< movementgallery "brac" >}}