---
title: "Brac 190"
date: 2009-06-21T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Brac 190","Brac","190","Swiss","17 Jewels","watch","watches","wristwatch","wristwatches","pin lever","Switzerland"]
description: "Brac 190"
abstract: ""
preview_image: "brac_190-mini.jpg"
image: "Brac_190.jpg"
movementlistkey: "brac"
caliberkey: "190"
manufacturers: ["brac"]
manufacturers_weight: 190
categories: ["movements","movements_b","movements_b_brac_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "j/josmar/Josmar_HAU.jpg"
    description: "Josmar mens' watch"
links: |
  * [Ranfft Watches: Brac 190](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Brac_190)
---
Lorem Ipsum