---
title: "Brac"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: ["Brac","Swiss","Breitenbach","Solothurn"]
categories: ["movements","movements_b"]
movementlistkey: "brac"
description: "Movements of the swiss manufacturer Brac from Breitenbach in the canton of Solothurn"
abstract: "Movements of the swiss manufacturer Brac from Breitenbach in the canton of Solothurn"
---
(Brac, Breitenbach (Solothurn), Switzerland)
{{< movementlist "brac" >}}

{{< movementgallery "brac" >}}