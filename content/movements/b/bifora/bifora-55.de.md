---
title: "Bifora 55"
date: 2015-10-11T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora","Bidlingmaier","Schwäbisch Gmünd","Formwerk","15 Jewels","15 Rubis","Schraubenunruh"]
description: "Bifora 55 - ein frühes Damenuhren-Formwerk "
abstract: "Ein frühes Damenuhren-Formwerk von Bifora"
preview_image: "bifora_55-mini.jpg"
image: "Bifora_55.jpg"
movementlistkey: "bifora"
caliberkey: "55"
manufacturers: ["bifora"]
manufacturers_weight: 5500
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Vielen Dank an [Herrn Keilhammer](/supporters/g-keilhammer/) für die Spende dieses Werks!"
usagegallery: 
  - image: "b/bifora/Bifora_Ancre_DAU_Bifora_55.jpg"
    description: "Bifora Ancre Damenuhr"
labor: |
  Leider ist das vorliegende Werk nicht stoßgesichert und so kam es nicht überraschend mit gebrochener Unruhwelle in die Werkstatt. Ein Service und eine Justierung auf der Zeitwaage erübrigten sich somit.
---
Lorem Ipsum