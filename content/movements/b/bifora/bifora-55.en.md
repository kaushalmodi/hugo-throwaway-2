---
title: "Bifora 55"
date: 2015-10-17T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora","Bidlingmaier","Schwäbisch Gmünd","Germany","Bifora 55","form movement","15 jewels","15 rubis","screw balance"]
description: "Bifora 55 - an early ladies' watch form movement"
abstract: "An early ladies' watch form movement from Bifora."
preview_image: "bifora_55-mini.jpg"
image: "Bifora_55.jpg"
movementlistkey: "bifora"
caliberkey: "55"
manufacturers: ["bifora"]
manufacturers_weight: 5500
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Unfortunately, the specimen shown here is not shock protected and so suffers of a broken balance staff. Hence no service and regulation was made.
donation: "Many thanks go to [Mr. Keilhammer](/supporters/g-keilhammer/) for the donation of that movement!"
usagegallery: 
  - image: "b/bifora/Bifora_Ancre_DAU_Bifora_55.jpg"
    description: "Bifora Ancre ladies' watch"
---
Lorem Ipsum