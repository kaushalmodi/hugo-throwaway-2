---
title: "Bifora 68 CLD / Dugena 709"
date: 2009-06-21T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Uhrenbastler","Bifora 68 CLD","Bifora","68 CLD","17 Jewels","Bidlingmaier","watch","watches","wristwatch","wristwatches","caliber","german","germany"]
description: "Bifora 68 CLD"
abstract: ""
preview_image: "bifora_68_cld-mini.jpg"
image: "Bifora_68_CLD.jpg"
movementlistkey: "bifora"
caliberkey: "68 CLD"
manufacturers: ["bifora"]
manufacturers_weight: 6850
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_Calendar_DAU.jpg"
    description: "Dugena Calendar ladies' watch"
links: |
  * [Ranfft Watches: Bifora 68 (CLD)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Bifora_68_CLD)
---
Lorem Ipsum