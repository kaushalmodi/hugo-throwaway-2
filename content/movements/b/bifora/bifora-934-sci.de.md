---
title: "Bifora 934 SCI / Dugena 704"
date: 2010-01-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 934","Dugena 704","17 Jewels","Bidlingmaier","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Werk","17 Steine"]
description: "Bifora 934 / Dugena 704"
abstract: ""
preview_image: "bifora_934sci-mini.jpg"
image: "Bifora_934SCI.jpg"
movementlistkey: "bifora"
caliberkey: "934 SCI"
manufacturers: ["bifora"]
manufacturers_weight: 93450
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_Jongster.jpg"
    description: "Dugena Jongster Herrenuhr"
---
Lorem Ipsum