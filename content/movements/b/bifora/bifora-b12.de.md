---
title: "Bifora B12"
date: 2017-03-25T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora B12","Quarz"]
description: "Bifora B12 - Das erste hauseigene Quarzwerk von Bifora. Kurzvorstellung mit Video."
abstract: "Das erste Quarzwerk von Bifora, das vom Aufbau her noch sehr an ein eletromechanisches Werk erinnert und mit einem Schaltanker arbeitet."
preview_image: "bifora_b12-mini.jpg"
image: "Bifora_B12.jpg"
movementlistkey: "bifora"
caliberkey: "B12"
manufacturers: ["bifora"]
manufacturers_weight: 1200
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
links: |
  * [Crazywatches.pl: Bifora B12 (1973)](http://www.crazywatches.pl/bifora-b12-quartz-1973)
  * [Hartmut Wynen: Bifora](http://www.hwynen.de/bifora.html)
labor: |
  <p>Das vorliegende Exemplar ist voll funktionsfähig, verliert aber ab und an ein paar Sekunden pro Tag, was sich pro Woche auf etwa eine Minute summiert. Das Fehlerbild deutet entweder auf ein nicht mehr ausreichend geöltes Werk (wäre nach 40 Jahren kein Wunder) oder aber auf einen gealterten Quarz hin, für den es kaum noch Ersatz geben dürfte.</p><p>Für Archivzwecke ist dies aber unerheblich, da das Werk prinzipiell läuft.</p>
usagegallery: 
  - image: "b/bifora/Bifora_Quartz_HU_Bifora_B12.jpg"
    description: "Bifora Quartz Herrenuhr"
---
Lorem Ipsum