---
title: "Bifora 70"
date: 2017-10-20T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 70","Handaufzug","16 Jewels","16 Rubis","Palettenanker","7 1/4 Linien"]
description: "Bifora 70 - das kleinste runde Handaufzugswerk von Bifora"
abstract: "Das kleinste runde Handaufzugswerk von Bifora aus Schwäbisch Gmünd"
preview_image: "bifora_70-mini.jpg"
image: "Bifora_70.jpg"
movementlistkey: "bifora"
caliberkey: "70"
manufacturers: ["bifora"]
manufacturers_weight: 7000
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk samt Bifora-Uhr ist eine Spende von [Götz Schweitzer](/supporters/goetz-schweitzer/) an das Uhrwerksarchiv. Ganz herzlichen Dank für die tolle Unterstützung!"
usagegallery: 
  - image: "b/bifora/Bifora_DAU_Bifora_70.jpg"
    description: "Bifora Damenuhr"
  - image: "d/dugena/Dugena_DAU_Bifora_70.jpg"
    description: "Dugena Damenuhr"
timegrapher_old: 
  rates:
    ZO: "+7"
    ZU: "+9"
    3O: "-90"
    6O: "-150"
    9O: "-50"
    12O: "-2"
  amplitudes:
    ZO: "206"
    ZU: "197"
    3O: "177"
    6O: "161"
    9O: "144"
    12O: "163"
  beaterrors:
    ZO: "2.8"
    ZU: "2.0"
    3O: "2.0"
    6O: "2.6"
    9O: "2.6"
    12O: "2.0"
labor: |
  <p>Das vorliegende Exemplar kam in funktionsfähigem Zustand ins Labor, so daß auf eine Reinigung und Ölung verzichtet wurde.</p><p>Bei dem zweiten Exemplar, jenes, das als Dugena-Werk ausgezeichnet war, ist die Unruhwelle gebrochen, und auch die Welle des Minutenrads.</p>
---
Lorem Ipsum