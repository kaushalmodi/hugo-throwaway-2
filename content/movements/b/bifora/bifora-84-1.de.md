---
title: "Bifora 84/1"
date: 2009-11-14T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 84/1","Bifora","84/1","Bidlingmaier","15 Jewels","Joseph Bidlingmaier","Schwäbisch Gmünd","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Deutschland","15 Steine"]
description: "Bifora 84/!"
abstract: ""
preview_image: "bifora_84_1-mini.jpg"
image: "Bifora_84_1.jpg"
movementlistkey: "bifora"
caliberkey: "84/1"
manufacturers: ["bifora"]
manufacturers_weight: 8410
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum