---
title: "Bifora"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "de"
keywords: ["Bifora","Bidlingmaier","Schwäbisch Gmünd","BIdlingmaierFOrmAnker"]
categories: ["movements","movements_b"]
movementlistkey: "bifora"
abstract: "Uhrwerke der Marke Bifora des deutschen Herstellers Bidlingmaier aus Schwäbisch Gmünd."
description: "Uhrwerke der Marke Bifora des deutschen Herstellers Bidlingmaier aus Schwäbisch Gmünd."
---
(Josef Bidlingmaier, Schwäbisch Gmünd, Deutschland)
{{< movementlist "bifora" >}}

{{< movementgallery "bifora" >}}