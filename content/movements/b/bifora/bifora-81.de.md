---
title: "Bifora 81"
date: 2015-11-15T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora","Bidlingmaier","Schwäbisch Gmünd","17 Jewels","Bishock","17 Steine","Zentralsekunde","17 Rubis"]
description: "Bifora 81 - ein kleines, recht hoch bauendes Handaufzugswerk für Damenuhren mit Zentralsekunde"
abstract: "Ein Handaufzugswerk für Damenuhren, das wegen seiner direkten Zentralsekunden-Indikation ungewöhnlich hoch gebaut ist. Wie üblich bei Bifora, so wird auch hier ein zifferblattseitiges Gesperr verwendet."
preview_image: "bifora_81-mini.jpg"
image: "Bifora_81.jpg"
movementlistkey: "bifora"
caliberkey: "81"
manufacturers: ["bifora"]
manufacturers_weight: 8100
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>Das vorliegende Werk kam verharzt und mit losem Unruhkloben ins Labor. Nach einer intensiven Reinigung stellt sich beim Zusammenbau leider heraus, daß die Unruhspirale nicht mehr ihre Form hält und an den Schenkeln schleift, was sich für einen Laien nicht beheben läßt. Der extrem gedrängte Aufbau im Bereich der Unruh macht dies alles auch nicht einfacher.</p><p>Kurz gesagt, das Werk könnte theoretisch wieder laufen, die Unruhspirale steht dem aber leider entgegen.</p>
donation: "Ganz herzlichen Dank an [Erwin](/supporters/erwin/) für die Spende von Uhr und Werk!"
usagegallery: 
  - image: "anonym/DAU_Bifora_81.jpg"
    description: "anonyme Damenuhr, evtl. von Bifora"
---
Lorem Ipsum