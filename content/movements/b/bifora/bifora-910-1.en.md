---
title: "Bifora 910/1"
date: 2014-11-02T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 910/1","Bifora","Automatic","pillar construction","23 Jewels","selfwinding"]
description: "Bifora 910/1 - A german midsize selfwinding movement from Schwäbisch Gmünd"
abstract: "A rather simple selfwinding movement from Germany in pillar construction, but with pallet lever and a selfwinding system which works in both directions. "
preview_image: "bifora_910_1-mini.jpg"
image: "Bifora_910_1.jpg"
movementlistkey: "bifora"
caliberkey: "910/1"
manufacturers: ["bifora"]
manufacturers_weight: 91010
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  The movement came into a watch with missing back in very bad and dirty condition and got a full service treatmement. And although the rates are pretty poor (see below), the movement was at least brought back to life.
timegrapher_old: 
  images:
    ZO: Zeitwaage_Bifora_910_1_ZO.jpg
    ZU: Zeitwaage_Bifora_910_1_ZU.jpg
    3O: Zeitwaage_Bifora_910_1_3O.jpg
    6O: Zeitwaage_Bifora_910_1_6O.jpg
    9O: Zeitwaage_Bifora_910_1_9O.jpg
    12O: Zeitwaage_Bifora_910_1_12O.jpg
  values:
    ZO: "-30"
    ZU: "+30"
    3O: "-120"
    6O: "-140"
    9O: "-?"
    12O: "-105"
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_910_1.jpg"
    description: "Bifora mens' watch"
---
Lorem Ipsum