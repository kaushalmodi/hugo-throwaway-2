---
title: "Bifora 734"
date: 2017-11-04T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 734","manual wind","15 Jewels","15 Rubis","pallet lever","7 3/4 lignes","screw balance","center second"]
description: "Bifora 734 - A very hard to find tiny manual wind movement from Bidlingmaier in Schwäbisch Gmünd, Germany"
abstract: "The Bifora 734, a tiny manual wind movement from Schwäbisch Gmünd in Germany is a true rarity."
preview_image: "bifora_734-mini.jpg"
image: "Bifora_734.jpg"
movementlistkey: "bifora"
caliberkey: "734"
manufacturers: ["bifora"]
manufacturers_weight: 73400
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum