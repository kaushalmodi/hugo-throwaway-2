---
title: "Bifora 91/1"
date: 2009-04-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Bifora 91/1","Bifora 91","Bifora","Bidlingmaier","Bishock","watch","watches","wristwatch","wristwatches","movement","caliber","17","jewels"]
description: "Bifora 91/1"
abstract: ""
preview_image: "bifora_91_1-mini.jpg"
image: "Bifora_91_1.jpg"
movementlistkey: "bifora"
caliberkey: "91/1"
manufacturers: ["bifora"]
manufacturers_weight: 9110
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bifora/Bifora_DAU.jpg"
    description: "Bifora ladies' watch"
  - image: "b/bifora/Bifora_DAU_2.jpg"
    description: "Bifora ladies' watch"
---
Lorem Ipsum