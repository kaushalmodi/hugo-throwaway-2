---
title: "Bifora 934"
date: 2010-01-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Glucydur","Schraubenunruh","Bishock","kleine Sekunde","zifferblattseitiges Gesperr","Pfeileraufbau","Wippenaufzug","Bifora 934","Bifora","934","17 Jewels","Bidlingmaier","Schwäbisch Gmünd","Deutschland","17 Steine"]
description: "Bifora 934 - DAS Standardwerk von Bifora, hier in der wohl besten Ausführung."
abstract: ""
preview_image: "bifora_934-mini.jpg"
image: "Bifora_934.jpg"
movementlistkey: "bifora"
caliberkey: "934"
manufacturers: ["bifora"]
manufacturers_weight: 93400
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_934.jpg"
    description: "Bifora Herrenuhr"
timegrapher_old: 
  description: |
    Das vorliegende Exemplar kam in komplett verharztem Zustand in die Werkstatt und wurde gereinigt und geölt. Dennoch sind die Zeitwaagenwerte stark schwankend, wobei insbesondere die hängenden Positionen, allen voran "12 oben" und "9 oben" massive Ausreißer zeigen. Entweder ist die Unruh schlecht ausbalanciert, oder die Unruhwelle ist verbogen.
  images:
    ZO: Zeitwaage_Bifora_934_ZO.jpg
    ZU: Zeitwaage_Bifora_934_ZU.jpg
    3O: Zeitwaage_Bifora_934_3O.jpg
    6O: Zeitwaage_Bifora_934_6O.jpg
    9O: Zeitwaage_Bifora_934_9O.jpg
    12O: Zeitwaage_Bifora_934_12O.jpg
  values:
    ZO: "+45"
    ZU: "+50"
    3O: "-120"
    6O: "+6"
    9O: "-240"
    12O: "-240"
links: |
  * [Ranfft Uhren: Bifora 934](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Bifora_934)
---
Lorem Ipsum