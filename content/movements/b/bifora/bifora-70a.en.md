---
title: "Bifora 70A"
date: 2017-10-31T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 70A","Automatic","selfwinding","7 1/4 lignes","ladies' watch","24 Jewels. 24 Rubis"]
description: "Bifora 70A - The smallest selfwinding movement from Bifora in Schwäbisch Gmünd, Germany"
abstract: "The smallest selfwinding movement from Bifora in Germany had a diameter of only 16mm!"
preview_image: "bifora_70a-mini.jpg"
image: "Bifora_70A.jpg"
movementlistkey: "bifora"
caliberkey: "70A"
manufacturers: ["bifora"]
manufacturers_weight: 7050
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement including watch in good condition  is a kind donation of [Götz Schweitzer](/supporters/goetz-schweitzer/) to the movement archive. Thank you very much for your great support!"
usagegallery: 
  - image: "b/bifora/Bifora_DAU_Bifora_70A.jpg"
    description: "Bifora Automatic ladies' watch"
  - image: "b/bifora/Bifora_DAU_2_Bifora_70A.jpg"
    description: "Bifora Automatic ladies' watch"
---
Lorem Ipsum