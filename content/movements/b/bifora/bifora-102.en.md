---
title: "Bifora 102"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 102","Bifora","Bidlingmaier","watch","watches","wristwatch","wristwatches","movement","caliber","15","jewels","screw balance"]
description: "Bifora 102"
abstract: ""
preview_image: "bifora_102-mini.jpg"
image: "Bifora_102.jpg"
movementlistkey: "bifora"
caliberkey: "102"
manufacturers: ["bifora"]
manufacturers_weight: 10200
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum