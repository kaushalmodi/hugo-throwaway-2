---
title: "Bifora 49"
date: 2017-12-11T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 49","manual wind","17 Jewels","17 Rubis","pallet lever","form movement","screw balance"]
description: "Bifora 49 - The smallest manual wind movement from Bifora"
abstract: "The smallest movement from Bifora is not only visually pleasant, but also very well made."
preview_image: "bifora_49-mini.jpg"
image: "Bifora_49.jpg"
movementlistkey: "bifora"
caliberkey: "49"
manufacturers: ["bifora"]
manufacturers_weight: 49
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement with the Bifora watch is a kind donation from [Götz Schweitzer](/supporters/goetz-schweitzer/). Thank you very much for the great support of the movement archive!"
usagegallery: 
  - image: "b/bifora/Bifora_DU_Bifora_49.jpg"
    description: "Bifora ladies' watch"
---
Lorem Ipsum