---
title: "Bifora 735"
date: 2017-11-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 735","Handaufzug","17 Jewels","17 Rubis","Palettenanker","7 3/4 Linien","Schraubenunruh","Zentralsekunde","Incabloc"]
description: "Bifora 735 - Ein sehr selten zu findendes Handaufzugswerk von Bidlingmaier aus Schwäbisch Gmünd"
abstract: "Ein ebenfalls sehr selten zu findendes Handaufzugswerk mit Zentralsekunde in Damenuhrengröße aus Schwäbisch Gmünd."
preview_image: "bifora_735-mini.jpg"
image: "Bifora_735.jpg"
movementlistkey: "bifora"
caliberkey: "735"
manufacturers: ["bifora"]
manufacturers_weight: 73500
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum