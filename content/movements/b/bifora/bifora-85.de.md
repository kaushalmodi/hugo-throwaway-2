---
title: "Bifora 85"
date: 2018-01-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 85","Handaufzug","15 Jewels","15 Rubis","Palettenanker","Schraubenunruh","kleine Sekunde"]
description: "Bifora 85 - Ein recht altes Handaufzugswerk aus Schwäbisch Gmünd"
abstract: "Ein sehr schön anzusehendes Handaufzugswerk von Bifora."
preview_image: "bifora_85_mini.jpg"
image: "Bifora_85.jpg"
movementlistkey: "bifora"
caliberkey: "85"
manufacturers: ["bifora"]
manufacturers_weight: 8500
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende des funktionsfähigen Werks incl. rechteckiger Damenuhr (ohne Armband)."
usagegallery: 
  - image: "b/bifora/Bifora_DU_Bifora_85.jpg"
    description: "Bifora Damenuhr"
  - image: "b/bifora/Bifora_DU_2_Bifora_85.jpg"
    description: "Bifora Damenuhr"
---
Lorem Ipsum