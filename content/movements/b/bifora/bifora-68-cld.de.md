---
title: "Bifora 68 CLD / Dugena 709"
date: 2009-06-21T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 68 CLD","Bifora","68 CLD","17 Jewels","Bidlingmaier","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Deutschland","17 Steine","Datum"]
description: "Bifora 68 CLD"
abstract: ""
preview_image: "bifora_68_cld-mini.jpg"
image: "Bifora_68_CLD.jpg"
movementlistkey: "bifora"
caliberkey: "68 CLD"
manufacturers: ["bifora"]
manufacturers_weight: 6850
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Bifora 68 (CLD)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Bifora_68_CLD)
usagegallery: 
  - image: "d/dugena/Dugena_Calendar_DAU.jpg"
    description: "Dugena Calendar Damenuhr"
---
Lorem Ipsum