---
title: "Bifora"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: ["Bifora","Bidlingmaier","Schwäbisch Gmünd","BIdlingmaierFOrmAnker","Germany"]
categories: ["movements","movements_b"]
movementlistkey: "bifora"
abstract: "Movements of Bifora of the german manufacturer Bidlingmaier from Schwäbisch Gmünd"
description: "Movements of Bifora of the german manufacturer Bidlingmaier from Schwäbisch Gmünd"
---
(Josef Bidlingmaier, Schwäbisch Gmünd, Germany)
{{< movementlist "bifora" >}}

{{< movementgallery "bifora" >}}