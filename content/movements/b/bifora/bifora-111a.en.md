---
title: "Bifora 111A"
date: 2017-11-29T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 111A","Bifora","Bimag","Bidlingmaier","Automatic","22 Jewels","red-gold"]
description: "Bifora 111A - The most beautiful movement from Bifora"
abstract: "The red-gold selfwinding caliber 111A with golden colored screw balance is probably the most beautiful movement from the german manufacturer Bifora."
preview_image: "bifora_111a-mini.jpg"
image: "Bifora_111A.jpg"
movementlistkey: "bifora"
caliberkey: "111A"
manufacturers: ["bifora"]
manufacturers_weight: 11110
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_111A.jpg"
    description: "Bifora Automatic gents watch"
donation: "This movement and watch are a kind donation from [Götz Schweitzer](/supporters/goetz-schweitzer/). Thank you very much for the great support of the movement archive!"
---
Lorem Ipsum