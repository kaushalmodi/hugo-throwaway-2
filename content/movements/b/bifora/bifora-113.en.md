---
title: "Bifora 113"
date: 2009-04-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 113","Bifora","113","Bidlingmaier","Schwäbisch Gmünd","17 Jewels","watch","watches","wristwatch","wristwatches","caliber","Germany"]
description: "Bifora 113"
abstract: ""
preview_image: "bifora_113-mini.jpg"
image: "Bifora_113.jpg"
movementlistkey: "bifora"
caliberkey: "113"
manufacturers: ["bifora"]
manufacturers_weight: 11300
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bifora/Bifora_HAU.jpg"
    description: "Bifora 113 mens' watch"
links: |
  * [Ranfft Watches: Bifora 113](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Bifora_113)
---
Lorem Ipsum