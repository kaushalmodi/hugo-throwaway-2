---
title: "Bifora 85 S.C."
date: 2009-11-14T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 85","Bifora","85","Bidlingmaier","Schwäbisch Gmünd","17 Jewels","watch","watches","wristwatch","wristwatches","caliber","Germany"]
description: "Bifora 84/!"
abstract: ""
preview_image: "bifora_85_sc-mini.jpg"
image: "Bifora_85_SC.jpg"
movementlistkey: "bifora"
caliberkey: "85 S.C."
manufacturers: ["bifora"]
manufacturers_weight: 8550
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/bifora/Bifora_DAU_2.jpg"
    description: "Bifora ladies' watch"
---
Lorem Ipsum