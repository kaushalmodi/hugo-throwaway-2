---
title: "Bifora 84"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora","Bifora 84","Bidlingmaier","watch","watches","wristwatch","wristwatches","movement","caliber","15","jewels","screw balance"]
description: "Bifora 84"
abstract: ""
preview_image: "bifora_84-mini.jpg"
image: "Bifora_84.jpg"
movementlistkey: "bifora"
caliberkey: "84"
manufacturers: ["bifora"]
manufacturers_weight: 8400
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_DAU_Bifora_2.jpg"
    description: "Anker ladies' watch"
---
Lorem Ipsum