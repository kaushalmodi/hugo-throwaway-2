---
title: "Bifora 49"
date: 2017-12-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 49","Handaufzug","17 Jewels","17 Rubis","Palettenanker","Formwerk","Schraubenunruh"]
description: "Bifora 49 - Das kleinste Handaufzugswerk von Bifora"
abstract: "Das kleinste Werk von Bifora ist nicht optisch ein Highlight, sondern auch sehr gut verarbeitet"
preview_image: "bifora_49-mini.jpg"
image: "Bifora_49.jpg"
movementlistkey: "bifora"
caliberkey: "49"
manufacturers: ["bifora"]
manufacturers_weight: 49
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_DU_Bifora_49.jpg"
    description: "Bifora Damenuhr"
donation: "Dieses Werk samt Bifora-Uhr ist eine Spende von [Götz Schweitzer](/supporters/goetz-schweitzer/) an das Uhrwerksarchiv. Ganz herzlichen Dank für die tolle Unterstützung!"
---
Lorem Ipsum