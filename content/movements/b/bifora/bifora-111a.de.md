---
title: "Bifora 111A"
date: 2017-11-24T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bifora 111A","Bifora","Bimag","Bidlingmaier","Automatic","22 Jewels","rotgold","Rotor"]
description: "Bifora 111A - Das (subjektiv) schönste Automaticwerk von Bifora."
abstract: "Das rotgoldene Automatic-Kaliber 111A mit goldfarbiger Schraubenunruh ist wohl das schönste Werk aus dem Hause Bifora."
preview_image: "bifora_111a-mini.jpg"
image: "Bifora_111A.jpg"
movementlistkey: "bifora"
caliberkey: "111A"
manufacturers: ["bifora"]
manufacturers_weight: 11110
categories: ["movements","movements_b","movements_b_bifora"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk samt Uhr ist eine Spende von [Götz Schweitzer](/supporters/goetz-schweitzer/). Ganz herzlichen Dank für großartige Unterstützung des Uhrwerksarchivs."
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_111A.jpg"
    description: "Bifora Automatic Herrenuhr"
---
Lorem Ipsum