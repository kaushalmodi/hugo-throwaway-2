---
title: "Bifora 2030 SC"
date: 2017-01-16T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bifora 2030 SC","Bifora 2030","Bifora","Bidlingmaier FormAnker","screw balance","15 Jewels","center second"]
description: "Bifora 2030 SC - An early form movement with center second, made by Bifora"
abstract: "An early form pallet lever movement with center second and shock protection from the early 1940ies."
preview_image: "bifora_2030_sc-mini.jpg"
image: "Bifora_2030_SC.jpg"
movementlistkey: "bifora"
caliberkey: "2030 SC"
manufacturers: ["bifora"]
manufacturers_weight: 203050
categories: ["movements","movements_b","movements_b_bifora_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "b/bifora/Bifora_HAU_Bifora_2030_SC.jpg"
    description: "Bifora gents watch"
timegrapher_old: 
  rates:
    ZO: "+100"
    ZU: "+24"
    3O: "+50"
    6O: "-35"
    9O: "-759"
    12O: "-141"
  amplitudes:
    ZO: "197"
    ZU: "186"
    3O: "124"
    6O: "145"
    9O: "161"
    12O: "126"
  beaterrors:
    ZO: "1.9"
    ZU: "2.3"
    3O: "3.7"
    6O: "2.1"
    9O: "2.6"
    12O: "2.8"
labor: |
  The specimen shown here was in fair condition, when it came into the lab. Unfortunatly, the prolonged axle of the third wheel was already weakened, so it broke, when the seconds driving wheel was removed. Since it did not break the remaining function, it was left as it was and no service was made.
---
Lorem Ipsum