---
title: "Buren 04"
date: 2017-02-12T01:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Buren 04","Buren","Automatic","30 Jewels"]
description: "Buren 04: Eine sehr kleines Damen-Automatic-Werk mit einem, ins Werk direkt integrierten Automaticmechanismus"
abstract: "Ein absolut spektatuläres Automaticwerk in Centstück-Größe, bei dem der komplett Automaticmechanismus nicht auf dem Werk sitzt, sondern integriert wurde."
preview_image: "buren_04-mini.jpg"
image: "Buren_04.jpg"
movementlistkey: "buren"
caliberkey: "04"
manufacturers: ["buren"]
manufacturers_weight: 4
categories: ["movements","movements_b","movements_b_buren"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Leider ist das vorliegende Werk unvollständig und teilweise defekt (Federhauslager) und kann daher nicht auf der Zeitwaage getestet werden.
donation: "Dieses Werk ist eine Spende von [Klaus Brunnemer](/supporters/klaus-brunnemer/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
usagegallery: 
  - image: "h/hamilton/Hamilton_slender-matic_DAU_Buren_04.jpg"
    description: "Hamilton slender-matic Damenuhr  (ohne Gehäuse)"
---
Lorem Ipsum