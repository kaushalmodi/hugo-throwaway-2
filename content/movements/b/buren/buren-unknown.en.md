---
title: "Buren ???"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Buren Grand Prix","Büren Grand Prix","Buren","Büren","Grand Prix","17 Jewels","watch","watches","wristwatch","wristwatches","movement","selfwinding","caliber","Swiss"]
description: "An unknown movement by Buren"
abstract: ""
preview_image: "buren_unknown-mini.jpg"
image: "Buren_Unknown.jpg"
movementlistkey: "buren"
caliberkey: "???"
manufacturers: ["buren"]
manufacturers_weight: 0
categories: ["movements","movements_b","movements_b_buren_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/buren/Buren_DAU_Gold.jpg"
    description: "Büren ladies' watch"
---
Lorem Ipsum