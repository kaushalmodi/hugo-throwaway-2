---
title: "Buren ???"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Buren Grand Prix","Büren Grand Prix","Buren","Büren","Grand Prix","17 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Uhrwerk","17 Steine"]
description: "Ein unbekanntes Formwerk von Buren"
abstract: ""
preview_image: "buren_unknown-mini.jpg"
image: "Buren_Unknown.jpg"
movementlistkey: "buren"
caliberkey: "???"
manufacturers: ["buren"]
manufacturers_weight: 0
categories: ["movements","movements_b","movements_b_buren"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/buren/Buren_DAU_Gold.jpg"
    description: "Büren Damenuhr"
---
Lorem Ipsum