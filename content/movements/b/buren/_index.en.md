---
title: "Buren"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: ["Buren","Büren","Switzerland","Swiss"]
categories: ["movements","movements_b"]
movementlistkey: "buren"
abstract: "Movements from the swiss manufacturer Buren from Büren an der Aare."
description: "Movements from the swiss manufacturer Buren from Büren an der Aare."
---
(Uhrenfabrik Buren AG, Büren an der Aare, Switzerland)
{{< movementlist "buren" >}}

{{< movementgallery "buren" >}}