---
title: "Buren"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "de"
keywords: ["Buren","Büren","Schweiz"]
categories: ["movements","movements_b"]
movementlistkey: "buren"
abstract: "Uhrwerke des schweizer Herstellers Buren aus Büren an der Aare"
description: "Uhrwerke des schweizer Herstellers Buren aus Büren an der Aare"
---
(Uhrenfabrik Buren AG, Büren an der Aare, Schweiz)
{{< movementlist "buren" >}}

{{< movementgallery "buren" >}}