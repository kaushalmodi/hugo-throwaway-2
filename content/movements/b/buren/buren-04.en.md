---
title: "Buren 04"
date: 2017-02-19T01:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Buren 04","Buren","Automatic","30 Jewels"]
description: "Buren 04: A very small selfwinding movement for ladies' watches. It's a very flat construction, since the selfwinding mechanism is integrated into the movement."
abstract: "A spectaculary selfwinding movement with the size of an Eurocent coin, on which the complete selfwinding mechanism is not put on top of the movement, but integrated into it."
preview_image: "buren_04-mini.jpg"
image: "Buren_04.jpg"
movementlistkey: "buren"
caliberkey: "04"
manufacturers: ["buren"]
manufacturers_weight: 4
categories: ["movements","movements_b","movements_b_buren_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "h/hamilton/Hamilton_slender-matic_DAU_Buren_04.jpg"
    description: "Hamilton slender-matic ladies' watch  (case missing)"
labor: |
  Unfortunately, the specimen shown here is incomplete and partially broken (mainspring barrel bearing) and so cannot be tested on the timegrapher.
---
Lorem Ipsum