---
title: "Buren 350"
date: 2009-04-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Buren 350","Büren 350","Buren Grand Prix","Büren Grand Prix","Grand Prix","350","Chaton","Chatons","23 Jewels","watch","watches","wristwatch","wristwatches","ladies","movement","caliber","23","jewels","screw balance"]
description: "Buren 350"
abstract: ""
preview_image: "buren_350-mini.jpg"
image: "Buren_350.jpg"
movementlistkey: "buren"
caliberkey: "350"
manufacturers: ["buren"]
manufacturers_weight: 350
categories: ["movements","movements_b","movements_b_buren_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum