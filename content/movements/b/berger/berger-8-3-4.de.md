---
title: "Berger 8 3/4"
date: 2018-02-16T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Berger 8 3/4","Berger","Schweiz","Berger 16","Berger 38","Berger 42"]
description: "Berger 8 3/4 (Kaliber 16, 38 oder 42) - Ein seltenes Werk von einem noch rareren Hersteller aus der Schweiz"
abstract: "Von dem hochgradig unbekannten schweizer Hersteller Berger stammt dieses 8 3/4 linige Werk, dessen genaue Kalibernummer nicht zu ermitteln ist."
preview_image: "berger_8_3_4-mini.jpg"
image: "Berger_8_3_4.jpg"
movementlistkey: "berger"
caliberkey: "8 3/4"
manufacturers: ["berger"]
manufacturers_weight: 834
categories: ["movements","movements_b","movements_b_berger"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "anonym/DAU_Zifferblatt_Berger_8_3_4.jpg"
    description: "anonyme Damenuhr  (ohne Gehäuse)"
donation: "Das hier vorgestellt Werk ist eine Spende von [Klaus Brunnemer](/supporters/klaus-brunnemer/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
links: |
  * [Identifizierung im uhrforum.de](https://uhrforum.de/identifizierte-uhrwerke-die-weder-bei-ranfft-lorenz-noch-im-watch-wiki-zu-finden-sind-t86322-10#post2095760)
---
Lorem Ipsum