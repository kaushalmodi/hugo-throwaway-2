---
title: "Berger 8 3/4"
date: 2018-02-16T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Berger 8 3/4","Berger","Swiss","Switzerland","Berger 16","Berger 38","Berger 42"]
description: "Berger 8 3/4 (Caliber 16, 38 or 42) - A rare movement from an even rarer manufacturer"
abstract: "The highly unknown manufacturer Berger produced that 8 3/4 ligne movenent, whose exact caliber number cannot be determined."
preview_image: "berger_8_3_4-mini.jpg"
image: "Berger_8_3_4.jpg"
movementlistkey: "berger"
caliberkey: "8 3/4"
manufacturers: ["berger"]
manufacturers_weight: 834
categories: ["movements","movements_b","movements_b_berger_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "anonym/DAU_Zifferblatt_Berger_8_3_4.jpg"
    description: "anonyme Damenuhr  (ohne Gehäuse)"
links: |
  <p>* [Identification in the german uhrforum.de](https://uhrforum.de/identifizierte-uhrwerke-die-weder-bei-ranfft-lorenz-noch-im-watch-wiki-zu-finden-sind-t86322-10#post2095760)  (german language)</p><p>The specimen shown here is a kind donation of [Klaus Brunnemer](index.php?option=com_content&view=article&id=62:donated-movements-of-klaus-brunnemer&catid=10&lang=en-GB&Itemid=201). Thank you very much for the great support of the movement archive!</p>
---
Lorem Ipsum