---
title: "Bulova"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: ["Bulova","New York","USA","Biel","Switzerland","Swiss"]
categories: ["movements","movements_b"]
movementlistkey: "bulova"
abstract: "Movements from the swiss-american manufacturer Bulova"
description: "Movements from the swiss-american manufacturer Bulova"
---
(New York, USA / Biel, Schwitzerland)
{{< movementlist "bulova" >}}

{{< movementgallery "bulova" >}}