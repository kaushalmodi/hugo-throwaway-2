---
title: "Buser"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: ["Buser","Niederdorf","Switzerland","Swiss"]
categories: ["movements","movements_b"]
movementlistkey: "buser"
abstract: "Movements of the swiss manufacturer Buser from Niederdorf."
description: "Movements of the swiss manufacturer Buser from Niederdorf."
---
(Buser Freres & Co. SA, Niederdorf, Switzerland)
{{< movementlist "buser" >}}

{{< movementgallery "buser" >}}