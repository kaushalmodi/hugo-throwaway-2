---
title: "Buser"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "de"
keywords: ["Buser","Niederdorf","Schweiz"]
categories: ["movements","movements_b"]
movementlistkey: "buser"
description: "Uhrwerke des schweizer Herstellers Buser aus Niederdorf in der Schweiz"
abstract: "Uhrwerke des schweizer Herstellers Buser aus Niederdorf in der Schweiz"
---
(Buser Freres & Co. SA, Niederdorf, Schweiz)
{{< movementlist "buser" >}}

{{< movementgallery "buser" >}}