---
title: "H.F.Bauer 10 1/2 new"
date: 2017-10-05T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bauer 10 1/2","Bauer","10 1/2","H.F.Bauer","manual wind","15 Jewels","15 Rubis"]
description: "Bauer 10 1/2 new - a rather rare german manual wind movement from the 1940ies"
abstract: "A rare and visually appealing manual wind movement from Germany from the 1940ies."
preview_image: "bauer_10_12_neu-mini.jpg"
image: "Bauer_10_12_neu.jpg"
movementlistkey: "bauer"
caliberkey: "10 1/2 neu"
manufacturers: ["bauer"]
manufacturers_weight: 1012
categories: ["movements","movements_b","movements_b_hfb_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The specimen shown here came with broken setting lever screw into the lab, otherwise its condition was pretty good, so no service was made, only the setting lever screw was replaced.
timegrapher_old: 
  description: |
    The mean variation of the rates is on a level you can expect from a 70 to 80 year old movement of better quality, which did not get a service for probably a long time and which was heavily used. The rather large beat error is not uncommon to older movements, since its adjustment was difficult and time consuming, but since the amplitude of the balance is good, it's not that critical. In total, the Bauer 10 1/2 new shows a satisfying result on the timegrapher.      
  rates:
    ZO: "+1"
    ZU: "-24"
    3O: "-168"
    6O: "-70"
    9O: "+30"
    12O: "0"
  amplitudes:
    ZO: "268"
    ZU: "323"
    3O: "226"
    6O: "215"
    9O: "217"
    12O: "241"
  beaterrors:
    ZO: "3.5"
    ZU: "3.6"
    3O: "03.7"
    6O: "3.5"
    9O: "4.1"
    12O: "04.1"
---
Lorem Ipsum