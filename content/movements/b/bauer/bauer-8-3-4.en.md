---
title: "H.F.Bauer 8 3/4"
date: 2009-06-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bauer H.F. 8 3/4","Bauer H.F.","8 3/4","H.F.Bauer","Bauer H.F.","15 Jewels","watch","watches","wristwatch","wristwatches","caliber","germany"]
description: "HFB 8 3/4"
abstract: ""
preview_image: "bauer_8_34-mini.jpg"
image: "Bauer_8_34.jpg"
movementlistkey: "bauer"
caliberkey: "8 3/4"
manufacturers: ["bauer"]
manufacturers_weight: 834
categories: ["movements","movements_b","movements_b_hfb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_Bauer_8_34.jpg"
    description: "unmarked ladies' watch"
---
Lorem Ipsum