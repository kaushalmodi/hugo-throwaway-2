---
title: "H.F.Bauer B525"
date: 2009-06-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Bauer B 525","Bauer 525","H.F.Bauer","Bauer H.F.","15 Jewels","form movement","Germany","15 Rubis","Pforzheim"]
description: "HFB 525"
abstract: ""
preview_image: "bauer_525-mini.jpg"
image: "Bauer_525.jpg"
movementlistkey: "bauer"
caliberkey: "B 525"
manufacturers: ["bauer"]
manufacturers_weight: 525
categories: ["movements","movements_b","movements_b_hfb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/rhm/RHM_DAU.jpg"
    description: "RHM ladies' watch"
---
Lorem Ipsum