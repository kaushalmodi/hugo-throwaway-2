---
title: "H.F.Bauer 8 3/4"
date: 2009-06-09T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Bauer H.F. 8 3/4","Bauer H.F.","8 3/4","H.F.Bauer","15 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","7 Steine","Deutschland"]
description: "HFB 8 3/4"
abstract: ""
preview_image: "bauer_8_34-mini.jpg"
image: "Bauer_8_34.jpg"
movementlistkey: "bauer"
caliberkey: "8 3/4"
manufacturers: ["bauer"]
manufacturers_weight: 834
categories: ["movements","movements_b","movements_b_hfb"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_Bauer_8_34.jpg"
    description: "anonyme Damenuhr"
---
Lorem Ipsum