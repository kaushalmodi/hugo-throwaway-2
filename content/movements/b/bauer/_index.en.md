---
title: "Bauer, H.F. (HFB)"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "en"
keywords: ["H.F.Bauer","Bauer","HFB","Pforzheim","Germany"]
categories: ["movements","movements_b"]
movementlistkey: "bauer"
abstract: "Movements by H.F.Bauer, Pforzheim, Germany"
description: "Movements by H.F.Bauer, Pforzheim, Germany"
---
(Hermann Friedrich Bauer, Pforzheim, Germany)
{{< movementlist "bauer" >}}

{{< movementgallery "bauer" >}}