---
title: "Baumgartner 896"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 896","BFG 896","Baumgartner","BFG","896","1 Jewels","2 Jewels","mechanisch","Handaufzug","Stiftanker","Schweiz","Steine"]
description: "Baumgartner 896"
abstract: ""
preview_image: "baumgartner_896-mini.jpg"
image: "Baumgartner_896.jpg"
movementlistkey: "baumgartner"
caliberkey: "896"
manufacturers: ["baumgartner"]
manufacturers_weight: 896
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Minnie_Mouse.jpg"
    description: "anonyme Damenuhr mit Minnie Mouse-Motiv"
  - image: "d/dorothee/Dorothee_DAU_Baumgartner_896.jpg"
    description: "Dorothee Damenuhr"
---
Lorem Ipsum