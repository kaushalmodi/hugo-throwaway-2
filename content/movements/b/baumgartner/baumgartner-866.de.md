---
title: "Baumgartner 866"
date: 2009-11-29T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 866","BFG 866","BFG","Baumgartner","866","1 Jewel","Uhr","Uhren","Armbanduhr","Armbanduhren","Kinderuhr","Kaliber","Werk","1 Stein","Schweiz","Stiftanker"]
description: "Baumgartner 866"
abstract: ""
preview_image: "baumgartner_866-mini.jpg"
image: "Baumgartner_866.jpg"
movementlistkey: "baumgartner"
caliberkey: "866"
manufacturers: ["baumgartner"]
manufacturers_weight: 866
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Cowboyuhr.jpg"
    description: "anonyme Kinderuhr mit Cowboy-Motiv"
  - image: "b/buler/Buler_de_Luxe_HAU_Baumgartner_866.jpg"
    description: "Buler de Luxe Herrenuhr"
---
Lorem Ipsum