---
title: "Baumgartner 800"
date: 2012-12-30T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 800","BFG 800","BFG","Grenchen","Stiftanker","Roskopf","indirekte Zentralsekunde"]
description: "Baumgartner 800 - Ein Stiftankerwerk nach Roskopf-Art in besserer Ausführung. Mit Fotos, Makro-Aufnahmen, Video und Zeitwaagen-Ausgabe."
abstract: "Ein besseres Stiftankerwerk mit 5 Steinen und Zentralsekunde"
preview_image: "baumgartner_800-mini.jpg"
image: "Baumgartner_800.jpg"
movementlistkey: "baumgartner"
caliberkey: "800"
manufacturers: ["baumgartner"]
manufacturers_weight: 800
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "g/gisa/Gisa_HAU_Baumgartner_800.jpg"
    description: "Gisa Herrenuhr"
timegrapher_old: 
  description: |
    Das Baumgartner 800 läuft in den liegenden Position außerordentlich gut, kommt aber in den hängenden Lagen komplett aus dem Takt, was für eine nicht ordnungsgemäß ausgewuchtete Unruh spricht - möglicherweise ist hier die Rubin-Ellipse schuld. 
  images:
    ZO: Zeitwaage_Baumgartner_800_ZO.jpg
    ZU: Zeitwaage_Baumgartner_800_ZU.jpg
    3O: Zeitwaage_Baumgartner_800_3O.jpg
    6O: Zeitwaage_Baumgartner_800_6O.jpg
    9O: Zeitwaage_Baumgartner_800_9O.jpg
    12O: Zeitwaage_Baumgartner_800_12O.jpg
  values:
    ZO: "-8"
    ZU: "-+0"
    3O: "+240"
    6O: "+270"
    9O: "..."
    12O: ".."
links: |
  * [Homepage BFG-Baumgartner](http://www.bfg-baumgartner.de)
labor: |
  Das vorliegende Werk kam verharzt in die Werkstatt und wurde einer Komplettrevision (Reinigung, Ölen und Regulieren) unterzogen
---
Lorem Ipsum