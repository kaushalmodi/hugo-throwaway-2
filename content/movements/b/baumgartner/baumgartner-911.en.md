---
title: "Baumgartner 911"
date: 2018-01-20T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 911","Baumgartner","911","BFG 911","BFG","Swiss","5 Jewels","1 Jewel","watch","watches","wristwatch","wristwatches","caliber","pin lever","switzerland"]
description: "Baumgartner 911 - A common and often used swiss pin lever movement, made in the 1970ies and 1980ies."
abstract: "A pretty common late swiss pin lever movement"
preview_image: "baumgartner_911-mini.jpg"
image: "Baumgartner_911.jpg"
movementlistkey: "baumgartner"
caliberkey: "911"
manufacturers: ["baumgartner"]
manufacturers_weight: 911
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["true"]
links: |
  * [Ranfft Watches: Baumgartner 911](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Baumgartner_911)
usagegallery: 
  - image: "r/royal/Royal_Calendar_DAU.jpg"
    description: "Royal Calendar ladies' watch"
  - image: "g/geeta/Geeta_DAU_Zifferblatt.jpg"
    description: "Geeta ladies' watch  (dial only)"
  - image: "r/richard/Richard_De_Luxe_DU_Baumgartner_911.jpg"
    description: "Richard De Luxe ladies' watch"
donation: "This movement in the version without date and center second was donated by [Klaus Brunnemer](/supporters/en/) and another one (together with the Richard ladies' watch) is a donation by [Günter G.](/supporters/guenter-g/) Thank you very much!"
---
Lorem Ipsum