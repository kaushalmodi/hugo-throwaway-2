---
title: "Baumgartner 866 (QG, 13,5''')"
date: 2009-11-22T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 866","BFG 866","BFG","Baumgartner","866","1 Jewel","13","5'''","Uhr","Uhren","Armbanduhr","Armbanduhren","Kinderuhr","Kaliber","Werk","1 Stein","Schweiz","Stiftanker"]
description: "Baumgartner 866 QG 13,5'''"
abstract: ""
preview_image: "baumgartner_866_qg_13_5-mini.jpg"
image: "Baumgartner_866_QG_13_5.jpg"
movementlistkey: "baumgartner"
caliberkey: "866 (QG, 13,5)"
manufacturers: ["baumgartner"]
manufacturers_weight: 866135
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Dieses Werk wurde von [Klaus Brunnemer](/supporters/klaus-brunnemer/) gespendet. Ganz herzlichen Dank dafür!"
links: |
  * [Ranfft Uhren: Baumgartner 866 (QG)](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Baumgartner_866_CLD&)
---
Lorem Ipsum