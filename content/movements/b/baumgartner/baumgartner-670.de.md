---
title: "Baumgartner 670"
date: 2009-04-05T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 670","Baumgartner","670","BFG 670","1 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","1 Stein","Stiftanker","Schweiz"]
description: "Baumgartner 670"
abstract: ""
preview_image: "baumgartner_670-mini.jpg"
image: "Baumgartner_670.jpg"
movementlistkey: "baumgartner"
caliberkey: "670"
manufacturers: ["baumgartner"]
manufacturers_weight: 670
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/roneor/Roneor_HAU.jpg"
    description: "Roneor Herrenuhr im Alugehäuse"
---
Lorem Ipsum