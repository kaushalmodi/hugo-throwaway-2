---
title: "Baumgartner 866 (QG)"
date: 2009-11-22T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Uhrenbastler","Baumgartner 866 QG","BFG 866 QG","BFG","Baumgartner","866","1 Jewel","watch","watches","wristwatch","wristwatches","childrens` watch","movement","caliber","swiss","pin lever"]
description: "Baumgartner 866 QG"
abstract: ""
preview_image: "baumgartner_866-mini.jpg"
image: "Baumgartner_866.jpg"
movementlistkey: "baumgartner"
caliberkey: "866 (QG)"
manufacturers: ["baumgartner"]
manufacturers_weight: 866
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "b/buler/Buler_de_Luxe_HAU.jpg"
    description: "Buler de Luxe mens' watch"
---
Lorem Ipsum