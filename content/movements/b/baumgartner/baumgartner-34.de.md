---
title: "Baumgartner 34"
date: 2009-09-13T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Christoph Lorenz","Uhrenbastler","Baumgartner 34","BFG 34m Baumgartner","34","1 Jewels","BFG","Swiss","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","Stiftanker"]
description: "Baumgartner 34 "
abstract: ""
preview_image: "baumgartner_34-mini.jpg"
image: "Baumgartner_34.jpg"
movementlistkey: "baumgartner"
caliberkey: "34"
manufacturers: ["baumgartner"]
manufacturers_weight: 34
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Baumgartner 34](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Baumgartner_34)
usagegallery: 
  - image: "l/lamar/Lamar_de_Luxe_HAU.jpg"
    description: "Lamar de Luxe Herrenuhr"
  - image: "l/lugano/Lugano_DAU_Baumgartner_34.jpg"
    description: "Lugano Damenuhr"
---
Lorem Ipsum