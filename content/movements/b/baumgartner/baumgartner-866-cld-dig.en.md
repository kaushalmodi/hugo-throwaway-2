---
title: "Baumgartner 866 CLD DIG"
date: 2013-10-27T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 866","BFG 866","BFG","Grenchen","pin lever","Roskopf","indirectly driven center second","digital"]
description: "Baumgartner 866 CLD DIG - a large pin lever movement Roskopf type with digital time and date indication"
abstract: "A large 13 1/2 ligne pin lever movement of Roskopf type with digital time and date indication"
preview_image: "baumgartner_866_cld_dig-mini.jpg"
image: "Baumgartner_866_CLD_DIG.jpg"
movementlistkey: "baumgartner"
caliberkey: "866 CLD DIG"
manufacturers: ["baumgartner"]
manufacturers_weight: 866
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/sperina/Sperina_digital_BFG_866_DIG_CLD.jpg"
    description: "Sperina digital mens' watch"
timegrapher_old: 
  description: |
    Only in the position "dial up", the rates are perfect. Interestingly, this is the only position, where the only bearing ruby in the movement is effective. All other position show more or less poor rates, probably due to the water damages.
  images:
    ZO: Zeitwaage_Baumgartner_866_CLD_DIG_ZO.jpg
    ZU: Zeitwaage_Baumgartner_866_CLD_DIG_ZU.jpg
    3O: Zeitwaage_Baumgartner_866_CLD_DIG_3O.jpg
    6O: Zeitwaage_Baumgartner_866_CLD_DIG_6O.jpg
    9O: Zeitwaage_Baumgartner_866_CLD_DIG_9O.jpg
    12O: Zeitwaage_Baumgartner_866_CLD_DIG_12O.jpg
  values:
    ZO: "-+0"
    ZU: "+70"
    3O: "-240"
    6O: "+120"
    9O: "-60"
    12O: "-400"
links: |
  * [Homepage BFG-Baumgartner](http://www.bfg-baumgartner.de)
  * [Ranfft Uhren: Baumgartner 866 CLD DIG](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?01&archimedeshop&0&2uswk&Baumgartner_866_DIGCLD)
labor: |
  The specimen shown here can with water damage (rust on the main plate and axels) in the lab and got a full service (cleaning, oiling and regulation). Due to the lack of tools, no further de-rusting of the gears was done.
---
Lorem Ipsum