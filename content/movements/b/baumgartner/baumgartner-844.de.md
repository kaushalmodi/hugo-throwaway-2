---
title: "Baumgartner 844"
date: 2015-09-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 844","BFG 844","Stiftanker","KIF Trior","Roskopf"]
description: "Baumgartner (BFG) 844 - ein spätes Stiftankerwerk einfacher Bauweise"
abstract: "Ein einfaches schweizer Handaufzugswerk mit Stiftankerhemmung"
preview_image: "baumgartner_844-mini.jpg"
image: "Baumgartner_844.jpg"
movementlistkey: "baumgartner"
caliberkey: "844"
manufacturers: ["baumgartner"]
manufacturers_weight: 844
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/tc/TC_DAU_Baumgartner_844.jpg"
    description: "TC (Tchibo?) Damenuhr"
labor: |
  Das hier vorgestellte Werk war optisch in gutem Zustand, lief aber nicht sauber Nach erfolgtem Service stellte sich heraus, daß der Fehler eine abgebrochene Achse des Großbodenrads ist, ein sehr seltener Fehler, der aber möglicherweise auf ein "Überziehen", also Aufziehen mit Kraft über die Grenze hinaus zurückzuführen ist. Hierbei übt das Federhaus, das dann in direkter Verbindung mit der Krone steht, einen sehr großen Druck auf das Großbodenrad aus, dem es irgendwann nicht mehr standhalten kann.
---
Lorem Ipsum