---
title: "Baumgartner 866 CLD DIG"
date: 2013-10-26T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Baumgartner 866","BFG 866","BFG","Grenchen","Stiftanker","Roskopf","indirekte Zentralsekunde","Digital"]
description: "Baumgartner 866 CLD DIG - Ein großes Stiftankerwerk nach Roskopf mit digitaler Zeit- und Datumsanzeige"
abstract: "Ein großes 13 1/2 liniges Stiftankerwerk nach Roskopf mit digitaler Zeit- und Datumsanzeige"
preview_image: "baumgartner_866_cld_dig-mini.jpg"
image: "Baumgartner_866_CLD_DIG.jpg"
movementlistkey: "baumgartner"
caliberkey: "866 CLD DIG"
manufacturers: ["baumgartner"]
manufacturers_weight: 866
categories: ["movements","movements_b","movements_b_baumgartner"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam mit beträchtlichem Wasserschaden (Rostspuren an Platine und Trieben) in die Werkstatt und wurde einer Komplettrevision (Reinigung, Ölen und Regulieren) unterzogen. Mangels Werkzeugen konnten die Triebe nur mit Putzholz und -Mark oberflächlich entrostet werden.
links: |
  * [Homepage BFG-Baumgartner](http://www.bfg-baumgartner.de)
  * [Ranfft Uhren: Baumgartner 866 CLD DIG](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?01&archimedeshop&0&2uswk&Baumgartner_866_DIGCLD)
timegrapher_old: 
  description: |
    Einzig in der Lage "Zifferblatt oben" sind die Gangwerte gut, interessanterweise ist dies die einzige Position, wo der einzige(!) Lagerstein seine Wirkung zeigt. Alle anderen Gangwerte sind mehr oder weniger katastrophal. Vermutlich hat der Wassereinbruch hier starke Schäden an den Lagern verursacht.
  images:
    ZO: Zeitwaage_Baumgartner_866_CLD_DIG_ZO.jpg
    ZU: Zeitwaage_Baumgartner_866_CLD_DIG_ZU.jpg
    3O: Zeitwaage_Baumgartner_866_CLD_DIG_3O.jpg
    6O: Zeitwaage_Baumgartner_866_CLD_DIG_6O.jpg
    9O: Zeitwaage_Baumgartner_866_CLD_DIG_9O.jpg
    12O: Zeitwaage_Baumgartner_866_CLD_DIG_12O.jpg
  values:
    ZO: "-+0"
    ZU: "+70"
    3O: "-240"
    6O: "+120"
    9O: "-60"
    12O: "-400"
usagegallery: 
  - image: "s/sperina/Sperina_digital_BFG_866_DIG_CLD.jpg"
    description: "Sperina digital Herrenuhr"
---
Lorem Ipsum