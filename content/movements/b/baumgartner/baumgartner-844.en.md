---
title: "Baumgartner 844"
date: 2015-09-23T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Baumgartner 844","BFG 844","pin lever","KIF Trior","Roskopf"]
description: "Baumgartner (BFG) 844 - a late and simple pin lever movement"
abstract: "A simple swiss manual wind movement with pin lever escapement"
preview_image: "baumgartner_844-mini.jpg"
image: "Baumgartner_844.jpg"
movementlistkey: "baumgartner"
caliberkey: "844"
manufacturers: ["baumgartner"]
manufacturers_weight: 844
categories: ["movements","movements_b","movements_b_baumgartner_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/tc/TC_DAU_Baumgartner_844.jpg"
    description: "TC (Tchibo?) ladies' watch"
labor: |
  The movement shown here was visually in good condition, but didn't run well. After a service, the cause was clear: The axle of the second wheel was broken. This is a pretty unusual error, maybe a cause of too tight windup with too much force.
---
Lorem Ipsum