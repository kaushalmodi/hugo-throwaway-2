---
title: "Baumgartner"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["b"]
language: "de"
keywords: []
categories: ["movements","movements_b"]
movementlistkey: "baumgartner"
abstract: "(Baumgartner Frères, Grenchen, Schweiz)"
description: ""
---
(Baumgartner Frères, Grenchen, Schweiz)
{{< movementlist "baumgartner" >}}

{{< movementgallery "baumgartner" >}}