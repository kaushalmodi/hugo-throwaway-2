---
title: "Omega 690"
date: 2009-08-07T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Omega 690","Omega","690","17 Jewels","Biel","Bienne","Baguette","Fashion","Brandt Freres","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","Schweiz","17 Steine","Formwerk"]
description: "Omega 690 - Ein seltenes, extrem kleines Baguette-Formwerk für Schmuckuhren"
abstract: "Ein seltenes, extrem kleines Baguette-Formwerk für Schmuckuhren"
preview_image: "omega_690-mini.jpg"
image: "Omega_690.jpg"
movementlistkey: "omega"
caliberkey: "690"
manufacturers: ["omega"]
manufacturers_weight: 690
categories: ["movements","movements_o","movements_o_omega"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Harald Hoeber]() für die Spende dieses Werks!"
usagegallery: 
  - image: "o/omega/Omega_Unknown_Zifferblatt.jpg"
    description: "unbekannte Omega-Schmuckuhr"
---
Lorem Ipsum