---
title: "Omega R13.5"
date: 2016-12-25T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Omega","R13.5","Omega 240","240","form movement","screw balance"]
description: "Omega R13.5 (Omega 240) - A rather common ladies' watch form movement from the 1940ies"
abstract: "A rather common manual wind movement of high quality."
preview_image: "omega_r13.5-mini.jpg"
image: "Omega_R13.5.jpg"
movementlistkey: "omega"
caliberkey: "R13.5"
manufacturers: ["omega"]
manufacturers_weight: 135
categories: ["movements","movements_o","movements_o_omega_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "o/omega/Omega_DAU_Omega_R13.5.jpg"
    description: "Omega ladies' watch"
timegrapher_old: 
  rates:
    ZO: "+29"
    ZU: "-7"
    3O: "+197"
    6O: "+115"
    9O: "-300"
    12O: "-270"
  amplitudes:
    ZO: "148"
    ZU: "134"
    3O: "201"
    6O: "131"
    9O: "133"
    12O: "125"
  beaterrors:
    ZO: "2.7"
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
donation: "This movement and watch are a donation from [Günter G](/supporters/guenter-g/). Thank you very much for your kind support!"
labor: |
  The specimen shown here dates from 1947 and shows noticeable signs of wear. The dial of the accompanyhing watch is hardly readable and on the movement there are many traces of rust. Additionally, the balance wheel suffered over time, maybe due to rust or maybe due to shocks, anyhow, its rates on the timegrapher are pretty poor, even after a service.
---
Lorem Ipsum