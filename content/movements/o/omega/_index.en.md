---
title: "Omega"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "en"
keywords: []
categories: ["movements","movements_o"]
movementlistkey: "omega"
abstract: "(Omega S.A., Biel-Bienne, Switzerland)"
description: ""
---
(Omega S.A., Biel-Bienne, Switzerland)
{{< movementlist "omega" >}}

{{< movementgallery "omega" >}}