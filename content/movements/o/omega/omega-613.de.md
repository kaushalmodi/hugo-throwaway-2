---
title: "Omega 613"
date: 2014-09-15T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Omega 613","Omega","Handaufzug","17 Jewels","17 Steine","Datum","Datumsschnellschaltung","Kupfer"]
description: "Omega 613 - das letzte klassische Handaufzugswerk für Herrenuhren in seiner Maximalausführung mit Exzenter-Regelage, Datum und Schellschaltung"
abstract: "Das letzte klassische (dreistellige) Handaufzugswerk für Herrenuhren in seiner Maximalausführung mit Exzenter-Regelage, Datum und Schellschaltung"
preview_image: "omega_613-mini.jpg"
image: "Omega_613.jpg"
movementlistkey: "omega"
caliberkey: "613"
manufacturers: ["omega"]
manufacturers_weight: 613
categories: ["movements","movements_o","movements_o_omega"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/omega/Omega_Geneve_HAU_Omega_613.jpg"
    description: "Omega Genève Herrenuhr Ref. 136.041"
donation: "Vielen Dank an [Frau Müller](/supporters/g-mueller/) für die Spende dieses Werks samt Uhr!"
---
Lorem Ipsum