---
title: "Omega 485"
date: 2014-09-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Omega 485","Omega","form movement","ladies' watch","17 Jewels","17 Rubis","KIF Ultraflex","copper"]
description: "Omega 485 - a late tiny form watch movement"
abstract: "A classical ladies' watch form movement, which was produced from 1969 on"
preview_image: "omega_485-mini.jpg"
image: "Omega_485.jpg"
movementlistkey: "omega"
caliberkey: "485"
manufacturers: ["omega"]
manufacturers_weight: 485
categories: ["movements","movements_o","movements_o_omega_en"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Many thanks to [Miss Müller](/supporters/g-mueller/) for the donation of movement and watch!"
timegrapher_old: 
  images:
    ZO: Zeitwaage_Omega_485_ZO.jpg
    ZU: Zeitwaage_Omega_485_ZU.jpg
    3O: Zeitwaage_Omega_485_3O.jpg
    6O: Zeitwaage_Omega_485_6O.jpg
    9O: Zeitwaage_Omega_485_9O.jpg
    12O: Zeitwaage_Omega_485_12O.jpg
  values:
    ZO: "+2"
    ZU: "+7"
    3O: "+4"
    6O: "+15"
    9O: "+27"
    12O: "+40"
usagegallery: 
  - image: "o/omega/Omega_DAU_Omega_485.jpg"
    description: "Omega Genève ladies' watch"
labor: |
  The movement shown here was in good condition, so a dissection and service was avoided. It was just adjusted a little bit on the timegrapher.
---
Lorem Ipsum