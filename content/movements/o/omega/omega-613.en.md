---
title: "Omega 613"
date: 2014-09-16T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Omega 613","Omega","manual wind","17 Jewels","17 Rubis","date","quickset","copper","red gold"]
description: "Omega 613 - the last classical handwound mens' watch movement in its maximum execution with excenter adjustement, date indicator and quickset mechanism"
abstract: "The last classical (three-digit movement number) manual wind movement for mens' watches with maximum features such as excenter regulator, date and quickset mechanism."
preview_image: "omega_613-mini.jpg"
image: "Omega_613.jpg"
movementlistkey: "omega"
caliberkey: "613"
manufacturers: ["omega"]
manufacturers_weight: 613
categories: ["movements","movements_o","movements_o_omega_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/omega/Omega_Geneve_HAU_Omega_613.jpg"
    description: "Omega Genève mens' watch Ref. 136.041"
donation: "Many thanks to [Mrs. Müller](/supporters/g-mueller/) for the great donation of this movement and watch!"
---
Lorem Ipsum