---
title: "Omega"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "de"
keywords: ["Omega","Biel","Bienne","Schweiz","Switzerland","Uhrwerk","Uhrwerke"]
categories: ["movements","movements_o"]
movementlistkey: "omega"
abstract: "Uhrwerke des schweizer Herstellers Omega aus Biel (Bienne)."
description: "Uhrwerke des schweizer Herstellers Omega aus Biel (Bienne)."
---
(Omega S.A., Biel-Bienne, Schweiz)
{{< movementlist "omega" >}}

{{< movementgallery "omega" >}}