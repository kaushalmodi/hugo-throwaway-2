---
title: "Otero 46"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero 46","Otero","46","Otto Epple","Königsbach","17 Jewels","Germany"]
description: "Otero 46"
abstract: ""
preview_image: "otero_46-mini.jpg"
image: "Otero_46.jpg"
movementlistkey: "otero"
caliberkey: "46"
manufacturers: ["otero"]
manufacturers_weight: 46
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "e/eppo/Eppo_HAU.jpg"
    description: "Eppo gents watch"
---
Lorem Ipsum