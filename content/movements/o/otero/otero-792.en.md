---
title: "Otero 792"
date: 2017-09-25T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero 792","Otero","792","excenter changer","selfwinding","automatic","25 Jewels","25 Rubis","center second","date"]
description: "Otero 792 - A selfwinding movement from Germany with excenter changer"
abstract: "German selfwinding movements rarely used excenter changers. Here is one of them, probably from their last species."
preview_image: "otero_792-mini.jpg"
image: "Otero_792.jpg"
movementlistkey: "otero"
caliberkey: "792"
manufacturers: ["otero"]
manufacturers_weight: 792
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "e/eppo/Eppo_Automatic_Otero_792.jpg"
    description: "Eppo Automatic gents watch"
---
Lorem Ipsum