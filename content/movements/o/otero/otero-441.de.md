---
title: "Otero 441"
date: 2009-10-30T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Otero 441","Otero","441","17 Jewels","21 Jewels","Epple","17 Steine","21 Steine","Datum"]
description: "Otero 441"
abstract: ""
preview_image: "otero_441-mini.jpg"
image: "Otero_441.jpg"
movementlistkey: "otero"
caliberkey: "441"
manufacturers: ["otero"]
manufacturers_weight: 441
categories: ["movements","movements_o","movements_o_otero"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Otero 441](http://ranfft.de/cgi-bin/bidfun-db.cgi?03&ranfft&102&2uswk&Otero_441)
usagegallery: 
  - image: "p/praetina/Praetina_HAU_3_Zifferblatt.jpg"
    description: "Prätina Herrenuhr  (nur Zifferblatt)"
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks in der Version mit Schraubenunruh"
---
Lorem Ipsum