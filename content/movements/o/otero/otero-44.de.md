---
title: "Otero 44"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Otero 44","Otero","44","17 Jewels","Schwarzwald"]
description: "Otero 44"
abstract: ""
preview_image: "otero_44-mini.jpg"
image: "Otero_44.jpg"
movementlistkey: "otero"
caliberkey: "44"
manufacturers: ["otero"]
manufacturers_weight: 44
categories: ["movements","movements_o","movements_o_otero"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "w/wh/WH.jpg"
    description: "WH Herrenuhr"
---
Lorem Ipsum