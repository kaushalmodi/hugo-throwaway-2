---
title: "Otero 28"
date: 2018-01-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero 28","Otero","Epple","17 Jewels","Monorex","17 Rubis","decentral second","screw balance"]
description: "Otero 28 - An older middle-class movement from Germany"
abstract: "An older german middle-class manual wind movement with pallet lever escapement and screw balance."
preview_image: "otero_28-mini.jpg"
image: "Otero_28.jpg"
movementlistkey: "otero"
caliberkey: "28"
manufacturers: ["otero"]
manufacturers_weight: 28
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  rates:
    ZO: "0"
    ZU: "0"
    3O: "-99"
    6O: "-143"
    9O: "-126"
    12O: "-74"
  amplitudes:
    ZO: "237"
    ZU: "285"
    3O: "227"
    6O: "200"
    9O: "223"
    12O: "227"
  beaterrors:
    ZO: "5.6"
    ZU: "4.4"
    3O: "6.8"
    6O: "6.7"
    9O: "5.8"
    12O: "6.2"
labor: |
  The specimen shown here was in a watch which shows eccessive signs of wear. Nevertheless it was still ticking strong, so no service was made.
---
Lorem Ipsum