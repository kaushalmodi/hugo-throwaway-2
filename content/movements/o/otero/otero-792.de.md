---
title: "Otero 792"
date: 2017-09-23T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Otero 792","Otero","792","Exzenterwechsler","Automatic","25 Jewels","25 Steine","Zentralsekunde","Datum"]
description: "Otero 792 - Ein Automaticwerk aus Deutschland mit Exzenterwechsler"
abstract: "Exzenteraufzüge wurden nur selten in Automaticwerken aus Deutschland verbaut. Hier ist eines, vielleicht sogar das letzte seiner Art."
preview_image: "otero_792-mini.jpg"
image: "Otero_792.jpg"
movementlistkey: "otero"
caliberkey: "792"
manufacturers: ["otero"]
manufacturers_weight: 792
categories: ["movements","movements_o","movements_o_otero"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "e/eppo/Eppo_Automatic_Otero_792.jpg"
    description: "Eppo Automatic Herrenuhr"
---
Lorem Ipsum