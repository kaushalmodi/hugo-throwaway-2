---
title: "Otero 437"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero 437","Otero","437","21 Jewels","Container","Germany"]
description: "Otero 437"
abstract: ""
preview_image: "otero_437-mini.jpg"
image: "Otero_437.jpg"
movementlistkey: "otero"
caliberkey: "437"
manufacturers: ["otero"]
manufacturers_weight: 437
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/pallas/Pallas_Eppo_DAU.jpg"
    description: "Pallas Eppo ladies' watch"
---
Lorem Ipsum