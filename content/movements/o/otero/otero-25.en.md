---
title: "Otero 25 / Dugena 818"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero 25","Dugena 818","Otero","25","Dugena","818","17 Jewels","form movement","caliber","germany"]
description: "Otero 25 / Dugena 818"
abstract: ""
preview_image: "otero_25-mini.jpg"
image: "Otero_25.jpg"
movementlistkey: "otero"
caliberkey: "25"
manufacturers: ["otero"]
manufacturers_weight: 25
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_DAU.jpg"
    description: "Dugena ladies' watch"
---
Lorem Ipsum