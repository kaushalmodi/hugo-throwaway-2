---
title: "Otero 334"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Otero","334","Otero 334","17","jewels","manual winding","german","Germany","black wood forest"]
description: "Otero 334"
abstract: ""
preview_image: "otero_334-mini.jpg"
image: "Otero_334.jpg"
movementlistkey: "otero"
caliberkey: "334"
manufacturers: ["otero"]
manufacturers_weight: 334
categories: ["movements","movements_o","movements_o_otero_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum