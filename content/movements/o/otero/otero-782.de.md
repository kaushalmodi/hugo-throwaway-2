---
title: "Otero 782"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Otero 782","Otero","782","25 Jewels","Automatic","25 Steine","Automatik","Deutschland"]
description: "Otero 782"
abstract: ""
preview_image: "otero_782-mini.jpg"
image: "Otero_782.jpg"
movementlistkey: "otero"
caliberkey: "782"
manufacturers: ["otero"]
manufacturers_weight: 782
categories: ["movements","movements_o","movements_o_otero"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Unknown_Otero_782.jpg"
    description: "Unbekannte Herren-Automatic"
---
Lorem Ipsum