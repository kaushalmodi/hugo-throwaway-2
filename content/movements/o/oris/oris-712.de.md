---
title: "Oris 712"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Oris 712","Oris","712","17 Jewels","Schweiz","Stiftanker","17 Steine"]
description: "Oris 712"
abstract: ""
preview_image: "oris_712-mini.jpg"
image: "Oris_712.jpg"
movementlistkey: "oris"
caliberkey: "712"
manufacturers: ["oris"]
manufacturers_weight: 712
categories: ["movements","movements_o","movements_o_oris"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/oris/Oris_Trio.jpg"
    description: "Oris Trio Herrenuhr"
---
Lorem Ipsum