---
title: "Oris 712"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Oris 712","Oris","712","17 Jewels","Swiss","Switzerland","pin lever","17 Rubis"]
description: "Oris 712"
abstract: ""
preview_image: "oris_712-mini.jpg"
image: "Oris_712.jpg"
movementlistkey: "oris"
caliberkey: "712"
manufacturers: ["oris"]
manufacturers_weight: 712
categories: ["movements","movements_o","movements_o_oris_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/oris/Oris_Trio.jpg"
    description: "Oris Trio gents watch"
---
Lorem Ipsum