---
title: "Oris 158"
date: 2010-03-27T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["pillar construction","axial shock absorber","partial shock absorbers","Oris 158","Oris","158"]
description: "A simple pin-lever movement with partial shock protection. Detailed description with photos, macro images, video and data sheet."
abstract: ""
preview_image: "oris_158-mini.jpg"
image: "Oris_158.jpg"
movementlistkey: "oris"
caliberkey: "158"
manufacturers: ["oris"]
manufacturers_weight: 158
categories: ["movements","movements_o","movements_o_oris_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/siro/Siro_Jewelled_Oris_158.jpg"
    description: "Siro Jewelled gents watch"
---
Lorem Ipsum