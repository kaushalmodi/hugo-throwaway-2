---
title: "Oris"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "en"
keywords: ["Oris","Höstein","Basel","Swiss","Switzerland","movement","movements"]
categories: ["movements","movements_o"]
movementlistkey: "oris"
abstract: "Movements of the swiss manufacturer Oris from Höstein near Basel"
description: "Movements of the swiss manufacturer Oris from Höstein near Basel"
---
(Oris, Höstein, Basel and Surroundings, Switzerland)
{{< movementlist "oris" >}}

{{< movementgallery "oris" >}}