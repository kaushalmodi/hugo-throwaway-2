---
title: "Orient 46943"
date: 2010-03-13T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Orient","46943","Orient 46943","Orient Star","Crystal","Automatic","Japan","21 Jewels","selfwinding","3 stars"]
description: "Orient 46943"
abstract: ""
preview_image: "orient_46943-mini.jpg"
image: "Orient_46943.jpg"
movementlistkey: "orient"
caliberkey: "46943"
manufacturers: ["orient"]
manufacturers_weight: 46943
categories: ["movements","movements_o","movements_o_orient_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/orient/Orient_Cystal_HAU.jpg"
    description: "Orient Cystal Automatic gents watch"
---
Lorem Ipsum