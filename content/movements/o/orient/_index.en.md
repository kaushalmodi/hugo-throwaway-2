---
title: "Orient"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "en"
keywords: ["Orient","Tokyo","Japan","movement","movements"]
categories: ["movements","movements_o"]
movementlistkey: "orient"
description: "Movements of the japanese manufacturer Orient from Tokyo."
abstract: "Movements of the japanese manufacturer Orient from Tokyo."
---
(Orient Watch Co Ltd., Tokyo, Japan)
{{< movementlist "orient" >}}

{{< movementgallery "orient" >}}