---
title: "Orient"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "de"
keywords: ["Orient","Tokio","Japan"]
categories: ["movements","movements_o"]
movementlistkey: "orient"
description: "Uhrwerke des japanischen Herstellers Orient aus Tokio."
abstract: "Uhrwerke des japanischen Herstellers Orient aus Tokio."
---
(Orient Watch Co Ltd., Tokio, Japan)
{{< movementlist "orient" >}}

{{< movementgallery "orient" >}}