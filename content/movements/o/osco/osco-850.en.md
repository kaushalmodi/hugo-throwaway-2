---
title: "Osco 850"
date: 2017-10-30T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Osco 850","Osco","850","Otto Schlund","Schlund","Schwenningen","17 Jewels","17 Rubis","Germany"]
description: "Osco 850 - A rather cheap but well made pallet lever construction"
abstract: "A rather cheap but reliable pallet lever construction from Germany"
preview_image: "osco_850-mini.jpg"
image: "Osco_850.jpg"
movementlistkey: "osco"
caliberkey: "850"
manufacturers: ["osco"]
manufacturers_weight: 8500
categories: ["movements","movements_o","movements_o_osco_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The specimen shown here came in good condition into the lab, only the mainspring was broken and replaced. No cleaning or service was made.
donation: "This movement including the Prätina watch is a kind donation of [U.Bohnet-Ströbel](/supporters/u-bohnet-stroebel/). Thank you very much for the support of the movement archive!"
timegrapher_old: 
  rates:
    ZO: "-52"
    ZU: "+50"
    3O: "+34"
    6O: "-15"
    9O: "+25"
    12O: "-23"
  amplitudes:
    ZO: "158"
    ZU: "199"
    3O: "165"
    6O: "169"
    9O: "153"
    12O: "159"
  beaterrors:
    ZO: "0.0"
    ZU: "0.1"
    3O: "0.0"
    6O: "0.8"
    9O: "0.9"
    12O: "0.0"
usagegallery: 
  - image: "p/praetina/Praetina_HU_Osco_850.jpg"
    description: "Prätina gents' watch"
  - image: "o/osco/Osco_HAU.jpg"
    description: "Osco S gents watch"
---
Lorem Ipsum