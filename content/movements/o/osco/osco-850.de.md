---
title: "Osco 850"
date: 2017-10-30T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Osco 850","Osco","850","Otto Schlund","Schlund","Schwenningen","17 Jewels","17 Steine"]
description: "Osco 850 - Ein Mittelklasse-Ankerwerk aus Deutschland"
abstract: "Ein kosteneffizient und in Pfeilerbauweise konstruiertes Ankerwerk aus Deutschland"
preview_image: "osco_850-mini.jpg"
image: "Osco_850.jpg"
movementlistkey: "osco"
caliberkey: "850"
manufacturers: ["osco"]
manufacturers_weight: 8500
categories: ["movements","movements_o","movements_o_osco"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Das Werk samt Prätina-Herrenuhr ist eine Spende von [U.Bohnet-Ströbel](/supporters/u-bohnet-stroebel/). Ganz herzlichen Dank für die Unterstützung des Uhrwerksarchivs!"
usagegallery: 
  - image: "p/praetina/Praetina_HU_Osco_850.jpg"
    description: "Prätina Herrenuhr"
  - image: "o/osco/Osco_HAU.jpg"
    description: "Osco S Herrenuhr"
timegrapher_old: 
  rates:
    ZO: "-52"
    ZU: "+50"
    3O: "+34"
    6O: "-15"
    9O: "+25"
    12O: "-23"
  amplitudes:
    ZO: "158"
    ZU: "199"
    3O: "165"
    6O: "169"
    9O: "153"
    12O: "159"
  beaterrors:
    ZO: "0.0"
    ZU: "0.1"
    3O: "0.0"
    6O: "0.8"
    9O: "0.9"
    12O: "0.0"
labor: |
  Das getestete Exemplar kam, mit Ausnahme einer defekten Zugfeder, die getauscht wurde, in gutem Zustand ins Labor, so daß auf eine Reinigung und auf einen Service verzichtet wurde.
---
Lorem Ipsum