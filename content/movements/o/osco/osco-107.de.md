---
title: "Osco 107"
date: 2015-09-25T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Osco 107","Schlund","Osco","107","Palettenanker","Anker","8 Jewels","8 Steine","Monorex","kleine Sekunde"]
description: "Osco 107 - ein preiswertes und solides Palettenankerwerk aus den späten 50er Jahren"
abstract: "Ein brauchbares Ankerwerk kann auch mit weniger als 15 Steinen auskommen, das beweist das Osco 107"
preview_image: "osco_107.mini.jpg"
image: "Osco_107.jpg"
movementlistkey: "osco"
caliberkey: "107"
manufacturers: ["osco"]
manufacturers_weight: 1070
categories: ["movements","movements_o","movements_o_osco"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  description: |
    Die Gangwerte können sich auch ohne Service durchaus sehen lassen, einzig und alleine die schlechte Auswuchtung der Unruh und der dadurch bedingte Lagefehler trübt das Bild ein wenig. Dafür zeigt das Werk auf der Zeitwaage stets einen einwandfreien gleichmäßigen Strich, was auf einen perfekten Zustand des Räderwerks hinweist. Nach jahrelangem heftigem Gebrauch ist das bemerkenswert!
  rates:
    ZO: "+2"
    ZU: "+28"
    3O: "+20"
    6O: "+19"
    9O: "-32"
    12O: "-30"
  amplitudes:
    ZO: "243"
    ZU: "231"
    3O: "202"
    6O: "200"
    9O: "195"
    12O: "190"
  beaterrors:
    ZO: "0.4"
    ZU: "0.4"
    3O: "0.1"
    6O: "0.8"
    9O: "1.0"
    12O: "0.4"
usagegallery: 
  - image: "o/osco/Osco_107_HAU_Osco_107.jpg"
    description: "Osco 107 Herrenuhr"
labor: |
  Das hier gezeigte Werk kam in einer Uhr, deren Gehäuse und Zifferblatt massive Abnutzungsspuren zeigten, auf dem Zifferblatt wurde sogar herumgekratzt. Dennoch lief es von Anfang an so gut, leicht im Plus, daß auf einen Service verzichtet werden konnte und das Werk nach dem Zerlegen nur minimal nachgeölt wurde.
---
Lorem Ipsum