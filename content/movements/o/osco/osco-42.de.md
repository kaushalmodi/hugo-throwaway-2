---
title: "Osco 42"
date: 2017-02-08T01:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Osco 42","Schlund","Osco","42","Palettenanker","Anker","15 Jewels","15 Steine","Glucydur","Schraubenunruh","kleine Sekunde"]
description: "Osco 42: Ein altes Formwerk mit Palettenanker und Glucydur-Schraubenunruh"
abstract: "Eines der ersten Werke von Osco, hier in der wohl besten Ausführung, mit einer Glucydur-Schraubenunruh."
preview_image: "osco_42-mini.jpg"
image: "Osco_42.jpg"
movementlistkey: "osco"
caliberkey: "42"
manufacturers: ["osco"]
manufacturers_weight: 420
categories: ["movements","movements_o","movements_o_osco"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  description: |
    Die optisch sehr schön anzusehende Glucydur-Unruh täuscht ein wenig über das miserable Gangergebnis hinweg. Der Fairneß halber muß man allerdings sagen, daß dieses Gangergebnis nach einer richtigen Revision sicher deutlich besser gewesen wäre. Ohne diese präsentiert sich das Werk also mehr oder weniger im Fundzustand, und dieser zeigt deutlich auf, daß das Werk nicht geschont und sehr lange getragen wurde:<br/> Mit diesen Werten kann man die Grenzen der Zeitwaage ausloten, das Werk selber schreit nach einer Revision!
  rates:
    ZO: "+-450"
    ZU: "+24"
    3O: "+600"
    6O: "-400"
    9O: "-550"
    12O: "+250"
  amplitudes:
    ZO: "126"
    ZU: "230"
    3O: "150"
    6O: "125"
    9O: "147"
    12O: "154"
  beaterrors:
    ZO: "2.6"
    ZU: "0.6"
    3O: "2.6"
    6O: "2.1"
    9O: "2.0"
    12O: "2.6"
usagegallery: 
  - image: "a/ankra/Ankra_HAU_Osco_42.jpg"
    description: "Ankra Herrenuhr  (mit unpassender Aufzugswelle)"
labor: |
  Das hier gezeigte Werk kam ohne Aufzugswelle ins Labor und war verharzt. Da es nur für Dokumentationszwecke wieder gangfähig gemacht werden sollte, wurde nur eine einfache Reinigung vorgenommen, für den täglichen Gebrauch wäre das nicht ausreichend gewesen.
---
Lorem Ipsum