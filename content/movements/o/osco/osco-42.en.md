---
title: "Osco 42"
date: 2017-02-12T01:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Osco 42","Schlund","Osco","42","pallet lever","lever","15 Jewels","15 Rubis","Glucydur","screw balance","decentral second","form movement"]
description: "Osco 42: An old form movement with pallet lever and screw balance."
abstract: "One of the first Osco movements, here in its probably finest version with a Glucydur screw balance."
preview_image: "osco_42-mini.jpg"
image: "Osco_42.jpg"
movementlistkey: "osco"
caliberkey: "42"
manufacturers: ["osco"]
manufacturers_weight: 420
categories: ["movements","movements_o","movements_o_osco_en"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  description: |
    The visually very pleasing Glucydur screw balance deceives a bit about the bad timegrapher results. For fairness one must say, that the rates would have been much better after a real service. Without that, the specimen tell about long and hard use:<br/> The rates nearly showed the limits of the timegrapher and the movement cries for a service!
  rates:
    ZO: "+-450"
    ZU: "+24"
    3O: "+600"
    6O: "-400"
    9O: "-550"
    12O: "+250"
  amplitudes:
    ZO: "126"
    ZU: "230"
    3O: "150"
    6O: "125"
    9O: "147"
    12O: "154"
  beaterrors:
    ZO: "2.6"
    ZU: "0.6"
    3O: "2.6"
    6O: "2.1"
    9O: "2.0"
    12O: "2.6"
usagegallery: 
  - image: "a/ankra/Ankra_HAU_Osco_42.jpg"
    description: "Ankra gents watch  (with non-matching stem)"
labor: |
  The specimen shown here was gummed and came without winding stem into the the lab. Das hier gezeigte Werk kam ohne Aufzugswelle ins Labor und war verharzt. Since it had to be made viable only for documentation purposes, only a simple cleaning was done, which would not have been sufficient for daily use.
---
Lorem Ipsum