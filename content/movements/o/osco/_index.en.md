---
title: "Osco"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "en"
keywords: ["Osco","Schlund","Villingen-Schwenningen","Germany"]
categories: ["movements","movements_o"]
movementlistkey: "osco"
description: "Uhrwerke by Osco, Otto Schlund GmbH in Villingen-Schwenningen, Germany"
abstract: "Uhrwerke by Osco, Otto Schlund GmbH in Villingen-Schwenningen, Germany"
---
(OSCO Uhrenfabrik Schlund GmbH, Villingen-Schwenningen, Germany)
{{< movementlist "osco" >}}

{{< movementgallery "osco" >}}