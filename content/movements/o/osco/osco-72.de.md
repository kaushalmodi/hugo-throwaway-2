---
title: "Osco 72"
date: 2009-11-28T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Osco 72","Osco","72","17 Jewels","Otto Schlund","Schwenningen","Formwerk","17 Steine"]
description: "Osco 72"
abstract: ""
preview_image: "osco_72-mini.jpg"
image: "Osco_72.jpg"
movementlistkey: "osco"
caliberkey: "72"
manufacturers: ["osco"]
manufacturers_weight: 720
categories: ["movements","movements_o","movements_o_osco"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/osco/Osco_DAU_3.jpg"
    description: "Osco Damenuhr"
---
Lorem Ipsum