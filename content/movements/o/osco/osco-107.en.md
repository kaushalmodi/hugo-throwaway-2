---
title: "Osco 107"
date: 2015-09-26T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Osco 107","Schlund","Osco","107","pallet lever","lever","8 Jewels","8 Rubis","Monorex","decentral second"]
description: "Osco 107 - an inexpensive but solid german pallet lever movement from the late 1950ies"
abstract: "An inexpensive but well made pallet lever movement, showing that you can also go with less than 15 jewels"
preview_image: "osco_107.mini.jpg"
image: "Osco_107.jpg"
movementlistkey: "osco"
caliberkey: "107"
manufacturers: ["osco"]
manufacturers_weight: 1070
categories: ["movements","movements_o","movements_o_osco_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>The specimen shown here was in a watch, which shows many signs of heavy use, there are even deep scratches on the dial.</p><p>Nevertheless, right from the start it showed a very good performance, slightly too fast, so that a service was not neccessiary. After disassembly, it was slightly oiled and adjusted.</p>
timegrapher_old: 
  description: |
    The rates are really good for a movement without service, only the bad balancing of the balance wheel and the resulting positional errors are a bit bad. But it nevertheless shows a perfect line on the timegrapher!
  rates:
    ZO: "+2"
    ZU: "+28"
    3O: "+20"
    6O: "+19"
    9O: "-32"
    12O: "-30"
  amplitudes:
    ZO: "243"
    ZU: "231"
    3O: "202"
    6O: "200"
    9O: "195"
    12O: "190"
  beaterrors:
    ZO: "0.4"
    ZU: "0.4"
    3O: "0.1"
    6O: "0.8"
    9O: "1.0"
    12O: "0.4"
usagegallery: 
  - image: "o/osco/Osco_107_HAU_Osco_107.jpg"
    description: "Osco 107 gents watch"
---
Lorem Ipsum