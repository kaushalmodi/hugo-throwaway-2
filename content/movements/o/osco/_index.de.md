---
title: "Osco"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["o"]
language: "de"
keywords: ["Osco","Schlund","Villingen-Schwenningen"]
categories: ["movements","movements_o"]
movementlistkey: "osco"
abstract: "Uhrwerke von Osco, Otto Schlund GmbH in Villingen-Schwenningen, Deutschland"
description: "Uhrwerke von Osco, Otto Schlund GmbH in Villingen-Schwenningen, Deutschland"
---
(OSCO Uhrenfabrik Schlund GmbH, Villingen-Schwenningen, Deutschland)
{{< movementlist "osco" >}}

{{< movementgallery "osco" >}}