---
title: "Osco 108"
date: 2015-03-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Osco","Schlund","Osco 108","Anker","Datum"]
description: "Osco 108 - Ein einfaches deutsches Ankerwerk aus den späten 50er Jahren mit Datumsanzeige, aber ohne Sekundenindikation"
abstract: "Ein einfaches deutsches Ankerwerk aus den späten 50er Jahren mit Datumsanzeige aber ohne Sekundenindikation"
preview_image: "osco_108-mini.jpg"
image: "Osco_108.jpg"
movementlistkey: "osco"
caliberkey: "108"
manufacturers: ["osco"]
manufacturers_weight: 1080
categories: ["movements","movements_o","movements_o_osco"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Das vorliegende Werk kam komplett verharzt in die Werkstatt, wurde vollständig zerlegt, gereinigt und geölt.
links: |
  * [Ranfft Uhren: Osco 108<br />
  ](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Osco_108&)
usagegallery: 
  - image: "o/osco/Osco_108_Osco_108.jpg"
    description: "Osco 108 Herrenuhr"
timegrapher_old: 
  description: |
    Die Gangabweichung in den hängenden Lagen ist deutlich höher, als in den liegenden Positionen, das spricht für eine recht große Abnutzung der Unruhwelle. Für ein einfaches Ankerwerk diesen Alters sind die Werte aber noch im Rahmen. Eine Justierung mehr ins Plus würde hier absolut tragetaugliche Werte ermöglichen.
  images:
    ZO: Zeitwaage_Osco_108_ZO.jpg
    ZU: Zeitwaage_Osco_108_ZU.jpg
    3O: Zeitwaage_Osco_108_3O.jpg
    6O: Zeitwaage_Osco_108_6O.jpg
    9O: Zeitwaage_Osco_108_9O.jpg
    12O: Zeitwaage_Osco_108_12O.jpg
  values:
    ZO: "-20"
    ZU: "-20"
    3O: "-70"
    6O: "-35"
    9O: "-50"
    12O: "-60"
---
Lorem Ipsum