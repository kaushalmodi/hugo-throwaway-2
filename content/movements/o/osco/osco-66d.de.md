---
title: "Osco 66D"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Osco 66D","Osco","66D","17 Jewels","Deutschland"]
description: "Osco 66D"
abstract: ""
preview_image: "osco_66d-mini.jpg"
image: "Osco_66D.jpg"
movementlistkey: "osco"
caliberkey: "66D"
manufacturers: ["osco"]
manufacturers_weight: 664
categories: ["movements","movements_o","movements_o_osco"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/osco/Osco_Parat_2.jpg"
    description: "Osco Parat Damenuhr"
  - image: "b/bergana/Bergana_HAU.jpg"
    description: "Bergana Herrenuhr"
---
Lorem Ipsum