---
title: "Osco 52"
date: 2009-04-12T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Osco 52 Parat","Osco","52","Parat","Otto Schlund","Pforzheim","17 Jewels","mechanisch","Handaufzug","Deutschland","Steine","Schraubenunruhe"]
description: "Osco 52"
abstract: ""
preview_image: "osco_52-mini.jpg"
image: "Osco_52.jpg"
movementlistkey: "osco"
caliberkey: "52"
manufacturers: ["osco"]
manufacturers_weight: 520
categories: ["movements","movements_o","movements_o_osco"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/osco/Osco_Parat.jpg"
    description: "Osco Parat Herrenuhr"
  - image: "z/zentra/Zentra_DAU.jpg"
    description: "ZentRa Damenuhr"
---
Lorem Ipsum