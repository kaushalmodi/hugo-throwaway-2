---
title: "Osco 108"
date: 2015-03-22T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Osco","Schlund","Osco 108","pallet lever","date"]
description: "Osco 108 - A simple german pallet lever movement from the late 1950ies, with date indication but without seconds indication"
abstract: "A simple german pallet-lever movement from the late 1950ies. It has got a date indication but lacks a second hand."
preview_image: "osco_108-mini.jpg"
image: "Osco_108.jpg"
movementlistkey: "osco"
caliberkey: "108"
manufacturers: ["osco"]
manufacturers_weight: 1080
categories: ["movements","movements_o","movements_o_osco_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  Tje specimen here required a full service.
links: |
  * [Ranfft Uhren: Osco 108<br />
  ](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Osco_108&)
usagegallery: 
  - image: "o/osco/Osco_108_Osco_108.jpg"
    description: "Osco 108 mens' watch"
timegrapher_old: 
  description: |
    The rates on the horizontal positions are much better than on the vertical positions. It's likely, that the pivots of the balance wheel are worn out a bit. But for a simple lever movement of that age, the rates are still OK. If the adjustment was made a little more into plus, they would be sufficcient for daily wear..
  images:
    ZO: Zeitwaage_Osco_108_ZO.jpg
    ZU: Zeitwaage_Osco_108_ZU.jpg
    3O: Zeitwaage_Osco_108_3O.jpg
    6O: Zeitwaage_Osco_108_6O.jpg
    9O: Zeitwaage_Osco_108_9O.jpg
    12O: Zeitwaage_Osco_108_12O.jpg
  values:
    ZO: "-20"
    ZU: "-20"
    3O: "-70"
    6O: "-35"
    9O: "-50"
    12O: "-60"
---
Lorem Ipsum