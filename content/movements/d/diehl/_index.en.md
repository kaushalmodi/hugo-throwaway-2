---
title: "Diehl"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "en"
keywords: ["Diehl","Nuremberg","Nürnberg","Germany","Junghans"]
categories: ["movements","movements_d"]
movementlistkey: "diehl"
description: "Movements from the german manufacturer Diehl from Nuremberg"
abstract: "Movements from the german manufacturer Diehl from Nuremberg"
---
(Diehl Stiftung & Co. KG, Nuremberg, Germany)
{{< movementlist "diehl" >}}

{{< movementgallery "diehl" >}}