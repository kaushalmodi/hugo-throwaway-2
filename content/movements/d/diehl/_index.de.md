---
title: "Diehl"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "de"
keywords: ["Diehl","Nürnberg","Junghans","Deutschland"]
categories: ["movements","movements_d"]
movementlistkey: "diehl"
description: "Uhrwerke von Diehl aus Nürnberg"
abstract: "Uhrwerke von Diehl aus Nürnberg"
---
(Diehl Stiftung & Co. KG, Nürnberg, Deutschland)
{{< movementlist "diehl" >}}

{{< movementgallery "diehl" >}}