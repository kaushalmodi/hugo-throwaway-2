---
title: "Doxa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "de"
keywords: ["Doxa","Neuchatel","Schweiz","ETA"]
categories: ["movements","movements_d"]
movementlistkey: "doxa"
description: "Uhrwerke von Doxa aus dem schweizerischen Neuchatel"
abstract: "Uhrwerke von Doxa aus dem schweizerischen Neuchatel"
---
(Doxa, Neuchatel, Schweiz)
{{< movementlist "doxa" >}}

{{< movementgallery "doxa" >}}