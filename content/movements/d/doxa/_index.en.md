---
title: "Doxa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "en"
keywords: ["Doxa","Neuchatel","Switzerland","Swiss","ETA"]
categories: ["movements","movements_d"]
movementlistkey: "doxa"
abstract: "Movements from the swiss manufacturer Doxa from Neuchatel"
description: "Movements from the swiss manufacturer Doxa from Neuchatel"
---
(Doxa, Neuchatel, Switzerland)
{{< movementlist "doxa" >}}

{{< movementgallery "doxa" >}}