---
title: "Doxa 103"
date: 2009-04-04T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["ETA 2391","Glucydur-Schraubenunruh","Nivarox-I","Doxa 103","Doxa","103","17 Jewels","Neuchatel","Schweiz","17 Steine"]
description: "Doxa 103 - Ein hochwertiges schweizer Werk mit 11 1/2 Linien Durchmesser. Detaillierte Beschreibung mit Bildern, Makro-Aufnahme, Video, Datenblatt und Zeitwaagenprotokoll."
abstract: ""
preview_image: "doxa_103-mini.jpg"
image: "Doxa_103.jpg"
movementlistkey: "doxa"
caliberkey: "103"
manufacturers: ["doxa"]
manufacturers_weight: 103
categories: ["movements","movements_d","movements_d_doxa"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: ETA 2391](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&ETA_2391) (Das Basis-Ebauche, ETA 2391)
labor: |
  <p>Das Werk kam komplett verharzt und in einer Uhr in bedauerlichem Zustand (siehe unten) ins Labor und wurde erstmal vollständig gereinigt und geölt.</p><p>Einen kleinen Defekt besitzt dieses Werk dennoch: Dadurch, daß die Zahnräder des Räderwerks so nah beieinander sind und minimal "eiern", kann es ab und zu vorkommen, daß sie sich leicht berühren und dadurch ein wenig das Werk abbremsen bzw. die Unruhschwingweite negativ beeinflussen.</p><p>Gut möglich, daß das eine Folge der schlechten Behandlung der Doxa-Uhr ist. Auf der, hier auf doppelte Genauigkeit eingestellten Zeitwaage machte das Werk zwar einen recht guten Eindruck, angesichts der guten Gangreglerpartie bleibt es aber sicher hinter seinen Möglichkeiten zurück.</p><p>Während die horizontalen Lagen noch mit +5 bzw. +15 akzeptabel sind, fallen drei von vier hängenden Lagen aus der Rolle und treiben die Uhr mit bis zu 45 Sekunden Nachgang pro Tag stark ins Minus. Eventuell ist die Ursache in den oben beschriebenen, sich leicht berührenden Zahnrädern zu suchen. Gerade in den vertikalen Lagen machen sich solche Fehler extrem stark bemerkbar, da hier die Zahnräder leicht in den Lagern "taumeln", was an der Höhenluft der Achsen liegt. Abhilfe könnten hier nur Decksteine leisten, die dieses Werk leider nicht besitzt.</p><p>\{movement\_timegrapher\}d/doxa|Doxa\_103\{/movement\_timegrapher\}</p>
usagegallery: 
  - image: "d/doxa/Doxa_HAU_Doxa_103.jpg"
    description: "Doxa Herrenuhr"
---
Lorem Ipsum