---
title: "Doxa ?"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","Doxa ?","Doxa","?","Doxa 4","Neuchatel","Swiss","15 Jewels","Guillaume","Chaton","Chatons","15 Rubis"]
description: "unknown Doxa movement"
abstract: ""
preview_image: "doxa_unknown-mini.jpg"
image: "Doxa_Unknown.jpg"
movementlistkey: "doxa"
caliberkey: "?"
manufacturers: ["doxa"]
manufacturers_weight: 0
categories: ["movements","movements_d","movements_d_doxa_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Anonym_Doxa_Unknown.jpg"
    description: "unmarked gents watch"
---
Lorem Ipsum