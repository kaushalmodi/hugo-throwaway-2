---
title: "Desa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "de"
keywords: ["Desa","Stiftanker","Schweiz","Rohwerk","Uhrwerk"]
categories: ["movements","movements_d"]
movementlistkey: "desa"
description: "Uhrwerke des schweizer Stiftankerwerke-Herstellers Desa aus Grenchen"
abstract: "Uhrwerke des schweizer Stiftankerwerke-Herstellers Desa aus Grenchen"
---
(Ebauches Desa, Grenchen, Schweiz)
{{< movementlist "desa" >}}

{{< movementgallery "desa" >}}