---
title: "Desa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "en"
keywords: ["Desa","pin lever","Switzerland","swiss","Grenchen"]
categories: ["movements","movements_d"]
movementlistkey: "desa"
abstract: "Movements of the swiss pin lever manufacturer Desa of Grenchen"
description: "Movements of the swiss pin lever manufacturer Desa of Grenchen"
---
(Ebauches Desa, Grenchen, Switzerland)
{{< movementlist "desa" >}}

{{< movementgallery "desa" >}}