---
title: "Desa 322"
date: 2016-02-21T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Desa 322","Desa","Ebauches Desa","Grenchen","Switzerland","Swiss","pin lever","1 Jewel","1 Rubis","Roskopf"]
description: "Desa 322 - A simple, 13 ligne swiss pin lever movement with a Roskopf construction"
abstract: "A simple, 13 ligne swiss pin lever movement with a Roskopf construction"
preview_image: "desa_322-mini.jpg"
image: "Desa_322.jpg"
movementlistkey: "desa"
caliberkey: "322"
manufacturers: ["desa"]
manufacturers_weight: 322
categories: ["movements","movements_d","movements_d_desa_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "This movement and pendant watch was donated by [Günter G.](/supporters/guenter-g/) Thank you very much for supporting the movement archive!"
usagegallery: 
  - image: "l/lucerne/Lucerne_Umhaengeuhr.jpg"
    description: "Lucerne pendant watch"
timegrapher_old: 
  rates:
    ZO: "+29"
    ZU: "+34"
    3O: "-40"
    6O: "-19"
    9O: "-16"
    12O: "-9"
  amplitudes:
    ZO: ""
    ZU: ""
    3O: ""
    6O: ""
    9O: ""
    12O: ""
  beaterrors:
    ZO: ""
    ZU: "0.6"
    3O: "0.8"
    6O: "0.8"
    9O: ""
    12O: "1.7"
labor: |
  The specimen shown here was in good condition and required only a minimal service and an adjustment, since at the beginning, it ran four minutes fast per day.
---
Lorem Ipsum