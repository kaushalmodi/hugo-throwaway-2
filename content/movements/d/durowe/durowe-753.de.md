---
title: "DuRoWe 753"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 753","Formwerk","17 Jewels","Schraubenunruh","Incabloc"]
description: "DuRoWe 753"
abstract: ""
preview_image: "durowe_753-mini.jpg"
image: "Durowe_753.jpg"
movementlistkey: "durowe"
caliberkey: "753"
manufacturers: ["durowe"]
manufacturers_weight: 753
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/laco/Laco_DAU.jpg"
    description: "Laco Damenuhr"
---
Lorem Ipsum