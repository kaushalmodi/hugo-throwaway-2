---
title: "DuRoWe 552"
date: 2015-11-02T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Durowe 552","552","Automatic","selfwinding","rocking bar","25 Jewels","25 Rubis","Duromat"]
description: "Durowe 552 - The first selfwinding movement of the legendary \"Duromat\" series by the german manufactury Deutsche UhrenRohwerke (DuRoWe)"
abstract: "The first movement of the \"Duromat\" caliber series from Pforzheim. With this movement a long success story was started in 1952."
preview_image: "durowe_552-mini.jpg"
image: "Durowe_552.jpg"
movementlistkey: "durowe"
caliberkey: "552"
manufacturers: ["durowe"]
manufacturers_weight: 552
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  rates:
    ZO: "+5"
    ZU: "+12"
    3O: "+47"
    6O: "+36"
    9O: "+29"
    12O: "-30"
  amplitudes:
    ZO: "223"
    ZU: "226"
    3O: "175"
    6O: "170"
    9O: "176"
    12O: "180"
  beaterrors:
    ZO: "3.0"
    ZU: "3.3"
    3O: "4.6"
    6O: "4.8"
    9O: "3.8"
    12O: "4.3"
usagegallery: 
  - image: "e/Ebs/EBS_HAU_Durowe_552.jpg"
    description: "EBS gents watch"
  - image: "l/licht/Licht_Automatic_HAU_Durowe_552.jpg"
    description: "Licht Automatic gents watch"
labor: |
  The specimen shown here was house in a very dirty case and suffered from a blocked selfwinding system and poor rates. After a service, the rates are much better now, only the damaged wheel on the selfwinding system could not be repaired.
---
Lorem Ipsum