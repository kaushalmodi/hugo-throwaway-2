---
title: "DuRoWe 585"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 585","DuRoWe","585","Deutsche Uhren-Rohwerke","Pforzheim","17 Jewels","Deutschland","17 Steine"]
description: "DuRoWe 585"
abstract: ""
preview_image: "durowe_585-mini.jpg"
image: "Durowe_585.jpg"
movementlistkey: "durowe"
caliberkey: "585"
manufacturers: ["durowe"]
manufacturers_weight: 585
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/laco/Laco_HAU.jpg"
    description: "Laco Herrenuhr"
---
Lorem Ipsum