---
title: "DuRoWe 753"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["DuRoWe 753","form movement","17 Jewels","screw balance","Incabloc","Germany"]
description: "DuRoWe 753"
abstract: ""
preview_image: "durowe_753-mini.jpg"
image: "Durowe_753.jpg"
movementlistkey: "durowe"
caliberkey: "753"
manufacturers: ["durowe"]
manufacturers_weight: 753
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/laco/Laco_DAU.jpg"
    description: "Laco ladies' watch"
---
Lorem Ipsum