---
title: "DuRoWe 50"
date: 2010-01-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["messingfarbig","angliert","Schraubenunruh","Ölsenke","DuRoWe 50","DuRoWe","50","15 Jewels","Deutsche Uhrenrohwerke","Pforzheim","Formwerk","15 Steine","Deutschland"]
description: "DuRoWe 50 - Eines der ersten Werke der Durowe aus den 40er oder 30er Jahren."
abstract: ""
preview_image: "durowe_50-mini.jpg"
image: "Durowe_50.jpg"
movementlistkey: "durowe"
caliberkey: "50"
manufacturers: ["durowe"]
manufacturers_weight: 50
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum