---
title: "DuRoWe 435"
date: 2015-01-15T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 435","DuRoWe","Handaufzug","Datum","Zentralsekunde","17 Jewels","Schraubenunruh","Duroswing"]
description: "DuRoWe 435 - Ein eher seltenes Handaufzugswerk mit Datum der Deutschen Uhren-Rohwerke aus Mitte der 50er Jahre"
abstract: "Aus der Mitte der 50er Jahre stammt dieses deutsche Handaufzugswerk mit seiner ungewöhnlichen Brückenform"
preview_image: "durowe_435-mini.jpg"
image: "Durowe_435.jpg"
movementlistkey: "durowe"
caliberkey: "435"
manufacturers: ["durowe"]
manufacturers_weight: 435
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>Das vorliegende Exemplar kam in sehr gutem Zustand ins Labor, um das Werk bestmöglich zu präsentieren, wurde noch ein Service mit Reinigung durchgeführt, soweit das Werk bestmöglich auf der Zeitwaage reguliert.</p><p>Auf der Zeitwaage zeigt das Werk relativ gute Werte, die allerdings stark lageabhängig sind. Möglicherweise liegt hier schon ein größerer Verschleiß der Unruhwelle vor.</p><p>Die gemessenen Amplitudenwerte sind mit Vorsicht zu genießen, da der genaue Hebewinkel unbekannt ist!</p><p>\{zeitwaage\}ZO=+20|2.4|210,ZU=+8|2.2|208,12O=-4|3.0|175,3O=-30|3.0|179,6O=-185|2.7|171,9O=-170|2.8|162\{/zeitwaage\}</p>
donation: "Dieses Werk samt Adora-Uhr ist eine Spende von [Gundula Stein](/supporters/gundula-stein/). Ganz herzlichen Dank dafür!"
usagegallery: 
  - image: "a/adora/Adora_HAU_Durowe_435.jpg"
    description: "Adora Herrenuhr"
---
Lorem Ipsum