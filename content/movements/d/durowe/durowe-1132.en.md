---
title: "DuRoWe 1132"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Durowe 1132","Durowe","1132","Deutsche Uhren-Roh-Werke","Deutsche Uhrenrohwerke","Pforzheim","17 Jewels","Germany","17 Rubis","screw balance"]
description: "DuRoWe 1132"
abstract: ""
preview_image: "durowe_1132-mini.jpg"
image: "Durowe_1132.jpg"
movementlistkey: "durowe"
caliberkey: "1132"
manufacturers: ["durowe"]
manufacturers_weight: 1132
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/lort/Lort_HAU.jpg"
    description: "Lort gents watch 'Made in Canada'"
---
Lorem Ipsum