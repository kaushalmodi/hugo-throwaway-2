---
title: "DuRoWe 55"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Christoph Lorenz","DuRoWe 55","DuRoWe","Deutsche Uhren Rohwerke","55","17 Jewels","17 Rubis","ladies` watch","form movement"]
description: "DuRoWe 55"
abstract: ""
preview_image: "durowe_55-mini.jpg"
image: "Durowe_55.jpg"
movementlistkey: "durowe"
caliberkey: "55"
manufacturers: ["durowe"]
manufacturers_weight: 55
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/stowa/Stowa_Amor.jpg"
    description: "Stowa Amor ladies' watch"
---
Lorem Ipsum