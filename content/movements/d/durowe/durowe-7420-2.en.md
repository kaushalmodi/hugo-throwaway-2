---
title: "DuRoWe 7420/2 (INT)"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["DuRoWe 7420/2","DuRoWe","7420/2","INT 7420/2","7420","Deutsche Uhren Rohwerke","17 Jewels","Germany"]
description: "DuRoWe 7420/2 (INT)"
abstract: ""
preview_image: "durowe_7420_2-mini.jpg"
image: "Durowe_7420_2.jpg"
movementlistkey: "durowe"
caliberkey: "7420/2"
manufacturers: ["durowe"]
manufacturers_weight: 74202
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ankra/Ankra_710.jpg"
    description: "Ankra 710 gents watch"
links: |
  * [Ranfft Uhren: DuRoWe 7420/2](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Durowe_7420_2)
---
Lorem Ipsum