---
title: "DuRoWe 1032"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Durowe 1032","Durowe","1032","Deutsche Uhren-Roh-Werke","Deutsche Uhrenrohwerke","Pforzheim","21 Jewels","Germany"]
description: "DuRoWe 1032"
abstract: ""
preview_image: "durowe_1032-mini.jpg"
image: "Durowe_1032.jpg"
movementlistkey: "durowe"
caliberkey: "1032"
manufacturers: ["durowe"]
manufacturers_weight: 1032
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/licht/Licht_HAU.jpg"
    description: "Licht gents watch"
  - image: "e/ehr/Ehr_HAU.jpg"
    description: "Ehr gents watch"
---
Lorem Ipsum