---
title: "DuRoWe 260"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe","Deutsche Uhren Rohwerke","Pforzheim","Schwaben","Schwarzwald","21"]
description: "DuRoWe 260"
abstract: ""
preview_image: "durowe_260-mini.jpg"
image: "Durowe_260.jpg"
movementlistkey: "durowe"
caliberkey: "260"
manufacturers: ["durowe"]
manufacturers_weight: 260
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum