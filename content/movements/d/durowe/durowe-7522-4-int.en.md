---
title: "DuRoWe 7522/4 (INT)"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["DuRoWe 7522/4 INT","DuRoWe","INT","7522","7522/4","Deutsche Uhrenrohwerke","Pforzheim","17 Jewels","mechanical","selfwindig","automatic","balance","germany"]
description: "DuRoWe 7522/4 (INT)"
abstract: ""
preview_image: "durowe_7522_4-mini.jpg"
image: "Durowe_7522_4.jpg"
movementlistkey: "durowe"
caliberkey: "7522/4 INT"
manufacturers: ["durowe"]
manufacturers_weight: 75224
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/osco/Osco_Automatic.jpg"
    description: "Osco Automatic gents watch"
---
Lorem Ipsum