---
title: "DuRoWe 422"
date: 2009-06-28T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 422","DuRoWe","422","17 Jewels","Deutsche UhrenRohwerke","Pforzheim","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","17 Steine","Handaufzug","Schraubenunruh"]
description: "DuRoWe 422"
abstract: ""
preview_image: "durowe_422-mini.jpg"
image: "Durowe_422.jpg"
movementlistkey: "durowe"
caliberkey: "422"
manufacturers: ["durowe"]
manufacturers_weight: 422
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: DuRoWe 422](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Durowe_422)
  * [Watch Wiki: DuRoWe 422](https://watch-wiki.org/index.php?title=Durowe_422)
usagegallery: 
  - image: "l/licht/Licht_HAU_2.jpg"
    description: "Licht Herrenuhr"
---
Lorem Ipsum