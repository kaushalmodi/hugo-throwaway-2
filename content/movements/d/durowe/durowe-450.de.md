---
title: "DuRoWe 450 / Kienzle 071/17"
date: 2016-04-24T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 450","Kienzle 071/17","17 Jewels","17 Rubis","Palettenanker"]
description: "DuRoWe 450 / Kienzle 071/17"
abstract: ""
preview_image: "durowe_450-mini.jpg"
image: "Durowe_450.jpg"
movementlistkey: "durowe"
caliberkey: "450"
manufacturers: ["durowe"]
manufacturers_weight: 450
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum