---
title: "DuRoWe 590/1"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 590/1","DuRoWe 590","DuRoWe","590/1","590","Deutsche Uhrenrohwerke","Duromat","Pforzheim","30 Jewels","mechanisch","Automatik","Automatic","Deutschland","Steine"]
description: "DuRoWe 590/1"
abstract: ""
preview_image: "durowe_590_1-mini.jpg"
image: "Durowe_590_1.jpg"
movementlistkey: "durowe"
caliberkey: "590/1"
manufacturers: ["durowe"]
manufacturers_weight: 5901
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/ducado/Ducado_Anker_Automatic.jpg"
    description: "Ducado Anker Automatic Herrenuhr"
---
Lorem Ipsum