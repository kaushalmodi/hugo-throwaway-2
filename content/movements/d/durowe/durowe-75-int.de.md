---
title: "DuRoWe 75 (INT)"
date: 2010-01-06T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Ebauches S.A.","INT","Duro-Shock","Palettenankerwerk","Kupplungsaufzug","Durowe 75","Durowe","75","17 Jewels","Pforzheim","Hummel","Laco","Lacher","Deutschland","Formwerk"]
description: "Durowe 75 - Ein typisches Damenuhrenformwerk."
abstract: ""
preview_image: "durowe_75-mini.jpg"
image: "Durowe_75.jpg"
movementlistkey: "durowe"
caliberkey: "75 (INT)"
manufacturers: ["durowe"]
manufacturers_weight: 75
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
timegrapher_old: 
  description: |
    Das vorliegende Exemplar des DuRoWe 75 kam komplett verharzt in die Werkstatt, wurde zerlegt, gereinigt, geölt und wieder zusammengesetzt. Auf der Zeitwaage zeigt es nun sehr ordentliche Gangwerte, und wären nicht die beiden Außreißer bei "3 oben" und "Zifferblatt unten", würde es sogar im Chronometerbereich laufen. Für ein Formwerk dieser Größe eine erstaunlich gute Performance!
  images:
    ZO: Zeitwaage_Durowe_75_ZO.jpg
    ZU: Zeitwaage_Durowe_75_ZU.jpg
    3O: Zeitwaage_Durowe_75_3O.jpg
    6O: Zeitwaage_Durowe_75_6O.jpg
    9O: Zeitwaage_Durowe_75_9O.jpg
    12O: Zeitwaage_Durowe_75_12O.jpg
links: |
  <p>* [Ranfft Uhren: Durowe 75](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Durowe_75) (im Bild ist allerdings die Version des AS 1977 gezeigt)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende dieses Werks!</p>
---
Lorem Ipsum