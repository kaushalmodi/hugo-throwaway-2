---
title: "DuRoWe 70"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Durowe 70","Formwerk","Palettenanker","17 Jewels","17 Rubis"]
description: "Durowe 70"
abstract: ""
preview_image: "durowe_70-mini.jpg"
image: "Durowe_70.jpg"
movementlistkey: "durowe"
caliberkey: "70"
manufacturers: ["durowe"]
manufacturers_weight: 70
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum