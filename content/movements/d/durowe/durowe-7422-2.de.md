---
title: "DuRoWe 7422/2 (INT) / Dugena 2113"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["7422/2","7422/2","7422","INT 7422/2","INT 7422","INT","Dugena 2113","2113","Dugena","Durowe","Deutsche Uhren Rohwerke","17 Jewels","mechanisch","Handaufzug","Deutschland"]
description: "DuRoWe 7422/2 (INT)"
abstract: ""
preview_image: "durowe_7422_2-mini.jpg"
image: "Durowe_7422_2.jpg"
movementlistkey: "durowe"
caliberkey: "7422/2"
manufacturers: ["durowe"]
manufacturers_weight: 74222
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum