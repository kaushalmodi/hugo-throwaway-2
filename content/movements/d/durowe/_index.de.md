---
title: "Durowe"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "de"
keywords: ["DuRoWe","Deutsche Uhren-Rohwerke","Pforzheim","Laco"]
categories: ["movements","movements_d"]
movementlistkey: "durowe"
abstract: "Uhrwerke der DuRoWe, der Deutschen Uhren-Rohwerke aus Pforzheim."
description: "Uhrwerke der DuRoWe, der Deutschen Uhren-Rohwerke aus Pforzheim."
---
(Deutsche Uhren-Roh-Werke (DuRoWe), Ludwig Hummel, Pforzheim, Deutschland)
{{< movementlist "durowe" >}}

{{< movementgallery "durowe" >}}