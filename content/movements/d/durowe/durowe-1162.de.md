---
title: "DuRoWe 1162"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 1162","DuRoWe","Deutsche Uhren Rohwerke","25 Jewels","automatic","mechanisch","Handaufzug","Deutschland","Automatik","25 Steine"]
description: "DuRoWe 1162"
abstract: ""
preview_image: "durowe_1162-mini.jpg"
image: "Durowe_1162.jpg"
movementlistkey: "durowe"
caliberkey: "1162"
manufacturers: ["durowe"]
manufacturers_weight: 1162
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum