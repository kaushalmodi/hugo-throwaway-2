---
title: "DuRoWe 900 / Timex M82"
date: 2010-07-23T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Laco 900","Timex 82","Laco","Timex Lady Electric","Timex M82","M82","Durowe","ladies' watch","8","jewels","battery"]
description: "Durowe 900 / Timex M82"
abstract: ""
preview_image: "durowe_900-mini.jpg"
image: "Durowe_900.jpg"
movementlistkey: "durowe"
caliberkey: "900"
manufacturers: ["durowe"]
manufacturers_weight: 900
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/9214-8270.jpg"
    description: "Timex Electric ladies' watch"
---
Lorem Ipsum