---
title: "DuRoWe 50"
date: 2010-01-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["gilt","angled","screw balance","oil sink","DuRoWe 50","DuRoWe","50","15 Jewels","Deutsche Uhrenrohwerke","Pforzheim","form movement","germany"]
description: "DuRoWe 50 - One of the first Durowe movements from the 40ies or 30ies."
abstract: ""
preview_image: "durowe_50-mini.jpg"
image: "Durowe_50.jpg"
movementlistkey: "durowe"
caliberkey: "50"
manufacturers: ["durowe"]
manufacturers_weight: 50
categories: ["movements","movements_d","movements_d_durowe_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum