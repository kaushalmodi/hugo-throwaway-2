---
title: "DuRoWe 7525 (INT)"
date: 2009-06-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 7525 (INT)","DuRoWe","7525 (INT)","INT 7525","Deutsche Uhren Rohwerke","25 Jewels","automatic","selfwinding","Duromat","Deutschland","25 Steine","Automatik"]
description: "DuRoWe 7525 (INT)"
abstract: ""
preview_image: "durowe_7525-mini.jpg"
image: "Durowe_7525.jpg"
movementlistkey: "durowe"
caliberkey: "7525"
manufacturers: ["durowe"]
manufacturers_weight: 7525
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_HAU_Automatic_25_Rubis.jpg"
    description: "Anker Automatic Herrenuhr"
---
Lorem Ipsum