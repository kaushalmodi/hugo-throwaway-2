---
title: "DuRoWe 7410/2 (INT)"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 7410/2","DuRoWe","7410/2","INT 7410/2","7410","Deutsche Uhren Rohwerke","17 Jewels","Deutschland","17 Steine"]
description: "DuRoWe 7410/2 (INT)"
abstract: ""
preview_image: "durowe_7410_2-mini.jpg"
image: "Durowe_7410_2.jpg"
movementlistkey: "durowe"
caliberkey: "7410/2"
manufacturers: ["durowe"]
manufacturers_weight: 74102
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/avon/Avon.jpg"
    description: "Avon Werbeuhr"
links: |
  * [Ranfft Uhren: DuRoWe 7410/2](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Durowe_7410_2)
---
Lorem Ipsum