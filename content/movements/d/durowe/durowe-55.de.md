---
title: "DuRoWe 55"
date: 2009-04-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["DuRoWe 55","DuRoWe","Deutsche Uhren Rohwerke","55","17 Jewels","17","Steine","Damenuhr","Formwerk"]
description: "DuRoWe 55"
abstract: ""
preview_image: "durowe_55-mini.jpg"
image: "Durowe_55.jpg"
movementlistkey: "durowe"
caliberkey: "55"
manufacturers: ["durowe"]
manufacturers_weight: 55
categories: ["movements","movements_d","movements_d_durowe"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/stowa/Stowa_Amor.jpg"
    description: "Stowa Amor Damenuhr"
---
Lorem Ipsum