---
title: "Dugena"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "en"
keywords: ["Dugena","Germany"]
categories: ["movements","movements_d"]
movementlistkey: "dugena"
abstract: "non-inhouse movement, which were used and labelled by Dugena"
description: "non-inhouse movement, which were used and labelled by Dugena"
---
(Deutsche Uhrengenossenschaft Alpina, Eisenach/Berlin/Darmstadt, Germany)

Dugena did not produce own movement, but used movements of many different manufacturies and gave them their own numbers. Because of this and because of the fact, that all Dugena movements were listed in the Flume Werksucher issues, they are displayed here.
{{< movementlist "dugena" >}}

{{< movementgallery "dugena" >}}