---
title: "Derby 34"
date: 2009-05-03T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Derby 34","Derby","D34","34","10 Jewels","mechanisch","Handaufzug","Schweiz","Steine","Zylinderhemmung","Cylinder"]
description: "Derby 34"
abstract: ""
preview_image: "derby_34-mini.jpg"
image: "Derby_34.jpg"
movementlistkey: "derby"
caliberkey: "34"
manufacturers: ["derby"]
manufacturers_weight: 34
categories: ["movements","movements_d","movements_d_derby"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "m/mady/Mady_DAU.jpg"
    description: "Mady Damenuhr"
---
Lorem Ipsum