---
title: "Derby"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "de"
keywords: ["Derby","Schweiz","Rohwerk","Uhrwerk"]
categories: ["movements","movements_d"]
movementlistkey: "derby"
abstract: "Uhrwerke des schweizer Herstellers Derby"
description: "Uhrwerke des schweizer Herstellers Derby"
---
(Derby, Schweiz)
{{< movementlist "derby" >}}

{{< movementgallery "derby" >}}