---
title: "Derby"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["d"]
language: "en"
keywords: ["Derby","Swiss","Switzerland","movements"]
categories: ["movements","movements_d"]
movementlistkey: "derby"
abstract: "Movements from the swiss manufacturer Derby"
description: "Movements from the swiss manufacturer Derby"
---
(Derby, Switzerland)
{{< movementlist "derby" >}}

{{< movementgallery "derby" >}}