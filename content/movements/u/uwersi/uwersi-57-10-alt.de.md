---
title: "Uwersi 57/10 alt"
date: 2016-01-23T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Uwersi 57/10 alt","Uwersi","57/10","VUFE","Uhrenwerk Ersingen","Vereinigte Uhrenfabriken Ersingen","Zentralsekunde","21 Jewels","21 Steine","21 Rubis","Palettenanker"]
description: "Uwersi 57/10 alt - Eines der qualitativ besten Uhrwerke der Uwersi (VUFE)."
abstract: "Eines der hochwertigsten Uhrwerke der Uwersi mit Palettenankerhemmung und jeder Menge Decksteinen."
preview_image: "uwersi_57_10_alt-mini.jpg"
image: "Uwersi_57_10_alt.jpg"
movementlistkey: "uwersi"
caliberkey: "57/10 alt"
manufacturers: ["uwersi"]
manufacturers_weight: 571000
categories: ["movements","movements_u","movements_u_uwersi"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/anker/Anker_HAU_Uwersi_57_10.jpg"
    description: "Anker Herrenuhr"
donation: "Das vorliegende Werk wurde von [Klaus Brunnemer](/supporters/klaus-brunnemer/) gespendet. Ganz herzlichen Dank dafür!"
labor: |
  Das vorliegende Exemplar kam verharzt und mit defektem Aufzug in die Werkstatt. Es wurde zwar ein Service durchgeführt, aber die fehlende Feder der Sperrklinke konnte mangels Ersatzteilen noch nicht ersetzt werden. Aus diesem Grund konnten auch keine Messungen auf der Zeitwaage durchgeführt werden.
---
Lorem Ipsum