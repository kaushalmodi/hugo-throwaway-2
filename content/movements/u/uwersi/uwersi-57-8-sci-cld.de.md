---
title: "Uwersi 57/8 (SCI CLD)"
date: 2009-11-20T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Uwersi 57/8","Uhrenwerke Ersingen","Helmut Epperlein","Vereinigte Uhrenfabriken","Vereinigte Uhrenfabriken Ersingen","VUFE","V.U.F.E","21 Rubis","21 Jewels","21 Steine","Stiftanker","Deutschland"]
description: "Uwersi 57/8 mit indirekter Zentralsekunde, Kalender und Stiftanker mit Steinen"
abstract: ""
preview_image: "uwersi_57_8_sci_cld-mini.jpg"
image: "Uwersi_57_8_SCI_CLD.jpg"
movementlistkey: "uwersi"
caliberkey: "57/8 SCI CLD"
manufacturers: ["uwersi"]
manufacturers_weight: 570820
categories: ["movements","movements_u","movements_u_uwersi"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_HAU_21_Rubis.jpg"
    description: "Anker Herrenuhr"
---
Lorem Ipsum