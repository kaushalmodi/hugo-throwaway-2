---
title: "Uwersi 57/6"
date: 2017-03-05T01:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Uwersi","VUFE","Epperlein","Ersingen","57/6","form movement","pallet lever","17 Jewels","17 Rubis","manual wind","rocking bar"]
description: "Uwersi 57/6: A small form movement for ladies' watches"
abstract: "The smallest Uwersi manual wind movement even has got a true pallet lever escapement."
preview_image: "uwersi_57_6-mini.jpg"
image: "Uwersi_57_6.jpg"
movementlistkey: "uwersi"
caliberkey: "57/6"
manufacturers: ["uwersi"]
manufacturers_weight: 570600
categories: ["movements","movements_u","movements_u_uwersi_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/anker/Anker_DAU_Uwersi_57_6.jpg"
    description: "Anker ladies' watch"
---
Lorem Ipsum