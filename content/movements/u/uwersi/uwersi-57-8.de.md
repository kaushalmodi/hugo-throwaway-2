---
title: "Uwersi 57/8"
date: 2015-01-02T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Uwersi 57/8","Uwersi","57/8","2 Jewels","5 Jewels","Stiftanker","Pfeilerwerk"]
description: "Uwersi 57/8 - ein Stiftankerwerk aus Deutschland, das in verschiedenen Ausführungen erhältlich war"
abstract: "Ein recht einfaches deutsches Stiftankerwerk, das in diversen Ausführungen erhältlich war"
preview_image: "uwersi_57_8-mini.jpg"
image: "Uwersi_57_8.jpg"
movementlistkey: "uwersi"
caliberkey: "57/8"
manufacturers: ["uwersi"]
manufacturers_weight: 570800
categories: ["movements","movements_u","movements_u_uwersi"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/shaon/Shaon_HAU_Uwersi_57_8_s.jpg"
    description: "Shaon Herrenuhr"
labor: |
  <p>Das vorliegende Exemplar kam im verdreckten und verharzten Zustand in das Labor und bekam einen Vollservice mit Reinigung und anschließendem Ölen verpaßt. Anschließend lief es zwar wieder, aber die Gangwerte sind nicht berauschend und typisch für ein schlechtes Stiftankerwerk. Möglicherweise sind auch die Unruhlager schon zu sehr verschlissen, das würde die in Summe über drei Minuten Gangabweichung über alle Lagen erklären:</p><p>\{zeitwaage\}ZO=+44|2.5|164,ZU=-54|3.1|189,12O=-125|3.8|130,3O=-150|-|-,6O=-147|3.8|134,9O=-138|3.8|127\{/zeitwaage\}</p>
---
Lorem Ipsum