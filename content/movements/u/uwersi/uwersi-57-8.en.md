---
title: "Uwersi 57/8"
date: 2015-01-04T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Uwersi 57/8","Uwersi","57/8","2 Jewels","5 Jewels","pin lever","pillar construction"]
description: "Uwersi 57/8 - a german pin lever movement, which was available in several versions."
abstract: "A rather simple german pin lever movement, which was available in different versions."
preview_image: "uwersi_57_8-mini.jpg"
image: "Uwersi_57_8.jpg"
movementlistkey: "uwersi"
caliberkey: "57/8"
manufacturers: ["uwersi"]
manufacturers_weight: 570800
categories: ["movements","movements_u","movements_u_uwersi_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>The specimen shown here was dirty and gummed and got a full service, including cleaning and oling. It was brought back to life, but the rates are pretty poor, but not too bad for a simple pin lever movement. Probably, the balance wheel bearings are already too much worn, that would at least justify the large deviations over all positions:</p><p>\{zeitwaage\}ZO=+44|2.5|164,ZU=-54|3.1|189,12O=-125|3.8|130,3O=-150|-|-,6O=-147|3.8|134,9O=-138|3.8|127\{/zeitwaage\}</p>
usagegallery: 
  - image: "s/shaon/Shaon_HAU_Uwersi_57_8_s.jpg"
    description: "Shaon mens' watch"
---
Lorem Ipsum