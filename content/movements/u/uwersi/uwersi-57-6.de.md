---
title: "Uwersi 57/6"
date: 2017-03-05T01:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Uwersi","VUFE","Epperlein","Ersingen","57/6","Formwerk","Palettenanker","17 Jewels","17 Rubis","17 Steine","Wippenaufzug","Handaufzug"]
description: "Uwersi 57/6: Ein kleines Formwerk für Damenuhren"
abstract: "Das kleinste Handaufzugswerk der Uwersi besitzt sogar eine reinrassige Palettenankerhemmung. "
preview_image: "uwersi_57_6-mini.jpg"
image: "Uwersi_57_6.jpg"
movementlistkey: "uwersi"
caliberkey: "57/6"
manufacturers: ["uwersi"]
manufacturers_weight: 570600
categories: ["movements","movements_u","movements_u_uwersi"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "a/anker/Anker_DAU_Uwersi_57_6.jpg"
    description: "Anker Damenuhr"
---
Lorem Ipsum