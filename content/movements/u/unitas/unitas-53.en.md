---
title: "Unitas 53"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Unitas 53","Unitas","53","15 Jewels","form movement","caliber","switzerland"]
description: "Unitas 53"
abstract: ""
preview_image: "unitas_53-mini.jpg"
image: "Unitas_53.jpg"
movementlistkey: "unitas"
caliberkey: "53"
manufacturers: ["unitas"]
manufacturers_weight: 530
categories: ["movements","movements_u","movements_u_unitas_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "f/fako/Fako_Watch.jpg"
    description: "Fako Watch ladies' watch"
---
Lorem Ipsum