---
title: "Unitas 6325"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Unitas 6325","Unitas","6325","21 Jewels","swiss"]
description: "Unitas 6325"
abstract: ""
preview_image: "unitas_6325-mini.jpg"
image: "Unitas_6325.jpg"
movementlistkey: "unitas"
caliberkey: "6325"
manufacturers: ["unitas"]
manufacturers_weight: 63250
categories: ["movements","movements_u","movements_u_unitas_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/omikron/Omikron.jpg"
    description: "Omikron gents watch"
  - image: "a/ankra/Ankra_74.jpg"
    description: "Ankra 74 gents watch"
links: |
  * [Ranfft Uhren: Unitas 6325](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Unitas_6325)
---
Lorem Ipsum