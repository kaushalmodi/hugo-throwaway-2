---
title: "Urofa 542"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Urofa 542","Urofa","542","Dugena 814","Dugena","814","Deutsche Uhrenrohwerke Fabrik","15 Jewels","15 Steine","Formwerk","Deutschland"]
description: "Urofa 542"
abstract: ""
preview_image: "urofa_542-mini.jpg"
image: "Urofa_542.jpg"
movementlistkey: "urofa"
caliberkey: "542"
manufacturers: ["urofa"]
manufacturers_weight: 542
categories: ["movements","movements_u","movements_u_urofa"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "d/dugena/Dugena_DAU_2.jpg"
    description: "Dugena Damenuhr"
---
Lorem Ipsum