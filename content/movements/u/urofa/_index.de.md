---
title: "Urofa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["u"]
language: "de"
keywords: ["Urofa","Uhren-Rohwerke-Fabrik","Glashütte","Deutschland"]
categories: ["movements","movements_u"]
movementlistkey: "urofa"
description: "Uhrwerke des Glashütter Herstellers Urofa (Uhren-Rohwerke-Fabrik) aus Deutschland."
abstract: "Uhrwerke des Glashütter Herstellers Urofa (Uhren-Rohwerke-Fabrik) aus Deutschland."
---
(Uhren-Rohwerke-Fabrik Glashütte AG, Glashütte, Deutschland)
{{< movementlist "urofa" >}}

{{< movementgallery "urofa" >}}