---
title: "Urofa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["u"]
language: "en"
keywords: ["Urofa","Uhren-Rohwerke-Fabrik","Glashütte","Germany"]
categories: ["movements","movements_u"]
movementlistkey: "urofa"
description: "Movements of Urofa, the Uhren-Rohwerke-Fabrik of Glashütte, Germany"
abstract: "Movements of Urofa, the Uhren-Rohwerke-Fabrik of Glashütte, Germany"
---
(Uhren-Rohwerke-Fabrik Glashütte AG, Glashütte, Germany)
{{< movementlist "urofa" >}}

{{< movementgallery "urofa" >}}