---
title: "UMF 53-52 (M 9)"
date: 2017-09-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["UMF","Ruhla","DDR","M9","UMF 53-52","53-52","pin lever","center second","GDR"]
description: "UMF 53-52 / UMF 9: A pin lever movement with center second"
abstract: "A rather unknown pin lever movement with center second indication, produced in the early 1960ies. Also known as UMF M 9."
preview_image: "umf_53-52-mini.jpg"
image: "UMF_53-52.jpg"
movementlistkey: "umf"
caliberkey: "53-52"
manufacturers: ["umf"]
manufacturers_weight: 5352
categories: ["movements","movements_u","movements_u_umf_en"]
widgets:
  relatedmovements: true
featured: ["true"]
---
Lorem Ipsum