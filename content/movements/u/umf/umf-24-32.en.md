---
title: "UMF 24-32"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["UMF 24-32","UMF","24-32","Ruhla","0 Jewels","pin lever","GDR","Eastern Germany"]
description: "UMF 24-32"
abstract: ""
preview_image: "umf_24-32-mini.jpg"
image: "UMF_24-32.jpg"
movementlistkey: "umf"
caliberkey: "24-32"
manufacturers: ["umf"]
manufacturers_weight: 2432
categories: ["movements","movements_u","movements_u_umf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: UMF 24-32](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&UMF_24_32)
usagegallery: 
  - image: "r/ruhla/Ruhla_HAU.jpg"
    description: "Ruhla gents watch"
  - image: "r/ruhla/Ruhla_Lady_Star.jpg"
    description: "Ruhla Lady Star ladies' watch"
---
Lorem Ipsum