---
title: "UMF 33-50"
date: 2009-06-20T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["UMF 33-50","UMF","33-50","7 Jewels","DDR","GDR","7 Steine","Handaufzug"]
description: "UMF 33-50"
abstract: ""
preview_image: "umf_33-50-mini.jpg"
image: "UMF_33-50.jpg"
movementlistkey: "umf"
caliberkey: "33-50"
manufacturers: ["umf"]
manufacturers_weight: 3350
categories: ["movements","movements_u","movements_u_umf"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/ruhla/Ruhla_7Jewels_DAU.jpg"
    description: "Ruhla Damenuhr"
links: |
  * [Ranfft Uhren: UMF 33-50](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&UMF_33_50)
  * [](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&UMF_24_33)[Watch Wiki: UMF 33-50](http://www.watch-wiki.de/index.php?title=UMF_33-50)
---
Lorem Ipsum