---
title: "UMF 24-33"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["UMF 24-33","UMF","24-33","Ruhla","0 Jewels","0 Steine","Stiftanker","DDR","Deutschland"]
description: "UMF 24-33"
abstract: ""
preview_image: "umf_24-33-mini.jpg"
image: "UMF_24-33.jpg"
movementlistkey: "umf"
caliberkey: "24-33"
manufacturers: ["umf"]
manufacturers_weight: 2433
categories: ["movements","movements_u","movements_u_umf"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: UMF 24-33](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&UMF_24_33)
usagegallery: 
  - image: "k/karex/Karex_DAU.jpg"
    description: "Karex Damenuhr"
---
Lorem Ipsum