---
title: "UMF 24-26"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["UMF 24-26","UMF 24","Ruhla","24","Ruhla 24","DDR","eye mover","million","millions","Germany","germany","Eastern Germany","GDR"]
description: "UMF 24-26"
abstract: ""
preview_image: "umf_24-26-mini.jpg"
image: "UMF_24-26.jpg"
movementlistkey: "umf"
caliberkey: "24-26"
manufacturers: ["umf"]
manufacturers_weight: 2426
categories: ["movements","movements_u","movements_u_umf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "s/saxon/Saxon_Augenwender.jpg"
    description: "Saxon eye mover watch"
---
Lorem Ipsum