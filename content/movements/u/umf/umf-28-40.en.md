---
title: "UMF 28-40"
date: 2009-05-30T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["UMF 28-40","UMF","28-40","Ruhla","0 Jewels","Quarz","Quartz","UMF 24","Kaliber 24","Caliber 24","pin lever","GDR","Eastern Germany"]
description: "UMF 28-40"
abstract: ""
preview_image: "umf_28-40-mini.jpg"
image: "UMF_28-40.jpg"
movementlistkey: "umf"
caliberkey: "28-40"
manufacturers: ["umf"]
manufacturers_weight: 2840
categories: ["movements","movements_u","movements_u_umf_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "r/ruhla/Ruhla_Quarz_HAU.jpg"
    description: "Ruhla Quarz 32768 gents watch"
---
Lorem Ipsum