---
title: "UMF"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["u"]
language: "en"
keywords: ["UMF","Ruhla","VEB. DDR"]
categories: ["movements","movements_u"]
movementlistkey: "umf"
abstract: "Uhrwerke der UMF (VEB Uhren und Maschinenfabrik Ruhla, Deutschland)"
description: "Uhrwerke der UMF (VEB Uhren und Maschinenfabrik Ruhla, Deutschland)"
---
(VEB Uhren und Maschinenfabrik Ruhla, Ruhla, Germany (former GDR))
{{< movementlist "umf" >}}

{{< movementgallery "umf" >}}