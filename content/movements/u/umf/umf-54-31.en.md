---
title: "UMF 54-31"
date: 2018-03-11T01:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["UMF 54-31","UMF M 54","UMF Start","Thiel Start","pin lever","0 Jewels","balance bridge","GDR","DDR","Ruhla"]
description: "UMF 54-31: A large, simple constructed pin lever movement, which debuted as Thias Start and was also known as UMF Start and UMF M 54."
abstract: "The east german pin lever movement, which debuted as \"Thiel Start\", here in its last but one version from 1959"
preview_image: "umf_54-31-mini.jpg"
image: "UMF_54-31.jpg"
movementlistkey: "umf"
caliberkey: "54-31"
manufacturers: ["umf"]
manufacturers_weight: 5431
categories: ["movements","movements_u","movements_u_umf_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "u/umf/UMF_HU_UMF_54-31.jpg"
    description: "UMF gents watch, model 414/4/701"
donation: "This movement is a kind donation from [Mario](/supporters/mario/). Thank you very much for your great support of the movement archive."
---
Lorem Ipsum