---
title: "Venus"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["v"]
language: "de"
keywords: ["Venus","Moutier","Münster","Schweiz"]
categories: ["movements","movements_v"]
movementlistkey: "venus"
description: "Uhrwerke des schweizer Herstellers Venus aus Moutier (Münster)"
abstract: "Uhrwerke des schweizer Herstellers Venus aus Moutier (Münster)"
---
(Venus S.A., Moutier (Münster), Schweiz)
{{< movementlist "venus" >}}

{{< movementgallery "venus" >}}