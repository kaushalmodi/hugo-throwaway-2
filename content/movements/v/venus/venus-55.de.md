---
title: "Venus 55"
date: 2010-04-06T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Formwerk","Schraubenunruh","langen Rückerzeiger","Palettenanker","Chaton","Ölsenke","Kupplungsaufzug","Venus 55","Venus","55","15 Jewels","Chaton","Schweiz","15 Steine"]
description: "Venus 55 - Ein gut verarbeitetes Damen-Formwerk mit 15 Steinen. Detaillierte Beschreibung mit Bildern und Datenblatt."
abstract: ""
preview_image: "venus_55-mini.jpg"
image: "Venus_55.jpg"
movementlistkey: "venus"
caliberkey: "55"
manufacturers: ["venus"]
manufacturers_weight: 55
categories: ["movements","movements_v","movements_v_venus"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ankra/Ankra_DAU_3.jpg"
    description: "Ankra Damenuhr"
---
Lorem Ipsum