---
title: "Venus"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["v"]
language: "en"
keywords: ["Venus","Moutier","Münster","Switzerland","Swiss"]
categories: ["movements","movements_v"]
movementlistkey: "venus"
abstract: "Movements of the swiss manufacturer Venus from Moutier (Münster)"
description: "Movements of the swiss manufacturer Venus from Moutier (Münster)"
---
(Venus S.A., Moutier (Münster), Switzerland)
{{< movementlist "venus" >}}

{{< movementgallery "venus" >}}