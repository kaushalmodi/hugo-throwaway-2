---
title: "Venus 55"
date: 2010-04-06T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["form movement","screw balace","long regulator arm","pallet lever","chatons","yoke winding","Venus 55","Venus","55","15 Jewels","Chaton","swiss","switzerland"]
description: "Venus 55 - A quality ladies' form watch movement with 15 jewels. Detailed description with photos and data sheet."
abstract: ""
preview_image: "venus_55-mini.jpg"
image: "Venus_55.jpg"
movementlistkey: "venus"
caliberkey: "55"
manufacturers: ["venus"]
manufacturers_weight: 55
categories: ["movements","movements_v","movements_v_venus_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/ankra/Ankra_DAU_3.jpg"
    description: "Ankra ladies' watch"
---
Lorem Ipsum