---
title: "Venus 54"
date: 2011-03-19T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Venus 54 - A manual wind form movement with 15 jewels. Detailed description with photos","video and data sheet."]
description: "Venus 54 - A manual wind form movement with 15 jewels. Detailed description with photos, video and data sheet."
abstract: ""
preview_image: "venus_54-mini.jpg"
image: "Venus_54.jpg"
movementlistkey: "venus"
caliberkey: "54"
manufacturers: ["venus"]
manufacturers_weight: 54
categories: ["movements","movements_v","movements_v_venus_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_Zifferblatt_Venus_54.jpg"
    description: "unmarked ladies' watch"
---
Lorem Ipsum