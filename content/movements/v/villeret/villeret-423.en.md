---
title: "Villeret 423"
date: 2016-12-27T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Villeret 423","Aurore","Incabloc","Screw balance","17 Jewels","small second","decentral second","manual wind"]
description: "Villeret 423 - a simple, but well made manual wind movement"
abstract: "A simple, but very well made manual wind movement."
preview_image: "villeret_423-mini.jpg"
image: "Villeret_423.jpg"
movementlistkey: "villeret"
caliberkey: "423"
manufacturers: ["villeret"]
manufacturers_weight: 423
categories: ["movements","movements_v","movements_v_villeret_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  The specimen shown here came without case into the lab. Although cannon pinion and hour wheel are missing, it performs pretty well on the timegrapher. If it had been serviced, it might have shown even better rates.
timegrapher_old: 
  rates:
    ZO: "-11"
    ZU: "+1"
    3O: "-3"
    6O: "+5"
    9O: "+23"
    12O: "0"
  amplitudes:
    ZO: "211"
    ZU: "274"
    3O: "210"
    6O: "179"
    9O: "167"
    12O: "198"
  beaterrors:
    ZO: "2.0"
    ZU: "1.7"
    3O: "2.5"
    6O: "2.4"
    9O: "2.0"
    12O: "2.3"
---
Lorem Ipsum