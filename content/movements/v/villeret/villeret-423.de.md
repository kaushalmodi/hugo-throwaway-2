---
title: "Villeret 423"
date: 2016-03-04T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Villeret 423","Aurore","Incabloc","Schraubenunruh","17 Jewels","kleine Sekunde","dezentrale Sekunde"]
description: "Villeret 423 - ein einfaches, aber sehr gut verarbeitetes Handaufzugswerk"
abstract: "Ein einfaches, aber sehr gut verarbeitetes Handaufzugswerk"
preview_image: "villeret_423-mini.jpg"
image: "Villeret_423.jpg"
movementlistkey: "villeret"
caliberkey: "423"
manufacturers: ["villeret"]
manufacturers_weight: 423
categories: ["movements","movements_v","movements_v_villeret"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  rates:
    ZO: "-11"
    ZU: "+1"
    3O: "-3"
    6O: "+5"
    9O: "+23"
    12O: "0"
  amplitudes:
    ZO: "211"
    ZU: "274"
    3O: "210"
    6O: "179"
    9O: "167"
    12O: "198"
  beaterrors:
    ZO: "2.0"
    ZU: "1.7"
    3O: "2.5"
    6O: "2.4"
    9O: "2.0"
    12O: "2.3"
labor: |
  Das hier gezeigte Exemplar kam als loses Werk ins Labor. Obwohl ihm Minuten- und Stundenrohr fehlen, zeigt es auf der Zeitwaage in fast allen Positionen gute Gangwerte. Eine Reinigung wurde nicht vorgenommen, diese hätte vermutlich nochmals bessere Gangwerte zur Folge gehabt.
---
Lorem Ipsum