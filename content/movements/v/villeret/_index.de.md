---
title: "Villeret"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["v"]
language: "de"
keywords: ["Villeret","Aurore Villeret","Aurore","Schweiz"]
categories: ["movements","movements_v"]
movementlistkey: "villeret"
description: "Uhrwerke des schweizer Herstellers Villeret / Aurore Villeret"
abstract: "Uhrwerke des schweizer Herstellers Villeret / Aurore Villeret"
---
(Aurore Villeret, Fabrique d'ébauches Bernoises S.A., Villeret, Schweiz)
{{< movementlist "villeret" >}}

{{< movementgallery "villeret" >}}