---
title: "Zaria 2016"
date: 2011-01-15T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["achsengelagert","beidseitigen Aufzug","Zaria 2016","Zaria","2016","30 Jewels","Penza","Pensa","Automatic","Russland","UdSSR","30 Steine","Automatik"]
description: "Zaria 2016"
abstract: ""
preview_image: "zaria_2016-mini.jpg"
image: "Zaria_2016.jpg"
movementlistkey: "zaria"
caliberkey: "2016"
manufacturers: ["zaria"]
manufacturers_weight: 201600
categories: ["movements","movements_z","movements_z_zaria"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
usagegallery: 
  - image: "z/zaria/Zaria_DAU_Zifferblatt_Zaria_2016.jpg"
    description: "Zaria Damenuhr  (ohne Gehäuse)"
---
Lorem Ipsum