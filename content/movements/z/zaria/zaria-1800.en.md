---
title: "Zaria 1800"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Zaria 1800","Zaria","1800","Zarja","Saria","Sarja","16 Jewels","Russia","USSR","Sowjet Union"]
description: "Zaria 1800"
abstract: ""
preview_image: "zaria_1800-mini.jpg"
image: "Zaria_1800.jpg"
movementlistkey: "zaria"
caliberkey: "1800"
manufacturers: ["zaria"]
manufacturers_weight: 180000
categories: ["movements","movements_z","movements_z_zaria_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "z/zaria/Zaria_DAU.jpg"
    description: "Zaria ladies' watch"
  - image: "u/umf/UMF_Ruhla_DAU_16_Rubis.jpg"
    description: "UMF Ruhla ladies' watch"
  - image: "z/zaria/Zaria_DAU_3.jpg"
    description: "Zaria ladies' watch"
---
Lorem Ipsum