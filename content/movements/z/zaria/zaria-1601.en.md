---
title: "Zaria 1601"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Zaria 1601","Zaria","Zaria 1601","1601","1601","Russia","UdSSR","Sowjet Union"]
description: "Zaria 1601"
abstract: ""
preview_image: "zaria_1601-mini.jpg"
image: "Zaria_1601.jpg"
movementlistkey: "zaria"
caliberkey: "1601"
manufacturers: ["zaria"]
manufacturers_weight: 160100
categories: ["movements","movements_z","movements_z_zaria_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/Pinguin.jpg"
    description: "motive watch &quot;Penguin&quot;"
  - image: "g/geneve/Geneve_DAU.jpg"
    description: "Geneve ladies' watch"
---
Lorem Ipsum