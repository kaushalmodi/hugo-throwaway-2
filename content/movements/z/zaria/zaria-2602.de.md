---
title: "Zaria 2602"
date: 2009-04-15T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Zaria 2602","Sarja 2602","Zaria","Saria","Penza 2602","Penza","2602","Swjesda","15 Jewels","Russland","UdSSR","Sowjetunion"]
description: "Zaria 2602"
abstract: ""
preview_image: "zaria_2602-mini.jpg"
image: "Zaria_2602.jpg"
movementlistkey: "zaria"
caliberkey: "2602"
manufacturers: ["zaria"]
manufacturers_weight: 260200
categories: ["movements","movements_z","movements_z_zaria"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "p/penza/Penza_HAU.jpg"
    description: "Penza Offiziersuhr"
---
Lorem Ipsum