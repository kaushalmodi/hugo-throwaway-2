---
title: "Zenith 57.8"
date: 2014-10-09T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Zenith 57.8","Zenith","57","15 Jewels","form movement","manual wind","hand wound","ladies' watch"]
description: "Zenith 57.8 - a tiny form movement, beating with 21600 A/h."
abstract: "A tiny, very beautiful form movement, which, despite its high age, already beats with 21600 A/h."
preview_image: "zenith_57.8-mini.jpg"
image: "Zenith_57.8.jpg"
movementlistkey: "zenith"
caliberkey: "57.8"
manufacturers: ["zenith"]
manufacturers_weight: 578
categories: ["movements","movements_z","movements_z_zenith_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "z/zenith/Zenith_DAU_Zifferblatt.jpg"
    description: "Zenith ladies' watch  (only dial)"
---
Lorem Ipsum