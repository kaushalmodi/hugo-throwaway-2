---
title: "Zenith 57.8"
date: 2014-10-07T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Zenith 57.8","Zenith","57","15 Jewels","Formwerk","Handaufzug","Damenuhr"]
description: "Zenith 57.8 - ein kleines Formwerk, das bereits mit 21600 Halbschwingungen pro Stunde arbeitet"
abstract: "Ein kleines, optisch sehr ansprechendes Formwerk, das trotz seines Alters bereits mit 21600 Halbschwingungen pro Stunde arbeitete"
preview_image: "zenith_57.8-mini.jpg"
image: "Zenith_57.8.jpg"
movementlistkey: "zenith"
caliberkey: "57.8"
manufacturers: ["zenith"]
manufacturers_weight: 578
categories: ["movements","movements_z","movements_z_zenith"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "z/zenith/Zenith_DAU_Zifferblatt.jpg"
    description: "Zenith Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum