---
title: "Kasper 200 alt"
date: 2010-01-08T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kasper 200 alt","Kasper","200 alt","10 Jewels","Germany","ladies` watch","form movement","cylinder escapement"]
description: "Kasper 200 alt (old)"
abstract: ""
preview_image: "kasper_200_alt-mini.jpg"
image: "Kasper_200_alt.jpg"
movementlistkey: "kasper"
caliberkey: "200 alt"
manufacturers: ["kasper"]
manufacturers_weight: 2001
categories: ["movements","movements_k","movements_k_kasper_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "f/fleig/Fleig_DAU.jpg"
    description: "Fleig ladies' watch"
---
Lorem Ipsum