---
title: "Kasper 100"
date: 2011-04-18T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kasper 100","Kasper","100","6 Jewels","Pforzheim","Deutschland","6 Steine","Zylinderwerk"]
description: "Kasper 100 - Ein Vorkriegs-Zylinderwerk mit 6 Steinen. Detaillierte Beschreibung mit Bildern, Video und Datenblatt."
abstract: ""
preview_image: "kasper_100-mini.jpg"
image: "Kasper_100.jpg"
movementlistkey: "kasper"
caliberkey: "100"
manufacturers: ["kasper"]
manufacturers_weight: 1000
categories: ["movements","movements_k","movements_k_kasper"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [Klaus Brunnemer](/supporters/klaus-brunnemer/) für die Spende dieses Werks!"
usagegallery: 
  - image: "anonym/DAU_Kasper_100.jpg"
    description: "anonyme Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum