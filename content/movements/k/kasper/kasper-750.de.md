---
title: "Kasper 750"
date: 2017-10-23T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kasper 750","Kasper","Palettenanker","17 Jewels","Monorex","17 Rubis","Zentralsekunde","Pfeilerwerk","Wippenaufzug"]
description: "Kasper 750 - Ein qualitativ hochwertiges Pfeilerwerk mit Palettenanker"
abstract: "Ein recht hochwertiges Ankerwerk in Pfeilerbauweise mit einem ungewöhnlich konstruierten Wippenaufzug."
preview_image: "kasper_750-mini.jpg"
image: "Kasper_750.jpg"
movementlistkey: "kasper"
caliberkey: "750"
manufacturers: ["kasper"]
manufacturers_weight: 7500
categories: ["movements","movements_k","movements_k_kasper"]
widgets:
  relatedmovements: true
featured: ["true"]
links: |
  * [Ranfft Uhren: Kasper 750](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&Kasper_750) (gezeigt wird vermutlich ein Kasper 700)
usagegallery: 
  - image: "a/anker/Anker_HU_Kasper_750.jpg"
    description: "Anker Herrenuhr"
---
Lorem Ipsum