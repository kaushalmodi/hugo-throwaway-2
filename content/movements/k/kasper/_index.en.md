---
title: "Kasper"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["k"]
language: "en"
keywords: ["Kasper","Emil Kasper","Pforzheim","Germany","movement"]
categories: ["movements","movements_k"]
movementlistkey: "kasper"
description: "Movements of the german manufacturer Kasper from Pforzheim"
abstract: "Movements of the german manufacturer Kasper from Pforzheim"
---
(Emil Kasper & Co., Pforzheim, Germany)
{{< movementlist "kasper" >}}

{{< movementgallery "kasper" >}}