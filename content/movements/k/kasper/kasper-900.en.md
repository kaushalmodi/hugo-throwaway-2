---
title: "Kasper 900"
date: 2017-03-10T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["pallet lever movement","directly driven center second","Monorex shock protection","screw balance","long regulator arm","yoke winding system","Kasper 900","Kasper","900","Pforzheim","17 Jewels","germany","21 Jewels","21 Rubis"]
description: "Kasper 900 - A high quality selfwinding movement with 35 jewels. Detailed description with photos, video and data sheet."
abstract: "The version with 21 jewels is now also part of the movement archive."
preview_image: "kasper_900-mini.jpg"
image: "Kasper_900.jpg"
movementlistkey: "kasper"
caliberkey: "900"
manufacturers: ["kasper"]
manufacturers_weight: 9000
categories: ["movements","movements_k","movements_k_kasper_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "e/ebp/EBP_Monorex_HAU_Kasper_900.jpg"
    description: "E.B.P. Monorex gents watch"
  - image: "a/anker/Anker_HAU_Kasper_900.jpg"
    description: "Anker gents watch"
donation: "The version with 21 jewels, including the watch with the black dial, is a kind donation of the [Ristow family](/supporters/ristow/). Thank you very much for the support of the movement archive!"
links: |
  * [Ranfft Uhren: Kasper 900](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?13&ranfft&0&2uswk&Kasper_900&)
---
Lorem Ipsum