---
title: "Kasper 1120"
date: 2018-03-03T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kasper 1120","Kasper","Palettenanker","17 Jewels","Incabloc","17 Rubis","Zentralsekunde","Formwerk"]
description: "Kasper 1120 - Eines der kleinsten Formwerke mit Zentralsekunde"
abstract: "Eines der kleinsten Formwerke mit Zentralsekunde"
preview_image: "kasper_1120-mini.jpg"
image: "Kasper_1120.jpg"
movementlistkey: "kasper"
caliberkey: "1120"
manufacturers: ["kasper"]
manufacturers_weight: 11200
categories: ["movements","movements_k","movements_k_kasper"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "k/kasper/Kasper_DU_Kasper_1120.jpg"
    description: "Kasper Damenuhr"
---
Lorem Ipsum