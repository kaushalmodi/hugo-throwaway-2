---
title: "Kasper 1120"
date: 2018-03-09T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kasper 1120","Kasper","pallet lever","17 Jewels","Incabloc","17 Rubis","center second","form movement","Germany"]
description: "Kasper 1120 - One of the smallest form movements with center second"
abstract: "One of the smallest form movements with center second"
preview_image: "kasper_1120-mini.jpg"
image: "Kasper_1120.jpg"
movementlistkey: "kasper"
caliberkey: "1120"
manufacturers: ["kasper"]
manufacturers_weight: 11200
categories: ["movements","movements_k","movements_k_kasper_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "k/kasper/Kasper_DU_Kasper_1120.jpg"
    description: "Kasper ladies' watch"
---
Lorem Ipsum