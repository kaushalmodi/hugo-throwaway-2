---
title: "Kasper 100"
date: 2011-04-18T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kasper 100","Kasper","100","6 Jewels","Pforzheim","germany","cylinder movement"]
description: "Kasper 100 - A pre-WWII cylinder movement with 6 jewels. Detailed description with photos, video and data sheet."
abstract: ""
preview_image: "kasper_100-mini.jpg"
image: "Kasper_100.jpg"
movementlistkey: "kasper"
caliberkey: "100"
manufacturers: ["kasper"]
manufacturers_weight: 1000
categories: ["movements","movements_k","movements_k_kasper_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "anonym/DAU_Kasper_100.jpg"
    description: "anonymous ladies' watch  (dial only)"
---
Lorem Ipsum