---
title: "Kasper 600"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kasper 600","Kasper","600","15 Jewels","Damenuhr","Formwerk","Deutschland"]
description: "Kasper 600"
abstract: ""
preview_image: "kasper_600-mini.jpg"
image: "Kasper_600.jpg"
movementlistkey: "kasper"
caliberkey: "600"
manufacturers: ["kasper"]
manufacturers_weight: 6000
categories: ["movements","movements_k","movements_k_kasper"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kasper/Kasper_DAU.jpg"
    description: "Kasper Damenuhr"
---
Lorem Ipsum