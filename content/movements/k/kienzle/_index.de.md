---
title: "Kienzle"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["k"]
language: "de"
keywords: ["Kienzle","Schwenningen"]
categories: ["movements","movements_k"]
movementlistkey: "kienzle"
description: "Uhrwerke der Kienzle Uhrenfabriken GmbH aus Schwenningen"
abstract: "Uhrwerke der Kienzle Uhrenfabriken GmbH aus Schwenningen"
---
(Kienzle Uhrenfabriken GmbH, Schwenningen, Deutschland)
{{< movementlist "kienzle" >}}

{{< movementgallery "kienzle" >}}