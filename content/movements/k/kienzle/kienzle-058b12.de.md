---
title: "Kienzle 058b12"
date: 2016-03-27T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 058b12","Kienzle","058","W058","Stiftanker","1 Jewel","1 Rubis","Wippenaufzug"]
description: "Kienzle 058b12 - Ein modern konstruiertes Stiftankerwerk mit Datumsanzeige"
abstract: "Ein modern konstruiertes, großes Handaufzugswerk mit Datumsanzeige"
preview_image: "kienzle_058b12-mini.jpg"
image: "Kienzle_058b12.jpg"
movementlistkey: "kienzle"
caliberkey: "058b12"
manufacturers: ["kienzle"]
manufacturers_weight: 5812
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "Dieses Werk (und beide Uhren) wurde von [Erwin](/supporters/erwin/) gespendet. Ganz herzlichen Dank dafür!"
timegrapher_old: 
  rates:
    ZO: "+13"
    ZU: "+6"
    3O: "+15"
    6O: "+4"
    9O: "+32"
    12O: "+45"
  amplitudes:
    ZO: "286"
    ZU: "292"
    3O: "301"
    6O: "297"
    9O: "301"
    12O: "293"
  beaterrors:
    ZO: "0.2"
    ZU: "0.8"
    3O: "0.3"
    6O: "0.6"
    9O: "0.9"
    12O: "0.7"
usagegallery: 
  - image: "k/kienzle/Kienzle_Markant_HAU_Chrom_Kienzle_058b12.jpg"
    description: "Kienzle Markant Herrenuhr"
  - image: "k/kienzle/Kienzle_Markant_HAU_Gold_Kienzle_058b12.jpg"
    description: "Kienzle Markant Herrenuhr"
labor: |
  Dank einer Spende lagen zwei Werke bzw. Uhren vor. Da beide verharzt waren und zudem bei einer Uhr die Zifferblattfüße abgebrochen sind, konnte eine Uhr wieder optimal aus zwei Werken hergestellt werden. Natürlich mußte eine Komplettreinigung der Werke durchgeführt werden, aber das Ergebnis kann sich durchaus sehen lassen!
---
Lorem Ipsum