---
title: "Kienzle 48/10c"
date: 2009-11-28T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 48/10c","Kienzle","48","10c","Schwenningen","10 Jewels","10 Steine","Stiftanker"]
description: "Kienzle 48/10c"
abstract: ""
preview_image: "kienzle_48_10c-mini.jpg"
image: "Kienzle_48_10c.jpg"
movementlistkey: "kienzle"
caliberkey: "48/10c"
manufacturers: ["kienzle"]
manufacturers_weight: 48103
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_DAU_Kienzle_048_10c.jpg"
    description: "Kienzle Damenuhr"
---
Lorem Ipsum