---
title: "Kienzle 058b25"
date: 2009-04-25T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 058b25","Kienzle","058b25","Schwenningen","17 Jewels","Germany","pallet lever"]
description: "Kienzle 058b25"
abstract: ""
preview_image: "kienzle_058b25-mini.jpg"
image: "Kienzle_058b25.jpg"
movementlistkey: "kienzle"
caliberkey: "058b25"
manufacturers: ["kienzle"]
manufacturers_weight: 58252
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Kienzle 058b25](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&a0&2uswk&Kienzle_058b25)
usagegallery: 
  - image: "k/kienzle/Kienzle_Sport_17Jewels.jpg"
    description: "Kienzle Sport gents watch"
---
Lorem Ipsum