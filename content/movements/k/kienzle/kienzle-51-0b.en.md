---
title: "Kienzle 51/0b"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 51/0b","Kienzle","51","0b Schwenningen","pin lever","Germany"]
description: "Kienzle 51/0b"
abstract: ""
preview_image: "kienzle_51_0b-mini.jpg"
image: "Kienzle_51_0b.jpg"
movementlistkey: "kienzle"
caliberkey: "51/0b"
manufacturers: ["kienzle"]
manufacturers_weight: 51002
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_2.jpg"
    description: "Kienzle gents watch"
---
Lorem Ipsum