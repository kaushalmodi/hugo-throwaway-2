---
title: "Kienzle 051/00f"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 051/00f","Schwenningen","0 Jewels","Steine","Stiftanker"]
description: "Kienzle 051/00f"
abstract: ""
preview_image: "kienzle_051_00f-mini.jpg"
image: "Kienzle_051_00f.jpg"
movementlistkey: "kienzle"
caliberkey: "051/00f"
manufacturers: ["kienzle"]
manufacturers_weight: 51006
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_4.jpg"
    description: "Kienzle Herrenuhr"
---
Lorem Ipsum