---
title: "Kienzle 058b12"
date: 2016-12-28T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 058b12","Kienzle","058","W058","pin lever","1 Jewel","1 Rubis","rocking bar"]
description: "Kienzle 058b12 - A modern constructed pin lever movement with center second"
abstract: "A modern constructed, large manual wind movement with date indication"
preview_image: "kienzle_058b12-mini.jpg"
image: "Kienzle_058b12.jpg"
movementlistkey: "kienzle"
caliberkey: "058b12"
manufacturers: ["kienzle"]
manufacturers_weight: 5812
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  Thanks to a donation, there are two movements (watches) with that movement. Since both were gummed, and additionally on one of the watches the dial feed were broken, it was possible to reconstruct one working watch with parts of both watches. Of course a revision had to be made, but the result was good!
timegrapher_old: 
  rates:
    ZO: "+13"
    ZU: "+6"
    3O: "+15"
    6O: "+4"
    9O: "+32"
    12O: "+45"
  amplitudes:
    ZO: "286"
    ZU: "292"
    3O: "301"
    6O: "297"
    9O: "301"
    12O: "293"
  beaterrors:
    ZO: "0.2"
    ZU: "0.8"
    3O: "0.3"
    6O: "0.6"
    9O: "0.9"
    12O: "0.7"
usagegallery: 
  - image: "k/kienzle/Kienzle_Markant_HAU_Chrom_Kienzle_058b12.jpg"
    description: "Kienzle Markant gents watch"
  - image: "k/kienzle/Kienzle_Markant_HAU_Gold_Kienzle_058b12.jpg"
    description: "Kienzle Markant gents watch"
donation: "This movement (and both watches) were kindly donated by [Erwin](/supporters/erwin/). Thank you very much!"
---
Lorem Ipsum