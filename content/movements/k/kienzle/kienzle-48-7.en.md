---
title: "Kienzle 48/7"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 48/7","Kienzle","48","Uhr","Uhren","Armbanduhr","Stiftanker","Germany","pin lever","ruby","rubis","jewels","7 jewels"]
description: "Kienzle 48/7"
abstract: ""
preview_image: "kienzle_48_7-mini.jpg"
image: "Kienzle_48_7.jpg"
movementlistkey: "kienzle"
caliberkey: "48/7"
manufacturers: ["kienzle"]
manufacturers_weight: 48700
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum