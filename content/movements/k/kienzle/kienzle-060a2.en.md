---
title: "Kienzle 060a2"
date: 2015-02-01T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 060a2","Kienzle 060","17 Jewels","17 Rubis","pin lever","ruby-pin lever","date","center second"]
description: "Kienzle 060a2 - A manual wind movement with 17 jewels and an own Kienzle ruby-pin lever excapement"
abstract: "A rather late but very modern Kienzle manual wind movement with 17 jewels and an inhouse made pin lever escapement with ruby pins."
preview_image: "kienzle_060a2-mini.jpg"
image: "Kienzle_060a2.jpg"
movementlistkey: "kienzle"
caliberkey: "060a2"
manufacturers: ["kienzle"]
manufacturers_weight: 602
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "k/kienzle/Kienzle_life_HAU_Kienzle_060a2.jpg"
    description: "Kienzle life mens' watch"
---
Lorem Ipsum