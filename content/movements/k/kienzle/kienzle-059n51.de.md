---
title: "Kienzle 059N51"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 059N51 Kienzle 059","059N51","Schwenningen","3 Jewels","Damenuhr","Deutschland","3 Steine"]
description: "Kienzle 059N51"
abstract: ""
preview_image: "kienzle_059n51-mini.jpg"
image: "Kienzle_059N51.jpg"
movementlistkey: "kienzle"
caliberkey: "059N51"
manufacturers: ["kienzle"]
manufacturers_weight: 59519
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_Madame.jpg"
    description: "Kienzle Madame Damenuhr"
---
Lorem Ipsum