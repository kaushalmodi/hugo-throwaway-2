---
title: "Kienzle 057/21d"
date: 2011-03-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 057/21d","21 Jewels","Schwenningen","Deutschland","21 Steine","Stiftanker","Volksautomatik"]
description: "Kienzle 057/21d"
abstract: ""
preview_image: "kienzle_057_21d-mini.jpg"
image: "Kienzle_057_21d.jpg"
movementlistkey: "kienzle"
caliberkey: "057/21d"
manufacturers: ["kienzle"]
manufacturers_weight: 57214
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: Kienzle 057/21](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?01&zenoshop&a0&2uswk&Kienzle_057_21)
labor: |
  Das vorliegende Werk kam lose, also ohne Gehäuse, ins Labor. Es wurde komplett zerlegt, gereinigt und geölt, wobei das Ölen der Ankerstifte nicht trivial ist und möglicherweise auch nicht optimal erfolgt ist.
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_Automatic_Kienzle_057_21d.jpg"
    description: "Kienzle Automatic Herrenuhr  (nur Zifferblatt)"
timegrapher_old: 
  description: |
    Die Gangwerte sind durchwachsen, für ein Stiftankerwerk aber durchaus nicht schlecht. Möglicherweise sind sie durch das nicht optimale Ölen des Ausgangs-Ankerstifts etwas verfälscht.
  images:
    ZO: Zeitwaage_Kienzle_057_21d_ZO.jpg
    ZU: Zeitwaage_Kienzle_057_21d_ZU.jpg
    3O: Zeitwaage_Kienzle_057_21d_3O.jpg
    6O: Zeitwaage_Kienzle_057_21d_6O.jpg
    9O: Zeitwaage_Kienzle_057_21d_9O.jpg
    12O: Zeitwaage_Kienzle_057_21d_12O.jpg
---
Lorem Ipsum