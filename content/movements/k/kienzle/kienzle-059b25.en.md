---
title: "Kienzle 059b25"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 059b25","Kienzle","059b25","Schwenningen","17 Jewels","Germany"]
description: "Kienzle 059b25"
abstract: ""
preview_image: "kienzle_059b25-mini.jpg"
image: "Kienzle_059b25.jpg"
movementlistkey: "kienzle"
caliberkey: "059b25"
manufacturers: ["kienzle"]
manufacturers_weight: 59252
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_Sport.jpg"
    description: "Kienzle Sport gents watch"
---
Lorem Ipsum