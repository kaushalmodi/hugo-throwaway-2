---
title: "Kienzle 057/21d"
date: 2011-03-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 057/21d","21 Jewels","Schwenningen","Germany","21 Rubis","pin lever","Volksautomatik"]
description: "Kienzle 057/21d"
abstract: ""
preview_image: "kienzle_057_21d-mini.jpg"
image: "Kienzle_057_21d.jpg"
movementlistkey: "kienzle"
caliberkey: "057/21d"
manufacturers: ["kienzle"]
manufacturers_weight: 57214
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_HAU_Automatic_Kienzle_057_21d.jpg"
    description: "Kienzle gents watch  (dial only)"
timegrapher_old: 
  description: |
    The specimen tested here came into the lab without case. It got a full service, but the oiling of the escapement pins was non trivial and maybe not optimal done, too.     The timing results are quite OK, especially for a pin lever movement. Probably due to the problematic oiling of the release pin, they are worse than they have to be.
  images:
    ZO: Zeitwaage_Kienzle_057_21d_ZO.jpg
    ZU: Zeitwaage_Kienzle_057_21d_ZU.jpg
    3O: Zeitwaage_Kienzle_057_21d_3O.jpg
    6O: Zeitwaage_Kienzle_057_21d_6O.jpg
    9O: Zeitwaage_Kienzle_057_21d_9O.jpg
    12O: Zeitwaage_Kienzle_057_21d_12O.jpg
links: |
  * [Ranfft Uhren: Kienzle 057/21](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?11&zenoshop&a0&2uswk&Kienzle_057_21)
---
Lorem Ipsum