---
title: "Kienzle 060a1"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Kienzle 060a1","Kienzle","060a1","Schwenningen","7 Jewels","7 Steine","Stiftanker","Steinanker"]
description: "Kienzle 060a1"
abstract: ""
preview_image: "kienzle_060a1-mini.jpg"
image: "Kienzle_060a1.jpg"
movementlistkey: "kienzle"
caliberkey: "060a1"
manufacturers: ["kienzle"]
manufacturers_weight: 60011
categories: ["movements","movements_k","movements_k_kienzle_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "k/kienzle/Kienzle_life_HAU.jpg"
    description: "Kienzle life gents watch"
---
Lorem Ipsum