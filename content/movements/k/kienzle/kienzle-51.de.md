---
title: "Kienzle 51"
date: 2009-04-11T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 51","Kienzle","51","4 Jewels","4 Steine","Handaufzug","Deutschland"]
description: "Kienzle 51"
abstract: ""
preview_image: "kienzle_51-mini.jpg"
image: "Kienzle_51.jpg"
movementlistkey: "kienzle"
caliberkey: "51"
manufacturers: ["kienzle"]
manufacturers_weight: 51000
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum