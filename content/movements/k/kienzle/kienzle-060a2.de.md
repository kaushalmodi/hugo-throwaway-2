---
title: "Kienzle 060a2"
date: 2015-01-17T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Kienzle 060a2","Kienzle 060","17 Jewels","17 Steine","Stiftanker","Rubin-Stiftanker","Datum","Zentralsekunde"]
description: "Kienzle 060a2 - Ein Handaufzugswerk mit 17 Steinen und der kienzle-eigenen Rubin-Stiftankerhemmung"
abstract: "Ein vergleichsweise spätes aber sehr modernes Kienzle-Handaufzugswerk mit 17 Steinen und hauseigener Stiftanker-Hemmung mit Rubinstiften"
preview_image: "kienzle_060a2-mini.jpg"
image: "Kienzle_060a2.jpg"
movementlistkey: "kienzle"
caliberkey: "060a2"
manufacturers: ["kienzle"]
manufacturers_weight: 602
categories: ["movements","movements_k","movements_k_kienzle"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "k/kienzle/Kienzle_life_HAU_Kienzle_060a2.jpg"
    description: "Kienzle life Herrenuhr"
---
Lorem Ipsum