---
title: "Timex M78"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M78","Timex","M78 mechanical","manual wind","ladies' watch"]
description: "Timex M78"
abstract: ""
preview_image: "timex_m78-mini.jpg"
image: "Timex_M78.jpg"
movementlistkey: "timex"
caliberkey: "M78"
manufacturers: ["timex"]
manufacturers_weight: 78
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "This movement and watch were kindly donated by [G.Keilhammer](/supporters/g-keilhammer/). Thank you very much!"
usagegallery: 
  - image: "t/timex/Timex_xxxxx-078xx.jpg"
    description: "unmarked Timex watch, 70ies"
---
Lorem Ipsum