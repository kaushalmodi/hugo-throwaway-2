---
title: "Timex M102"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M102","Timex","M102 mechanical","manual wind","ladies` watch"]
description: "Timex M102"
abstract: ""
preview_image: "timex_m102-mini.jpg"
image: "Timex_M102.jpg"
movementlistkey: "timex"
caliberkey: "M102"
manufacturers: ["timex"]
manufacturers_weight: 102
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_13850_10275.jpg"
    description: "Timex ladies' watch model 13850"
---
Lorem Ipsum