---
title: "Timex M41"
date: 2016-11-16T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M41","Timex","M41","Electric","elektrisch","elektromechanisch","Datum","Zentralsekunde","Durowe","Laco"]
description: "Timex M41 - Ein elektromechanisches Werk aus der zweiten Timex-Generation. Frühe Ausführungen dieses Werks wurden noch in Deutschland von Durowe hergestellt."
abstract: " Ein einfaches elektromechanisches Werk, das von Timex in den frühen 70er Jahren in großen Stückzahlen verbaut wurde. Seine frühen Ausführungen wurden sogar in Deutschland produziert."
preview_image: "timex_m41-mini.jpg"
image: "Timex_M41.jpg"
movementlistkey: "timex"
caliberkey: "M41"
manufacturers: ["timex"]
manufacturers_weight: 41
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_9644-4170_Zifferblatt.jpg"
    description: "Timex Herrenuhr Modell 9644  (ohne Gehäuse)"
donation: "Dieses Werk wurde von [R.Ludwig](/supporters/r-ludwig/) gespendet, ganz herzlichen Dank dafür!"
---
Lorem Ipsum