---
title: "Timex M108"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M108","Timex","M108","Automatic","Self-Wind mechanical","manual wind","mens' watch"]
description: "Timex M108"
abstract: ""
preview_image: "timex_m108-mini.jpg"
image: "Timex_M108.jpg"
movementlistkey: "timex"
caliberkey: "M108"
manufacturers: ["timex"]
manufacturers_weight: 108
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_34623-10888.jpg"
    description: "Timex gents watch model 34623"
---
Lorem Ipsum