---
title: "Timex M43"
date: 2016-01-10T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M43","Timex 43","Logotime","Quarz","Quartz","Klappanker","Wippanker","US4128992 A"]
description: "Timex M43 - Ein ungewöhnliches Quarzwerk der zweiten Generation"
abstract: "Ein sehr ungewöhnliches Quarzwerk der zweiten Generation mit einen Klappanker-Antrieb"
preview_image: "timex_m43-mini.jpg"
image: "Timex_M43.jpg"
movementlistkey: "timex"
caliberkey: "M43"
manufacturers: ["timex"]
manufacturers_weight: 43
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_56219-04379.jpg"
    description: "Timex Herrenuhr 56219-04379"
  - image: "t/timex/Timex_54218-04379.jpg"
    description: "Timex Damenuhr 54218-04379"
links: |
  * [US Patent US4128992 A - Rocking motor for low cost quartz watch](https://www.google.com/patents/US4128992) (eingetragen 21.3.1977)
labor: |
  Das vorliegende Exemplar krankte an korridierten und abgebrochenen Batteriekontakten, die aber vergleichsweise einfach ersetzt werden konnten. Seitdem schaltet es deutlich vernehmbar sehr exakt jede Minute, die tägliche Gangabweichung liegt bei -0.5 Sekunden pro Tag - unjustiert, wohlgemerkt!
---
Lorem Ipsum