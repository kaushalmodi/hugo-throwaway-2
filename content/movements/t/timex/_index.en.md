---
title: "Timex"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["t"]
language: "en"
keywords: []
categories: ["movements","movements_t"]
movementlistkey: "timex"
description: ""
abstract: "(Timex, Middlebury, Connecticut, USA)"
---
(Timex, Middlebury, Connecticut, USA)
{{< movementlist "timex" >}}

{{< movementgallery "timex" >}}