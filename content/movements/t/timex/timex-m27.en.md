---
title: "Timex M27"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M27","Timex","M27 mechanical","manual wind","mens' watch"]
description: "Timex M27"
abstract: ""
preview_image: "timex_m27-mini.jpg"
image: "Timex_M27.jpg"
movementlistkey: "timex"
caliberkey: "M27"
manufacturers: ["timex"]
manufacturers_weight: 27
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_26450-02775.jpg"
    description: "Timex gents watch model 26450"
---
Lorem Ipsum