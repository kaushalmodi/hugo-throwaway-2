---
title: "Timex M22"
date: 2014-06-01T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex","M22","Model 22","pin lever","V-Conic","center second"]
description: "Timex M22 - one of the earlies handwinding movements by Timex. Available with and without center second indication"
abstract: "One of the earlies handwound movements by Timex. Available in two versions, with and without center second."
preview_image: "timex_m22-mini.jpg"
image: "Timex_M22.jpg"
movementlistkey: "timex"
caliberkey: "M22"
manufacturers: ["timex"]
manufacturers_weight: 122
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/1010-2261.jpg"
    description: "Timex ladies' watch, model 1010, made in 1961"
---
Lorem Ipsum