---
title: "Timex M74"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex","21 jewels","Timex 21","21","Timex M74","M74","mechanical","balance","automatic","selfwinding"]
description: "Timex M74"
abstract: ""
preview_image: "timex_m74-mini.jpg"
image: "Timex_M74.jpg"
movementlistkey: "timex"
caliberkey: "M74"
manufacturers: ["timex"]
manufacturers_weight: 74
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum