---
title: "Timex M78"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M78","Timex","M78 mechanisch","Handaufzug","Damenuhr"]
description: "Timex M78"
abstract: ""
preview_image: "timex_m78-mini.jpg"
image: "Timex_M78.jpg"
movementlistkey: "timex"
caliberkey: "M78"
manufacturers: ["timex"]
manufacturers_weight: 78
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
donation: "Vielen Dank an [G.Keilhammer](/supporters/g-keilhammer/) für die Spende von Werk und Uhr!"
usagegallery: 
  - image: "t/timex/Timex_xxxxx-078xx.jpg"
    description: "unbekanntes Modell, 70er Jahre"
---
Lorem Ipsum