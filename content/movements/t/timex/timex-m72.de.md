---
title: "Timex M72"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex","21 jewels","Timex 21","21","Timex M72","72","mechanisch","Handaufzug","Steine"]
description: "Timex M72"
abstract: ""
preview_image: "timex_m72-mini.jpg"
image: "Timex_M72.jpg"
movementlistkey: "timex"
caliberkey: "M72"
manufacturers: ["timex"]
manufacturers_weight: 72
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_6524-7268.jpg"
    description: "Timex Herrenuhr Modell 6524"
links: |
  * [Artikel aus dem Timex Watch Forum  (Alles über die 21-steinigen Timex-Uhrwerke)](http://www.network54.com/Forum/message?forumid=102266&messageid=1083539182)
---
Lorem Ipsum