---
title: "Timex M22"
date: 2014-06-01T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex","M22","Model 22","Stiftanker","V-Conic","Zentralsekunde"]
description: "Timex M22 - eines der frühesten Handaufzugswerke von Timex, steinlos, mit und ohne Zentralsekunde erhältlich"
abstract: "Eines der frühesten Handaufzugswerke von Timex. Mit und ohne Zentralsekunde erhältlich"
preview_image: "timex_m22-mini.jpg"
image: "Timex_M22.jpg"
movementlistkey: "timex"
caliberkey: "M22"
manufacturers: ["timex"]
manufacturers_weight: 122
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/1010-2261.jpg"
    description: "Timex Damenuhr, Modell 1010, von 1961"
---
Lorem Ipsum