---
title: "Timex M104"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M104","Timex","M104 mechanical","manual wind"]
description: "Timex M104"
abstract: ""
preview_image: "timex_m104-mini.jpg"
image: "Timex_M104.jpg"
movementlistkey: "timex"
caliberkey: "M104"
manufacturers: ["timex"]
manufacturers_weight: 104
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_25169-10481.jpg"
    description: "Timex gents watch model 25169"
---
Lorem Ipsum