---
title: "Timex M69"
date: 2017-01-10T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex","Timex M69","Electric","Lady","69","electromechanical","mechanical","balance","battery","cell"]
description: "Timex M69"
abstract: "The smallest electromagnetical movement by Timex. It even has got a plastic lever."
preview_image: "timex_m69-mini.jpg"
image: "Timex_M69.jpg"
movementlistkey: "timex"
caliberkey: "M69"
manufacturers: ["timex"]
manufacturers_weight: 69
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["true"]
donation: "The youngest movement from Taiwan (with plastic lever) and the accompanying watch are a donation from [R.Ludwig](/supporters/r-ludwig/). Thank you very much!"
usagegallery: 
  - image: "t/timex/Timex_80160-06973.jpg"
    description: "Timex Electric ladies' watch model 80160"
  - image: "t/timex/Timex_80361-06976.jpg"
    description: "Timex Electric ladies' watch model 80361"
  - image: "t/timex/Timex_80460-6972.jpg"
    description: "Timex Electric ladies' watch model 80460"
  - image: "t/timex/Timex_802604.jpg"
    description: "Timex Electric ladies' watch model 802604"
---
Lorem Ipsum