---
title: "Timex M24"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M24","Timex","M24 mechanisch","Handaufzug","Herrenuhr"]
description: "Timex M24"
abstract: ""
preview_image: "timex_m24-mini.jpg"
image: "Timex_M24.jpg"
movementlistkey: "timex"
caliberkey: "M24"
manufacturers: ["timex"]
manufacturers_weight: 24
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_1010-2463.jpg"
    description: "Timex Damenuhr Modell 1010"
  - image: "t/timex/Timex_23271-02478.jpg"
    description: "Timex Taucheruhr Modell 23271"
---
Lorem Ipsum