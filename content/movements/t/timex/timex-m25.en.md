---
title: "Timex M25"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M25","Timex","M25 mechanical","manual wind","mens' watch"]
description: "Timex M25"
abstract: ""
preview_image: "timex_m25-mini.jpg"
image: "Timex_M24.jpg"
movementlistkey: "timex"
caliberkey: "M25"
manufacturers: ["timex"]
manufacturers_weight: 25
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_23571-2572.jpg"
    description: "Timex gents watch model 23571"
  - image: "t/timex/Timex_23770-02575.jpg"
    description: "Timex divers' watch model 23770"
  - image: "t/timex/Timex_23772-02575.jpg"
    description: "Timex divers' watch model 23772"
  - image: "t/timex/Timex_27677-02575.jpg"
    description: "Timex divers' watch model 27677"
---
Lorem Ipsum