---
title: "Timex M114"
date: 2010-08-28T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M114","Timex","M114 mechanical","manual wind"]
description: "Timex M114"
abstract: ""
preview_image: "timex_m114-mini.jpg"
image: "Timex_M114.jpg"
movementlistkey: "timex"
caliberkey: "M114"
manufacturers: ["timex"]
manufacturers_weight: 114
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_19932-11483.jpg"
    description: "Timex ladies' watch model 19932"
---
Lorem Ipsum