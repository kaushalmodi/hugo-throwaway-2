---
title: "Timex M71"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex","Timex M71","Electric","Lady","71","820","Timex 820","elektromechanisch","mechanisch","Unruh","Unruhe","Damen","Damenuhr","Batterie"]
description: "Timex M71"
abstract: ""
preview_image: "timex_m71-mini.jpg"
image: "Timex_M71.jpg"
movementlistkey: "timex"
caliberkey: "M71"
manufacturers: ["timex"]
manufacturers_weight: 71
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_83650-07173.jpg"
    description: "Timex Electric Damenuhr Modell 83650"
---
Lorem Ipsum