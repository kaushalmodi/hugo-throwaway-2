---
title: "Timex M64"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M64","Timex","M64","Quartz","Quarz","Taiwan mechanisch","elektromechanisch","Herrenuhr"]
description: "Timex M64"
abstract: " Ein Werk der ersten Quarzwerke-Generation von Timex, hier in seiner zweiten Überarbeitung. "
preview_image: "timex_m64-mini.jpg"
image: "Timex_M64.jpg"
movementlistkey: "timex"
caliberkey: "M64"
manufacturers: ["timex"]
manufacturers_weight: 64
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
---
Lorem Ipsum