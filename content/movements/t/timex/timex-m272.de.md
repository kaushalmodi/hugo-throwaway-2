---
title: "Timex M272"
date: 2018-02-03T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M272","Timex","M272","Quarz","flach","Portescap","Germany","Lavet-Schrittmotor","Lavet","Patente","DE3214683","US4376996A"]
description: "Timex M272 - Ein ultraflaches Quarzwerk mit sehr ungewöhnlichem Aufbau"
abstract: "Ein sehr ungewöhnlich konstruiertes Quarzwerk aus den frühen 80er Jahren, das durch eine Vielzahl von Patenten geschützt wurde."
preview_image: "timex_m272-mini.jpg"
image: "Timex_M272.jpg"
movementlistkey: "timex"
caliberkey: "M272"
manufacturers: ["timex"]
manufacturers_weight: 272
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_40253-27283.jpg"
    description: "Timex Damenuhr, Modell 40253"
---
Lorem Ipsum