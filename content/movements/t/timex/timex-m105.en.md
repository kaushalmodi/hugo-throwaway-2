---
title: "Timex M105"
date: 2017-01-05T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M105","Timex","M105 mechanical","manual wind"]
description: "Timex M105"
abstract: ""
preview_image: "timex_m105-mini.jpg"
image: "Timex_M105.jpg"
movementlistkey: "timex"
caliberkey: "M105"
manufacturers: ["timex"]
manufacturers_weight: 105
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_25418-10580A.jpg"
    description: "Timex gents watch model 25418"
  - image: "t/timex/Timex_27671-10578.jpg"
    description: "Timex diver model 27671"
  - image: "t/timex/Timex_27729-10579.jpg"
    description: "Timex gents watch model 27729"
donation: "The taiwanese M105 including watch were kindly donated by [R.Ludwig](/supporters/r-ludwig/). Thank you very much!"
---
Lorem Ipsum