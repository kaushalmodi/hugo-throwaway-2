---
title: "Timex"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["t"]
language: "de"
keywords: ["Timex","Connecticut","Middlebury","USA","US Time"]
categories: ["movements","movements_t"]
movementlistkey: "timex"
abstract: "Uhrwerke von Timex"
description: "Uhrwerke von Timex"
---
(Timex, Middlebury, Connecticut, USA)
{{< movementlist "timex" >}}

{{< movementgallery "timex" >}}