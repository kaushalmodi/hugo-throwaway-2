---
title: "Timex M41"
date: 2016-11-18T23:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Timex M41","Timex","M41","Electric","electromechanical","battery","Laco","center second","Durowe"]
description: "Timex M41 - An electromechanical movement from the second Timex generation. Early movements were still produced in Germany."
abstract: "A simple electromechanical movement, which Timex produced in large quantities in then 1970ies. Early versions were still produced in Germany."
preview_image: "timex_m41-mini.jpg"
image: "Timex_M41.jpg"
movementlistkey: "timex"
caliberkey: "M41"
manufacturers: ["timex"]
manufacturers_weight: 41
categories: ["movements","movements_t","movements_t_timex_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "t/timex/Timex_9644-4170_Zifferblatt.jpg"
    description: "Timex gents watch model 9644  (without case)"
donation: "This movement is a kind donation from [R.Ludwig](/supporters/r-ludwig/). Thank you very much!"
---
Lorem Ipsum