---
title: "Timex M31"
date: 2010-07-24T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Timex M31","Timex","M31","Automatic","Self-Wind mechanisch","Handaufzug","Herrenuhr"]
description: "Timex M31"
abstract: ""
preview_image: "timex_m31-mini.jpg"
image: "Timex_M31.jpg"
movementlistkey: "timex"
caliberkey: "M31"
manufacturers: ["timex"]
manufacturers_weight: 31
categories: ["movements","movements_t","movements_t_timex"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/timex/Timex_4014-3162.jpg"
    description: "Timex &quot;Self-Wind&quot; Herrenuhr Modell 4014"
---
Lorem Ipsum