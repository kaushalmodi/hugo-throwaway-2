---
title: "Tschudin 25/19"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Tschudin","25/19","Tschudin 25/19","15 Jewels","Uhr","Uhren","Armbanduhr","Armbanduhren","Kaliber","15 Steine","Handaufzug","Deutschland","Formwerk"]
description: "Tschudin 25/19"
abstract: ""
preview_image: "tschudin_25_19-mini.jpg"
image: "Tschudin_25_19.jpg"
movementlistkey: "tschudin"
caliberkey: "25/19"
manufacturers: ["tschudin"]
manufacturers_weight: 2519
categories: ["movements","movements_t","movements_t_tschudin"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "o/otero/Otero_Anonym.jpg"
    description: "unmarkierte Otero oder Epple Herrenuhr"
---
Lorem Ipsum