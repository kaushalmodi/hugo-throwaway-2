---
title: "Tschudin"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["t"]
language: "en"
keywords: ["Tschudin","Pforzheim","Weil am Rhein","Germany"]
categories: ["movements","movements_t"]
movementlistkey: "tschudin"
abstract: "Movements of the german manufacturer Tschudin, located in Pforzheim and Weil am Rhein."
description: "Movements of the german manufacturer Tschudin, located in Pforzheim and Weil am Rhein."
---
(Tschudin, Weil am Rhein / Pforzheim, Germany)
{{< movementlist "tschudin" >}}

{{< movementgallery "tschudin" >}}