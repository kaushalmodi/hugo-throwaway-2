---
title: "Tissot 709"
date: 2014-01-05T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Tissot 709","Tissot","709","Handaufzug","Damenuhr","Duofix","Incabloc"]
description: "Tissot 709 - ein kleines, hochwertiges Handaufzugswerk für Damenuhren"
abstract: "Ein kleines, hochwertiges Handaufzugswerk für Damenuhren"
preview_image: "tissot_709-mini.jpg"
image: "Tissot_709.jpg"
movementlistkey: "tissot"
caliberkey: "709"
manufacturers: ["tissot"]
manufacturers_weight: 70900
categories: ["movements","movements_t","movements_t_tissot"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tissot/Tissot_DAU_Zifferblatt_Tissot_709.jpg"
    description: "Tissot Damenuhr (nur Zifferblatt)"
---
Lorem Ipsum