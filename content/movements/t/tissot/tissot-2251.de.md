---
title: "Tissot 2251"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Tissot 2251","Tissot","2251","Lanco","Astrolon","1 Jewels","mechanisch","Handaufzug","Plastik","Steine"]
description: "Tissot 2251"
abstract: ""
preview_image: "tissot_2251-mini.jpg"
image: "Tissot_2251.jpg"
movementlistkey: "tissot"
caliberkey: "2251"
manufacturers: ["tissot"]
manufacturers_weight: 225100
categories: ["movements","movements_t","movements_t_tissot"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "l/lanco/Lanco_Astrolon_HAU.jpg"
    description: "Lanco Astrolon Herrenuhr"
links: |
  * [Ranfft Uhren: Tissot 2251](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Tissot_2251)
  * [The Tissot "Astrolon" Plastic Watch  (Detaillierter Artikel über die "Astrolon"-Werksserie)](http://members.iinet.net.au/~fotoplot/tissot/tissot.html)
---
Lorem Ipsum