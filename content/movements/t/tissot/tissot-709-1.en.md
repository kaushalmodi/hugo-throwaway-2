---
title: "Tissot 709-1"
date: 2014-01-19T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Tissot 709-1","Tissot 709","Tissot","709-1","handwind","manual wind","ladies' watch","Duofix","Incabloc"]
description: "Tissot 709-1 - a tiny handwound movement for ladies' watches"
abstract: "Ein tiny high-quality handwound movement for womens' watches"
preview_image: "tissot_709-1-mini.jpg"
image: "Tissot_709-1.jpg"
movementlistkey: "tissot"
caliberkey: "709-1"
manufacturers: ["tissot"]
manufacturers_weight: 70910
categories: ["movements","movements_t","movements_t_tissot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tissot/Tissot_DAU_Zifferblatt_Tissot_709-1.jpg"
    description: "Tissot ladies' watch  (only dial)"
---
Lorem Ipsum