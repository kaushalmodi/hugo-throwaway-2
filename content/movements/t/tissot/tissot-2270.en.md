---
title: "Tissot 2270"
date: 2009-04-14T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Tissot 2270","Tissot","2270","Aetos S.A.","Aetos","Astrolon","1 Jewels","mechanical","balance","manual wind","disposable","plastic"]
description: "Tissot 2270"
abstract: ""
preview_image: "tissot_2270-mini.jpg"
image: "Tissot_2270.jpg"
movementlistkey: "tissot"
caliberkey: "2270"
manufacturers: ["tissot"]
manufacturers_weight: 227000
categories: ["movements","movements_t","movements_t_tissot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [The Tissot "Astrolon" Plastic Watch  (Fascinating article by Paul Delury and Rob Berkavicius)](http://members.iinet.net.au/~fotoplot/tissot/tissot.html)
usagegallery: 
  - image: "l/lanco/Lanco_HAU.jpg"
    description: "Lanco gents watch"
---
Lorem Ipsum