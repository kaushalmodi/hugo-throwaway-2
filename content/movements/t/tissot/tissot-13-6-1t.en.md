---
title: "Tissot 13.6-1T"
date: 2009-08-06T22:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Tissot 13.6-1T","Tissot","13.6-1T","15 Jewels","Tissot et Fils","Le Locle","Neuenburg","swiss","switzerland","form movement"]
description: "Tissot 13.6-1T"
abstract: ""
preview_image: "tissot_13_6-1t-mini.jpg"
image: "Tissot_13_6-1T.jpg"
movementlistkey: "tissot"
caliberkey: "13.6-1T"
manufacturers: ["tissot"]
manufacturers_weight: 13611
categories: ["movements","movements_t","movements_t_tissot_en"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "t/tissot/Tissot_DAU_1_Zifferblatt.jpg"
    description: "Tissot Damenuhr  (dial only)"
links: |
  <p>* [Ranfft Uhren: Tissot 13.6-1T](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&Tissot_13_6_1T)</p><p>This movement was donated by [Harald Hoeber](index.php?option=com_content&view=article&id=61:donated-movements-of-harald-hoeber&catid=10&lang=en-GB&Itemid=201). Thank you very much!</p>
---
Lorem Ipsum