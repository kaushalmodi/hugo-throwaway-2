---
title: "Tissot 13.6-1T"
date: 2009-08-06T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Tissot 13.6-1T","Tissot","13.6-1T","15 Jewels","Tissot et Fils","Le Locle","Neuenburg","Schweiz","15 Steine","Formwerk"]
description: "Tissot 13.6-1T"
abstract: ""
preview_image: "tissot_13_6-1t-mini.jpg"
image: "Tissot_13_6-1T.jpg"
movementlistkey: "tissot"
caliberkey: "13.6-1T"
manufacturers: ["tissot"]
manufacturers_weight: 13611
categories: ["movements","movements_t","movements_t_tissot"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  <p>* [Ranfft Uhren: Tissot 13.6-1T](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&0&2uswk&Tissot_13_6_1T)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende dieses Werks!</p>
usagegallery: 
  - image: "t/tissot/Tissot_DAU_1_Zifferblatt.jpg"
    description: "Tissot Damenuhr  (nur Zifferblatt)"
---
Lorem Ipsum