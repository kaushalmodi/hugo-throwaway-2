---
title: "Thiel"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["t"]
language: "en"
keywords: ["Thiel","Ruhla","Germany"]
categories: ["movements","movements_t"]
movementlistkey: "thiel"
abstract: "Movements from the german manufacturer Thiel (Gebrüder Thiel) from Ruhla."
description: "Movements from the german manufacturer Thiel (Gebrüder Thiel) from Ruhla."
---
(Gebrüder (Brothers) Thiel GmbH Ruhla, Ruhla, Germany)
{{< movementlist "thiel" >}}

{{< movementgallery "thiel" >}}