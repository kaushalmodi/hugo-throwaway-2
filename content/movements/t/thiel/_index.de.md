---
title: "Thiel"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["t"]
language: "de"
keywords: ["Thiel","Ruhla","Deutschland"]
categories: ["movements","movements_t"]
movementlistkey: "thiel"
description: "Uhrwerke des deutschen Herstellers Thiel (Gebrüder Thiel) aus Ruhla."
abstract: "Uhrwerke des deutschen Herstellers Thiel (Gebrüder Thiel) aus Ruhla."
---
(Gebrüder Thiel GmbH Ruhla, Ruhla, Deutschland)
{{< movementlist "thiel" >}}

{{< movementgallery "thiel" >}}