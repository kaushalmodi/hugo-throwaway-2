---
title: "Newmark"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["n"]
language: "de"
keywords: []
categories: ["movements","movements_n"]
movementlistkey: "newmark"
description: "Uhrwerke von Newmark"
abstract: "Uhrwerke von Newmark"
---
(Louis Newmark Ltd., Croydon, Großbritannien)
{{< movementlist "newmark" >}}

{{< movementgallery "newmark" >}}