---
title: "Newmark 10 1/2'''"
date: 2014-10-16T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Newmark 10 1/2","Newmark","Handaufzug","5 Jewels","Stiftanker","kleine Sekunde"]
description: "Newmark 10 1/2 - ein sehr seltenes Stiftanker-Manufakturwerk aus Großbritannien"
abstract: "Auch in Großbritannien wurden in den 50er Jahren Uhrwerke hergestellt, das seltene Stiftanker-Werk Newmark 10 1/2''' ist eines davon"
preview_image: "newmark_10_1_2-mini.jpg"
image: "Newmark_10_1_2.jpg"
movementlistkey: "newmark"
caliberkey: "10 1/2'''"
manufacturers: ["newmark"]
manufacturers_weight: 1012
categories: ["movements","movements_n","movements_n_newmark"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "n/newmark/Newmark_Crescent_Newmark_10_1_2.jpg"
    description: "Newmark Crescent Cocktail-Uhr"
links: |
  Ein wenig Literatur über diesen seltenen Hersteller findet man im [WatchUSeek-Forum](http://forums.watchuseek.com/f11/newmark-watch-co-info-please-612406.html).
---
Lorem Ipsum