---
title: "Newmark"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["n"]
language: "en"
keywords: []
categories: ["movements","movements_n"]
movementlistkey: "newmark"
description: "17jewels.info - Movements from the British manufacturer Newmark"
abstract: "17jewels.info - Movements from the British manufacturer Newmark"
---
(Louis Newmark Ltd., Croydon, United Kingdom)
{{< movementlist "newmark" >}}

{{< movementgallery "newmark" >}}