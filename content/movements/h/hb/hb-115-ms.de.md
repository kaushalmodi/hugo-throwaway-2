---
title: "HB 115 MS"
date: 2009-06-05T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["HB 115MS","HB","115MS","Hermann Becker","Pforzheim","17 Jewels","17 Steine"]
description: "HB 115 MS"
abstract: ""
preview_image: "hb_115_ms-mini.jpg"
image: "HB_115MS.jpg"
movementlistkey: "hb"
caliberkey: "115 MS"
manufacturers: ["hb"]
manufacturers_weight: 115
categories: ["movements","movements_h","movements_h_hb"]
widgets:
  relatedmovements: true
featured: ["false"]
links: |
  * [Ranfft Uhren: HB 115](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&HB_115)
usagegallery: 
  - image: "h/herma/Herma_HAU.jpg"
    description: "Herma Herrenuhr"
  - image: "u/utex/Utex_HAU.jpg"
    description: "Utex Herrenuhr"
---
Lorem Ipsum