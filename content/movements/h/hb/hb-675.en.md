---
title: "HB 675"
date: 2014-03-15T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["HB 675","HB","675","Hermann Becker","manual wind","15 Jewels","15 Rubis","form movement","screw balance"]
description: "HB 675 - An early and oddly shaped ladies' watch form movement"
abstract: "An early ladies' watch form movement by HB with an interesting shape"
preview_image: "hb_675-mini.jpg"
image: "HB_675.jpg"
movementlistkey: "hb"
caliberkey: "675"
manufacturers: ["hb"]
manufacturers_weight: 675
categories: ["movements","movements_h","movements_h_hb_en"]
widgets:
  relatedmovements: true
featured: ["false"]
labor: |
  On the specimen shown here, the main spring was broken and replaced (together with the mainspring barrel), and a revision (cleaning and oling) was made. The rest was at least visually OK.
usagegallery: 
  - image: "a/anker/Anker_DAU_HB_675.jpg"
    description: "Anker ladies' watch"
timegrapher_old: 
  description: |
    In the horizontal positions, the movement performs rather well, but vertically, the rates are really bad.
  images:
    ZO: Zeitwaage_HB_675_ZO.jpg
    ZU: Zeitwaage_HB_675_ZU.jpg
    3O: Zeitwaage_HB_675_3O.jpg
    6O: Zeitwaage_HB_675_6O.jpg
    9O: Zeitwaage_HB_675_9O.jpg
    12O: Zeitwaage_HB_675_12O.jpg
  values:
    ZO: "-30"
    ZU: "+15"
    3O: "-320"
    6O: "-32"
    9O: "-145"
    12O: "-210"
---
Lorem Ipsum