---
title: "HB"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["h"]
language: "de"
keywords: ["HB","Deutschland","Hermann Becker"]
categories: ["movements","movements_h"]
movementlistkey: "hb"
description: "Uhrwerke der HB (Hermann Becker KG) aus Pforzheim in Deutschland"
abstract: "Uhrwerke der HB (Hermann Becker KG) aus Pforzheim in Deutschland"
---
(Hermann Becker KG, Pforzheim, Deutschland)
{{< movementlist "hb" >}}

{{< movementgallery "hb" >}}