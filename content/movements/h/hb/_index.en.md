---
title: "HB"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["h"]
language: "en"
keywords: ["HB","Germany","Hermann Becker"]
categories: ["movements","movements_h"]
movementlistkey: "hb"
abstract: "Movements by HB (Hermann Becker KG) from Pforzheim in Germany"
description: "Movements by HB (Hermann Becker KG) from Pforzheim in Germany"
---
(Hermann Becker KG, Pforzheim, Germany)
{{< movementlist "hb" >}}

{{< movementgallery "hb" >}}