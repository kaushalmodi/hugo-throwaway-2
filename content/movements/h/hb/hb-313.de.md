---
title: "HB 313"
date: 2009-09-20T22:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["HB 313","HB","313","Hermann Becker","Pforzheim","25 Jewels","Automatic","Ultramatic","Deutschland","25 Steine","Automatik"]
description: "HB 313"
abstract: ""
preview_image: "hb_313-mini.jpg"
image: "HB_313.jpg"
movementlistkey: "hb"
caliberkey: "313"
manufacturers: ["hb"]
manufacturers_weight: 313
categories: ["movements","movements_h","movements_h_hb"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "a/anker/Anker_HAU_Taucheruhr.jpg"
    description: "Anker Automatic Taucheruhr"
  - image: "o/oridam/Ordiam_HAU_Zifferblatt.jpg"
    description: "Ordiam Herrenuhr  (nur Zifferblatt)"
links: |
  <p>* [Ranfft Uhren: HB 313](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?00&ranfft&&2uswk&HB_313)</p><p>Vielen Dank an [Harald Hoeber](index.php?option=com_content&view=article&id=52:spenden-von-harald-hoeber&catid=9&lang=de-DE&Itemid=109) für die Spende dieses Werks in der alten Ausführung!</p>
---
Lorem Ipsum