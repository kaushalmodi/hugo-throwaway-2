---
title: "Hamazawa 5026B"
date: 2015-09-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["Hamazawa 5026B","Hamazawa","Japan","17 Jewels","Automatic","Magic Lever","Excenter","disposable","Diashock"]
description: "Hamazawa 5026B - a very simple japanese selfwinding movement with day/date display"
abstract: "A simple, rationally constructed japanese selfwinding movement with day and weekday indication."
preview_image: "hamazawa_5206b-mini.jpg"
image: "Hamazawa_5206B.jpg"
movementlistkey: "hamazawa"
caliberkey: "5026B"
manufacturers: ["hamazawa"]
manufacturers_weight: 5026
categories: ["movements","movements_h","movements_h_hamazawa_en"]
widgets:
  relatedmovements: true
featured: ["true"]
timegrapher_old: 
  rates:
    ZO: "+29"
    ZU: "+38"
    3O: "-95"
    6O: "-65"
    9O: "-87"
    12O: "-43"
  amplitudes:
    ZO: "154"
    ZU: "150"
    3O: "188"
    6O: "135"
    9O: "200"
    12O: "200"
  beaterrors:
    ZO: "0.3"
    ZU: "0.2"
    3O: "0.2"
    6O: "0.9"
    9O: "0.5"
    12O: "0.1"
usagegallery: 
  - image: "d/dufonte/Dufonte_LP_HAU_Hamazawa_5206B.jpg"
    description: "Dufonte LP mens' watch"
labor: |
  <p>The movement shown here was quite gummed up. Its case was full of cracks and dents and shows many, many years of heavily use. On the timegrapher, it was not possible get a usable signal on the timegrapher.</p><p>After a full service and some days to break in, it showed acceptable rates which are perfectly ok for daily use.</p>
---
Lorem Ipsum