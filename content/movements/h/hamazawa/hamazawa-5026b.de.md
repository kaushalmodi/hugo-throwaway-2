---
title: "Hamazawa 5026B"
date: 2015-09-13T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["Hamazawa 5026B","Hamazawa","Japan","17 Jewels","Automatic","Magic Lever","Exzenter","Einweg","Diashock"]
description: "Hamazawa 5026B - ein japanisches Automaticwerk einfachster Bauweise mit Tag/Datumsanzeige"
abstract: "Ein einfaches, rationell konstruiertes Japanisches Automaticwerk mit Tages- und Wochentagsanzeige"
preview_image: "hamazawa_5206b-mini.jpg"
image: "Hamazawa_5206B.jpg"
movementlistkey: "hamazawa"
caliberkey: "5026B"
manufacturers: ["hamazawa"]
manufacturers_weight: 5026
categories: ["movements","movements_h","movements_h_hamazawa"]
widgets:
  relatedmovements: true
featured: ["true"]
labor: |
  <p>Das vorliegende Werk kam verharzt in die Werkstatt. Es sitzt in einem Gehäuse, dem die Spuren jahrelangen Tragens ohne Schonung gut anzusehen sind. Zwei Sprünge im Glas und ein verbogener Sekundenzeiger sind Zeugen der Nutzung. Auf der Zeigwaage war kein vernünftiger Gang mehr zu erkennen.</p><p>Nach einem Service mit Reinigung und anschließender Ölung läuft das Werk wieder passabel, es dauerte allerdings einige Tage, bis sich das Öl so gut verteilt hatte, daß auch auf der Zeitwaage wieder halbwegs passable Gangleistungen gemessen werden könnten:</p>
timegrapher_old: 
  rates:
    ZO: "+29"
    ZU: "+38"
    3O: "-95"
    6O: "-65"
    9O: "-87"
    12O: "-43"
  amplitudes:
    ZO: "154"
    ZU: "150"
    3O: "188"
    6O: "135"
    9O: "200"
    12O: "200"
  beaterrors:
    ZO: "0.3"
    ZU: "0.2"
    3O: "0.2"
    6O: "0.9"
    9O: "0.5"
    12O: "0.1"
usagegallery: 
  - image: "d/dufonte/Dufonte_LP_HAU_Hamazawa_5206B.jpg"
    description: "Dufonte LP Herrenuhr"
---
Lorem Ipsum