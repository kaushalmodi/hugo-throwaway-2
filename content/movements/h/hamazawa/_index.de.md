---
title: "Hamazawa"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["h"]
language: "de"
keywords: ["Hamazawa","Japan"]
categories: ["movements","movements_h"]
movementlistkey: "hamazawa"
description: "Uhrwerke des japanischen Herstellers Hamazawa"
abstract: "Uhrwerke des japanischen Herstellers Hamazawa"
---
(Hamazawa, Japan)
{{< movementlist "hamazawa" >}}

{{< movementgallery "hamazawa" >}}