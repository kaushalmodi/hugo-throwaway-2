---
title: "HMT 020"
date: 2009-04-11T23:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["HMT 020","HMT","020","Hindustan Machine Tools Ltd.","Citizen","17 Jewels","Indien","17 Steine"]
description: "HMT 020"
abstract: ""
preview_image: "hmt_020-mini.jpg"
image: "HMT_020.jpg"
movementlistkey: "hmt"
caliberkey: "020"
manufacturers: ["hmt"]
manufacturers_weight: 20
categories: ["movements","movements_h","movements_h_hmt"]
widgets:
  relatedmovements: true
featured: ["false"]
usagegallery: 
  - image: "h/hmt/HMT_Sona_HAU.jpg"
    description: "HMT Sona Herrenuhr"
---
Lorem Ipsum