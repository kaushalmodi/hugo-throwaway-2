---
title: "HMT"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["h"]
language: "en"
keywords: ["HMT","HMT Limited","Hindustan Machine Tools","Hindustan Machine Tools Limited","Banglore","India"]
categories: ["movements","movements_h"]
movementlistkey: "hmt"
abstract: "Movements of the indian manufacturer HMT (Hindustan Machine Tools Limited) from Bangalore, India"
description: "Movements of the indian manufacturer HMT (Hindustan Machine Tools Limited) from Bangalore, India"
---
(HMT Limited / Hindustan Machine Tools Limited, Bangalore, India)
{{< movementlist "hmt" >}}

{{< movementgallery "hmt" >}}