---
title: "HMT 6906"
date: 2016-01-01T00:00:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["HMT 6906","HMT 6902 HMT","6906","Hindustan Machine Tools Ltd.","Citizen","17 Jewels","Indien","17 Steine","Bangalore","Automatic","Day-Date","Citizen 6500"]
description: "HMT 6906 - Ein einseitig aufziehendes Automaticwerk mit Tag- und Datumsanzeige aus Indien. Manchmal auch als HMT 6902 bezeichnet"
abstract: "Ein sehr gut ausgestattes indisches Automaticwerk auf Basis eines Citizen-Werks. "
preview_image: "hmt_6906-mini.jpg"
image: "HMT_6906.jpg"
movementlistkey: "hmt"
caliberkey: "6906"
manufacturers: ["hmt"]
manufacturers_weight: 6906
categories: ["movements","movements_h","movements_h_hmt"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "h/hmt/HMT_Rajat_HMT_6906.jpg"
    description: "HMT Rajat Automatic Herrenuhr"
labor: |
  Das hier vorliegende Exemplar kam, wie man anhand der Bilder erkennen kann, gut gebraucht und leicht verdreckt ins Labor. Leider konnte die verklebte Unruhspirale auch durch mehrfaches Reinigen bis jetzt nicht in Ordnung gebracht werden. Das restliche Räderwerk wurde erfolgreich gereinigt und geölt.
---
Lorem Ipsum