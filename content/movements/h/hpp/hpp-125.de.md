---
title: "HPP 125"
date: 2015-11-07T14:18:00+02:00
draft: "false"
type: "movement"
language: "de"
keywords: ["HPP","HPP 125","Henzi & Pfaff","Pforzheim","16 Jewels","16 Steine","Hercules","Shockproof","Schraubenunruh","indirekte Zentralsekunde"]
description: "HPP 125 - Ein großes Handaufzugswerk aus den frühen 50er Jahren mit indirekter Zentralsekunde, Schraubenunruh und hauseigener \"Shockproof\"-Stoßsicherung"
abstract: "Das HPP 125 ist ein sehr großes Handaufzugswerk, das in den frühen 50er Jahren gefertigt wurde und schon mit einer, in diesem Fall hauseigenen, Stoßsicherung beeindrucken konnte."
preview_image: "hpp_125-mini.jpg"
image: "HPP_125.jpg"
movementlistkey: "hpp"
caliberkey: "125"
manufacturers: ["hpp"]
manufacturers_weight: 125
categories: ["movements","movements_h","movements_h_hpp"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "h/hercules/Hercules_HAU_HPP_125.jpg"
    description: "Hercules Herrenuhr (Sekundenzeiger fehlt)"
donation: "Dieses Werk samt Uhr ist eine [Spende von Erwin](/supporters/erwin/) an das Archiv. Ganz herzlichen Dank dafür!"
labor: |
  <p>Das vorliegende Werk kam verharzt in die Werkstatt und wurde einer Komplettrevision unterzogen.</p><p>Anschließend lief es wieder mit, zumindest liegend, sehr passabler Amplitude (268°), allerdings brach diese hängend auf nur noch rund 150° ein, zudem konnte der Magnetismus und/oder die Verunreinigungen der Unruhspirale nicht reslos beseitigt werden, so daß immer wieder ein starker Vorgang (3 Minuten pro Tag) auftrat, der aber auch genauso schnell wieder wegging.</p><p>Dennoch, für ein 65 Jahre altes Werk, das, glaubt man dem Gehäuse, ziemlich geschunden wurde, ein ordentliches Ergebnis.</p><p><br /></p>
---
Lorem Ipsum