---
title: "HPP 125"
date: 2015-11-08T14:18:00+02:00
draft: "false"
type: "movement"
language: "en"
keywords: ["HPP","HPP 125","Henzi & Pfaff","Pforzheim","Germany","16 Jewels","16 Rubis","Hercules","Shockproof","screw balance","indirect center second"]
description: "HPP 125 - A large manual wind movement from the early 1950ies with indirectly driven center second, screw balance and inhouse \"Shockproof\" shock protection"
abstract: "The HPP is a very large manual wind movement, which was made in the early 1950ies and already has got a shock protection, here an inhouse made type."
preview_image: "hpp_125-mini.jpg"
image: "HPP_125.jpg"
movementlistkey: "hpp"
caliberkey: "125"
manufacturers: ["hpp"]
manufacturers_weight: 125
categories: ["movements","movements_h","movements_h_hpp_en"]
widgets:
  relatedmovements: true
featured: ["true"]
usagegallery: 
  - image: "h/hercules/Hercules_HAU_HPP_125.jpg"
    description: "Hercules gents watch  (second hand missing)"
donation: "This movement and watch is a donation from [Erwin](/supporters/erwin/). Thank you very much!"
labor: |
  <p>The specimen shown here came resinified into the lab and got a full service.</p><p>Afterwards, it performed very well again in horizontal positions with an amplitude of 268°. In the vertical positions, it decreased to 150°, a sign, that the balance axle more or less reached its end of life. Additionally, the dirt and magentizm of the hairspring could not be fully removed, so that from time to time, the movement speeded up to 3 minutes per day, which went away after a short time again.</p><p>Nevertheless, for 65 year old and heavily used (according to the case) movement, a very fair result.</p><p><br /></p>
---
Lorem Ipsum