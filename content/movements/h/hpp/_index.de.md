---
title: "HPP (Henzi & Pfaff)"
date: 1995-01-01T00:00:00+01:00
draft: false
type: "manufacturer"
manufacturerletters: ["h"]
language: "de"
keywords: ["FHF","Fontainemelon","Font","Schweiz"]
categories: ["movements","movements_h"]
movementlistkey: "hpp"
description: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
abstract: "Uhrwerke der FHF, der Fabrique d'Horlogerie de Fontainemelon, Fontainemelon, Schweiz"
---
(Henzi & Pfaff, Pforzheim, Deutschland)
{{< movementlist "hpp" >}}

{{< movementgallery "hpp" >}}